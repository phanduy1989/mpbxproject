package com.viettel.interfaces;

/**
 * Created by duyuno on 7/20/17.
 */
public interface InputDialogListener {
    public void onChange(String textAfter);
}
