package com.viettel.interfaces;

public interface NotifyHasMorePages {
	public void mayHasMorePages();
	public void notifyNoMorePages();
}
