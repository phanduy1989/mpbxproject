package com.viettel.interfaces;

import com.viettel.model.DepartmentModel;

/**
 * Created by duyuno on 8/5/17.
 */
public interface OnDepartSelectedListener {
    void onGroupSelect(DepartmentModel menu);
}
