package com.viettel.interfaces;


public interface ConfirmListener {
	public void doAccept();
	public void doCancel();
}

