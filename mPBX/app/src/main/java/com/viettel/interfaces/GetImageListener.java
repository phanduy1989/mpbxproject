package com.viettel.interfaces;

public interface GetImageListener {

	public void getFromCamera();
	public void getFromGallery();
}
