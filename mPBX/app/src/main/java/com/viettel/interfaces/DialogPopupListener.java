package com.viettel.interfaces;


public interface DialogPopupListener {
	public void doAccept(String... inputContent);
	public void doCancel();
}