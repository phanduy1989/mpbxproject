package com.viettel.interfaces;

public interface OnClickShopGroupListener {
	public void onClickGroup(int groupPosition, boolean isExpanded);
}
