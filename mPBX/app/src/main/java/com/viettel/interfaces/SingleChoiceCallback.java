package com.viettel.interfaces;

/**
 * Created by duyuno on 8/23/17.
 */
public interface SingleChoiceCallback {
    public void onChose(int which, String text);
}
