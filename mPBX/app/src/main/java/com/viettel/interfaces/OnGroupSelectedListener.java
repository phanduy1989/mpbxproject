package com.viettel.interfaces;

import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;

/**
 * Created by duyuno on 8/5/17.
 */
public interface OnGroupSelectedListener {
    void onGroupSelect(ColleagueGroup menu);
}
