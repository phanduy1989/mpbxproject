package com.viettel.interfaces;


public interface ResponseListener {
	public void processResponse(String response);
	public void processResponse(int error, String content);
}
