/*
 * Name: $RCSfile: WebServiceConfig.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 2:37:15 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.viettel.api;

/**
 * WebServiceConfig class contains web service settings
 * 
 * @author Lemon
 */
public final class WebServiceConfig {
	// Network tempCreatedDate out: 15s
	public static int NETWORK_TIME_OUT = 15000;

	// result code for activity result
	public static int RESULT_OK = 1;

	// ===================== DOMAIN =====================
	public static String PROTOCOL_HTTP = "http://";

//	public static String PROTOCOL_HTTPS = "https:";
	
	public static String language = "vi";
//	public static String language = "en";
//	
//	public static String DOMAIN = "https://smartmotor.viettel.vn/";
//	public static String DOMAIN = "http://smartmotor.viettel.vn/";
	
//	public static String DOMAIN = "http://203.113.138.115/";
//	public static String DOMAIN = "http://192.168.153.1:9800/";
//	public static String DOMAIN = "http://103.1.210.161:9000/";
//	public static String DOMAIN = "http://103.1.210.161:9600/";
//	public static String DOMAIN = "http://103.1.210.161:9400/";
//	public static String DOMAIN = "http://103.1.210.161:9800/";
	public static String DOMAIN = "http://103.1.210.135:9115/";
//	public static String DOMAIN = "http://10.61.75.58:8080/";
//	public static String DOMAIN = "http://localhost:8080/";
//	public static String DOMAIN = "http://smartmotor.vn:8080/";
//	public static String DOMAIN = "http://103.1.210.161:9000/";
//	public static String DOMAIN = "//10.0.2.2:8080/";
	
	public static String CONTEXT_ROOT = "mtapi";
	
	public static String DOMAIN_CHARGING = DOMAIN + CONTEXT_ROOT;
//	public static String CONTEXT_ROOT = "mtapiformobile";

//	public static String APP_DOMAIN = PROTOCOL_HTTPS + DOMAIN + CONTEXT_ROOT  + "/rest/mobile/";
	public static String APP_DOMAIN = DOMAIN + CONTEXT_ROOT  + "/rest/mobile/";
	public static String CAPCHA_URL = DOMAIN_CHARGING  + "/jcaptcha.jpg";
	
	public static String URL_AUTHEN_HTTPS = DOMAIN + CONTEXT_ROOT + "/rest/auth/login/";
//	public static String URL_AUTHEN_HTTPS = DOMAIN + CONTEXT_ROOT + "/rest/auth/newlogin/";
	public static String URL_CHANGE_PASS = DOMAIN + CONTEXT_ROOT + "/rest/auth/";
	public static String URL_LOGOUT_HTTPS = DOMAIN + CONTEXT_ROOT + "/rest/auth/logout/";
	
	public static String WSDL_URL = DOMAIN + "/mgw/mobilegw?WSDL";
//	public static String WSDL_URL = "http:" + DOMAIN + "/mgwcam/mobilegw?WSDL";
	
	public static String WSDL_NAME_SPACE = "http://mobilegateway.viettel.com/";
	public static String WSDL_METHOD_NAME = "doCommand";
	
//	public static String URL_AUTHEN_HTTPS = "http://103.1.210.157:9000/mtapi/rest/auth/login/";
//	public static String URL_AUTHEN_LOCAL = "http://10.0.2.2:8080/mtapi/rest/auth";
//	public static String URL_AUTHEN_LOCAL = "http://10.0.2.2:8080/mtapi/login";
	
//	public static String TCP_SERVER = "103.1.210.135";
//	public static int TCP_PORT = 9116;
	
	public static String TCP_SERVER = "125.235.40.29";
	public static int TCP_PORT = 9009;
	public static int TIME_OUT = 20;
	
	public static String[] RESPONSE_EMAILS = new String[]{"cskh@viettel.com.vn"};
	
	// ===================== PARAMETER ================
	public static String PARAM_USER = "username";
	public static String PARAM_PASSWORD = "password";

	// ====================== RESPONSE ================

	public static String RESPONSE_LOGIN_FAIL = "false";
	public static String RESPONSE_REGISTER_EXIST_EMAIL = "User is avaiable";
	public static String RESPONSE_REGISTER_SUCCESS = "success";
	public static String RESPONSE_REGISTER_FAIL = "";

	// ============== KEY JSON =================
	//=================== TAG KEY =============
	public static String TAG_KEY_DATA = "data";
	//    public static String TAG_KEY_CHILDREN = "children";
	public static String TAG_KEY_USERINFO = "userInfo";
	public static String TAG_KEY_TOKEN = "token";
	public static String TAG_KEY_USERS = "users";
	public static String TAG_KEY_USER = "user";
	public static String TAG_KEY_SENDER = "sender";
	public static String TAG_KEY_ROOM_ID = "room_id";

	//===================KEY PROFILE=================
	public static String KEY_PROFILE_UPDATEDAT = "updatedAt";
	public static String KEY_PROFILE_ID = "id";
	public static String KEY_PROFILE_PHONE = "phone";
	public static String KEY_PROFILE_USERNAME = "username";
	public static String KEY_PROFILE_NAME = "name";
	public static String KEY_PROFILE_EMAIL = "email";
	public static String KEY_PROFILE_CREATEDAT = "createdAt";
	public static String KEY_PROFILE_APP_ID = "app_id";
	public static String KEY_PROFILE_AVATAR = "avatar";


	//===================KEY DANH BA =================
	public static String KEY_DANHBA_APP_ID = "app_id";
	public static String KEY_DANHBA_PASSWORD = "password";
	public static String KEY_DANHBA_AVATAR = "avatar";
	public static String KEY_DANHBA_ID = "id";
	public static String KEY_DANHBA_PHONE = "phone";
	public static String KEY_DANHBA_USERNAME = "username";
	public static String KEY_DANHBA_EMAIL = "email";
	public static String KEY_DANHBA_CREATEDAT = "createdAt";
	public static String KEY_DANHBA_NAME = "name";
	public static String KEY_DANHBA_UPDATEDAT = "updatedAt";
	public static String KEY_DANHBA_ONLINE = "online";
	public static String KEY_DANHBA_ROOM_ID = "room_id";


	//===================KEY NHOM DANH BA =================
	public static String KEY_NHOM_DANHBA_UPDATEDAT = "updatedAt";
	public static String KEY_NHOM_DANHBA_ID = "id";
	public static String KEY_NHOM_DANHBA_USER_ID = "user_id";
	public static String KEY_NHOM_DANHBA_STATUS = "status";
	public static String KEY_NHOM_DANHBA_CREATEDAT = "createdAt";
	public static String KEY_NHOM_DANHBA_NAME = "name";

	//===================KEY TIN NHAN =================
	public static String KEY_TINNHAN_UPDATEDAT = "updatedAt";
	public static String KEY_TINNHAN_ID = "id";
	public static String KEY_TINNHAN_USER_ID = "user_id";
	public static String KEY_TINNHAN_CREATEDAT = "createdAt";
	public static String KEY_TINNHAN_NAME = "name";
	public static String KEY_TINNHAN_LAST_TIME_SEND_MESSAGE = "last_time_send_message";
	public static String KEY_TINNHAN_SENDER_ID = "sender_id";
	public static String KEY_TINNHAN_LAST_MESSAGE = "last_message";
	public static String KEY_TINNHAN_TYPE = "type";
	public static String KEY_TINNHAN_UNREAD_MESSAGE = "unread_message";

	//===================KEY SENDER CHAT =================
	public static String KEY_SENDER_CHAT_ID = "id";
	public static String KEY_SENDER_CHAT_APP_ID = "app_id";
	public static String KEY_SENDER_CHAT_USERNAME = "username";
	public static String KEY_SENDER_CHAT_NAME = "name";
	public static String KEY_SENDER_CHAT_EMAIL = "email";
	public static String KEY_SENDER_CHAT_PASSWORD = "password";
	public static String KEY_SENDER_CHAT_AVATAR = "avatar";
	public static String KEY_SENDER_CHAT_PHONE = "phone";
	public static String KEY_SENDER_CHAT_CREATEDAT = "createdAt";
	public static String KEY_SENDER_CHAT_UPDATEDAT = "updatedAt";

	//===================KEY MESSAGE =================
	public static String KEY_MESSAGE_CHAT_ID = "_id";
	public static String KEY_MESSAGE_CHAT_CONTENT = "content";
	public static String KEY_MESSAGE_CHAT_USER_ID = "user_id";
	public static String KEY_MESSAGE_CHAT_ROOM_ID = "room_id";
	public static String KEY_MESSAGE_CHAT_CREATEDAT = "createdAt";
	public static String KEY_MESSAGE_CHAT_UPDATEDAT = "updatedAt";
	public static String KEY_MESSAGE_CHAT_DELETEDAT = "deletedAt";
	public static String KEY_MESSAGE_CHAT_MSG_ID = "msg_id";
	public static String KEY_MESSAGE_CHAT_TYPE = "type";
	public static String KEY_MESSAGE_FILE_NAME = "file_name";

	//===================KEY STATUS =================
	public static String KEY_STATUS_ONLINE = "online";
	public static String KEY_STATUS_USER_ID = "user_id";
	
}
