package com.viettel.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ae.team.connectapi.config.WebServiceConfig;
import com.ae.team.connectapi.utility.YeuThichListener;
import com.androidquery.AQuery;
import com.viettel.model.DanhBaInfo;
import com.viettel.mpbx.R;
import com.viettel.view.activity.authen.ChatActivity;
import com.viettel.view.activity.authen.ThanhVienNhomActivity;

import java.util.ArrayList;


public class ThanhVienNhomAdapter extends RecyclerView.Adapter<ThanhVienNhomViewHolder> {

    private ArrayList<DanhBaInfo> contactInfoLista;
    private Context context;
    private int w = 84, h = 84;
    private AQuery aq;
    private YeuThichListener yeuThichListener;
    public boolean checkhide = false;
    private Animation an;
    private String tag,user_id;


    public ThanhVienNhomAdapter(Context context, ArrayList<DanhBaInfo> contactInfoList, String user_id, YeuThichListener yeuThichListener, String tag) {
        this.context = context;
        this.contactInfoLista = contactInfoList;
        this.yeuThichListener = yeuThichListener;
        this.user_id = user_id;
        this.tag = tag;
        aq = new AQuery(context);
    }

    @Override
    public ThanhVienNhomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_thanhvien, parent, false);
        aq = aq.recycle(itemView);
        return new ThanhVienNhomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ThanhVienNhomViewHolder holder, final int position) {
        final DanhBaInfo item = contactInfoLista.get(position);

        try {

            holder.lblNoAvatar.setTextColor(context.getResources().getColor(R.color.white));
            holder.lblName.setText(item.getName().equals("") ? item.getUsername() : item.getName());
            if (checkhide == true) {
                if (!item.getId().equals(user_id)) {
                    startAmi(holder.layoutImage, holder.lblName, context);
                    holder.btnDelete.setVisibility(View.VISIBLE);

                }
            } else {
                stopAmi(holder.layoutImage, holder.lblName);
                holder.btnDelete.setVisibility(View.GONE);
            }
            if (item.getId().equals(user_id)) {
                holder.imageKeyAdmin.setVisibility(View.VISIBLE);
            } else {
                holder.imageKeyAdmin.setVisibility(View.GONE);
            }

            if (item.getOnline().equals("1"))
            {
                holder.lblOnline.setVisibility(View.VISIBLE);
            }else
            {
                holder.lblOnline.setVisibility(View.GONE);
            }
//            aq.id(imgAvatar).image(R.drawable.bgavatar);
//            String name = item.getUsername().substring(0, 1);
//            holder.lblNoAvatar.setText(name.toUpperCase());
//            if (null != item.getAvatar() && !"".equals(item.getAvatar())) {
                aq.id(holder.imgAvatar).image(item.getAvatar().contains("http") ? item.getAvatar() : WebServiceConfig.API_URL + item.getAvatar(),
                        true, true, w, R.drawable.avatar2, null, 0, 0);
                holder.lblNoAvatar.setVisibility(View.GONE);
//            } else {
//                StringUtility.CutTextCustomer(item.getName().equals("") ? item.getUsername() : item.getName(), aq, holder.imgAvatar, holder.lblNoAvatar);
//            }
//            holder.imgAvatar.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    yeuThichListener.onChangeYeuThich();
//                    checkhide = true;
//                    notifyDataSetChanged();
//                }
//            });
            holder.layoutConten.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tag.equals("GROUP") && checkhide == false && ChatActivity.chat == false && !item.getId().equals(user_id)) {
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("group_rom_id", item.getRoom_id() == null ? "" : item.getRoom_id());
                        intent.putExtra("name",(item.getName() ==  null ||  item.getName().equals("")  ) ? item.getUsername() : item.getName());
                        intent.putExtra("avatar", item.getAvatar());
                        intent.putExtra("user_id", item.getId());
                        intent.putExtra("id_tin_nhan", item.getId());
                        intent.putExtra("is_group", "0");
                        intent.putExtra("tag", "ROOM");
                        context.startActivity(intent);
                    }
                }
            });
//
            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ThanhVienNhomActivity.vitri = position;
                    yeuThichListener.onChangeYeuThich();
                }
            });

        } catch (NullPointerException e) {
        } catch (Exception e) {
        }

    }


//    private String sizeimage(final CircularImageView imageView) {
//        ViewTreeObserver vto = imageView.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
//                w = imageView.getMeasuredHeight();
//                h = imageView.getMeasuredWidth();
//                return true;
//            }
//        });
//        return w + "_" + h + "/";
//    }


    @Override
    public int getItemCount() {
        return contactInfoLista.size();
    }

    private void startAmi(RelativeLayout relativeLayout, TextView textView, Context context) {
        an = AnimationUtils.loadAnimation(context, R.anim.shake_image_nhom);
        relativeLayout.startAnimation(an);
        textView.startAnimation(an);
    }

    private void stopAmi(RelativeLayout relativeLayout, TextView textView) {

        relativeLayout.clearAnimation();
        textView.clearAnimation();
    }
}
