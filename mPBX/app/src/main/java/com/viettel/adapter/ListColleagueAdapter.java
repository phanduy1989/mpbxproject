package com.viettel.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.adapter.holder.ColleagueHolder;
import com.viettel.adapter.holder.ColleagueItem;
import com.viettel.adapter.holder.DepartmentHolder;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.OnGroupSelectedListener;
import com.viettel.interfaces.RecyclerItemClickListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.mpbx.R;
import com.viettel.utils.CircleTransform;
import com.viettel.utils.RoundedTransformation;
import com.viettel.view.activity.business.colleague.ListColleagueActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mc.kim on 8/25/2016.
 */
public class ListColleagueAdapter extends ExpandableRecyclerAdapter<DepartmentHolder, ColleagueHolder>
        implements RecyclerItemClickListener.OnItemClickListener, View.OnClickListener, MPBXFilter {


    private LayoutInflater mInflater = null;
    private BABaseActivity listColleagueActivity;

    private ArrayList<ColleagueGroup> listData;
    private ArrayList<ColleagueGroup> listDisplayData;

    private boolean isSystem;

    public void setSystem(boolean system) {
        isSystem = system;
    }

    public ListColleagueAdapter(BABaseActivity listColleagueActivity, LayoutInflater layoutInflater, List<ColleagueItem> parentItemList) {
        super(parentItemList);
        this.listColleagueActivity = listColleagueActivity;
        this.mInflater = layoutInflater;
    }

    public void refreshData(ArrayList<ColleagueGroup> list) {

    }

    @Override
    public DepartmentHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View view = mInflater.inflate(R.layout.item_colleague_header, parentViewGroup, false);

        RelativeLayout root = (RelativeLayout) view.findViewById(R.id.root);
//        root.setOnTouchListener(null);

//        view.setClickable(false);
//        view.setOnClickListener(null);
//        view.setOnTouchListener(null);
        TextView menuTxt = (TextView) view.findViewById(R.id.menuTxt);
        ImageView btnExpand = (ImageView) view.findViewById(R.id.btnExpand);
//        LinearLayout layoutTitle = (LinearLayout) view.findViewById(R.id.layoutTitle);
        btnExpand.setTag(view);
//        layoutTitle.setTag(view);
        menuTxt.setTag(view);
        root.setTag(view);
        root.setOnClickListener(this);
//        btnExpand.setOnClickListener(this);
        return new DepartmentHolder(view);
    }

    @Override
    public ColleagueHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View view = mInflater.inflate(R.layout.item_colleague_child, null, false);

        ImageButton btnPhone = (ImageButton) view.findViewById(R.id.btnPhone);
        ImageButton btnAddFavorite = (ImageButton) view.findViewById(R.id.btnAddFavorite);
        LinearLayout root = (LinearLayout) view.findViewById(R.id.rootChild);
        root.setOnClickListener(this);
        root.setTag(view);

//        view.setClickable(true);

//        view.setTag(view);
        btnPhone.setTag(view);
        btnPhone.setOnClickListener(this);
        btnAddFavorite.setTag(view);
        btnAddFavorite.setOnClickListener(this);
//        view.setOnClickListener(this);
        return new ColleagueHolder(view);
    }

    @Override
    public void onBindParentViewHolder(DepartmentHolder parentViewHolder, int position, ParentListItem parentListItem) {

        ColleagueItem parentsMenu = (ColleagueItem) parentListItem;
        parentViewHolder.menuTxt.setText(parentsMenu.getColleagueGroup().getGroupName() + (parentsMenu.getChildCount() > 0 ? " (" + parentsMenu.getChildItemList().size() + ")" : ""));

        if(!isSystem) {
            parentViewHolder.btnExpanded.setVisibility(parentsMenu.hasChild() ? View.VISIBLE : View.GONE);
            if (parentsMenu.hasChild()) {
                parentViewHolder.btnExpanded.setVisibility(View.VISIBLE);
            } else {
                parentViewHolder.btnExpanded.setVisibility(View.GONE);
            }
        } else {
            parentViewHolder.btnExpanded.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBindChildViewHolder(ColleagueHolder childViewHolder, int position, Object childListItem) {
        ColleagueModel subMenu = (ColleagueModel) childListItem;
        childViewHolder.txtName.setText(subMenu.getColleagueName());
        childViewHolder.txtPhone.setText("" + subMenu.getNumberOfExt());

        if(subMenu.getFileName() != null) {
            Picasso.with(listColleagueActivity)
                    .load(subMenu.getFileName())
                    .placeholder(R.drawable.avatar2)
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(childViewHolder.avatar);
        }

    }

    private RecyclerView mRecyclerView = null;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.mRecyclerView = recyclerView;
    }


    @Override
    public void onItemClick(View view, int position) {
        Log.e("onItemClick", "OK");
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    public Object getListItem(int position) {
        return super.getListItem(position);
    }

    @Override
    public void onClick(View v) {
        View adapterView = (View) v.getTag();
        if (adapterView == null) {
            return;
        }

        int position = mRecyclerView.getChildLayoutPosition(adapterView);

        Object listItem = getListItem(position);
        if (listItem instanceof ColleagueModel) {
            ColleagueModel colleagueModel = (ColleagueModel) listItem;
            if(v.getId() == R.id.btnPhone) {
                listColleagueActivity.showCallLayout(colleagueModel);
            } else if(v.getId() == R.id.btnAddFavorite){
                listColleagueActivity.goToChoseAvaiGroup(colleagueModel);
            } else {
                listColleagueActivity.goToColleagueInfo(colleagueModel);
            }
        } else {
            onClickExpand(position);
        }


//        if (v.getId() == R.id.btnExpand) {
//
//            onClickExpand(position);
//
//        } else {
//            Object listItem = getListItem(position);
//            Menu selectedMenu = null;
//            if (listItem instanceof ParentWrapper) {
//                ParentWrapper wrapper = (ParentWrapper) listItem;
//                ParentListItem item = wrapper.getParentListItem();
//                ColleagueItem menu = (ColleagueItem) item;
//                selectedMenu = menu.getMenu();
//            } else {
//                Menu menu = (Menu) listItem;
//                selectedMenu = menu;
//            }

//            if (!disableMenu(selectedMenu)) {
//                if (selectedMenu.isClickable()) {
//                    if (mListener != null)
//                        mListener.onMenuSelected(selectedMenu);
//                } else {
//                    onClickExpand(position);
//                }
//            }
//        }
    }

    public void onClickExpand(int position) {
        int positionExpanding = -1;
        int menuExpandingChildCount = -1;

        for (int i = 0, count = getItemCount(); i < count; i++) {
            if (i == position) {
                continue;
            }
            Object listItem = getListItem(i);
            if (listItem instanceof ParentWrapper) {
                ParentWrapper wrapper = (ParentWrapper) listItem;
                ParentListItem item = wrapper.getParentListItem();

                if (wrapper.isExpanded()) {
                    ColleagueItem menu = (ColleagueItem) item;
                    positionExpanding = i;
                    menuExpandingChildCount = menu.getChildItemList() != null ? menu.getChildItemList().size() : 0;
                    collapseParent(item);
                }
            }
        }
        int updateNextExpand = positionExpanding < 0 || positionExpanding > position ? position : position - menuExpandingChildCount;
        Object listItemPosition = getListItem(updateNextExpand);
        if (listItemPosition instanceof ParentWrapper) {
            ParentWrapper wrapper = (ParentWrapper) listItemPosition;
            ParentListItem item = wrapper.getParentListItem();

            if(!isSystem) {
                if (wrapper.isExpanded()) {
                    collapseParent(item);
                } else {
                    expandParent(item);
                }
            } else {
                if(onGroupSelectedListener != null) {
                    onGroupSelectedListener.onGroupSelect(((ColleagueItem) item).getColleagueGroup());
                }
            }
        }

        notifyDataSetChanged();
    }

    private OnChildSelectedListener mListener = null;
    private OnGroupSelectedListener onGroupSelectedListener = null;

    public void setOnChildSelectedListener(OnChildSelectedListener mListener) {
        this.mListener = mListener;
    }

    public void setOnGroupSelectedListener(OnGroupSelectedListener onGroupSelectedListener) {
        this.onGroupSelectedListener = onGroupSelectedListener;
    }

    @Override
    public void filter(String key) {

    }
}
