package com.viettel.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viettel.mpbx.R;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class PhonesAdapter extends BaseAdapter {
    public ArrayList<String> listPhones;

    Activity activity;

    public PhonesAdapter(Activity activity) {
        this.activity = activity;
    }
    public PhonesAdapter(Activity activity, ArrayList<String> phoneNumbers) {
        this.activity = activity;
        listPhones = phoneNumbers;
    }

    public void setData(ArrayList<String> list) {
        listPhones = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listPhones != null ? listPhones.size() : 0;
    }

    @Override
    public String getItem(int position) {
        return listPhones != null ? listPhones.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        public TextView txtPhone;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_phone, null);
            viewHolder = new ViewHolder();
            viewHolder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String phoneNumber = getItem(position);

        if (phoneNumber != null) {
            viewHolder.txtPhone.setText(phoneNumber);
        }

        return convertView;
    }
}
