package com.viettel.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.ParseException;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ae.team.connectapi.utility.YeuThichListener;
import com.androidquery.AQuery;
import com.viettel.model.NoiDungInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.authen.ChatActivity;
import com.viettel.view.activity.authen.ViewImageActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;



/**
 * Created by VanHa on 7/23/2015.
 */
public class ChatAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<NoiDungInfo> arrChatinfo;
    private static LayoutInflater inflater = null;
    private int lastAnimatedPosition = -1;
    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;
    private YeuThichListener yeuThichListener;

    private AQuery aq;
    private int w = 84, h = 84;
    int width = 100;
    int widthima = 100;
    private String is_group;
    private String user_id="";

    public ChatAdapter(Activity activity,
                       ArrayList<NoiDungInfo> arrChatinfo,String user_id, String is_group, YeuThichListener yeuThichListener) {
        context = activity;
        this.arrChatinfo = arrChatinfo;
        this.user_id = user_id;
        this.is_group = is_group;
        this.yeuThichListener = yeuThichListener;
        aq = new AQuery(context);
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        width = width / 3;
        widthima = size.x / 2;
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }

    @Override
    public int getCount() {
        return (arrChatinfo == null) ? 0 : arrChatinfo.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        Date date2 = new Date();
        Date date3 = new Date();
        NoiDungInfo item2 = null;
        NoiDungInfo item3 = null;
        final NoiDungInfo item = (NoiDungInfo) arrChatinfo.get(position);

        if (!user_id.equals(item.getSenderChatInfo().getId())) {
            convertView = inflater.inflate(R.layout.chat_item_left, parent, false);

            holder = new ViewHolder();
            holder.lblmessage_chat = (TextView) convertView
                    .findViewById(R.id.message_chat);
            holder.lbldate = (TextView) convertView
                    .findViewById(R.id.sendtime_chat);
            holder.lblNoAvatar = (TextView) convertView
                    .findViewById(R.id.lblNoAvatar);
            holder.avatar_chat = (ImageView) convertView
                    .findViewById(R.id.avatar_chat);
            holder.imagContent = (ImageView) convertView.findViewById(R.id.imgContent);
            holder.lblShowView = (TextView) convertView.findViewById(R.id.lblShowView);
            holder.lblContentFile = (TextView) convertView.findViewById(R.id.lblContentFile);
            holder.lblDownLoadFile = (TextView) convertView.findViewById(R.id.lblDownLoadFile);
            holder.layoutFile = (RelativeLayout) convertView.findViewById(R.id.layoutFile);
        } else {
            convertView = inflater.inflate(R.layout.chat_item_right, parent,
                    false);
            holder = new ViewHolder();
            holder.lblmessage_chat = (TextView) convertView
                    .findViewById(R.id.message_chat);
            holder.lbldate = (TextView) convertView
                    .findViewById(R.id.sendtime_chat);
            holder.lblNoAvatar = (TextView) convertView
                    .findViewById(R.id.lblNoAvatar);
            holder.lblShowView = (TextView) convertView.findViewById(R.id.lblShowView);
            holder.imagContent = (ImageView) convertView.findViewById(R.id.imgContent);
            holder.avatar_chat = (ImageView) convertView
                    .findViewById(R.id.avatar_chat);
            holder.lblContentFile = (TextView) convertView.findViewById(R.id.lblContentFile);
            holder.lblDownLoadFile = (TextView) convertView.findViewById(R.id.lblDownLoadFile);
            holder.layoutFile = (RelativeLayout) convertView.findViewById(R.id.layoutFile);
        }
        aq = aq.recycle(convertView);
        holder.lblNoAvatar.setTextColor(context.getResources().getColor(R.color.white));
        if (!item.getCreatedAt().equals("")) {
            try {
                date = format.parse(getDate(item.getCreatedAt()));

            } catch (java.text.ParseException e) {

                e.printStackTrace();
            }
        }
        if (arrChatinfo.size() > position + 1) {
            item2 = (NoiDungInfo) arrChatinfo.get(position + 1);

            if (!item2.getCreatedAt().equals("")) {
                try {
                    date2 = format.parse(getDate(item2.getCreatedAt()));

                } catch (java.text.ParseException e) {

                    e.printStackTrace();
                }
                if (item.getSenderChatInfo().getId() != null && item2.getSenderChatInfo().getId() != null && item.getSenderChatInfo().getId().equals(item2.getSenderChatInfo().getId())) {
//                    holder.lbldate.setVisibility(View.GONE);
                    if (date.compareTo(date2) != 0) {
                        holder.lbldate.setVisibility(View.VISIBLE);
                        holder.lblShowView.setVisibility(View.VISIBLE);
                        holder.lblShowView.setText(getFullDate(item.getCreatedAt()));

                    } else {
                        holder.lblShowView.setVisibility(View.GONE);
                        holder.lbldate.setVisibility(View.GONE);
                    }
                } else {
                    holder.lbldate.setVisibility(View.VISIBLE);
                }
            } else holder.lblShowView.setText(getFullDate(item.getCreatedAt()));

        }

        if (position > 0) {
            item3 = (NoiDungInfo) arrChatinfo.get(position - 1);
            if (!item3.getCreatedAt().equals("") && item3.getCreatedAt() != null) {
                try {
                    date3 = format.parse(getDate(item3.getCreatedAt()));

                } catch (java.text.ParseException e) {

                    e.printStackTrace();
                }
            }
        }

        String ten_nguoi_gui = "";
        if (position == 0
                || (position > 0 && item3.getSenderChatInfo().getId() != null && !item3.getSenderChatInfo().getId().equals(
                item.getSenderChatInfo().getId()))) {

            if (!user_id.equals(item.getSenderChatInfo().getId())) {
//                holder.avatar_chat.setVisibility(View.VISIBLE);
                ShowImage(true, aq, holder.avatar_chat, holder.lblNoAvatar, (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()), item.getSenderChatInfo().getAvatar(), item.getType());
//                holder.lblNoAvatar.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(15, 0, width, 0);
                holder.lblmessage_chat.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

                holder.lblmessage_chat
                        .setBackgroundResource(R.drawable.bgchat_left);
                holder.lblmessage_chat.setPadding(25, 10, 10, 10);
                holder.lblmessage_chat.setLayoutParams(lp);
                ten_nguoi_gui = getTen("1", (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()));

            } else {
                holder.lblmessage_chat.setTextColor(context.getResources().getColor(R.color.white));
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(width, 0, 15, 0);
                holder.lblmessage_chat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

                holder.lblmessage_chat
                        .setBackgroundResource(R.drawable.bgchat_right);
                holder.lblmessage_chat.setPadding(10, 10, 25, 10);
                holder.lblmessage_chat.setLayoutParams(lp);
            }
        } else {

            if (!item.getCreatedAt().equals("") && item3.getSenderChatInfo() != null && !item3.getCreatedAt().equals("")) {
                if (date3.compareTo(date) != 0) {
                    if (!user_id.equals(item.getSenderChatInfo().getId())) {
                        ShowImage(true, aq, holder.avatar_chat, holder.lblNoAvatar, (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()), item.getSenderChatInfo().getAvatar(), item.getType());
//                        holder.avatar_chat.setVisibility(View.VISIBLE);
//                        holder.lblNoAvatar.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(15, 0, width, 0);
                        holder.lblmessage_chat.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

                        holder.lblmessage_chat
                                .setBackgroundResource(R.drawable.bgchat_left);
                        holder.lblmessage_chat.setPadding(25, 10, 10, 10);
                        holder.lblmessage_chat.setLayoutParams(lp);
                        ten_nguoi_gui = getTen("1", (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()));
                    } else {
                        holder.lblmessage_chat.setTextColor(context.getResources().getColor(R.color.white));
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(width, 0, 15, 0);
                        holder.lblmessage_chat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        holder.lblmessage_chat
                                .setBackgroundResource(R.drawable.bgchat_right);
                        holder.lblmessage_chat.setPadding(10, 10, 25, 10);
                        holder.lblmessage_chat.setLayoutParams(lp);
                    }
                } else {
                    if (!user_id.equals(item.getSenderChatInfo().getId())) {
//                        holder.avatar_chat.setVisibility(View.GONE);
//                        holder.lblNoAvatar.setVisibility(View.GONE);
                        ShowImage(false, aq, holder.avatar_chat, holder.lblNoAvatar, (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()), item.getSenderChatInfo().getAvatar(), item.getType());
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(15, 0, width, 0);
                        holder.lblmessage_chat.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

                        holder.lblmessage_chat
                                .setBackgroundResource(R.drawable.bg_left);
                        holder.lblmessage_chat.setPadding(25, 10, 10, 10);

//                        LinearLayout.LayoutParams txt = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        holder.lblmessage_chat.setLayoutParams(lp);

                    } else {
                        holder.lblmessage_chat.setTextColor(context.getResources().getColor(R.color.white));
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(width, 0, 15, 0);
                        holder.lblmessage_chat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

                        holder.lblmessage_chat
                                .setBackgroundResource(R.drawable.bg_right);
                        holder.lblmessage_chat.setPadding(10, 10, 25, 10);
                        holder.lblmessage_chat.setLayoutParams(lp);
                    }
                }

            } else {

                if (!user_id.equals(item.getSenderChatInfo().getId())) {
//                    holder.avatar_chat.setVisibility(View.GONE);
//                    holder.lblNoAvatar.setVisibility(View.GONE);
                    ShowImage(false, aq, holder.avatar_chat, holder.lblNoAvatar, (item.getSenderChatInfo().getName() != null && !item.getSenderChatInfo().getName().equals("") ? item.getSenderChatInfo().getName() : item.getSenderChatInfo().getUsername()), item.getSenderChatInfo().getAvatar(), item.getType());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(15, 0, width, 0);
                    holder.lblmessage_chat.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);


                    holder.lblmessage_chat
                            .setBackgroundResource(R.drawable.bg_left);
                    holder.lblmessage_chat.setPadding(20, 10, 10, 10);

//                    LinearLayout.LayoutParams txt = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    holder.lblmessage_chat.setLayoutParams(lp);

                } else {

                    holder.lblmessage_chat.setTextColor(context.getResources().getColor(R.color.white));
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(width, 0, 15, 0);
                    holder.lblmessage_chat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

                    holder.lblmessage_chat
                            .setBackgroundResource(R.drawable.bg_right);
                    holder.lblmessage_chat.setPadding(10, 10, 20, 10);
                    holder.lblmessage_chat.setLayoutParams(lp);
                }
            }
        }
        if (is_group != null && is_group.equals("1")) {
            if ("remove_add_user".equals(item.getType())) {
                holder.lblmessage_chat.setVisibility(View.GONE);
                holder.lblShowView.setVisibility(View.VISIBLE);
                holder.lbldate.setVisibility(View.GONE);
                holder.lblShowView.setText(item.getContent());
                holder.lbldate.setVisibility(View.GONE);
            }
        }

        if (item.getType().equals("0") || item.getType().equals("")) {

            holder.imagContent.setVisibility(View.GONE);
            holder.lblmessage_chat.setVisibility(View.VISIBLE);
            holder.layoutFile.setVisibility(View.GONE);

        } else {

            holder.lblmessage_chat.setVisibility(View.GONE);
//            holder.lblContentFile
//            holder.lblDownLoadFile
//            holder.layoutFile = (R
            if (item.getType().equals("1")) {
                holder.layoutFile.setVisibility(View.GONE);
                holder.imagContent.setVisibility(View.VISIBLE);

                aq.id(holder.imagContent).image("http://125.212.225.58:3003" + item.getContent(),
                        true, true, widthima, 0, null, 0, 0);
            } else {
                holder.lblmessage_chat.setTextColor(context.getResources().getColor(R.color.white));
                holder.lblContentFile.setText(item.getFile_name());
                holder.imagContent.setVisibility(View.GONE);
                holder.layoutFile.setVisibility(View.VISIBLE);

            }

        }
        holder.lblmessage_chat.setText(Html.fromHtml(ten_nguoi_gui + item.getContent()));
        String time = "";


        if (user_id.equals(item.getSenderChatInfo().getId())) {
            if ("3".equals(item.getTrang_thai())) {

                time = context.getResources().getString(R.string.lbldanggui);
            } else {
                if ("1".equals(item.getTrang_thai())) {
                    time = context.getResources().getString(R.string.lbldanhan);

                } else if ("2".equals(item.getTrang_thai())) {

                    if (arrChatinfo.size() > position + 1) {
                        if (!item.getSenderChatInfo().getId().equals(item2.getSenderChatInfo().getId())) {
                            if (!"0".equals(item.getThoi_gian_client()) || !"".equals(item.getThoi_gian_client())) {
                                time = getDateTime(item.getThoi_gian_client());
                            } else if (!"".equals(item.getCreatedAt())) {
                                time = getDateTime(item.getCreatedAt());
                            } else {

                                time = context.getResources().getString(R.string.lbldagui);
                            }
                        }
                    } else {
                        time = context.getResources().getString(R.string.lbldaxem);
                    }
                } else {

                    if (!"0".equals(item.getThoi_gian_client()) || !"".equals(item.getThoi_gian_client())) {

                        if (arrChatinfo.size() > position + 1) {

                            time = item.getThoi_gian_client().equals("") ? getDateTime(item.getCreatedAt()) : StringUtility.getDateTimetoTimeStamp(context, item.getThoi_gian_client());
                        } else {
                            if (item.getTrang_thai().equals("2"))
                                time = context.getResources().getString(R.string.lbldaxem);
                            else {
                                if (item.getTrang_thai().equals("0"))
                                    time = context.getResources().getString(R.string.lbldagui);
                                else if (item.getTrang_thai().equals("1"))
                                    time = context.getResources().getString(R.string.lbldanhan);
                            }

                        }
                    } else if (!"".equals(item.getCreatedAt())) {

                        if (arrChatinfo.size() > position + 1) {
                            time = getDateTime(item.getCreatedAt());
                        } else {
                            if (item.getTrang_thai().equals("2"))
                                time = context.getResources().getString(R.string.lbldaxem);
                            else {
                                if (item.getTrang_thai().equals("0"))
                                    time = context.getResources().getString(R.string.lbldagui);
                                else
                                    time = context.getResources().getString(R.string.lbldanhan);
                            }
                        }
                    } else {

                        time = context.getResources().getString(R.string.lbldagui);
                    }
                }
            }

        } else {

            if (!item.getCreatedAt().equals("")) {

                time = getDateTime(item.getCreatedAt());
            } else {
                time = context.getResources().getString(R.string.lblcapnhat);
            }
        }

        holder.lbldate.setText(time);

        holder.imagContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewImageActivity.class);
                intent.putExtra("img", "http://125.212.225.58:3003" + item.getContent());
                context.startActivity(intent);
            }
        });

        holder.lblDownLoadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatActivity.urlDownLoad = "http://125.212.225.58:3003" + item.getContent();
                ChatActivity.filename = item.getFile_name();
                yeuThichListener.onChangeYeuThich();
            }
        });
        return convertView;
    }

    private void ShowImage(boolean check, AQuery aq, ImageView image, TextView text, String nguoi_gui_ten, String avatar, String typeUser) {

        if (check == true && !"remove_add_user".equals(typeUser)) {
//            if (null != avatar && !"".equals(avatar)) {
                image.setVisibility(View.VISIBLE);
                aq.id(image).image(avatar.contains("http") ? avatar : "http://125.212.225.58:3003" + avatar,
                        true, true, 50, R.drawable.avatar2, null, 0, 0);
                text.setVisibility(View.GONE);
//            } else {
//                StringUtility.CutTextCustomer(nguoi_gui_ten, aq, image, text);
//            }
        } else {
            text.setVisibility(View.GONE);
            image.setVisibility(View.GONE);
        }

    }

    private String getDateTime(String time) {
        try {
            if (time != null) {

                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                inputFormat.setTimeZone(TimeZone.getTimeZone("UCT"));
                SimpleDateFormat outputFormat =
                        new SimpleDateFormat("hh:mm");

                Date date = inputFormat.parse(time);
                String outputText = outputFormat.format(date);

                return outputText;
            } else
                return context.getResources().getString(R.string.lblcapnhat);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NumberFormatException nfe) {
            Log.e("", "Input must be a number.");
        } catch (Exception e) {
            Log.e("", "Generic exception caught");
        }
        return context.getResources().getString(R.string.lblcapnhat);
    }

    private String getDate(String time) {

        try {
            if (time != null) {

                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                inputFormat.setTimeZone(TimeZone.getTimeZone("UCT"));
                SimpleDateFormat outputFormat =
                        new SimpleDateFormat("dd-MM-yyyy");

                Date date = inputFormat.parse(time);
                String outputText = outputFormat.format(date);
                return outputText;
            } else
                return context.getResources().getString(R.string.lblcapnhat);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NumberFormatException nfe) {
            Log.e("", "Input must be a number.");
        } catch (Exception e) {
            Log.e("", "Generic exception caught");
        }
        return context.getResources().getString(R.string.lblcapnhat);
    }

    private String getFullDate(String time) {

        try {
            if (time != null) {

                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                inputFormat.setTimeZone(TimeZone.getTimeZone("UCT"));
                SimpleDateFormat outputFormat =
                        new SimpleDateFormat("hh:mm, dd/MM/yyyy");

                Date date = inputFormat.parse(time);
                String outputText = outputFormat.format(date);

                return outputText;
            } else
                return context.getResources().getString(R.string.lblcapnhat);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NumberFormatException nfe) {
            Log.e("", "Input must be a number.");
        } catch (Exception e) {
            Log.e("", "Generic exception caught");
        }
        return context.getResources().getString(R.string.lblcapnhat);
    }

    public class ViewHolder {

        TextView lbldate, lblmessage_chat, lblShowView, lblNoAvatar, lblContentFile, lblDownLoadFile;
        ImageView avatar_chat;
        ImageView imagContent;
        LinearLayout layoutText;
        RelativeLayout layoutFile;

    }

    private String getTen(String is_group, String ten_nguoi_gui) {
        try {
            String ten = "";
            ten = (is_group != null && is_group.equals("1")) ? ("<font color='#919191' > <small>" + ten_nguoi_gui.trim() + "</small></font>" + "<br>") : "";
            return ten;
        } catch (NullPointerException e) {
            return "";
        } catch (Exception e) {
            return "";
        } catch (Error er) {
            return "";
        }

    }


}
