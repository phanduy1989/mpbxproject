package com.viettel.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.adapter.holder.ColleagueHolder;
import com.viettel.adapter.holder.ColleagueItem;
import com.viettel.adapter.holder.DepartItem;
import com.viettel.adapter.holder.DepartmentHolder;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.OnDepartSelectedListener;
import com.viettel.interfaces.OnGroupSelectedListener;
import com.viettel.interfaces.RecyclerItemClickListener;
import com.viettel.model.Agent;
import com.viettel.model.ColleagueModel;
import com.viettel.mpbx.R;
import com.viettel.utils.CircleTransform;
import com.viettel.utils.RoundedTransformation;
import com.viettel.view.base.BABaseActivity;

import java.util.List;

/**
 * Created by mc.kim on 8/25/2016.
 */
public class ListDepartAdapter extends ExpandableRecyclerAdapter<DepartmentHolder, ColleagueHolder>
        implements RecyclerItemClickListener.OnItemClickListener, View.OnClickListener {


    private LayoutInflater mInflater = null;
    private BABaseActivity listColleagueActivity;

    private boolean isSystem;

    public void setSystem(boolean system) {
        isSystem = system;
    }

    public ListDepartAdapter(BABaseActivity listColleagueActivity, LayoutInflater layoutInflater, List<DepartItem> parentItemList) {
        super(parentItemList);
        this.listColleagueActivity = listColleagueActivity;
        this.mInflater = layoutInflater;
    }

    @Override
    public DepartmentHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View view = mInflater.inflate(R.layout.item_colleague_header, parentViewGroup, false);

        RelativeLayout root = (RelativeLayout) view.findViewById(R.id.root);
//        root.setOnTouchListener(null);

//        view.setClickable(false);
//        view.setOnClickListener(null);
//        view.setOnTouchListener(null);
        TextView menuTxt = (TextView) view.findViewById(R.id.menuTxt);
        ImageView btnExpand = (ImageView) view.findViewById(R.id.btnExpand);
//        LinearLayout layoutTitle = (LinearLayout) view.findViewById(R.id.layoutTitle);
        btnExpand.setTag(view);
//        layoutTitle.setTag(view);
        menuTxt.setTag(view);
        root.setTag(view);
        root.setOnClickListener(this);
//        btnExpand.setOnClickListener(this);
        return new DepartmentHolder(view);
    }

    @Override
    public ColleagueHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View view = mInflater.inflate(R.layout.item_colleague_child, null, false);

        ImageButton btnPhone = (ImageButton) view.findViewById(R.id.btnPhone);
        ImageButton btnAddFavorite = (ImageButton) view.findViewById(R.id.btnAddFavorite);
        LinearLayout root = (LinearLayout) view.findViewById(R.id.rootChild);
        root.setOnClickListener(this);
        root.setTag(view);

//        view.setClickable(true);

//        view.setTag(view);
        btnPhone.setTag(view);
        btnPhone.setOnClickListener(this);
        btnAddFavorite.setTag(view);
        btnAddFavorite.setOnClickListener(this);
//        view.setOnClickListener(this);
        return new ColleagueHolder(view);
    }

    @Override
    public void onBindParentViewHolder(DepartmentHolder parentViewHolder, int position, ParentListItem parentListItem) {

        DepartItem parentsMenu = (DepartItem) parentListItem;
        parentViewHolder.menuTxt.setText(parentsMenu.getColleagueGroup().getDeptName() + (parentsMenu.getChildCount() > 0 ? " (" + parentsMenu.getChildItemList().size() + ")" : ""));

        if(!isSystem) {
            parentViewHolder.btnExpanded.setVisibility(parentsMenu.hasChild() ? View.VISIBLE : View.GONE);
            if (parentsMenu.hasChild()) {
                parentViewHolder.btnExpanded.setVisibility(View.VISIBLE);
            } else {
                parentViewHolder.btnExpanded.setVisibility(View.GONE);
            }
        } else {
            parentViewHolder.btnExpanded.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBindChildViewHolder(ColleagueHolder childViewHolder, int position, Object childListItem) {
        Agent subMenu = (Agent) childListItem;
        childViewHolder.txtName.setText(subMenu.getEmployeeName());
        childViewHolder.txtPhone.setText("" + subMenu.getPhoneNumber());

        Picasso.with(listColleagueActivity)
                .load(subMenu.getFileName())
                .placeholder(R.drawable.avatar2)
                .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                .into(childViewHolder.avatar);

    }

    private RecyclerView mRecyclerView = null;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.mRecyclerView = recyclerView;
    }


    @Override
    public void onItemClick(View view, int position) {
        Log.e("onItemClick", "OK");
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    public Object getListItem(int position) {
        return super.getListItem(position);
    }

    @Override
    public void onClick(View v) {
        View adapterView = (View) v.getTag();
        if (adapterView == null) {
            return;
        }

        int position = mRecyclerView.getChildLayoutPosition(adapterView);

        Object listItem = getListItem(position);
        if (listItem instanceof Agent) {
            Agent colleagueModel = (Agent) listItem;
            if(v.getId() == R.id.btnPhone) {
                listColleagueActivity.showCallLayout(colleagueModel);
            } else {
                listColleagueActivity.goToChoseAvaiGroup(colleagueModel);
            }
        } else {
            onClickExpand(position);
        }


//        if (v.getId() == R.id.btnExpand) {
//
//            onClickExpand(position);
//
//        } else {
//            Object listItem = getListItem(position);
//            Menu selectedMenu = null;
//            if (listItem instanceof ParentWrapper) {
//                ParentWrapper wrapper = (ParentWrapper) listItem;
//                ParentListItem item = wrapper.getParentListItem();
//                ColleagueItem menu = (ColleagueItem) item;
//                selectedMenu = menu.getMenu();
//            } else {
//                Menu menu = (Menu) listItem;
//                selectedMenu = menu;
//            }

//            if (!disableMenu(selectedMenu)) {
//                if (selectedMenu.isClickable()) {
//                    if (mListener != null)
//                        mListener.onMenuSelected(selectedMenu);
//                } else {
//                    onClickExpand(position);
//                }
//            }
//        }
    }

    public void onClickExpand(int position) {
        int positionExpanding = -1;
        int menuExpandingChildCount = -1;

        for (int i = 0, count = getItemCount(); i < count; i++) {
            if (i == position) {
                continue;
            }
            Object listItem = getListItem(i);
            if (listItem instanceof ParentWrapper) {
                ParentWrapper wrapper = (ParentWrapper) listItem;
                ParentListItem item = wrapper.getParentListItem();

                if (wrapper.isExpanded()) {
                    DepartItem menu = (DepartItem) item;
                    positionExpanding = i;
                    menuExpandingChildCount = menu.getChildItemList() != null ? menu.getChildItemList().size() : 0;
                    collapseParent(item);
                }
            }
        }
        int updateNextExpand = positionExpanding < 0 || positionExpanding > position ? position : position - menuExpandingChildCount;
        Object listItemPosition = getListItem(updateNextExpand);
        if (listItemPosition instanceof ParentWrapper) {
            ParentWrapper wrapper = (ParentWrapper) listItemPosition;
            ParentListItem item = wrapper.getParentListItem();

            if(onGroupSelectedListener != null) {
                onGroupSelectedListener.onGroupSelect(((DepartItem) item).getColleagueGroup());
            }
        }

        notifyDataSetChanged();
    }

    private OnChildSelectedListener mListener = null;
    private OnDepartSelectedListener onGroupSelectedListener = null;

    public void setOnChildSelectedListener(OnChildSelectedListener mListener) {
        this.mListener = mListener;
    }

    public void setOnGroupSelectedListener(OnDepartSelectedListener onGroupSelectedListener) {
        this.onGroupSelectedListener = onGroupSelectedListener;
    }
}
