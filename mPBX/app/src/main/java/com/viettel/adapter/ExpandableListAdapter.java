package com.viettel.adapter;

/**
 * Created by duyuno on 8/17/17.
 */
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.request.colleague.RemoveColleagueRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;

public class ExpandableListAdapter extends BaseExpandableListAdapter implements MPBXFilter{

    private BABaseActivity activity;
    private ArrayList<ColleagueGroup> listGroup;
    private ArrayList<ColleagueGroup> listGroupDisplay;

    private ExpandableListView expandableListView;

    public ExpandableListAdapter(BABaseActivity context, ExpandableListView expandableListView) {
        this.activity = context;
        this.expandableListView = expandableListView;
    }

    public void setData(ArrayList<ColleagueGroup> list) {
        if(listGroup == null) {
            listGroup = new ArrayList<>();
        } else {
            listGroup.clear();
        }

        listGroup.addAll(list);

        if(listGroupDisplay == null) {
            listGroupDisplay = new ArrayList<>();
        } else {
            listGroupDisplay.clear();
        }

        listGroupDisplay.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ColleagueModel getChild(int groupPosition, int childPosititon) {

        ColleagueGroup colleagueGroup = getGroup(groupPosition);
        if(colleagueGroup == null) return null;

        if(colleagueGroup.getListMember() == null) return null;

        return childPosititon < colleagueGroup.getListMember().size() ? colleagueGroup.getListMember().get(childPosititon) : null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View view, ViewGroup parent) {

        final ColleagueModel colleagueModel = getChild(groupPosition, childPosition);
        final ColleagueGroup colleagueGroup = getGroup(groupPosition);

        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_colleague_child, null);
        }

        ImageButton btnPhone = (ImageButton) view.findViewById(R.id.btnPhone);
        ImageButton btnAddFavorite = (ImageButton) view.findViewById(R.id.btnAddFavorite);
        ImageButton btnDelete = (ImageButton) view.findViewById(R.id.btnDelete);

        TextView txtName = (TextView) view.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) view.findViewById(R.id.txtPhone);
        ImageView avatar = (ImageView) view.findViewById(R.id.avatar);

        txtName.setText(colleagueModel.getColleagueName());
        txtPhone.setText("" + colleagueModel.getPhoneNumber());

        if(colleagueModel.getFileName() != null) {
            Picasso.with(activity)
                    .load(colleagueModel.getFileName())
                    .placeholder(R.drawable.avatar2)
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(avatar);
        }

        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showCallLayout(colleagueModel);
            }
        });

        btnAddFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.goToChoseAvaiGroup(colleagueModel);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtility.showDialogConfirm(activity, "Xóa <b>" + colleagueModel.getColleagueName() +  "</b> ra khỏi nhóm <b>" + colleagueGroup.getGroupName()+ "</b>?", new ConfirmListener() {

                    @Override
                    public void doCancel() {

                    }

                    @Override
                    public void doAccept() {
                        doDeleteColleague(colleagueGroup, colleagueModel);
                    }
                });
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.goToColleagueInfo(colleagueModel);
            }
        });

        return view;
    }

    public void doDeleteColleague(final ColleagueGroup colleagueGroup, final ColleagueModel colleagueModel) {
        activity.showLoading();

        RemoveColleagueRequest removeMemberRequest = new RemoveColleagueRequest();

        ArrayList<Long> listMemberId = new ArrayList<>();
        listMemberId.add((long)colleagueModel.getColleagueId());

        removeMemberRequest.setData(colleagueGroup.getGroupId(), listMemberId);

        ApiController.doPostRequest(activity, removeMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        removeMember(colleagueModel, colleagueGroup, listGroup);
                        removeMember(colleagueModel, colleagueGroup, listGroupDisplay);
                        notifyDataSetChanged();
                        Toast.makeText(activity, "Xóa thành công!", Toast.LENGTH_SHORT).show();
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void removeMember(ColleagueModel colleagueModel, ColleagueGroup colleagueGroup, ArrayList<ColleagueGroup> listGroupFilter) {
        if(colleagueModel == null || colleagueGroup == null || listGroupFilter == null) return;
        for(ColleagueGroup colleagueGroup1 : listGroupFilter) {
            if(colleagueGroup1.getGroupId() == colleagueGroup.getGroupId()) {
                if(colleagueGroup1.getListMember() == null) {
                    break;
                }

                int position = -1;

                for(int i = 0, size = colleagueGroup1.getListMember().size(); i < size; i++) {
                    ColleagueModel colleagueModel1 = colleagueGroup1.getListMember().get(i);
                    if(colleagueModel1.getColleagueId() == colleagueModel.getColleagueId()) {
                        position = i;
                        break;
                    }
                }

                if(position >= 0) {
                    colleagueGroup1.getListMember().remove(position);
                }
            }
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ColleagueGroup group = getGroup(groupPosition);

        return group != null && group.getListMember() != null ? group.getListMember().size() : 0;
    }

    @Override
    public ColleagueGroup getGroup(int groupPosition) {
        return listGroupDisplay != null && groupPosition < listGroupDisplay.size() ? listGroupDisplay.get(groupPosition) : null;
    }

    @Override
    public int getGroupCount() {
        return listGroupDisplay != null ? listGroupDisplay.size() : 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    class GroupViewholder {
        TextView menuTxt;
        ImageView btnExpand;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View view, ViewGroup parent) {
        ColleagueGroup colleagueGroup = getGroup(groupPosition);
        GroupViewholder groupViewholder;
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_colleague_header, null);

            groupViewholder = new GroupViewholder();
            groupViewholder.menuTxt = (TextView) view.findViewById(R.id.menuTxt);
            groupViewholder.btnExpand = (ImageView) view.findViewById(R.id.btnExpand);
            view.setTag(groupViewholder);

        } else {
            groupViewholder = (GroupViewholder) view.getTag();
        }

        if(colleagueGroup != null) {
            groupViewholder.menuTxt.setText("" + colleagueGroup.getGroupName() + " (" + getChildrenCount(groupPosition) + ")");

            if (colleagueGroup.isExpand()) {
                groupViewholder.btnExpand.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                expandableListView.expandGroup(groupPosition);
            } else {
                groupViewholder.btnExpand.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                expandableListView.collapseGroup(groupPosition);
            }
        }
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void filter(String key) {
        if(listGroup == null || listGroup.isEmpty()) return;

        ((BABaseActivity) activity).showContent();

        if(key == null || key.trim().isEmpty()) {

            listGroupDisplay = new ArrayList<>();
            listGroupDisplay.addAll(listGroup);
            notifyDataSetChanged();
            return;
        }

        key = StringUtility.convertVietnameseToAscii(key).toLowerCase().trim();

        listGroupDisplay = new ArrayList<>();

        for(ColleagueGroup colleagueGroup : listGroup) {
            ColleagueGroup cg = new ColleagueGroup(colleagueGroup);
            if(colleagueGroup.getListMember() != null) {
                ArrayList<ColleagueModel> listMember = new ArrayList<>();
                listMember.addAll(colleagueGroup.getListMember());
                cg.setListMember(listMember);


            }
            listGroupDisplay.add(cg);

        }

//        listGroupDisplay.addAll(listGroup);

        for(int s = listGroupDisplay.size(), i = s - 1; i >= 0; i--) {
            ColleagueGroup colleagueGroup = listGroupDisplay.get(i);
            if(colleagueGroup.getListMember() != null) {

                for(int size = colleagueGroup.getListMember().size(), j = size - 1; j >= 0; j--) {
                    ColleagueModel colleagueModel = colleagueGroup.getListMember().get(j);

                    String colleagueName = StringUtility.convertVietnameseToAscii(colleagueModel.getColleagueName());
                    String depertMentName = StringUtility.convertVietnameseToAscii(colleagueModel.getDepartmentName());

                    Log.e("Info", "" + colleagueName + ", " + depertMentName);

                    if((colleagueName == null || !colleagueName.contains(key))
                            && (colleagueModel.getPhoneNumber() == null || !colleagueModel.getPhoneNumber().contains(key))
                            && (depertMentName == null || !depertMentName.contains(key))
                            ) {
                        colleagueGroup.getListMember().remove(j);
                    }
                }
            }

            if(colleagueGroup.getListMember() == null || colleagueGroup.getListMember().isEmpty()) {
                listGroupDisplay.remove(i);
            }
        }

        notifyDataSetChanged();



    }
}