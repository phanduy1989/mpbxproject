package com.viettel.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.model.Agent;
import com.viettel.model.ContactObject;
import com.viettel.model.DepartmentModel;
import com.viettel.mpbx.R;
import com.viettel.utils.CircleTransform;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.colleague.ListColleagueInGroupActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by duyuno on 7/11/17.
 */
public class SubpartAdapter extends BaseAdapter implements MPBXFilter{
    public ArrayList<Object> listData;
    public ArrayList<Object> listDataDisplay;

    BABaseActivity activity;

    public SubpartAdapter(BABaseActivity activity) {
        this.activity = activity;
    }

    public void setData(ArrayList<Object> list) {
        if(listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }

        listData.addAll(list);

        if(listDataDisplay == null) {
            listDataDisplay = new ArrayList<>();
        } else {
            listDataDisplay.clear();
        }

        listDataDisplay.addAll(list);

        notifyDataSetChanged();
    }
    public void addData(Object list) {
        if(listData == null) {
            listData = new ArrayList<>();
        }
        listData.addAll((Collection<?>) list);
        if(listDataDisplay == null) {
            listDataDisplay = new ArrayList<>();
        }
        listDataDisplay.addAll((Collection<?>) list);



        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataDisplay != null ? listDataDisplay.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return listDataDisplay != null ? listDataDisplay.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void filter(String key) {
        if(listData == null || listData.isEmpty()) return;

        ((BABaseActivity) activity).showContent();

        if(key == null || key.trim().isEmpty()) {

            listDataDisplay = new ArrayList<>();
            listDataDisplay.addAll(listData);
            notifyDataSetChanged();
            return;
        }
        key = StringUtility.convertVietnameseToAscii(key).toLowerCase().trim();


        listDataDisplay = new ArrayList<>();

        for(Object obj : listData) {
            if(obj instanceof DepartmentModel) {
                String name = StringUtility.convertVietnameseToAscii(((DepartmentModel) obj).getDeptName());
                if(name != null && name.contains(key)) {
                    listDataDisplay.add(obj);
                }
            } else {
                String name = StringUtility.convertVietnameseToAscii(((Agent) obj).getEmployeeName());
                String phone = ((Agent) obj).getPhoneNumber();
                if((name != null && name.contains(key)) || (phone != null && phone.contains(key))) {
                    listDataDisplay.add(obj);
                }
            }
        }


        notifyDataSetChanged();

    }

    class ViewHolder {
        public ImageView avatar;
        public ImageView btnExpand;
        public TextView txtName;
        public TextView txtPhone;
        public ImageButton btnPhone, btnAddFavorite;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Object obj = getItem(position);

//        ViewHolder viewHolder = null;
//        if (convertView == null) {
//            viewHolder = new ViewHolder();
//            if (obj instanceof DepartmentModel) {
//                convertView = activity.getLayoutInflater().inflate(R.layout.item_colleague_header, null);
//                viewHolder.txtName = (TextView) convertView.findViewById(R.id.menuTxt);
//                viewHolder.btnExpand = (ImageView) convertView.findViewById(R.id.btnExpand);
//                viewHolder.btnExpand.setVisibility(View.GONE);
//            } else {
//                convertView = activity.getLayoutInflater().inflate(R.layout.item_colleague_child, null);
//                viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
//                viewHolder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
//                viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
//                viewHolder.btnPhone = (ImageButton) convertView.findViewById(R.id.btnPhone);
//                viewHolder.btnAddFavorite = (ImageButton) convertView.findViewById(R.id.btnAddFavorite);
//            }
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }

        ImageView avatar = null;
         ImageView btnExpand;
         TextView txtName;
         TextView txtPhone = null;
         ImageButton btnPhone = null, btnAddFavorite = null;

        if (obj instanceof DepartmentModel) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_colleague_header, null);
            txtName = (TextView) convertView.findViewById(R.id.menuTxt);
            btnExpand = (ImageView) convertView.findViewById(R.id.btnExpand);
            btnExpand.setVisibility(View.GONE);
        } else {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_colleague_child, null);
            txtName = (TextView) convertView.findViewById(R.id.txtName);
            txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
            avatar = (ImageView) convertView.findViewById(R.id.avatar);
            btnPhone = (ImageButton) convertView.findViewById(R.id.btnPhone);
            btnAddFavorite = (ImageButton) convertView.findViewById(R.id.btnAddFavorite);
            convertView.findViewById(R.id.btnDelete).setVisibility(View.GONE);
        }



        if (obj instanceof DepartmentModel) {
//            int count = ((DepartmentModel) obj).getListMember() != null ? ((DepartmentModel) obj).getListMember().size():0;
//            txtName.setText(((DepartmentModel) obj).getDeptName() + " (" + count + ")");
            txtName.setText(((DepartmentModel) obj).getDeptName());
        } else {
            final Agent agent = (Agent) obj;

            txtName.setText(agent.getEmployeeName());
            txtPhone.setText(agent.getPhoneNumber());

            if(agent.getFileName() != null) {
                Picasso.with(activity)
                        .load(agent.getFileName())
                        .placeholder(R.drawable.avatar2)
                        .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                        .into(avatar);
            }

            btnPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.showCallLayout(agent);
                }
            });

            btnAddFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.goToChoseAvaiGroup(agent);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(activity instanceof ListColleagueInGroupActivity) {
                        ((ListColleagueInGroupActivity) activity).goToColleagueInfo(agent);
                    }
                }
            });

        }

        return convertView;
    }
}
