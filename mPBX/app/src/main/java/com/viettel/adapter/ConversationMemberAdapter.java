package com.viettel.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationMember;
import com.viettel.model.request.conference.RemoveMemberRequest;
import com.viettel.model.request.conference.MuteMemberRequest;
import com.viettel.model.request.conference.TerminateMemberRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.utils.CircleTransform;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.PhoneUtil;
import com.viettel.view.activity.business.conversation.ConversationInfoActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class ConversationMemberAdapter extends BaseAdapter {
    public ArrayList<ConversationMember> listData;

    BABaseActivity activity;
    public int conversationType;
    public int conferenceId;
    public boolean isUpcomingTime;

    public ConversationMemberAdapter(BABaseActivity activity, int conversationType, int conferenceId) {
        this.activity = activity;
        this.conversationType = conversationType;
        this.conferenceId = conferenceId;
    }

    public void setUpcomingTime(boolean upcomingTime) {
        isUpcomingTime = upcomingTime;
    }

    public void setData(ArrayList<ConversationMember> list) {
        listData = list;
        notifyDataSetChanged();
    }

    public ArrayList<ConversationMember> getListData() {
        if (listData == null || listData.isEmpty()) return null;
        return listData;
    }

    public interface OnClickMemberAction {
        public void onClickMute(ConversationMember conversationMember);

        public void onClickRemove(ConversationMember conversationMember);
    }

    @Override
    public int getCount() {
        return listData != null ? listData.size() : 0;
    }

    @Override
    public ConversationMember getItem(int position) {
        return listData != null ? listData.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        public TextView txtName;
        public TextView txtPhone;
        public LinearLayout statusRect;

        public ImageButton btnRemove, btnMicro, btnTerMinate;
        public ImageView avatar;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        final ConversationMember contactModel = getItem(position);
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_conversation_member, null);
            viewHolder = new ViewHolder();
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
            viewHolder.btnRemove = (ImageButton) convertView.findViewById(R.id.btnRemove);
            viewHolder.btnMicro = (ImageButton) convertView.findViewById(R.id.btnMicro);
            viewHolder.btnTerMinate = (ImageButton) convertView.findViewById(R.id.btnTerMinate);
            viewHolder.statusRect = (LinearLayout) convertView.findViewById(R.id.statusRect);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);

            if (conversationType == ConversationGroup.FINISHED) {
                viewHolder.btnRemove.setVisibility(View.GONE);
                viewHolder.btnMicro.setVisibility(View.GONE);
            } else if (conversationType == ConversationGroup.ON_GOING) {
                if (contactModel.getCallStatus() != 0 && contactModel.getCallStatus() != 3) {
                    viewHolder.btnTerMinate.setVisibility(View.VISIBLE);
                }
                viewHolder.btnRemove.setVisibility(View.GONE);
            }

            if (isUpcomingTime && conversationType == ConversationGroup.UP_COMMING) {
                viewHolder.btnRemove.setVisibility(View.GONE);
                viewHolder.btnMicro.setVisibility(View.GONE);
                viewHolder.btnTerMinate.setVisibility(View.GONE);
            }

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        if (contactModel != null) {
            viewHolder.txtName.setText(contactModel.getMemberName());

            int status = contactModel.getStatus();

            if (status == ConversationMember.STATUS_ONLINE) {
                viewHolder.statusRect.setBackgroundResource(R.drawable.drawable_green_rect);
            } else {
                viewHolder.statusRect.setBackgroundResource(R.drawable.drawable_red_rect);
            }

            viewHolder.txtPhone.setText(PhoneUtil.makeCorrectPhoneFormat(contactModel.getPhoneNumber()));

            switch (contactModel.getCallStatus()) {
                case ConversationMember.INCALL:
                    viewHolder.btnMicro.setImageResource(R.drawable.micro_active);
                    break;
                case ConversationMember.MUTE:
                    viewHolder.btnMicro.setImageResource(R.drawable.micro_disable);
                    break;
            }

            viewHolder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (conversationType != ConversationGroup.UP_COMMING) {
                        return;
                    }

                    DialogUtility.showDialogConfirm(activity, "Xóa <b>" + contactModel.getMemberName() + "</b> khỏi cuộc thoại?", new ConfirmListener() {
                        @Override
                        public void doAccept() {
                            doRemoveMember(contactModel, position);
                        }

                        @Override
                        public void doCancel() {

                        }
                    });

                }
            });
            viewHolder.btnMicro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (conversationType == ConversationGroup.UP_COMMING) {
                        doChangeMute(contactModel);
                        return;
                    }


                    String message = contactModel.getCallStatus() == ConversationMember.INCALL ? "Tắt micro hội nghị" : "Bật micro hội nghị";
                    message += " đối với <b>" + contactModel.getMemberName() + "</b> ?";

                    DialogUtility.showDialogConfirm(activity, message, new ConfirmListener() {
                        @Override
                        public void doAccept() {
                            doChangeMute(contactModel);
                        }

                        @Override
                        public void doCancel() {

                        }
                    });

                }
            });

            viewHolder.btnTerMinate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogUtility.showDialogConfirm(activity, "Ngắt kết nối với <b>" + contactModel.getMemberName() + "</b>?", new ConfirmListener() {
                        @Override
                        public void doAccept() {
                            doTerminate(contactModel, position);
                        }

                        @Override
                        public void doCancel() {

                        }
                    });

                }
            });

            if (contactModel.getFileName() != null) {
                Picasso.with(activity)
                        .load(contactModel.getFileName())
                        .placeholder(R.drawable.avatar2)
                        .transform(new CircleTransform())
                        .into(viewHolder.avatar);
            }
        }

        return convertView;
    }

    public void doChangeMute(final ConversationMember conversationMember) {
        activity.showLoading();

        MuteMemberRequest muteMemberRequest = new MuteMemberRequest();

        muteMemberRequest.setData(conversationMember.getConferenceMemberId(), conversationMember.getChangeCallStatus(), conferenceId);

        ApiController.doPostRequest(activity, muteMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        conversationMember.changeMute();
                        notifyDataSetChanged();
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void doTerminate(final ConversationMember conversationMember, final int position) {
        activity.showLoading();

        TerminateMemberRequest muteMemberRequest = new TerminateMemberRequest();

        muteMemberRequest.setData(conversationMember.getConferenceMemberId(), conferenceId);

        ApiController.doPostRequest(activity, muteMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        if (activity instanceof ConversationInfoActivity) {
                            ((ConversationInfoActivity) activity).initData();
                        }
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void doRemoveMember(final ConversationMember conversationMember, final int position) {
        activity.showLoading();

        RemoveMemberRequest removeMemberRequest = new RemoveMemberRequest();

        removeMemberRequest.setData(conversationMember.getConferenceMemberId(), conferenceId);

        ApiController.doPostRequest(activity, removeMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {

                        if (activity instanceof ConversationInfoActivity) {
                            ((ConversationInfoActivity) activity).remarkEditted();
                        }

                        listData.remove(position);
                        notifyDataSetChanged();
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }
}
