package com.viettel.adapter.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ae.team.connectapi.config.WebServiceConfig;
import com.ae.team.connectapi.utility.YeuThichListener;
import com.androidquery.AQuery;
import com.viettel.model.DanhBaInfo;
import com.viettel.mpbx.R;

import java.util.ArrayList;
import java.util.Locale;


public class TaoNhomDanhBaAdapter extends RecyclerView.Adapter<TaoNhomDanhBaViewHolder> {

    private ArrayList<DanhBaInfo> contactInfoLista;
    private ArrayList<DanhBaInfo> contactInfoListabbbb;
    private Context context;
    private YeuThichListener yeuThichListener;
    private int w = 84, h = 84;
    private AQuery aq;


    public TaoNhomDanhBaAdapter(Context context, ArrayList<DanhBaInfo> contactInfoList, YeuThichListener yeuThichListener) {
        this.context = context;
        this.contactInfoLista = contactInfoList;
        this.contactInfoListabbbb = new ArrayList<>();
        this.contactInfoListabbbb.addAll(contactInfoLista);
        this.yeuThichListener = yeuThichListener;
        aq = new AQuery(context);
    }

    @Override
    public TaoNhomDanhBaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_danhba_taonhom, parent, false);
        aq = aq.recycle(itemView);
        return new TaoNhomDanhBaViewHolder(itemView);
    }

    private int layvitriBatDau(String chuoi, String chuoitimkiem) {
        int vitri = 0;
        vitri = chuoi.indexOf(chuoitimkiem);
        return vitri;
    }

    @Override
    public void onBindViewHolder(final TaoNhomDanhBaViewHolder holder, int position) {
        final DanhBaInfo item = contactInfoLista.get(position);

        try {

            holder.lblName.setTextColor(context.getResources().getColor(R.color.lblname));
            holder.lblNoAvatar.setTextColor(context.getResources().getColor(R.color.white));
            holder.lblBage.setTextColor(context.getResources().getColor(R.color.white));

            holder.lblNameDes.setText(item.getCreatedAt());
            holder.lblName.setText(item.getName().equals("") ? item.getUsername() : item.getName());
//            holder.lblNoAvatar.setVisibility(View.VISIBLE);
//            aq.id(imgAvatar).image(R.drawable.bgavatar);
//            String name = item.getUsername().substring(0, 1);
//            holder.lblNoAvatar.setText(name.toUpperCase());

            if (item.getOnline().equals("1"))
            {
                holder.lblOnline.setVisibility(View.VISIBLE);
            }else
            {
                holder.lblOnline.setVisibility(View.GONE);
            }
            if ("1".equals(item.getCheck_chon())) {
                holder.cbChon.setChecked(true);
            } else {
                holder.cbChon.setChecked(false);
            }
            holder.layoutConten.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if ("1".equals(item.getCheck_chon())) {
                        holder.cbChon.setChecked(false);
                        item.setCheck_chon("");
                    } else {
                        holder.cbChon.setChecked(true);
                        item.setCheck_chon("1");
                    }
                    yeuThichListener.onChangeYeuThich();

                }
            });
//            if (null != item.getAvatar() && !"".equals(item.getAvatar())) {
                aq.id(holder.imgAvatar).image(item.getAvatar().contains("http") ? item.getAvatar() : WebServiceConfig.API_URL + item.getAvatar(),
                        true, true, w, R.drawable.avatar2, null, 0, 0);
                holder.lblNoAvatar.setVisibility(View.GONE);
//            } else {
//                StringUtility.CutTextCustomer(item.getName().equals("") ? item.getUsername() : item.getName(), aq, holder.imgAvatar, holder.lblNoAvatar);
//            }

        } catch (NullPointerException e) {
        } catch (Exception e) {
        }

    }

//    private String sizeimage(final CircularImageView imageView) {
//        ViewTreeObserver vto = imageView.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
//                w = imageView.getMeasuredHeight();
//                h = imageView.getMeasuredWidth();
//                return true;
//            }
//        });
//        return w + "_" + h + "/";
//    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            contactInfoLista.clear();
            if (charText.length() == 0) {
                contactInfoLista.addAll(contactInfoListabbbb);
            } else {
                for (DanhBaInfo wp : contactInfoListabbbb) {
                    if (wp.getUsername().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        contactInfoLista.add(wp);
                        notifyDataSetChanged();
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return contactInfoLista.size();
    }
}
