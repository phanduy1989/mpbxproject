package com.viettel.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.mpbx.R;
import com.viettel.widgets.CustomTextView;


public class DanhBaViewHolder extends RecyclerView.ViewHolder {
    public TextView lblNoAvatar,lblNameDes,lblOnline;
    public CustomTextView lblName;
    public RelativeLayout layoutConten, layoutImage;
    public ImageView imgAvatar;
    public LinearLayout layoutText;

    public DanhBaViewHolder(View itemView) {
        super(itemView);
        lblNoAvatar = (TextView) itemView.findViewById(R.id.lblNoAvatar);

        lblName = (CustomTextView) itemView.findViewById(R.id.lblName);
        lblNameDes = (TextView) itemView.findViewById(R.id.lblNameDes);
        imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
        layoutConten = (RelativeLayout) itemView.findViewById(R.id.layoutConten);
        layoutText = (LinearLayout) itemView.findViewById(R.id.layoutText);
        layoutImage = (RelativeLayout) itemView.findViewById(R.id.layoutImage);
        lblOnline = (TextView) itemView.findViewById(R.id.lblOnline);
    }
}
