package com.viettel.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.mpbx.R;


public class TinNhanHolder extends RecyclerView.ViewHolder {
    public TextView lblName, lblNameDes, lblNoAvatar, lblTime;
    public ImageView imgAvatar;
    public CheckBox cbChon;


    public TinNhanHolder(View itemView) {
        super(itemView);
        lblName = (TextView) itemView.findViewById(R.id.lblName);
        lblNameDes = (TextView) itemView.findViewById(R.id.lblNameDes);
        lblNoAvatar = (TextView) itemView.findViewById(R.id.lblNoAvatar);
        lblTime = (TextView) itemView.findViewById(R.id.lblTime);
        imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);

    }
}
