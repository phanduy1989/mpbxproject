package com.viettel.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ContactObject;
import com.viettel.model.CustomerModel;
import com.viettel.model.request.contact.DeleteContactRequest;
import com.viettel.model.response.contact.GetContactInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.contact.ListContactActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

public class ListContactAdapter extends BaseSwipeAdapter implements MPBXFilter{

    private Context mContext;

    private ArrayList<ContactObject> listData;
    private ArrayList<ContactObject> listDataDisplay;

    public ListContactAdapter(Context mContext) {
        this.mContext = mContext;
    }
//    public ListCustomerAdapter(Context mContext, ArrayList<CustomerModel> list) {
//        this.mContext = mContext;
//        listData = list;
//    }

    public void setListData(ArrayList<ContactObject> listData) {
        if(this.listData == null) {
            this.listData = new ArrayList<>();
        } else {
            this.listData.clear();
        }
        this.listData.addAll(listData);
        if(this.listDataDisplay == null) {
            this.listDataDisplay = new ArrayList<>();
        } else {
            this.listDataDisplay.clear();
        }
        this.listDataDisplay.addAll(listData);

        notifyDatasetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, View convertView, ViewGroup parent) {

//        if(convertView != null) return convertView;

        View v = LayoutInflater.from(mContext).inflate(R.layout.listview_item, null);
        final SwipeLayout swipeLayout = (SwipeLayout)v.findViewById(getSwipeLayoutResourceId(position));

        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewWithTag("Bottom2"));

        final ContactObject contactObject = getItem(position);

//        swipeLayout.setTag(customerModel);


        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                contactObject.setOpen(true);
            }

            @Override
            public void onClose(SwipeLayout layout) {
                contactObject.setOpen(false);
            }
        });
//        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
//            @Override
//            public void onDoubleClick(SwipeLayout layout, boolean surface) {
////                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
//                if(surface) {
//                    swipeLayout.close(true);
//                }
//            }
//        });

        swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ContactObject customerModel1 = (ContactObject) swipeLayout.getTag();
                if(contactObject.isOpen()) {
                    swipeLayout.close(true);
                }
                else {
                    swipeLayout.close(true);
//                    ((ListContactActivity)mContext).goToDetail(contactObject.getContactId());
                }

            }
        });

        if(!contactObject.isOpen()) {

            swipeLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.close();
                }
            }, 100);

        }

        return v;
    }

    public void doDelete(final ContactObject customerModel,final int position) {
        ((ListContactActivity)mContext).showLoading();

        DeleteContactRequest deleteContactRequest = new DeleteContactRequest();
        deleteContactRequest.setData("" + customerModel.getContactId());

        ApiController.doPostRequest(mContext, deleteContactRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                ((ListContactActivity)mContext).closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetContactInfoResponse signInResponse = gson.fromJson(response, GetContactInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        removeObject(customerModel);

                        ((ListContactActivity) mContext).initData(position);

                        Toast.makeText(mContext, "Xóa danh bạ thành công!", Toast.LENGTH_SHORT).show();

                        if(listDataDisplay.isEmpty()) {
                            ((BABaseActivity) mContext).showError(mContext.getResources().getString(R.string.alertNoContactFound), false);
                        }
                    } else {
//                                ((ListContactActivity)mContext).showError(signInResponse, true);
                        Toast.makeText(mContext, "" + signInResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, ((ListContactActivity)mContext).getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                Toast.makeText(mContext, ((ListContactActivity)mContext).getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void removeObject(ContactObject contactObject) {

        if(listDataDisplay == null || listDataDisplay.isEmpty() || listData == null || listData.isEmpty()) return;

        int position = -1;

        for(int i = 0 , size = listDataDisplay.size(); i < size; i++) {
            ContactObject contactObject1 = listDataDisplay.get(i);
            if(contactObject1.getContactId() == contactObject.getContactId()) {
                position = i;
                break;
            }
        }

        if(position >= 0) {
            listDataDisplay.remove(position);
        }

        int originIdx = -1;

        for(int i = 0, size = listData.size(); i < size; i++) {
            ContactObject contactObject1 = listData.get(i);
            if(contactObject.getContactId() == contactObject1.getContactId()) {
                originIdx = i;
                break;
            }
        }

        if(originIdx >= 0) {
            listData.remove(originIdx);
        }

        if(!listDataDisplay.isEmpty()) {
            if(position < listDataDisplay.size()) {
                listDataDisplay.get(position).setOpen(false);
            }
        }

        notifyDataSetChanged();


    }

    @Override
    public void filter(String key) {
        if(listData == null || listData.isEmpty()) return;

        ((BABaseActivity) mContext).showContent();

        if(key == null || key.trim().isEmpty()) {

            listDataDisplay = new ArrayList<>();
            listDataDisplay.addAll(listData);
            notifyDataSetChanged();
            return;
        }
        key = StringUtility.convertVietnameseToAscii(key.trim()).toLowerCase();


        listDataDisplay = new ArrayList<>();

        for(ContactObject contactObject : listData) {
            String name = StringUtility.convertVietnameseToAscii(contactObject.getContactName());
            if((name != null && name.contains(key)) || (contactObject.getPhoneNumber().contains(key))) {
                listDataDisplay.add(contactObject);
            }
        }
        notifyDataSetChanged();
        notifyDataSetInvalidated();

        if(listDataDisplay.isEmpty()) {
            ((BABaseActivity) mContext).showError(mContext.getResources().getString(R.string.alertNoContactFound), false);
        }
    }

    class ViewHolder {
        TextView textName, textPhone, textCompany;
        ImageView avatar;
        View btnPhone, btnEdit, btnDelete, layoutFront;
        SwipeLayout swipeLayout;
    }

    @Override
    public void fillValues(final int position, View convertView) {
        ViewHolder viewHolder = null;
        if(convertView.getTag() != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            viewHolder = new ViewHolder();
            viewHolder.textName = (TextView)convertView.findViewById(R.id.textName);
            viewHolder.textPhone = (TextView)convertView.findViewById(R.id.textPhone);
            viewHolder.textCompany = (TextView)convertView.findViewById(R.id.textCompany);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            viewHolder.swipeLayout = (SwipeLayout) convertView.findViewById(getSwipeLayoutResourceId(position));

            viewHolder.btnPhone = convertView.findViewById(R.id.btnPhone);
            viewHolder.btnEdit = convertView.findViewById(R.id.edit);
            viewHolder.btnDelete = convertView.findViewById(R.id.delete);
            viewHolder.layoutFront = convertView.findViewById(R.id.layoutFront);
            convertView.setTag(viewHolder);
        }
        final ContactObject customerModel = getItem(position);
//        TextView textName = (TextView)convertView.findViewById(R.id.textName);
//        TextView textPhone = (TextView)convertView.findViewById(R.id.textPhone);
        if(customerModel != null) {
            viewHolder.textName .setText("" + customerModel.getContactName());
            if(customerModel.getPhoneNumber() == null || customerModel.getPhoneNumber().isEmpty()) {
                viewHolder.textPhone.setVisibility(View.GONE);
            } else {
                viewHolder.textPhone.setVisibility(View.VISIBLE);
                viewHolder.textPhone.setText("" + PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()));
            }
            viewHolder.textCompany.setVisibility(View.GONE);
//            if(customerModel.getPhoneNumber() == null || customerModel.getPhoneNumber().isEmpty()) {
//                viewHolder.textExtPhone.setVisibility(View.GONE);
//            } else {
//                viewHolder.textExtPhone.setVisibility(View.VISIBLE);
//                viewHolder.textExtPhone.setText("Số máy lẻ: " + customerModel.getPhoneNumber());
//            }

            if(customerModel.getFileName() != null) {
                Picasso.with(mContext)
                        .load(customerModel.getFileName()).placeholder(mContext
                        .getResources().getDrawable(R.drawable.avatar2))
                        .error(mContext.getResources().getDrawable(R.drawable.avatar2))
                        .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                        .into(viewHolder.avatar);
            }

            viewHolder.btnPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ListContactActivity)mContext).showCallLayout(customerModel);
                }
            });
            viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ListContactActivity)mContext).goToCreateCustomerAcitivity(customerModel);
                }
            });

            viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DialogUtility.showDialogConfirm(((ListContactActivity)mContext), mContext.getResources().getString(R.string.msgDeleteContact), new ConfirmListener() {

                        @Override
                        public void doCancel() {

                        }

                        @Override
                        public void doAccept() {
//                            swipeLayout.close(true);
                            doDelete(customerModel, position);
                        }
                    });

                }
            });
            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.layoutFront.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!customerModel.isOpen()) {
                        ((ListContactActivity)mContext).goToDetail(customerModel.getContactId());
                    } else {
//                        customerModel.setOpen(false);
                        getItem(position).setOpen(false);

                        finalViewHolder.swipeLayout.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finalViewHolder.swipeLayout.close();
                            }
                        }, 100);

                        notifyDataSetChanged();
                    }

                }
            });

        }

    }

    @Override
    public int getCount() {
        return listDataDisplay != null ? listDataDisplay.size() : 0;
    }

    @Override
    public ContactObject getItem(int position) {
        return listDataDisplay != null ? listDataDisplay.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
