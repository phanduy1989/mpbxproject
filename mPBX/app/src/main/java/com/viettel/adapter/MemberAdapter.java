package com.viettel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viettel.adapter.holder.MemberHolder;
import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationMember;
import com.viettel.model.CustomerModel;
import com.viettel.mpbx.R;
import com.viettel.utils.CircleTransform;
import com.viettel.utils.PhoneUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by duyuno on 7/11/17.
 */
public class MemberAdapter extends RecyclerView.Adapter  {
    public ArrayList<ConversationMember> listData;

    Activity activity;

    public MemberAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setData(Object list) {

        if(list == null) return;

        if(listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }

        listData.addAll((ArrayList<ConversationMember>)list);
        notifyDataSetChanged();
    }

    public void clearData() {
        if(listData != null) {
            listData.clear();
            notifyDataSetChanged();
        }
    }

    public ArrayList<ConversationMember> getData() {
        return listData;
    }

    public ArrayList<ConversationMember> getListMember() {
        ArrayList<ConversationMember> listResult = new ArrayList<>();
        for(ConversationMember conversationMember : listData) {
            if(conversationMember.getConferenceMemberId() > 0) {
                listResult.add(conversationMember);
            }
        }

        return listResult;
    }
    public ArrayList<ConversationMember> getListNonMember() {
        ArrayList<ConversationMember> listResult = new ArrayList<>();
        for(ConversationMember conversationMember : listData) {
            if(conversationMember.getConferenceMemberId() <= 0) {
                listResult.add(conversationMember);
            }
        }

        return listResult;
    }

//    public ArrayList<ContactModel> getListContact() {
//        if(listData == null || listData.isEmpty()) return null;
//
//        ArrayList<ContactModel> listContact = new ArrayList<>();
//
//        for(PBXModel pbxModel : listData) {
//            if(pbxModel instanceof ContactModel) {
//                listContact.add((ContactModel) pbxModel);
//            } else if(pbxModel instanceof CustomerModel) {
//                CustomerModel customerModel = (CustomerModel) pbxModel;
//                ContactModel contactModel = new ContactModel();
//                contactModel.setName(customerModel.getCustomerName());
//                contactModel.setPhone(customerModel.getPhoneNumber() != null ? customerModel.getPhoneNumber() : customerModel.getNumberOfExt());
//                listContact.add(contactModel);
//            } else {
//                ColleagueModel colleagueModel = (ColleagueModel) pbxModel;
//                ContactModel contactModel = new ContactModel();
//                contactModel.setName(colleagueModel.getColleagueName());
//                contactModel.setPhone(colleagueModel.getPhoneNumber() != null ? colleagueModel.getPhoneNumber() : colleagueModel.getNumberOfExt());
//                listContact.add(contactModel);
//            }
//        }
//        return listContact;
//    }


    public void addMember(ConversationMember pbxModel) {
        if(listData == null) {
            listData = new ArrayList<>();
        }

        listData.add(pbxModel);

        notifyDataSetChanged();
    }
    public void addMembers(ArrayList<ConversationMember> listModel) {

        if(listModel == null || listModel.isEmpty()) return;
        if(listData == null) {
            listData = new ArrayList<>();
            listData.addAll(listModel);
            notifyDataSetChanged();
            return;
        }

        if(listData.isEmpty()) {
            listData.addAll(listModel);
            notifyDataSetChanged();
            return;
        }

        HashMap<String, Integer> hash = new HashMap<>();
        for (ConversationMember conversationMember : listData) {
            hash.put(PhoneUtil.makeCorrectPhoneFormat(conversationMember.getPhoneNumber()), 1);
        }

        for(ConversationMember conversationMember : listModel) {
            if(!hash.containsKey(PhoneUtil.makeCorrectPhoneFormat(conversationMember.getPhoneNumber()))) {
                listData.add(conversationMember);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImageView imageView = new ImageView(activity);
        return new MemberHolder(imageView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MemberHolder memberHolder = (MemberHolder) holder;
        memberHolder.imageView.setImageResource(R.drawable.avatar);

        Object object = listData.get(position);
        String fileName = null;
        if(object instanceof ColleagueModel) {
            fileName = ((ColleagueModel) object).getFileName();
        } else if(object instanceof CustomerModel) {
            fileName = ((CustomerModel) object).getFileName();
        } else if(object instanceof ConversationMember) {
            fileName = ((ConversationMember) object).getFileName();
        }
        if(fileName != null) {
            Picasso.with(activity)
                    .load(fileName)
                    .placeholder(R.drawable.avatar)
                    .transform(new CircleTransform())
                    .into(memberHolder.imageView);
        }

        return;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listData != null ? listData.size() : 0;
    }

    class ViewHolder {
        public TextView name;
        public TextView phone;
    }
}
