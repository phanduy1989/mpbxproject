package com.viettel.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viettel.model.ConversationModel;
import com.viettel.mpbx.R;
import com.viettel.utils.DateUtil;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class ConversationModelAdapter extends BaseAdapter {
    public ArrayList<ConversationModel> listData;

    Activity activity;

    public ConversationModelAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setData(ArrayList<ConversationModel> list) {
        listData = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listData != null ? listData.size() : 0;
    }

    @Override
    public ConversationModel getItem(int position) {
        return listData != null ? listData.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        public TextView txtName, txtInfo, txtDate, txtTime;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_conversation, null);
            viewHolder = new ViewHolder();
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.txtInfo = (TextView) convertView.findViewById(R.id.txtInfo);
            viewHolder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            viewHolder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ConversationModel conversationModel = getItem(position);

        if (conversationModel != null) {
            viewHolder.txtName.setText(conversationModel.getConferenceName());
            viewHolder.txtInfo.setText("Thành viên: " + conversationModel.getTotalMember());
            viewHolder.txtDate.setText(DateUtil.convertOnlyDay(conversationModel.getStartDate()));
            viewHolder.txtTime.setText(DateUtil.convertOnlyTime(conversationModel.getStartDate()));
        }

        return convertView;
    }
}
