package com.viettel.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.mpbx.R;
import com.viettel.widgets.CustomTextView;


public class TaoNhomDanhBaViewHolder extends RecyclerView.ViewHolder {
    public TextView lblNoAvatar,lblBage,lblNameDes, lblOnline;
    public CustomTextView lblName;
    public RelativeLayout layoutBadger,layoutConten, layoutImage;
    public ImageView imgAvatar;
    public LinearLayout layoutText;
    public CheckBox cbChon;

    public TaoNhomDanhBaViewHolder(View itemView) {
        super(itemView);
        lblNoAvatar = (TextView) itemView.findViewById(R.id.lblNoAvatar);
        lblBage = (TextView) itemView.findViewById(R.id.lblBage);
        lblName = (CustomTextView) itemView.findViewById(R.id.lblName);
        lblNameDes = (TextView) itemView.findViewById(R.id.lblNameDes);
        layoutBadger = (RelativeLayout) itemView.findViewById(R.id.layoutBadger);
        imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
        layoutConten = (RelativeLayout) itemView.findViewById(R.id.layoutConten);
        layoutText = (LinearLayout) itemView.findViewById(R.id.layoutText);
        layoutImage = (RelativeLayout) itemView.findViewById(R.id.layoutImage);
        cbChon = (CheckBox) itemView.findViewById(R.id.cbChon);
        lblOnline = (TextView) itemView.findViewById(R.id.lblOnline);
    }
}
