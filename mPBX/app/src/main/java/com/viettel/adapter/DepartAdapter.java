package com.viettel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.model.ColleagueGroup;
import com.viettel.model.CustomerModel;
import com.viettel.model.DepartmentModel;
import com.viettel.mpbx.R;
import com.viettel.utils.StringUtil;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 8/17/17.
 */
public class DepartAdapter extends BaseAdapter implements MPBXFilter{

    ArrayList<DepartmentModel> listData;
    ArrayList<DepartmentModel> listDataDisplay;

    BABaseActivity activity;

    public DepartAdapter(BABaseActivity activity) {
        this.activity = activity;
    }

    public void setData(ArrayList<DepartmentModel> list) {
        if(listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }

        listData.addAll(list);

        if(listDataDisplay == null) {
            listDataDisplay = new ArrayList<>();
        } else {
            listDataDisplay.clear();
        }

        listDataDisplay.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataDisplay != null ? listDataDisplay.size() : 0;
    }

    @Override
    public DepartmentModel getItem(int position) {
        return listDataDisplay != null ? listDataDisplay.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        DepartmentModel colleagueGroup = getItem(position);

        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_colleague_header, null);
        }

        TextView menuTxt = (TextView) view.findViewById(R.id.menuTxt);
//        int count = colleagueGroup.getListMember() != null ? colleagueGroup.getListMember().size() : 0;
//        menuTxt.setText(colleagueGroup.getDeptName() + " (" + count + ")");
        menuTxt.setText(colleagueGroup.getDeptName());
        ImageView btnExpand = (ImageView) view.findViewById(R.id.btnExpand);
        btnExpand.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void filter(String key) {
        if(listData == null || listData.isEmpty()) return;

        ((BABaseActivity) activity).showContent();

        if(key == null || key.trim().isEmpty()) {

            listDataDisplay = new ArrayList<>();
            listDataDisplay.addAll(listData);
            notifyDataSetChanged();
            return;
        }

        key = StringUtility.convertVietnameseToAscii(key).toLowerCase().trim();


        listDataDisplay = new ArrayList<>();

        for(DepartmentModel contactObject : listData) {
            String name = StringUtility.convertVietnameseToAscii(contactObject.getDeptName());
            if(name != null && name.contains(key)) {
                listDataDisplay.add(contactObject);
            }
        }

        notifyDataSetChanged();

//        if(listDataDisplay.isEmpty()) {
//            ((BABaseActivity) mContext).showError(mContext.getResources().getString(R.string.alertNoCustomerFound), false);
//        }
    }
}
