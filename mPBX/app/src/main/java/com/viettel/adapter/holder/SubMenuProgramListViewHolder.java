package com.viettel.adapter.holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationModel;
import com.viettel.mpbx.R;
import com.viettel.utils.DataManager;
import com.viettel.utils.DateUtil;
import com.viettel.view.activity.business.conversation.ListConversationActivity;

import java.util.ArrayList;

/**
 * Created by mc.kim on 10/3/2016.
 */
public class SubMenuProgramListViewHolder extends RecyclerView.ViewHolder {
    private TextView title = null;
    private LayoutInflater mInflater = null;

    //    private ConversationModelAdapter conversationModelAdapter = null;
    private View currentItem;
    private Activity activity;

    private LinearLayout listConversation;

    private ConversationGroup conversationGroup;

    public void setConversationGroup(ConversationGroup conversationGroup) {
        this.conversationGroup = conversationGroup;

        updateData(conversationGroup.getListChild());

//        conversationModelAdapter.setData(DataManager.genListConversationModel());
//        title.setText(conversationGroup.getName() + " (" + conversationModelAdapter.getCount() + ")");
    }

    public SubMenuProgramListViewHolder(Activity activity, View itemView, LayoutInflater inflater, View.OnClickListener callback) {
        super(itemView);
        this.activity = activity;
        mInflater = inflater;
        title = (TextView) itemView.findViewById(R.id.title);
        listConversation = (LinearLayout) itemView.findViewById(R.id.listConversation);

        currentItem = itemView;


//        FullitemListView listView = (FullitemListView) itemView.findViewById(R.id.listView);
//        LinearLayoutManager manager = new LinearLayoutManager(inflater.getContext());
//        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        conversationModelAdapter = new ConversationModelAdapter(activity);
//
//
//        listView.setAdapter(conversationModelAdapter);
    }

    public void updateData(ArrayList<ConversationModel> listData) {
        listConversation.removeAllViews();
        if (listData == null || listData.isEmpty()) {
            return;
        }
        title.setText(conversationGroup.getName() + " (" + listData.size() + ")");

        for (int i = 0, size = listData.size(); i < size; i++) {
            ConversationModel conversationModel = listData.get(i);
            RelativeLayout convertView = (RelativeLayout) mInflater.inflate(R.layout.item_conversation, null);

            TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
            TextView txtInfo = (TextView) convertView.findViewById(R.id.txtInfo);
            TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            TextView txtTime = (TextView) convertView.findViewById(R.id.txtTime);

            txtName.setText(conversationModel.getConferenceName());
            txtInfo.setText("Thành viên: " + conversationModel.getTotalMember());
            switch (conversationModel.getConversationType()) {
                case ConversationGroup.ON_GOING:
                    txtDate.setVisibility(View.GONE);
                    txtTime.setText(DateUtil.convertOnlyTime(conversationModel.getStartDate()));
                    break;
                case ConversationGroup.UP_COMMING:
                    txtDate.setText(DateUtil.convertOnlyDay(conversationModel.getStartDate()));
                    txtTime.setText(DateUtil.convertOnlyTime(conversationModel.getStartDate()));
                    break;
                case ConversationGroup.FINISHED:
                    txtDate.setText(DateUtil.convertOnlyDay(conversationModel.getStartDate()));
                    txtTime.setText(DateUtil.convertOnlyTime(conversationModel.getStartDate()) + "-" + DateUtil.convertOnlyTime(conversationModel.getEndDate()));
                    break;
            }



            listConversation.addView(convertView);

            convertView.setOnClickListener(onClickListener);
            convertView.setTag(conversationModel);
            View view = new View(activity);
            view.setLayoutParams(new LinearLayout.LayoutParams(1, (int) (activity.getResources().getDimension(R.dimen.margin_item))));
            listConversation.addView(view);
        }
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ConversationModel conversationModel = (ConversationModel) v.getTag();

            ((ListConversationActivity) activity).goToDetail(conversationModel);
        }
    };

}
