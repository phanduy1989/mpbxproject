package com.viettel.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ConversationMember;
import com.viettel.model.request.conference.RemoveMemberRequest;
import com.viettel.model.request.conference.MuteMemberRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class ConversationInviteMemberAdapter extends BaseAdapter {
    public ArrayList<ConversationMember> listData;

    BABaseActivity activity;
    public int conversationType;
    public int conferenceId;

    public ConversationInviteMemberAdapter(BABaseActivity activity, int conversationType, int conferenceId) {
        this.activity = activity;
        this.conversationType = conversationType;
        this.conferenceId = conferenceId;
    }

    public void setData(ArrayList<ConversationMember> list) {
        listData = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if(listData != null) {
            listData.clear();
        }

        notifyDataSetChanged();
    }

    public ArrayList<ConversationMember> getListData() {
        if(listData == null || listData.isEmpty()) return null;
        return listData;
    }

    public ArrayList<ConversationMember> getListPicked() {
        if(listData == null || listData.isEmpty()) return null;

        ArrayList<ConversationMember> listResult = null;

        for(ConversationMember conversationMember : listData) {
            if(conversationMember.isChosed()) {
                if(listResult == null) {
                    listResult = new ArrayList<>();
                }
                listResult.add(conversationMember);
            }
        }

        return listResult;
    }

    public interface OnClickMemberAction {
        public void onClickMute(ConversationMember conversationMember);
        public void onClickRemove(ConversationMember conversationMember);
    }

    @Override
    public int getCount() {
        return listData != null ? listData.size() : 0;
    }

    @Override
    public ConversationMember getItem(int position) {
        return listData != null ? listData.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        public TextView txtName;
        public TextView txtPhone;
        public LinearLayout statusRect;

        public ImageView avatar;
        public ImageView iconPick;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_conversation_invite_member, null);
            viewHolder = new ViewHolder();
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
            viewHolder.statusRect = (LinearLayout) convertView.findViewById(R.id.statusRect);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            viewHolder.iconPick = (ImageView) convertView.findViewById(R.id.iconPick);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final ConversationMember contactModel = getItem(position);

        if (contactModel != null) {
            viewHolder.txtName.setText(contactModel.getMemberName());

            int status = contactModel.getStatus();

            if(status == ConversationMember.STATUS_ONLINE) {
                viewHolder.statusRect.setBackgroundResource(R.drawable.drawable_green_rect);
            } else {
                viewHolder.statusRect.setBackgroundResource(R.drawable.drawable_red_rect);
            }

            if(contactModel.isChosed()) {
                viewHolder.iconPick.setImageResource(R.drawable.check_invite);
            } else {
                viewHolder.iconPick.setImageResource(R.drawable.nocheck_invite);
            }

            viewHolder.txtPhone.setText(PhoneUtil.makeCorrectPhoneFormat(contactModel.getPhoneNumber()));
            if(contactModel.getFileName() != null) {
                Picasso.with(activity)
                        .load(contactModel.getFileName())
                        .placeholder(R.drawable.avatar2)
                        .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                        .into(viewHolder.avatar);
            }
        }

        return convertView;
    }

    public void doChangeMute(final ConversationMember conversationMember) {
        activity.showLoading();

        MuteMemberRequest muteMemberRequest = new MuteMemberRequest();

        muteMemberRequest.setData(conversationMember.getConferenceMemberId(), conversationMember.getChangeCallStatus(), conferenceId);

        ApiController.doPostRequest(activity, muteMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        conversationMember.changeMute();
                        notifyDataSetChanged();
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }
    public void doRemoveMember(final ConversationMember conversationMember,final int position) {
        activity.showLoading();

        RemoveMemberRequest removeMemberRequest = new RemoveMemberRequest();

        removeMemberRequest.setData(conversationMember.getConferenceMemberId(), conferenceId);

        ApiController.doPostRequest(activity, removeMemberRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                activity.closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        listData.remove(position);
                        notifyDataSetChanged();
                    } else {
                        activity.showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                activity.showError(activity.getResources().getString(R.string.text_no_network), true);
            }
        });
    }
}
