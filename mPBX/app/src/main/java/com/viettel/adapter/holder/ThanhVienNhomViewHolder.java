package com.viettel.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.mpbx.R;


public class ThanhVienNhomViewHolder extends RecyclerView.ViewHolder {
    public TextView lblNoAvatar,lblName,lblOnline;
    public ImageView imgAvatar;
//    public RelativeLayout layoutContent;
    public RelativeLayout layoutImage,layoutConten;
    public ImageButton btnDelete;
    public ImageView imageKeyAdmin;

    public ThanhVienNhomViewHolder(View itemView) {
        super(itemView);
        lblNoAvatar = (TextView) itemView.findViewById(R.id.lblNoAvatar);
        lblName = (TextView) itemView.findViewById(R.id.lblName);
        imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
        layoutConten = (RelativeLayout) itemView.findViewById(R.id.layoutConten);
        layoutImage = (RelativeLayout) itemView.findViewById(R.id.layoutImage);
        btnDelete = (ImageButton) itemView.findViewById(R.id.btnDelete);
        imageKeyAdmin = (ImageView) itemView.findViewById(R.id.imageKeyAdmin);
        lblOnline = (TextView) itemView.findViewById(R.id.lblOnline);
    }
}
