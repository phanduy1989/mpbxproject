package com.viettel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.viettel.adapter.holder.DanhBaViewHolder;
import com.viettel.model.DanhBaInfo;
import com.viettel.mpbx.R;

import java.util.ArrayList;
import java.util.Locale;


public class DanhBaAdapter extends RecyclerView.Adapter<DanhBaViewHolder> {

    private ArrayList<DanhBaInfo> contactInfoLista;
    private ArrayList<DanhBaInfo> contactInfoListabbbb;
    private Context context;
    private int w = 84, h = 84;
    private AQuery aq;


    public DanhBaAdapter(Context context, ArrayList<DanhBaInfo> contactInfoList) {
        this.context = context;
        this.contactInfoLista = contactInfoList;
        this.contactInfoListabbbb = new ArrayList<>();
        this.contactInfoListabbbb.addAll(contactInfoLista);
        aq = new AQuery(context);
    }

    @Override
    public DanhBaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_danhba, parent, false);
        aq = aq.recycle(itemView);
        return new DanhBaViewHolder(itemView);
    }

    private int layvitriBatDau(String chuoi, String chuoitimkiem) {
        int vitri = 0;
        vitri = chuoi.indexOf(chuoitimkiem);
        return vitri;
    }

    @Override
    public void onBindViewHolder(final DanhBaViewHolder holder, int position) {
        final DanhBaInfo contact = contactInfoLista.get(position);

        try {

//            holder.lblName.setTextColor(context.getResources().getColor(R.color.lblname));
            holder.lblNoAvatar.setTextColor(context.getResources().getColor(R.color.white));
            holder.lblNameDes.setText(contact.getPhone());
            holder.lblName.setText(contact.getName().equals("") ? contact.getUsername() : contact.getName());
//            holder.lblNoAvatar.setVisibility(View.VISIBLE);
//            aq.id(imgAvatar).image(R.drawable.bgavatar);
//            String name = contact.getUsername().substring(0, 1);
//            holder.lblNoAvatar.setText(name.toUpperCase());
            if (contact.getOnline().equals("1")) {
                holder.lblOnline.setVisibility(View.VISIBLE);
            } else {
                holder.lblOnline.setVisibility(View.GONE);
            }
                aq.id(holder.imgAvatar).image(contact.getAvatar().contains("http") ? contact.getAvatar() : "http://125.212.225.58:3003" + contact.getAvatar(),
                        true, true, w, R.drawable.avatar2, null, 0, 0);
                holder.lblNoAvatar.setVisibility(View.GONE);


        } catch (NullPointerException e) {
        } catch (Exception e) {
        }

    }

//    private String sizeimage(final CircularImageView imageView) {
//        ViewTreeObserver vto = imageView.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
//                w = imageView.getMeasuredHeight();
//                h = imageView.getMeasuredWidth();
//                return true;
//            }
//        });
//        return w + "_" + h + "/";
//    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            contactInfoLista.clear();
            if (charText.length() == 0) {
                contactInfoLista.addAll(contactInfoListabbbb);
            } else {
                for (DanhBaInfo wp : contactInfoListabbbb) {
                    if (wp.getUsername().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        contactInfoLista.add(wp);
                        notifyDataSetChanged();
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return contactInfoLista.size();
    }
}
