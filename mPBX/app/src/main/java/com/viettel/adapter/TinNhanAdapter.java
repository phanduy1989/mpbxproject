package com.viettel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.viettel.adapter.holder.TinNhanHolder;
import com.viettel.model.TinNhanInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.StringUtility;

import java.util.ArrayList;
import java.util.Locale;



public class TinNhanAdapter extends RecyclerView.Adapter<TinNhanHolder> {

    private ArrayList<TinNhanInfo> contactInfoLista;
    private ArrayList<TinNhanInfo> contactInfoListabbbb;
    private Context context;
    private int w = 84, h = 84;
    private AQuery aq;
    private String user_id = "";


    public TinNhanAdapter(Context context, ArrayList<TinNhanInfo> contactInfoList, String user_id) {
        this.context = context;
        this.contactInfoLista = contactInfoList;
        this.user_id = user_id;
        this.contactInfoListabbbb = new ArrayList<>();
        this.contactInfoListabbbb.addAll(contactInfoLista);
        aq = new AQuery(context);
    }

    @Override
    public TinNhanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tinnhan, parent, false);
        aq = aq.recycle(itemView);
        return new TinNhanHolder(itemView);
    }

    private int layvitriBatDau(String chuoi, String chuoitimkiem) {
        int vitri = 0;
        vitri = chuoi.indexOf(chuoitimkiem);
        return vitri;
    }

    @Override
    public void onBindViewHolder(final TinNhanHolder holder, int position) {
        final TinNhanInfo item = contactInfoLista.get(position);

        try {

            holder.lblName.setTextColor(context.getResources().getColor(R.color.lblname));
            holder.lblNoAvatar.setTextColor(context.getResources().getColor(R.color.white));
            String content = "";
            if (item.getLast_message() != null && !item.getLast_message().trim().equals("")) {
                content = (item.getSenderChatInfo().getId() != null && item.getSenderChatInfo().equals(user_id)) ? ( "Bạn: " + (null != item.getLast_message() ? item.getLast_message() : "")) : (item.getSenderChatInfo().getName() + ": " + (null != item.getLast_message() ? item.getLast_message() : ""));
            } else {
                content =  "";
            }
            String time = "";

            if (!item.getLast_time_send_message().equals("")) {
                java.util.Date date = new java.util.Date(Long.parseLong(StringUtility.getDateTimeStamp(item.getLast_time_send_message())) * 1000);

                time = StringUtility.getLastUpdateInfo(context, date).replace("-", "");
            } else {
                time = StringUtility.getDateTimeStamp(item.getLast_time_send_message());
            }

            if (item.getUnread_message() != null && !item.getUnread_message().equals("0") && !item.getUnread_message().equals("")) {
                holder.lblName.setTextColor(context.getResources().getColor(R.color.lblname));
                holder.lblTime.setTextColor(context.getResources().getColor(R.color.lblname));
                holder.lblNameDes.setTextColor(context.getResources().getColor(R.color.lblname));
            } else {
                holder.lblName.setTextColor(context.getResources().getColor(R.color.lblthich));
                holder.lblTime.setTextColor(context.getResources().getColor(R.color.lblthich));
                holder.lblNameDes.setTextColor(context.getResources().getColor(R.color.lblthich));
            }
            holder.lblNameDes.setText(content);
            holder.lblName.setText(item.getName());
            holder.lblTime.setText(time);
//            holder.lblNoAvatar.setVisibility(View.VISIBLE);
//            aq.id(imgAvatar).image(R.drawable.bgavatar);
//            String name = item.getName().substring(0, 1);
//            holder.lblNoAvatar.setText(name.toUpperCase());
        if (item.getSenderChatInfo().getAvatar()!= null && "".equals(item.getSenderChatInfo().getAvatar()) )
        {
            aq.id(holder.imgAvatar).image("http".contains(item.getSenderChatInfo().getAvatar()) ? item.getSenderChatInfo().getAvatar() : "http://125.212.225.58:3003" + item.getSenderChatInfo().getAvatar(),
                    true, true, w, R.drawable.avatar2, null, 0, 0);
        }else
        {
            aq.id(holder.imgAvatar).backgroundColor(R.drawable.avatar2);
        }

                holder.lblNoAvatar.setVisibility(View.GONE);



        } catch (NullPointerException e) {
            Log.e("", "TINNHANADAPTER :  " + e.toString());
        } catch (Exception e) {
            Log.e("", "TINNHANADAPTER :  " + e.toString());
        }

    }

//    private String sizeimage(final CircularImageView imageView) {
//        ViewTreeObserver vto = imageView.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
//                w = imageView.getMeasuredHeight();
//                h = imageView.getMeasuredWidth();
//                return true;
//            }
//        });
//        return w + "_" + h + "/";
//    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            contactInfoLista.clear();
            if (charText.length() == 0) {
                contactInfoLista.addAll(contactInfoListabbbb);
            } else {
                for (TinNhanInfo wp : contactInfoListabbbb) {
                    if (wp.getName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        contactInfoLista.add(wp);
                        notifyDataSetChanged();
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return contactInfoLista.size();
    }
}
