package com.viettel.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viettel.adapter.holder.SubMenuProgramListViewHolder;
import com.viettel.model.ConversationGroup;
import com.viettel.mpbx.R;

import java.util.ArrayList;

/**
 * Created by mc.kim on 9/30/2016.
 */
public class ListConferenceAdapter extends RecyclerView.Adapter {
    private final int TYPE_SPACE = 0;
    private final int TYPE_PROGRAM = 1;
    private final int TYPE_SEARCH = 2;
    private ArrayList<ConversationGroup> mGroupRes = null;
    private FragmentManager fm = null;
    private RecyclerView mParentsView = null;
    private View.OnClickListener mCallback = null;
    private Activity activity;

    public ListConferenceAdapter(Activity activity, FragmentManager fm, RecyclerView parentsView, View.OnClickListener callback) {
        this.fm = fm;
        this.activity = activity;
        this.mParentsView = parentsView;
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        if (viewType == TYPE_PROGRAM) {
//            View itemView = inflater
//                    .inflate(R.layout.item_group_conversation, parent, false);
//            return new SubMenuProgramListViewHolder(activity, itemView, inflater, mCallback);
//        }
        View itemView = inflater
                .inflate(R.layout.item_group_conversation, parent, false);
        return new SubMenuProgramListViewHolder(activity, itemView, inflater, mCallback);

    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_PROGRAM;
    }

    public void setMenuList(ArrayList<ConversationGroup> menuList) {
        this.mGroupRes = menuList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SubMenuProgramListViewHolder subMenuProgramViewHolder = (SubMenuProgramListViewHolder) holder;
        subMenuProgramViewHolder.setConversationGroup(mGroupRes.get(position));
    }

    @Override
    public int getItemCount() {
        if (mGroupRes == null) {
            return 0;
        } else {
            return mGroupRes.size();
        }
    }
}
