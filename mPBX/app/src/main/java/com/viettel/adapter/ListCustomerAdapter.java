package com.viettel.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ContactObject;
import com.viettel.model.CustomerModel;
import com.viettel.model.PBXModel;
import com.viettel.model.request.customer.DeleteCustomerRequest;
import com.viettel.model.response.contact.GetContactInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.contact.ListContactActivity;
import com.viettel.view.activity.business.customer.ListCustomerActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

public class ListCustomerAdapter extends BaseSwipeAdapter implements MPBXFilter{

    private Context mContext;

    private ArrayList<CustomerModel> listData;
    private ArrayList<CustomerModel> listDataDisplay;

    public ListCustomerAdapter(Context mContext) {
        this.mContext = mContext;
    }
//    public ListCustomerAdapter(Context mContext, ArrayList<CustomerModel> list) {
//        this.mContext = mContext;
//        listData = list;
//    }

    public void setListData(ArrayList<CustomerModel> listData) {

        PBXModel.formatPhoneNumberCustomer(listData);

        if(this.listData == null) {
            this.listData = new ArrayList<>();
        } else {
            this.listData.clear();
        }
        this.listData.addAll(listData);
        if(this.listDataDisplay == null) {
            this.listDataDisplay = new ArrayList<>();
        } else {
            this.listDataDisplay.clear();
        }
        this.listDataDisplay.addAll(listData);
        notifyDatasetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listview_item, null);
        final SwipeLayout swipeLayout = (SwipeLayout)v.findViewById(getSwipeLayoutResourceId(position));

        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewWithTag("Bottom2"));

        final CustomerModel customerModel = getItem(position);

//        swipeLayout.setTag(customerModel);


        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                customerModel.setOpen(true);
            }

            @Override
            public void onClose(SwipeLayout layout) {
                customerModel.setOpen(false);
            }
        });

        swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                CustomerModel customerModel1 = (CustomerModel) swipeLayout.getTag();

                if(customerModel.isOpen()) {
                    swipeLayout.close(true);
                } else {
                    swipeLayout.close(true);
//                    ((ListCustomerActivity)mContext).goToDetail(customerModel1.getCustomerId());
                }

            }
        });

        if(!customerModel.isOpen()) {
            swipeLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.close();
                }
            }, 100);
        }



        return v;
    }

    public void doDelete(final CustomerModel customerModel, final int position) {
        ((ListCustomerActivity)mContext).showLoading();

        DeleteCustomerRequest deleteContactRequest = new DeleteCustomerRequest();
        deleteContactRequest.setData(customerModel.getCustomerId());

        ApiController.doPostRequest(mContext, deleteContactRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                ((ListCustomerActivity)mContext).closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetContactInfoResponse signInResponse = gson.fromJson(response, GetContactInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        removeObject(customerModel);

                        ((ListCustomerActivity) mContext).initData(position);
                        Toast.makeText(mContext, "Xóa khách hàng thành công!", Toast.LENGTH_SHORT).show();
//
//                        if(listData.isEmpty()) {
//                            ((BABaseActivity) mContext).showError(mContext.getResources().getString(R.string.alertNoCustomerFound), false);
//                        }
                    } else {
                        Toast.makeText(mContext, "" + signInResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void removeObject(CustomerModel customerModel) {

        if(listDataDisplay == null || listDataDisplay.isEmpty() || listData == null || listData.isEmpty()) return;

        int position = -1;

        for(int i = 0 , size = listDataDisplay.size(); i < size; i++) {
            CustomerModel customerModel1 = listDataDisplay.get(i);
            if(customerModel1.getCustomerId() == customerModel.getCustomerId()) {
                position = i;
                break;
            }
        }

        if(position >= 0) {
            listDataDisplay.remove(position);
        }

        int originIdx = -1;

        for(int i = 0, size = listData.size(); i < size; i++) {
            CustomerModel customerModel1 = listData.get(i);
            if(customerModel1.getCustomerId() == customerModel.getCustomerId()) {
                originIdx = i;
                break;
            }
        }

        if(originIdx >= 0) {
            listData.remove(originIdx);
        }

        if(!listDataDisplay.isEmpty()) {
            if(position < listDataDisplay.size()) {
                listDataDisplay.get(position).setOpen(false);
            }
        }

        notifyDataSetChanged();


    }

    @Override
    public void filter(String key) {
        if(listData == null || listData.isEmpty()) return;

        ((BABaseActivity) mContext).showContent();

        if(key == null || key.trim().isEmpty()) {

            listDataDisplay = new ArrayList<>();
            listDataDisplay.addAll(listData);
            notifyDataSetChanged();
            return;
        }

        key = StringUtility.convertVietnameseToAscii(key).toLowerCase().trim();

        listDataDisplay = new ArrayList<>();

        for(CustomerModel contactObject : listData) {
            String name = StringUtility.convertVietnameseToAscii(contactObject.getCustomerName());
            String phoneNumber = PhoneUtil.makeCorrectPhoneFormat(contactObject.getPhoneNumber());
            String phoneNumber2 = PhoneUtil.makeCorrectPhoneFormat(contactObject.getPhoneNumber2());
            String phoneNumber3 = PhoneUtil.makeCorrectPhoneFormat(contactObject.getPhoneNumber3());
            String companyName = StringUtility.convertVietnameseToAscii(contactObject.getCompanyName());
            if((name != null && name.contains(key))
                    || (phoneNumber != null && phoneNumber.contains(key))
                    || (phoneNumber2 != null && phoneNumber2.contains(key))
                    || (phoneNumber3 != null && phoneNumber3.contains(key))
                    || (companyName != null && companyName.contains(key))
                    ) {
                listDataDisplay.add(contactObject);
            }
        }
//
//        for(CustomerModel contactObject : listData) {
//            if(contactObject.getPhoneNumber().contains(key)) {
//                listDataDisplay.add(contactObject);
//            }
//        }

        notifyDataSetChanged();
        notifyDataSetInvalidated();

        if(listDataDisplay.isEmpty()) {
            ((BABaseActivity) mContext).showError(mContext.getResources().getString(R.string.alertNoCustomerFound), false);
        }
    }

    class ViewHolder {
        ImageView avatar;
        TextView textName, textPhone, textCompany;
        View btnPhone, btnEdit, btnDelete, layoutFront;
        SwipeLayout swipeLayout;
    }

    @Override
    public void fillValues(final int position, View convertView) {
        ViewHolder viewHolder = null;
        if(convertView.getTag() != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            viewHolder = new ViewHolder();
            viewHolder.textName = (TextView)convertView.findViewById(R.id.textName);
            viewHolder.textPhone = (TextView)convertView.findViewById(R.id.textPhone);
            viewHolder.textCompany = (TextView)convertView.findViewById(R.id.textCompany);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            viewHolder.btnPhone = convertView.findViewById(R.id.btnPhone);
            viewHolder.btnEdit = convertView.findViewById(R.id.edit);
            viewHolder.btnDelete = convertView.findViewById(R.id.delete);
            viewHolder.layoutFront = convertView.findViewById(R.id.layoutFront);
            viewHolder.swipeLayout = (SwipeLayout) convertView.findViewById(getSwipeLayoutResourceId(position));
//            viewHolder.swipe = convertView.findViewById(R.id.swipe);
            convertView.setTag(viewHolder);
        }
        final CustomerModel customerModel = getItem(position);
//        TextView textName = (TextView)convertView.findViewById(R.id.textName);
//        TextView textPhone = (TextView)convertView.findViewById(R.id.textPhone);
        if(customerModel != null) {
            viewHolder.textName .setText("" + customerModel.getCustomerName());
            if(customerModel.getPhoneNumber() == null || customerModel.getPhoneNumber().isEmpty()) {
                viewHolder.textPhone.setVisibility(View.GONE);
            } else {
                viewHolder.textPhone.setVisibility(View.VISIBLE);
                // viewHolder.textPhone.setText("Số điện thoại: " + customerModel.getPhoneNumber());
                viewHolder.textPhone.setText(customerModel.getPhoneNumber());
            }

            if(customerModel.getCompanyName() == null || customerModel.getCompanyName().isEmpty()) {
                viewHolder.textCompany.setVisibility(View.GONE);
            } else {
                viewHolder.textCompany.setVisibility(View.VISIBLE);
                viewHolder.textCompany.setText("" + customerModel.getCompanyName());
            }

            if(customerModel.getFileName() != null) {
                Picasso.with(mContext)
                        .load(customerModel.getFileName()).placeholder(mContext
                        .getResources().getDrawable(R.drawable.avatar2))
                        .error(mContext.getResources().getDrawable(R.drawable.avatar2))
                        .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                        .into(viewHolder.avatar);
            }

            viewHolder.btnPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ListCustomerActivity)mContext).showCallLayout(customerModel);
                }
            });
            viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ListCustomerActivity)mContext).goToCreateCustomerAcitivity(customerModel);
                }
            });

            viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtility.showDialogConfirm(((ListCustomerActivity)mContext), mContext.getResources().getString(R.string.msgDeleteCustomer), new ConfirmListener() {

                        @Override
                        public void doCancel() {

                        }

                        @Override
                        public void doAccept() {
//                            swipeLayout.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    swipeLayout.close();
//                                    doDelete(customerModel, position);
//                                }
//                            }, 100);

                            doDelete(customerModel, position);


                        }
                    });
                }
            });

            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.layoutFront.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!customerModel.isOpen()) {
                        ((ListCustomerActivity)mContext).goToDetail(customerModel.getCustomerId());
                    } else {
                        getItem(position).setOpen(false);

                        finalViewHolder.swipeLayout.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finalViewHolder.swipeLayout.close();
                            }
                        }, 100);

                        notifyDataSetChanged();
                    }

                }
            });

        }

    }

    @Override
    public int getCount() {
        return listDataDisplay != null ? listDataDisplay.size() : 0;
    }

    @Override
    public CustomerModel getItem(int position) {
        return listDataDisplay != null ? listDataDisplay.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
