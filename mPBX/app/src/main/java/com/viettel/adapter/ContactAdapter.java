package com.viettel.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.model.ContactObject;
import com.viettel.mpbx.R;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class ContactAdapter extends BaseAdapter implements MPBXFilter {
    public ArrayList<ContactObject> listData;
    public ArrayList<ContactObject> listDataDisplay;

    private boolean isSelectAll;
    private boolean isMultiselect;

    Activity activity;

    public ContactAdapter(Activity activity, boolean isMultiselect) {
        this.activity = activity;
        this.isMultiselect = isMultiselect;
    }

    public void setData(ArrayList<ContactObject> list) {
        if (listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }
        if (listDataDisplay == null) {
            listDataDisplay = new ArrayList<>();
        } else {
            listDataDisplay.clear();
        }
        listData.addAll(list);
        listDataDisplay.addAll(list);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        if(listDataDisplay == null) return 0;
        int i = 0;
        for (ContactObject contactModel : listDataDisplay) {
            if (contactModel.isChosed()) {
                i++;
            }
        }

        return i;
    }

    public void setSelectAll(boolean isSelectAll) {
        this.isSelectAll = isSelectAll;

        for (ContactObject contactModel : listData) {
            contactModel.setChosed(isSelectAll);
        }
        for (ContactObject contactModel : listDataDisplay) {
            contactModel.setChosed(isSelectAll);
        }

        notifyDataSetChanged();
    }

    public void choseItem(int position) {
        if (!isMultiselect) {
            for (int i = 0, size = listDataDisplay.size(); i < size; i++) {
                ContactObject contactModel = listDataDisplay.get(i);


                if (i == position) {
                    contactModel.setChosed(true);
                } else {
                    contactModel.setChosed(false);
                }
            }
        } else {
            ContactObject contactModel = getItem(position);
            contactModel.changeChosed();
        }

        notifyDataSetChanged();
    }

    public ContactObject getPickedItem() {
        for (ContactObject contactModel : listDataDisplay) {
            if (contactModel.isChosed()) {
                return contactModel;
            }
        }

        return null;
    }

    public ArrayList<ContactObject> getListPicked() {
        if (listData == null || listData.isEmpty()) return null;

        ArrayList<ContactObject> listResult = null;

        for (ContactObject contactModel : listData) {
            if (contactModel.isChosed()) {
                if (listResult == null) {
                    listResult = new ArrayList<>();
                }
                listResult.add(contactModel);
            }
        }

        return listResult;
    }

    @Override
    public int getCount() {
        return listDataDisplay != null ? listDataDisplay.size() : 0;
    }

    @Override
    public ContactObject getItem(int position) {
        return listDataDisplay != null ? listDataDisplay.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void filter(String key) {
        if (listData == null || listData.isEmpty()) return;

        ((BABaseActivity) activity).showContent();

        if (key == null || key.isEmpty()) {

            listDataDisplay = new ArrayList<>();
            listDataDisplay.addAll(listData);
            notifyDataSetChanged();
            return;
        }
        key = StringUtility.convertVietnameseToAscii(key).toLowerCase().trim();


        listDataDisplay = new ArrayList<>();

        for (ContactObject contactObject : listData) {
            String name = StringUtility.convertVietnameseToAscii(contactObject.getContactName());
            if ((name != null && name.contains(key)) || (contactObject.getPhoneNumber().contains(key))) {
                listDataDisplay.add(contactObject);
            }
        }
        notifyDataSetChanged();

//        if (listDataDisplay.isEmpty()) {
//            ((BABaseActivity) activity).showError(activity.getResources().getString(R.string.alertNoContactFound), false);
//        }
    }

    class ViewHolder {
        public TextView name;
        public TextView phone;
        public ImageView iconPick;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.item_contact, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.phone = (TextView) convertView.findViewById(R.id.textPhone);
            viewHolder.iconPick = (ImageView) convertView.findViewById(R.id.iconPick);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ContactObject contactModel = getItem(position);

        if (contactModel != null) {
            viewHolder.name.setText(contactModel.getContactName());
            viewHolder.phone.setText(PhoneUtil.makeCorrectPhoneFormat(contactModel.getPhoneNumber()));

            if (contactModel.isChosed()) {
                viewHolder.iconPick.setImageResource(R.drawable.check_invite);
            } else {
                viewHolder.iconPick.setImageResource(R.drawable.nocheck_invite);
            }
        }

        return convertView;
    }
}
