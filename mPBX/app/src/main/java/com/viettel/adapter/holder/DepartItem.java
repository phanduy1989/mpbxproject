package com.viettel.adapter.holder;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.DepartmentModel;

import java.util.List;

/**
 * Created by mc.kim on 8/25/2016.
 */
public class DepartItem implements ParentListItem {
    private DepartmentModel colleagueGroup;
    public DepartItem() {
    }

    public void setColleagueGroup(DepartmentModel colleagueGroup) {
        this.colleagueGroup = colleagueGroup;
    }

    public DepartmentModel getColleagueGroup() {
        return colleagueGroup;
    }

    public int getChildCount() {
        return colleagueGroup.getListMember() != null ? colleagueGroup.getListMember().size() : 0;
    }

    @Override
    public List<?> getChildItemList() {
        return  colleagueGroup.getListMember();
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public boolean hasChild() {
        return colleagueGroup.getListMember() != null && !colleagueGroup.getListMember().isEmpty();
    }
}
