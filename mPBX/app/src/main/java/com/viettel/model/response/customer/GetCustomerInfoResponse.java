package com.viettel.model.response.customer;

import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetCustomerInfoResponse extends ResponseObject {

    CustomerModel customerInfo;

    public CustomerModel getCustomerInfo() {
        return customerInfo;
    }
}
