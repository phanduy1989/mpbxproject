package com.viettel.model.request.customer;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class GetCustomerInfoRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public int customerId;
	}

	public void setData(int customerId) {
		bodyObject = new BodyObject();
		bodyObject.customerId = customerId;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "customerController/apiGetCusInfo";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();



		try {
			String json = gson.toJson(bodyObject, BodyObject.class);
			Log.e("BodyParam","" + json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
