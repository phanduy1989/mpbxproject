package com.viettel.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.viettel.utils.DateUtil;
import com.viettel.utils.Utils;

import java.util.Date;

/**
 * Created by duyuno on 7/14/17.
 */
public class ConversationModel implements Parcelable {

    private int conversationType;

    private int conferenceId;
    private String conferenceName;
//    private String start;
//    private String ended;
    private int totalMember;

    private String typeName;
    private Date start;
    private Date ended;

    public ConversationModel() {

    }


    public int getConferenceId() {
        return conferenceId;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public int getTotalMember() {
        return totalMember;
    }

    public int getConversationType() {
        return conversationType;
    }

    public void setConversationType(int conversationType) {
        this.conversationType = conversationType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    public Date getStartDate() {
        return start;
    }

    public void setStartDate(Date startDate) {
        this.start = startDate;
    }

    public Date getEndDate() {
        return ended;
    }

    public void setEndDate(Date endDate) {
        this.ended = endDate;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(conversationType);
        dest.writeInt(conferenceId);
        dest.writeString(conferenceName);
        dest.writeString(typeName);
        dest.writeString(DateUtil.convertFullDate(start));
        dest.writeString(DateUtil.convertFullDate(ended));
        dest.writeInt(totalMember);

    }

    // Parcelling part
    public ConversationModel(Parcel in) {
        this.conversationType = in.readInt();
        this.conferenceId = in.readInt();
        this.conferenceName = in.readString();
        this.typeName = in.readString();
        this.start = DateUtil.getConvertDate(in.readString());
        this.ended = DateUtil.getConvertDate(in.readString());
        this.totalMember = in.readInt();
    }

    @SuppressWarnings("rawtypes")
    public static final Creator CREATOR = new Creator() {
        public ConversationModel createFromParcel(Parcel in) {
            return new ConversationModel(in);
        }

        public ConversationModel[] newArray(int size) {
            return new ConversationModel[size];
        }
    };
}
