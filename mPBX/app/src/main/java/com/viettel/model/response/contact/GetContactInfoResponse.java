package com.viettel.model.response.contact;

import com.viettel.model.ContactObject;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetContactInfoResponse extends ResponseObject {

    ContactObject contact;

    public ContactObject getContactObject() {
        return contact;
    }
}
