package com.viettel.model.response.conference;

import com.viettel.model.ConversationMember;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetConferenceMemberLeftResponse extends ResponseObject {

    ArrayList<ConversationMember> listMemberLeft;

    public ArrayList<ConversationMember> getListMemberLeft() {
        return listMemberLeft;
    }
}
