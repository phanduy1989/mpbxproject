package com.viettel.model;

/**
 * Created by VanHa on 7/21/2015.
 */
public class DanhBaInfo {


    private String app_id;
    private String password;
    private String avatar;
    private String id;
    private String phone;
    private String username;
    private String name;
    private String email;
    private String createdAt;
    private String updatedAt;
    private String check_chon;
    private String online;
    private String room_id;

    public DanhBaInfo() {
    }

    public DanhBaInfo(String app_id, String password, String avatar, String id, String phone, String username, String name, String email, String createdAt, String updatedAt, String check_chon, String online, String room_id) {
        this.app_id = app_id;
        this.password = password;
        this.avatar = avatar;
        this.id = id;
        this.phone = phone;
        this.username = username;
        this.name = name;
        this.email = email;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.check_chon = check_chon;
        this.online = online;
        this.room_id = room_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCheck_chon() {
        return check_chon;
    }

    public void setCheck_chon(String check_chon) {
        this.check_chon = check_chon;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
}
