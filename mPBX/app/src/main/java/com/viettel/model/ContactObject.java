package com.viettel.model;

import android.os.Parcel;

/**
 * Created by duyuno on 8/4/17.
 */
public class ContactObject extends PBXModel{
    int agentId = -1;
    int contactId;
    String contactName;
    String phoneNumber;
    String companyName;
    String email;
    String address;
    String note;
    String fileName;
    private boolean isChosed;
    public void changeChosed() {
        isChosed = !isChosed;
    }

    public ContactObject() {
    }

    public ContactObject(String contactName, String phoneNumber, String companyName, String email, String address, String note, String fileName) {
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.companyName = companyName;
        this.note = note;
        this.email = email;
        this.address = address;
        this.fileName = fileName;
    }
    public ContactObject(String contactName, String phoneNumber, String companyName, String email, String address, String note, String fileName, int contactId) {
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.companyName = companyName;
        this.note = note;
        this.email = email;
        this.address = address;
        this.fileName = fileName;
        this.contactId = contactId;
    }

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    public boolean isChosed() {
        return isChosed;
    }

    public void setChosed(boolean chosed) {
        isChosed = chosed;
    }

    private boolean isOpen;

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getNote() {
        return note;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }


    public int getContactId() {
        return contactId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(contactId);
        dest.writeString(contactName);
        dest.writeString(phoneNumber);
    }

    public ContactObject(Parcel in) {
        contactId = in.readInt();
        contactName = in.readString();
        phoneNumber = in.readString();
    }

    @SuppressWarnings("rawtypes")
    public static final Creator CREATOR = new Creator() {
        public ContactObject createFromParcel(Parcel in) {
            return new ContactObject(in);
        }

        public ContactObject[] newArray(int size) {
            return new ContactObject[size];
        }
    };
}
