package com.viettel.model.request;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class GetUserInfoRequest extends BasePostRequestEntity {

	ApiObject bodyObject;


	public void setData() {
		bodyObject = new ApiObject();
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "loginController/apiGetIntroduce";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

//		Log.e("Token","" + bodyObject.token);

		try {
			String json = gson.toJson(bodyObject, ApiObject.class);
			Log.e("bodyParam", "" + json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
