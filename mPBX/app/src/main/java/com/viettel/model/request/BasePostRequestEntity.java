package com.viettel.model.request;

import org.apache.http.HttpEntity;

public abstract class BasePostRequestEntity {

	public abstract String genUrl();
	public abstract HttpEntity genHttpEntity();
//	public void setData(String... params){
//
//	}
    
}
