package com.viettel.model.response.customer;

import com.viettel.model.ColleagueGroup;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetListCustomerResponse extends ResponseObject {

    ArrayList<CustomerModel> listCustomer;

    public ArrayList<CustomerModel> getListCustomer() {
        return listCustomer;
    }
}
