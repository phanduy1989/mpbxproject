package com.viettel.model;

/**
 * Created by VanHa on 7/21/2015.
 */
public class NhomDanhBaInfo {
    private String updatedAt;
    private String id;
    private String user_id;
    private String status;
    private String createdAt;
    private String name;
    private String avatar;

    public NhomDanhBaInfo() {
    }

    public NhomDanhBaInfo(String updatedAt, String id, String user_id, String status, String createdAt, String name) {
        this.updatedAt = updatedAt;
        this.id = id;
        this.user_id = user_id;
        this.status = status;
        this.createdAt = createdAt;
        this.name = name;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
