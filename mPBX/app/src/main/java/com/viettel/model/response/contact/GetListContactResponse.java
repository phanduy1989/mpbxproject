package com.viettel.model.response.contact;

import com.viettel.model.ContactObject;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetListContactResponse extends ResponseObject {

    ArrayList<ContactObject> listContact;

    public ArrayList<ContactObject> getListContact() {
        return listContact;
    }
}
