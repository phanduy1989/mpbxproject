package com.viettel.model.response;

/**
 * Created by duyuno on 8/4/16.
 */
public class UploadFileResponse extends ResponseObject{
    private String fileName;

    public String getFilename() {
        return fileName;
    }
}
