package com.viettel.model.response.conference;

import com.viettel.model.ConversationMember;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetConferenceListAddCustomerResponse extends ResponseObject {

    ArrayList<CustomerModel> listMember;

    public ArrayList<CustomerModel> getListMember() {
        return listMember;
    }
}
