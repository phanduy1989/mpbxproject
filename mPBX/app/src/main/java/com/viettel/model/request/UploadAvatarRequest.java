package com.viettel.model.request;

import android.graphics.Bitmap.CompressFormat;

import com.viettel.GlobalInfo;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadAvatarRequest extends BasePostMultipartRequest {

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "api/upload";
	}

	@Override
	public MultipartEntity genMultipartEntity() {

		if (bitmap == null) {
			return null;
		}

		MultipartEntity reqEntity = new MultipartEntity();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 10, bos);
		byte[] data = bos.toByteArray();
		ByteArrayBody bab = new ByteArrayBody(data, System.currentTimeMillis() + ".jpg");
		reqEntity.addPart("files", bab);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

		try {
			reqEntity.addPart("time", new StringBody(sdf.format(new Date())));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return reqEntity;
	}
}
