package com.viettel.model;

import android.os.Parcel;

import com.viettel.utils.PhoneUtil;

/**
 * Created by duyuno on 7/11/17.
 */
public class ColleagueModel extends PBXModel{

    private int colleagueId;
    private int agentId;
    private int groupId;
    private String colleagueName;
    private String numberOfExt;
    private String numberOfCompany;
    private String departmentName;
    private String position;
    private String email;
    private String phoneNumber;
    private int deleteFlag;

    public ColleagueModel() {

    }

    public void setAgentId() {
        this.agentId = colleagueId;
    }

    public void changePhoneFormat() {
        if(phoneNumber != null) {
            phoneNumber = PhoneUtil.makeCorrectPhoneFormat(phoneNumber);
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    public int getAgentId() {
        return agentId;
    }

    public void syncId() {
        if(colleagueId > 0) {
            agentId = colleagueId;
        } else if (agentId > 0) {
            colleagueId = agentId;
        }
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public boolean isSelected;

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    private boolean isOpen;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getColleagueId() {
        return colleagueId;
    }

    public String getColleagueName() {
        return colleagueName;
    }

    public String getNumberOfExt() {
        return numberOfExt;
    }

    public void setColleagueName(String colleagueName) {
        this.colleagueName = colleagueName;
    }

    public void setNumberOfExt(String numberOfExt) {
        this.numberOfExt = numberOfExt;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public void changeSelected() {
        isSelected = !isSelected;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public String getDepartmentName() {
        return departmentName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public String getPosition() {
        return position;
    }

    public String getNumberOfCompany() {
        return numberOfCompany;
    }

    public void setNumberOfCompany(String numberOfCompany) {
        this.numberOfCompany = numberOfCompany;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(agentId);
        dest.writeInt(colleagueId);
        dest.writeString(colleagueName);
        dest.writeString(numberOfExt);
        dest.writeString(departmentName);
        dest.writeString(phoneNumber);
        dest.writeString(numberOfCompany);
    }

    public ColleagueModel(Parcel in) {
        agentId = in.readInt();
        colleagueId = in.readInt();
        colleagueName = in.readString();
        numberOfExt = in.readString();
        departmentName = in.readString();
        phoneNumber = in.readString();
        numberOfCompany = in.readString();
    }

    @SuppressWarnings("rawtypes")
    public static final Creator CREATOR = new Creator() {
        public ColleagueModel createFromParcel(Parcel in) {
            return new ColleagueModel(in);
        }

        public ColleagueModel[] newArray(int size) {
            return new ColleagueModel[size];
        }
    };
}
