package com.viettel.model.request.conference;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class MuteMemberRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public int conferenceMemberId;
		public int conferenceId;
		public int callStatus;
	}

	public void setData(int conferenceMemberId, int callStatus, int conferenceId) {
		bodyObject = new BodyObject();
		bodyObject.conferenceMemberId = conferenceMemberId;
		bodyObject.conferenceId = conferenceId;
		bodyObject.callStatus = callStatus;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "voiceConferenceController/apiMuteMember";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
