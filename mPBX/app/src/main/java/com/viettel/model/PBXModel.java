package com.viettel.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.viettel.utils.PhoneUtil;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/14/17.
 */
public class PBXModel implements Parcelable {

    public void changePhoneFormat() {

    }

    public boolean isSelected() {
        return false;
    }

    public void setSelected(boolean selected) {

    }

    public void changeSelected() {

    }

    public static void formatPhoneNumber(ArrayList<PBXModel> list) {
        if(list == null || list.isEmpty()) return;
        for(PBXModel pbxModel : list) {
            if (pbxModel instanceof ColleagueModel) {
                ((ColleagueModel) pbxModel).setPhoneNumber(PhoneUtil.makeCorrectPhoneFormat(((ColleagueModel) pbxModel).getPhoneNumber()));
            } else if (pbxModel instanceof CustomerModel) {
                ((CustomerModel) pbxModel).setPhoneNumber(PhoneUtil.makeCorrectPhoneFormat(((CustomerModel) pbxModel).getPhoneNumber()));
            }
        }
    }
    public static void formatMemberPhoneNumber(ArrayList<ConversationMember> list) {
        if(list == null || list.isEmpty()) return;
        for(ConversationMember pbxModel : list) {
            pbxModel.setPhoneNumber(PhoneUtil.makeCorrectPhoneFormat(pbxModel.getPhoneNumber()));

        }
    }
    public static void formatPhoneNumberCustomer(ArrayList<CustomerModel> list) {
        if(list == null || list.isEmpty()) return;
        for(CustomerModel pbxModel : list) {
            pbxModel.setPhoneNumber(PhoneUtil.makeCorrectPhoneFormat(pbxModel.getPhoneNumber()));
        }
    }
    public static void formatPhoneNumberColleague(ArrayList<ColleagueModel> list) {
        if(list == null || list.isEmpty()) return;
        for(ColleagueModel pbxModel : list) {
            pbxModel.setPhoneNumber(PhoneUtil.makeCorrectPhoneFormat(pbxModel.getPhoneNumber()));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
