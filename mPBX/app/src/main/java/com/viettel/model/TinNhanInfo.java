package com.viettel.model;

/**
 * Created by VanHa on 7/21/2015.
 */
public class TinNhanInfo {

    private String updatedAt;
    private String id;
    private String user_id;
    private String createdAt;
    private String name;
    private String last_time_send_message;
    private String sender_id;
    private String last_message;
    private String unread_message;
    private String type;
    private SenderChatInfo senderChatInfo;


    public TinNhanInfo() {
    }

    public TinNhanInfo(String updatedAt, String id, String user_id, String createdAt, String name, String last_time_send_message, String sender_id, String last_message, String unread_message, String type, SenderChatInfo senderChatInfo) {
        this.updatedAt = updatedAt;
        this.id = id;
        this.user_id = user_id;
        this.createdAt = createdAt;
        this.name = name;
        this.last_time_send_message = last_time_send_message;
        this.sender_id = sender_id;
        this.last_message = last_message;
        this.unread_message = unread_message;
        this.type = type;
        this.senderChatInfo = senderChatInfo;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_time_send_message() {
        return last_time_send_message;
    }

    public void setLast_time_send_message(String last_time_send_message) {
        this.last_time_send_message = last_time_send_message;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getUnread_message() {
        return unread_message;
    }

    public void setUnread_message(String unread_message) {
        this.unread_message = unread_message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public SenderChatInfo getSenderChatInfo() {
        return senderChatInfo;
    }

    public void setSenderChatInfo(SenderChatInfo senderChatInfo) {
        this.senderChatInfo = senderChatInfo;
    }

    @Override
    public String toString() {
        return "TinNhanInfo{" +
                "updatedAt='" + updatedAt + '\'' +
                ", id='" + id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", name='" + name + '\'' +
                ", last_time_send_message='" + last_time_send_message + '\'' +
                ", sender_id='" + sender_id + '\'' +
                ", last_message='" + last_message + '\'' +
                ", unread_message='" + unread_message + '\'' +
                ", type='" + type + '\'' +
                ", senderChatInfo=" + senderChatInfo +
                '}';
    }
}
