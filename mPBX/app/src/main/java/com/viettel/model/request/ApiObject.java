package com.viettel.model.request;

import com.viettel.store.AppSharePreference;
import com.viettel.store.GlobalValue;

import java.lang.reflect.Type;

/**
 * Created by duyuno on 7/16/17.
 */
public class ApiObject {
    private String token;
    private String numberOfCompany;
    private String agentId;
    private String businessId;

    public ApiObject() {
        agentId = GlobalValue.agentId > 0 ? ""+  GlobalValue.agentId : null;
        businessId = GlobalValue.bussinessId > 0 ? ""+  GlobalValue.bussinessId : null;
        token = GlobalValue.token != null ? GlobalValue.token : null;
        numberOfCompany = GlobalValue.numberOfCompany != null ? GlobalValue.numberOfCompany : null;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
