package com.viettel.model.response.conference;

import com.viettel.model.ConversationModel;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetConferenceListResponse extends ResponseObject {

    private int minConfig;
    ArrayList<ConversationModel> listConferencePre;
    ArrayList<ConversationModel> listConferenceDoing;
    ArrayList<ConversationModel> listConferencePast;

    public int getMinConfig() {
        return minConfig;
    }

    public ArrayList<ConversationModel> getListConferencePre() {
        return listConferencePre;
    }

    public ArrayList<ConversationModel> getListConferenceDoing() {
        return listConferenceDoing;
    }

    public ArrayList<ConversationModel> getListConferencePast() {
        return listConferencePast;
    }
}
