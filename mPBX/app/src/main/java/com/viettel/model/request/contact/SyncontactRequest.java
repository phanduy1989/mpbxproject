package com.viettel.model.request.contact;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.ContactObject;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.utils.PhoneUtil;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class SyncontactRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public ArrayList<ContactObject> listContact;
	}

	public void setData(ArrayList<ContactObject> listContact) {
		bodyObject = new BodyObject();
		if(listContact != null) {
			for(ContactObject contactObject : listContact) {
				String phone = contactObject.getPhoneNumber();
				contactObject.setPhoneNumber(PhoneUtil.removeSpecialCharecter(phone));
			}
		}
		bodyObject.listContact = listContact;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "contactController/apiSyncContact";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);

			Log.e("BodyParam: ", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
