package com.viettel.model.response.colleague;

import com.viettel.model.ColleagueModel;
import com.viettel.model.CustomerModel;
import com.viettel.model.response.ResponseObject;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetColleagueInfoResponse extends ResponseObject {

    ColleagueModel memberInfo;

    public ColleagueModel getMemberInfo() {
        return memberInfo;
    }
}
