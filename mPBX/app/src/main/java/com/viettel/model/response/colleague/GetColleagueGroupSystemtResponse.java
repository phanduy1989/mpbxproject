package com.viettel.model.response.colleague;

import com.viettel.model.Agent;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.DepartmentModel;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetColleagueGroupSystemtResponse extends ResponseObject {

    ArrayList<DepartmentModel> listDepart;
    ArrayList<Agent> listStaff;

    public ArrayList<DepartmentModel> getListDepart() {
        return listDepart;
    }

    public ArrayList<Agent> getListStaff() {
        return listStaff;
    }
}
