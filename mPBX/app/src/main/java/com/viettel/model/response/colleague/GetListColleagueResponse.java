package com.viettel.model.response.colleague;

import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationMember;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetListColleagueResponse extends ResponseObject {

    ArrayList<ColleagueModel> listColleague;

    public ArrayList<ColleagueModel> getListColleague() {
        return listColleague;
    }
}
