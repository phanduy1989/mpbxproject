package com.viettel.model.request.conference;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.ConversationMember;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class CreateConversationRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public String conferenceName;
		public String start;
		public String end;
		public ArrayList<ConversationMember> listMember;
		public ArrayList<ConversationMember> listNonMember;
	}

	public void setData(ArrayList<ConversationMember> list, ArrayList<ConversationMember> listNonMember, String conferenceName, String start, String end) {
		bodyObject = new BodyObject();
		bodyObject.conferenceName = conferenceName;
		bodyObject.start = start;
		bodyObject.end = end;
		bodyObject.listMember = list;
		bodyObject.listNonMember = listNonMember;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "voiceConferenceController/apiCreateMeeting";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);

			Log.e("BodyParam: ", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
