package com.viettel.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by duyuno on 7/19/17.
 */
public class ConversationMember extends PBXModel implements Parcelable{

    public static final int STATUS_UNJOIN = 0;
    public static final int STATUS_LEFT = 2;
    public static final int STATUS_ONLINE = 1;

    public static final int MUTE = 2;
    public static final int INCALL = 1;
    public static final int DISCONECT = 0;
    public static final int LEAVE = 3;

    private int conferenceMemberId;
    private int memberId;
    private int agentId;
    private String memberName;
    private String phoneNumber;
    private int callStatus;
    private Date joinTime;
    private Date leftTime;
    private String fileName;
    public boolean isSelected;

    private boolean isChosed;

    private int status;

    public void changeSelected() {
        isSelected = !isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public ConversationMember() {

    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMemberId() {
        return memberId;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public Date getLeftTime() {
        return leftTime;
    }

    public int getStatus() {
//        if(joinTime == null && leftTime == null) {
//            return STATUS_UNJOIN;
//        } else if (joinTime != null && leftTime == null) {
//            return  STATUS_ONLINE;
//        } else {
//            return STATUS_LEFT;
//        }
        return status;
    }

    public boolean isChosed() {
        return isChosed;
    }

    public void setChosed(boolean chosed) {
        isChosed = chosed;
    }

    public void changeChosed() {
        isChosed = !isChosed;
    }

    public String getFileName() {
        return fileName;
    }

    public String getStringStatus() {
        switch (status) {
            case STATUS_UNJOIN:
                return "Offline";
            case STATUS_LEFT:
                return "Rời hội nghị";
            case STATUS_ONLINE:
                return "Đang online";
        }

        return "Chưa xác định";
    }

    public int getConferenceMemberId() {
        return conferenceMemberId > 0 ? conferenceMemberId : memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String membephoneNumber) {
        this.phoneNumber = membephoneNumber;
    }

    public int getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(int callStatus) {
        this.callStatus = callStatus;
    }

    public int getChangeCallStatus () {
        return callStatus == MUTE ? INCALL : MUTE;
    }

    public void changeMute() {
        if(callStatus == MUTE) {
            callStatus = INCALL;
        } else {
            callStatus = MUTE;
        }
    }

    public void setConferenceMemberId(int conferenceMemberId) {
        this.conferenceMemberId = conferenceMemberId;
        agentId = conferenceMemberId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(conferenceMemberId);
        dest.writeInt(memberId);
        dest.writeInt(agentId);
        dest.writeString(memberName);
        dest.writeString(phoneNumber);
        dest.writeString(fileName);
        dest.writeInt(callStatus);
    }

    public ConversationMember(Parcel in) {
        conferenceMemberId = in.readInt();
        memberId = in.readInt();
        agentId = in.readInt();
        memberName = in.readString();
        phoneNumber = in.readString();
        fileName = in.readString();
        callStatus = in.readInt();
    }

    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ConversationMember createFromParcel(Parcel in) {
            return new ConversationMember(in);
        }

        public ConversationMember[] newArray(int size) {
            return new ConversationMember[size];
        }
    };
}
