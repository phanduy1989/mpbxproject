package com.viettel.model.response.conference;

import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationMember;
import com.viettel.model.ConversationModel;
import com.viettel.model.response.ResponseObject;

import java.util.Date;
import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetConferenceInfoResponse extends ResponseObject {

    public String conferenceName;
    public Date start;
    public Date end;

    ArrayList<ConversationMember> listMember;

    public ArrayList<ConversationMember> getListMember() {
        return listMember;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }
}
