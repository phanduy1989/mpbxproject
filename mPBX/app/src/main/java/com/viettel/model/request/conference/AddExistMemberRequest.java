package com.viettel.model.request.conference;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.ContactObject;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AddExistMemberRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public String voiceConferenceId;
		public ArrayList<ContactObject> listMember;
	}

	public void setData(ArrayList<ContactObject> list, String voiceConferenceId) {
		bodyObject = new BodyObject();
		bodyObject.voiceConferenceId = voiceConferenceId;
		bodyObject.listMember = list;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "voiceConferenceController/apiAddColleagueOrCustomer";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);

			Log.e("BodyParam: ", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
