package com.viettel.model.request.conference;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class GetListConferenceColleagueRequest extends BasePostRequestEntity {

    BodyObject bodyObject;

    class BodyObject extends ApiObject {
        public String conferenceId;
    }

    public void setData() {
        bodyObject = new BodyObject();
    }

    public void setData(String conferenceId) {
        bodyObject = new BodyObject();
        bodyObject.conferenceId = conferenceId;
    }

    @Override
    public String genUrl() {
        return GlobalInfo.ServerConfig.DOMAIN + "voiceConferenceController/apiGetListColleague";
    }

    @Override
    public HttpEntity genHttpEntity() {
        ByteArrayEntity baEntity = null;
        Gson gson = new Gson();


        try {
            String json = gson.toJson(bodyObject, BodyObject.class);
            Log.e("BodyParam", "" + json);
            baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
            baEntity.setContentEncoding("application/json");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return baEntity;
    }

}
