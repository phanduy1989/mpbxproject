package com.viettel.model.request.customer;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.utils.PhoneUtil;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class CreateCustomerRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public String customerName;
		public String companyName;
		public String position;
		public String phoneNumber;
		public String phoneNumber2;
		public String phoneNumber3;
		public String fileName;
	}

	public void setData(String customerName, String companyName, String position, String phoneNumber
			, String phoneNumber2, String phoneNumber3, String fileName) {
		bodyObject = new BodyObject();
		bodyObject.customerName = customerName;
		bodyObject.companyName = companyName;
		bodyObject.position = position;
		bodyObject.phoneNumber = PhoneUtil.makeCorrectPhoneFormat(phoneNumber);
		bodyObject.phoneNumber2 = phoneNumber2 != null && !phoneNumber2.isEmpty() ? PhoneUtil.makeCorrectPhoneFormat(phoneNumber2) : "";
		bodyObject.phoneNumber3 = phoneNumber3 != null && !phoneNumber3.isEmpty() ? PhoneUtil.makeCorrectPhoneFormat(phoneNumber3) : "";
		bodyObject.fileName = fileName;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "customerController/apiAddCustomer";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);

			Log.e("BodyParam: ", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
