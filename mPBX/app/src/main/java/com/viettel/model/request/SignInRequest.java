package com.viettel.model.request;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class SignInRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject {
		public String ipPhoneComNum;
		public String ipPassword;

	}

	public void setData(String... data) {
		bodyObject = new BodyObject();
		bodyObject.ipPhoneComNum = data[0];
		bodyObject.ipPassword = data[1];
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "loginController/apiLogin";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);
			Log.e("BodyParams", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
