package com.viettel.model.response.colleague;

import com.viettel.model.ColleagueGroup;
import com.viettel.model.response.ResponseObject;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class GetColleagueGroupResponse extends ResponseObject {

    ArrayList<ColleagueGroup> listGroup;

    public ArrayList<ColleagueGroup> getListGroup() {
        return listGroup;
    }
}
