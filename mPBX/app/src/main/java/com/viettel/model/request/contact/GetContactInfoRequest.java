package com.viettel.model.request.contact;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class GetContactInfoRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public int contactId;
	}

	public void setData(int contactId) {
		bodyObject = new BodyObject();
		bodyObject.contactId = contactId;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "contactController/apiGetContactInfo";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();



		try {
			String json = gson.toJson(bodyObject, BodyObject.class);
			Log.e("BodyParam","" + json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
