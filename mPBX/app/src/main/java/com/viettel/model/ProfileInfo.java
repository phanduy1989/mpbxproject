package com.viettel.model;

/**
 * Created by VanHa on 7/21/2015.
 */
public class ProfileInfo {
    private String id;
    private String app_id;
    private String username;
    private String name;
    private String email;
    private String avatar;
    private String phone;
    private String createdAt;
    private String updatedAt;
    private String token;

    public ProfileInfo() {
    }

    public ProfileInfo(String id, String app_id, String username, String name, String email, String avatar, String phone, String createdAt, String updatedAt, String token) {
        this.id = id;
        this.app_id = app_id;
        this.username = username;
        this.name = name;
        this.email = email;
        this.avatar = avatar;
        this.phone = phone;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
