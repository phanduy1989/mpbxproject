package com.viettel.model;

import java.util.ArrayList;

/**
 * Created by duyuno on 8/7/17.
 */
public class DepartmentModel {
    private int deptId;
    private int status;
    private String deptCode;
    private String deptName;
    private String description;
    private String parentId;
    private String pathId;
    private String deptLevel;


    public ArrayList<Agent> getListMember() {
        return null;
    }

    public int getDeptId() {
        return deptId;
    }

    public int getStatus() {
        return status;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public String getDescription() {
        return description;
    }

    public String getParentId() {
        return parentId;
    }

    public String getPathId() {
        return pathId;
    }

    public String getDeptLevel() {
        return deptLevel;
    }
}
