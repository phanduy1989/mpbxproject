package com.viettel.model;

/**
 * Created by duyuno on 8/7/17.
 */
public class Agent {
    private int agentId;
    private String employeeName;
    private String phoneNumber;
    private String code;
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCode() {
        return code;
    }

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }
}
