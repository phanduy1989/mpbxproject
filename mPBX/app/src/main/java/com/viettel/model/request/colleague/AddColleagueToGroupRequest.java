package com.viettel.model.request.colleague;

import android.util.Log;

import com.google.gson.Gson;
import com.viettel.GlobalInfo;
import com.viettel.model.ColleagueModel;
import com.viettel.model.request.ApiObject;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.utils.PhoneUtil;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

import java.io.UnsupportedEncodingException;

public class AddColleagueToGroupRequest extends BasePostRequestEntity {

	BodyObject bodyObject;

	class BodyObject extends ApiObject {
		public String groupId;
		public ColleagueModel colleagueInfo;
	}

	public void setData(ColleagueModel colleagueModel, String groupId) {
		bodyObject = new BodyObject();
		bodyObject.groupId = groupId;
		String phoneNumber = colleagueModel.getPhoneNumber();
		colleagueModel.setPhoneNumber(PhoneUtil.removeZoneCode(phoneNumber));
		bodyObject.colleagueInfo = colleagueModel;
	}

	@Override
	public String genUrl() {
		return GlobalInfo.ServerConfig.DOMAIN + "colleagueController/apiAddColleague";
	}

	@Override
	public HttpEntity genHttpEntity() {
		ByteArrayEntity baEntity = null;
		Gson gson = new Gson();

		try {
			String json = gson.toJson(bodyObject, BodyObject.class);

			Log.e("BodyParam: ", json);
			baEntity = new ByteArrayEntity(json.getBytes("UTF8"));
//             baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//             "application/json"));
//			baEntity.setContentEncoding("application/x-www-form-urlencoded");
            baEntity.setContentEncoding("application/json");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return baEntity;
	}

}
