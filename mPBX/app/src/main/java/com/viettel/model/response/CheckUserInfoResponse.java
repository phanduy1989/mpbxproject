package com.viettel.model.response;

import com.viettel.model.UserProfile;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/14/16.
 */
public class CheckUserInfoResponse extends ResponseObject{
    UserProfile user;

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }
}
