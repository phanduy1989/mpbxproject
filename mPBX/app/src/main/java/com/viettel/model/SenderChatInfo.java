package com.viettel.model;

/**
 * Created by VanHa on 7/25/2017.
 */

public class SenderChatInfo {

    private String updatedAt;
    private String id;
    private String phone;
    private String username;
    private String email;
    private String createdAt;
    private String app_id;
    private String avatar;
    private String password;
    private String name;
    private String room_id;

    public SenderChatInfo() {
    }

    public SenderChatInfo(String updatedAt, String id, String phone, String username, String email, String createdAt, String app_id, String avatar, String password, String name, String room_id) {
        this.updatedAt = updatedAt;
        this.id = id;
        this.phone = phone;
        this.username = username;
        this.email = email;
        this.createdAt = createdAt;
        this.app_id = app_id;
        this.avatar = avatar;
        this.password = password;
        this.name = name;
        this.room_id = room_id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
}
