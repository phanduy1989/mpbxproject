package com.viettel.model.response;

/**
 * Created by duyuno on 11/14/16.
 */
public class SignInResponse extends ResponseObject{

    private String token;
    private int userId;
    private int businessId;
    private int agentId;
    private String userName;
    private String callBack;
    private String roles;
    private String version;
    private String introduce;
    private String numberOfCompany;

    public String getRoles() {
        return roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBusinessId() {
        return businessId;
    }

    public int getAgentId() {
        return agentId;
    }

    public String getUserName() {
        return userName;
    }

    public String getCallBack() {
        return callBack;
    }

    public String getNumberOfCompany() {
        return numberOfCompany;
    }

    public String getVersion() {
        return version;
    }

    public String getIntroduce() {
        return introduce;
    }
}
