package com.viettel.model;

/**
 * Created by VanHa on 7/28/2015.
 */
public class NoiDungInfo {

    private String _id;
    private String content;
    private String user_id;
    private String room_id;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private String msg_id;
    private SenderChatInfo senderChatInfo;

    private String thoi_gian_client;
    private String loai_tin_nhan;
    private String trang_thai_gui;
    private String trang_thai;
    private String type;
    private String file_name;

    public NoiDungInfo() {
    }

    public NoiDungInfo(String _id, String content, String user_id, String room_id, String createdAt, String updatedAt, String deletedAt, String msg_id, SenderChatInfo senderChatInfo, String thoi_gian_client, String loai_tin_nhan, String trang_thai_gui, String trang_thai, String type, String file_name) {
        this._id = _id;
        this.content = content;
        this.user_id = user_id;
        this.room_id = room_id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.msg_id = msg_id;
        this.senderChatInfo = senderChatInfo;
        this.thoi_gian_client = thoi_gian_client;
        this.loai_tin_nhan = loai_tin_nhan;
        this.trang_thai_gui = trang_thai_gui;
        this.trang_thai = trang_thai;
        this.type = type;
        this.file_name = file_name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public SenderChatInfo getSenderChatInfo() {
        return senderChatInfo;
    }

    public void setSenderChatInfo(SenderChatInfo senderChatInfo) {
        this.senderChatInfo = senderChatInfo;
    }

    public String getThoi_gian_client() {
        return thoi_gian_client;
    }

    public void setThoi_gian_client(String thoi_gian_client) {
        this.thoi_gian_client = thoi_gian_client;
    }

    public String getLoai_tin_nhan() {
        return loai_tin_nhan;
    }

    public void setLoai_tin_nhan(String loai_tin_nhan) {
        this.loai_tin_nhan = loai_tin_nhan;
    }

    public String getTrang_thai_gui() {
        return trang_thai_gui;
    }

    public void setTrang_thai_gui(String trang_thai_gui) {
        this.trang_thai_gui = trang_thai_gui;
    }

    public String getTrang_thai() {
        return trang_thai;
    }

    public void setTrang_thai(String trang_thai) {
        this.trang_thai = trang_thai;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }
}