package com.viettel.model;

import com.viettel.utils.PhoneUtil;

/**
 * Created by duyuno on 8/5/17.
 */
public class TextPhoneRoleNullAble extends TextRole{

    public String checkInputError(String s) {
        if(s == null || s.isEmpty()) return null;
        for(int i = 0, length = s.length(); i < length; i++) {
            int charValue = s.charAt(i);
            if(charValue < 48 || charValue > 57) {
                return "Số điện thoại chỉ chứa các kí tự số";
            }
        }
        String phone = PhoneUtil.removeZoneCode(s);
        if(phone.length() < 8 || phone.length() > 15) {
            return "Số điện thoại phải có 8 - 15 kí tự số";
        }

        return null;
    }
}
