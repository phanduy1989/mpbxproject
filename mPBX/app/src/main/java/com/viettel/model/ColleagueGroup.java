package com.viettel.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/11/17.
 */
public class ColleagueGroup implements Parcelable {

    public ColleagueGroup(ColleagueGroup colleagueGroup) {
        this.groupName = colleagueGroup.getGroupName();
        this.groupId = colleagueGroup.getGroupId();
        this.totalMember = colleagueGroup.getTotalMember();
        this.isExpand = colleagueGroup.isExpand();

    }

    public void addMember(ColleagueModel colleagueModel) {
        if(listMember == null) {
            listMember = new ArrayList<>();
        }

        listMember.add(colleagueModel);
    }

    private String groupName;
    private int groupId;
    private int totalMember;

    private boolean isExpand;

    private boolean isSelected;

    private ArrayList<ColleagueModel> listMember;

    public String getGroupName() {
        return groupName;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getTotalMember() {
        return totalMember;
    }

    public ArrayList<ColleagueModel> getListMember() {
        return listMember;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean checkContainId(int id) {
        if(listMember == null || listMember.isEmpty()) return false;

        for(ColleagueModel colleagueModel : listMember) {
            if(colleagueModel.getColleagueId() == id) {
                return true;
            }
        }

        return false;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public void setListMember(ArrayList<ColleagueModel> listMember) {
        this.listMember = listMember;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(groupId);
        dest.writeInt(totalMember);
        dest.writeString(groupName);
    }

    public ColleagueGroup(Parcel in) {
        groupId = in.readInt();
        totalMember = in.readInt();
        groupName = in.readString();
    }

    @SuppressWarnings("rawtypes")
    public static final Creator CREATOR = new Creator() {
        public ColleagueGroup createFromParcel(Parcel in) {
            return new ColleagueGroup(in);
        }

        public ColleagueGroup[] newArray(int size) {
            return new ColleagueGroup[size];
        }
    };
}
