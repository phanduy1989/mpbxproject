package com.viettel.utils;

import android.content.Context;

import com.viettel.adapter.holder.ColleagueItem;
import com.viettel.adapter.holder.DepartItem;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationMember;
import com.viettel.model.DepartmentModel;
import com.viettel.model.ToolEntity;
import com.viettel.model.response.conference.GetConferenceListResponse;
import com.viettel.mpbx.R;
import com.viettel.view.fragment.business.ContactFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataManager {

	public static final HashMap<Integer, ToolEntity> hashMenu = new HashMap<>();

	public static void initMenu(Context context){
		hashMenu.put(ToolEntity.MENU_COLLEAGUE,new ToolEntity(ToolEntity.MENU_COLLEAGUE, context.getResources().getString(R.string.labelColleague),R.drawable.icon_thietlap, false, ""));
		hashMenu.put(ToolEntity.MENU_CUSTOMER,new ToolEntity(ToolEntity.MENU_CUSTOMER, context.getResources().getString(R.string.labelCustomer), R.drawable.icon_tintuc, ""));
		hashMenu.put(ToolEntity.MENU_CONTACT,new ToolEntity(ToolEntity.MENU_CONTACT, context.getResources().getString(R.string.labelContact), R.drawable.icon_hdsd, ContactFragment.class.getName()));
		hashMenu.put(ToolEntity.MENU_KEYBOARD,new ToolEntity(ToolEntity.MENU_KEYBOARD, context.getResources().getString(R.string.labelKeyboard), R.drawable.icon_qa, ""));
		hashMenu.put(ToolEntity.MENU_CONFERENCE,new ToolEntity(ToolEntity.MENU_CONFERENCE, context.getResources().getString(R.string.labelConferencCall), R.drawable.icon_policy, ""));
	}

	public static ArrayList<ToolEntity> genToolMenuEntities(Context context) {

		if(hashMenu.isEmpty()) {
			initMenu(context);
		}

		ArrayList<ToolEntity> list = new ArrayList<ToolEntity>();

		for (Map.Entry<Integer, ToolEntity> entry : hashMenu.entrySet()) {
			list.add(entry.getValue());
		}

		return list;
	}

//	public static ArrayList<CustomerModel> genListCustomer() {
//		ArrayList<CustomerModel> list = new ArrayList<>();
//
//		for(int i = 0; i < 20; i++) {
//			list.add(new CustomerModel("Name " + i, "Phone " + i));
//		}
//
//		return list;
//	}
	public static ArrayList<ConversationMember> genListPBXModel() {
		ArrayList<ConversationMember> list = new ArrayList<>();

		for(int i = 0; i < 20; i++) {
			ConversationMember pbxModel = new ConversationMember();
			pbxModel.setMemberName("Member " + i);
			pbxModel.setPhoneNumber("Phone " + i);
			list.add(pbxModel);
		}

		return list;
	}

	public static ArrayList<ColleagueItem> genListParentMenu(ArrayList<ColleagueGroup> listData) {
		ArrayList<ColleagueItem> list = new ArrayList<>();

		for(ColleagueGroup colleagueGroup : listData) {
			ColleagueItem parentsMenu = new ColleagueItem();
			parentsMenu.setColleagueGroup(colleagueGroup);
			list.add(parentsMenu);
		}
//		}

		return list;
	}
	public static ArrayList<DepartItem> genListParentDepart(ArrayList<DepartmentModel> listData) {
		ArrayList<DepartItem> list = new ArrayList<>();

		for(DepartmentModel colleagueGroup : listData) {
			DepartItem parentsMenu = new DepartItem();
			parentsMenu.setColleagueGroup(colleagueGroup);
			list.add(parentsMenu);
		}
//		}

		return list;
	}

	public static ArrayList<ColleagueModel> genListColleagueModel() {
		ArrayList<ColleagueModel> list = new ArrayList<>();

		for(int i = 0; i < 10; i++) {
			ColleagueModel colleagueModel = new ColleagueModel();
			colleagueModel.setColleagueName("Member " + i);
			colleagueModel.setNumberOfExt("Phone " + i);
			list.add(colleagueModel);
		}

		return list;
	}

	public static ArrayList<ConversationGroup> genListConverGroup(GetConferenceListResponse getConferenceListResponse) {



		ArrayList<ConversationGroup> listGroup = new ArrayList<>();

		if(getConferenceListResponse.getListConferenceDoing() != null && !getConferenceListResponse.getListConferenceDoing().isEmpty()) {
			ConversationGroup conversationGroup1 = new ConversationGroup();
			conversationGroup1.setType(ConversationGroup.ON_GOING);
			conversationGroup1.setName("Hội nghị đang diễn ra");
			conversationGroup1.setListChild(getConferenceListResponse.getListConferenceDoing());
			listGroup.add(conversationGroup1);
		}
		if(getConferenceListResponse.getListConferencePre() != null && !getConferenceListResponse.getListConferencePre().isEmpty()) {
			ConversationGroup conversationGroup2 = new ConversationGroup();
			conversationGroup2.setType(ConversationGroup.UP_COMMING);
			conversationGroup2.setName("Hội nghị sắp diễn ra");
			conversationGroup2.setListChild(getConferenceListResponse.getListConferencePre());
			listGroup.add(conversationGroup2);
		}
		if(getConferenceListResponse.getListConferencePast() != null && !getConferenceListResponse.getListConferencePast().isEmpty()) {
			ConversationGroup conversationGroup3 = new ConversationGroup();
			conversationGroup3.setType(ConversationGroup.FINISHED);
			conversationGroup3.setName("Hội nghị đã kết thúc");
			conversationGroup3.setListChild(getConferenceListResponse.getListConferencePast());
			listGroup.add(conversationGroup3);
		}


		return listGroup;
	}

	static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
	static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

//	public static ArrayList<ConversationModel> genListConversationModel(int type) {
//		ArrayList<ConversationModel> list = new ArrayList<>();
//		for(int i = 0; i < 3; i++) {
//			ConversationModel conversationModel = new ConversationModel();
//			conversationModel.setName("Conversation " + i);
//			conversationModel.setMemberCount((i + 1));
//			conversationModel.setConversationType(type);
//
//			Calendar calendar = Calendar.getInstance();
//
//			conversationModel.setStartDate(calendar.getTime());
//			calendar.add(Calendar.HOUR_OF_DAY, 3);
//			conversationModel.setEndDate(calendar.getTime());
//
//			list.add(conversationModel);
//		}
//		return list;
//	}

	private static String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
//			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

}
