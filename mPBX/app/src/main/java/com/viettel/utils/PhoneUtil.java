package com.viettel.utils;

/**
 * Created by duyuno on 8/5/17.
 */
public class PhoneUtil {
    public static String makeCorrectPhoneFormat(String phoneNumber) {

        if(phoneNumber == null || phoneNumber.length() == 0) return null;

        if (phoneNumber.startsWith("0") || phoneNumber.startsWith("84") || phoneNumber.startsWith("+84")) {
            return phoneNumber;
        }

        return "0" + phoneNumber;
    }

    public static String removeZoneCode(String input) {
        if(input == null || input.length() == 0) return null;
        if(input.startsWith("0")) {
            input = input.substring(1, input.length());
        } else if(input.startsWith("84")) {
            input = input.substring(2, input.length());
        } else if(input.startsWith("+84")) {
            input = input.substring(3, input.length());
        }

        return input;
    }

    public static String removeSpecialCharecter(String phone) {
        if(phone == null || phone.length() == 0) return "";

        StringBuilder sb = new StringBuilder("");

        for(int i = 0, length = phone.length(); i < length; i++) {
            int charValue = phone.charAt(i);
            if(charValue >= 48 && charValue <= 57) {
                sb.append(phone.charAt(i));
            }
        }

        return sb.toString();
    }
}
