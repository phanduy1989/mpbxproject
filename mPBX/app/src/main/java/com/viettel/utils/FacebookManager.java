package com.viettel.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

/**
 */
@SuppressLint("ValidFragment")
public class FacebookManager {
    private final String TOKEN_REMOVED = "{AccessToken token:ACCESS_TOKEN_REMOVED permissions:[user_posts, public_profile]}";
    public static final String PRIVACY_VALUE = "value";
    public static final String PRIVACY_FRIEND = "ALL_FRIENDS";
    public static final String PRIVACY_SELF = "SELF";
    public static final String PRIVACY_EVERYONE = "EVERYONE";
    public static final String DATA_TITLE = "name";
    public static final String DATA_PRIVACY = "privacy";
    public static final String DATA_MESSAGE = "message";
    public static final String DATA_CAPTION = "caption";
    public static final String DATA_DESCRITION = "description";
    public static final String DATA_LINK = "link";
    public static final String DATA_PICTURE = "url";


    private static final String TAG = FacebookManager.class.getSimpleName();
    public static final String CLASS_NAME = FacebookManager.class.getName();
    private static FacebookManager instance = null;
    private FragmentActivity mContext = null;
    private static CallbackManager callbackManager = null;
//    private Fragment mFragment = null;

    public static FacebookManager getInstance(FragmentActivity context){
        //if(instance == null){
            instance = new FacebookManager(context);
        //}
        return instance;
    }
    public FacebookManager(){
    }

    public FacebookManager(FragmentActivity context){
        super();
//        mFragment = fragment;
        this.mContext = context;
        init(mContext);
        if(callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
    }

    public CallbackManager getCallbackManager(){
        return callbackManager;
    }


    public void init(Context context){
        FacebookSdk.sdkInitialize(context);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public boolean needLogin() {
        AccessToken.getCurrentAccessToken();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {

            if (accessToken.getToken() == null) {
                return true;
            } else {
                if (accessToken.isExpired()) {
                    AccessToken.setCurrentAccessToken(null);
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }
    //TODO 수정

//    public void showPostingDialog(Program vod) {
//        Logger.d(TAG, "called showPostingDialog()");
//        if (needLogin()) {
//            Logger.d(TAG,"called showPostingDialog()-no login");
//            login(vod);
//        } else {
//            Logger.d(TAG,"called showPostingDialog()-login");
//            showPostDialog(vod, getLink(vod.getId()));
//        }
//    }


//    public void login(final Program mProgram) {
//        Logger.d(TAG,"called login()");
//        final MessageDialog dialog = new MessageDialog();
//        Bundle args = new Bundle();
//        args.putString(MessageDialog.PARAM_FACEBOOK_TITLE, mContext.getString(R.string.facebookLoginTitle));
//        args.putString(MessageDialog.PARAM_FACEBOOK_MESSAGE, mContext.getString(R.string.facebookLoginMsg1));
//        args.putString(MessageDialog.PARAM_FACEBOOK_MESSAGE2, mContext.getString(R.string.facebookLoginMsg2));
//        args.putString(MessageDialog.PARAM_BUTTON_1, mContext.getString(R.string.yes));
//        args.putString(MessageDialog.PARAM_BUTTON_2, mContext.getString(R.string.no));
//        dialog.setArguments(args);
//        dialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (v.getId() == R.id.button1) {
//                    processLogin(mProgram);
//                }
//                dialog.dismiss();
//            }
//        });
//        dialog.show(mContext.getSupportFragmentManager(), MessageDialog.CLASS_NAME);
//    }



    public void logOut(){
        if(!needLogin()) {
            LoginManager.getInstance().logOut();
        }
    }

//    private void showPostDialog(final Program mProgram, final String contentUrl) {
//        Logger.d(TAG, "showPostDialog()");
//        final ShareDialog shareDialog = new ShareDialog(mContext);
//        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
//
//            @Override
//            public void onSuccess(Sharer.Result result) {
//                if(result.getPostId() !=null)
//                App.getToast(mContext, mProgram.getTitle(WindmillConfiguration.LANGUAGE), mContext.getString(R.string.facebookSuccessUpload),false).show();
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Logger.d(TAG, "onError  :  " + error.toString());
//                if (error.toString().contains("facebookErrorCode: 190")) {
//                    AccessToken.setCurrentAccessToken(null);
//                    login(mProgram);
//                } else {
//                    // 에러 팝업
//                    showMessageDialog(error.getLocalizedMessage());
//                }
//            }
//
//            @Override
//            public void onCancel() {
//                Logger.d(TAG, "onCancel  :  ");
//            }
//        });
//
//        if (shareDialog.canShow(ShareLinkContent.class)) {
//            String url = DetailUtil.getPosterUrl(mProgram.getId(), DetailUtil.TYPE_POSTER);
//
//            String directorTxt = mProgram.getDirectors(WindmillConfiguration.LANGUAGE);
////            String directorTxt = DetailUtil.get
//            String starTxt = mProgram.getActors(WindmillConfiguration.LANGUAGE);
//            String description = mProgram.getSynopsis(WindmillConfiguration.LANGUAGE);//mContext.getString(R.string.vod_director) + " " + directorTxt + "|" + mContext.getString(R.string.vod_stars) + " " + starTxt;
//
//            ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                    .setImageUrl(WindmillConfiguration.LIVE ? Uri.parse(url) : null)
//                    .setContentTitle(mProgram.getTitle(WindmillConfiguration.LANGUAGE))
//                    .setContentDescription(description)
//                    .setContentUrl(Uri.parse(getLink(mProgram.getId())))
//                    .build();
//
//            shareDialog.show(linkContent, ShareDialog.Mode.FEED);
//        }
//    }

//
//    private void processLogin(final Program program) {
//        LoginBehavior loginBehavior = LoginBehavior.WEB_ONLY;
//        LoginManager.getInstance().setLoginBehavior(loginBehavior);
//        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//            }
//
//            @Override
//            public void onCancel() {
//            }
//
//            @Override
//            public void onError(FacebookException e) {
//
//            }
//        });
//        LoginManager.getInstance().logInWithPublishPermissions(mContext, Arrays.asList("publish_actions"));
//    }

    public void processLogin(FacebookCallback<LoginResult> callback) {
            LoginBehavior loginBehavior = LoginBehavior.NATIVE_WITH_FALLBACK;
            LoginManager.getInstance().setLoginBehavior(loginBehavior);
            LoginManager.getInstance().registerCallback(callbackManager,callback);

//            LoginManager.getInstance().logInWithPublishPermissions(mContext, Arrays.asList("publish_actions"));
            LoginManager.getInstance().logInWithReadPermissions(mContext, Arrays.asList("public_profile", "email"));
    }

    private String getLink(String programId){
        return "https://fb.me/1279877172028213?programId="+programId;
        //"http://viettel-test.droppages.com/";
        // "https://fb.me/1660175294238063";
        // "https://fb.me/945477508862322";
        // "//"content://com.recipe_app/recipe/search/pa";
        // https://play.google.com/store/apps/details?id=" + mContext.getPackageName();
    }
}