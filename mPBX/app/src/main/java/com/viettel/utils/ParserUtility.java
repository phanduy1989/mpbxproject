package com.viettel.utils;

import com.viettel.api.WebServiceConfig;
import com.viettel.model.DanhBaInfo;
import com.viettel.model.NhomDanhBaInfo;
import com.viettel.model.NoiDungInfo;
import com.viettel.model.ProfileInfo;
import com.viettel.model.SenderChatInfo;
import com.viettel.model.TinNhanInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;


public class ParserUtility {

    //------ parser Login --------------

    public static ProfileInfo parseLogin(String json) {
        ProfileInfo thongBaoInfo = new ProfileInfo();

        try {
            JSONObject object = new JSONObject(json);
            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_USERINFO);
            thongBaoInfo.setToken(object.getString(WebServiceConfig.TAG_KEY_TOKEN));
            thongBaoInfo.setId(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_ID));
            thongBaoInfo.setApp_id(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_APP_ID));
            thongBaoInfo.setUsername(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_USERNAME));
            thongBaoInfo.setName(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_NAME));
            thongBaoInfo.setEmail(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_EMAIL));
            thongBaoInfo.setAvatar(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_AVATAR));
            thongBaoInfo.setPhone(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_PHONE));
            thongBaoInfo.setCreatedAt(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_CREATEDAT));
            thongBaoInfo.setUpdatedAt(getStringValue(item,
                    WebServiceConfig.KEY_PROFILE_UPDATEDAT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return thongBaoInfo;
    }

    //------ parser list danh ba --------------
    public static ArrayList<DanhBaInfo> parselistDanhBa(String json) {
        ArrayList<DanhBaInfo> arrayList = new ArrayList<DanhBaInfo>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                DanhBaInfo thongBaoInfo = new DanhBaInfo();
                thongBaoInfo.setApp_id(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_APP_ID));
                thongBaoInfo.setPassword(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PASSWORD));
                thongBaoInfo.setAvatar(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_AVATAR));
                thongBaoInfo.setId(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ID));
                thongBaoInfo.setPhone(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PHONE));
                thongBaoInfo.setUsername(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_USERNAME));
                thongBaoInfo.setEmail(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_EMAIL));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_CREATEDAT));
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_UPDATEDAT));
                thongBaoInfo.setName(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_NAME));
                thongBaoInfo.setOnline(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ONLINE));
                thongBaoInfo.setCheck_chon("");
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }

    public static DanhBaInfo parseStatusOnline(String json) {
       DanhBaInfo arrayList = new DanhBaInfo();
        try {
            JSONObject object = new JSONObject(json);
            arrayList.setId(object.getString(WebServiceConfig.KEY_STATUS_USER_ID));
            arrayList.setOnline(object.getString(WebServiceConfig.KEY_STATUS_ONLINE));

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }

    //------ parser list thanh vien nhom --------------
    public static ArrayList<DanhBaInfo> parselistUserGroup(String json) {
        ArrayList<DanhBaInfo> arrayList = new ArrayList<DanhBaInfo>();
        try {
            JSONObject objectdata = new JSONObject(json);
//            JSONObject itemdata = objectdata.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            JSONObject object = new JSONObject(itemdata.toString());
            JSONArray item = objectdata.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                DanhBaInfo thongBaoInfo = new DanhBaInfo();
                thongBaoInfo.setApp_id(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_APP_ID));
                thongBaoInfo.setPassword(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PASSWORD));
                thongBaoInfo.setAvatar(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_AVATAR));
                thongBaoInfo.setId(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ID));
                thongBaoInfo.setPhone(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PHONE));
                thongBaoInfo.setUsername(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_USERNAME));
                thongBaoInfo.setName(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_NAME));
                thongBaoInfo.setEmail(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_EMAIL));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_CREATEDAT));
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_UPDATEDAT));
                thongBaoInfo.setOnline(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ONLINE));
                thongBaoInfo.setRoom_id(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ROOM_ID));
                thongBaoInfo.setCheck_chon("");
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }

    //------ parser list thanh vien nhom --------------
    public static ArrayList<DanhBaInfo> parselistUserRoom(String json) {
        ArrayList<DanhBaInfo> arrayList = new ArrayList<DanhBaInfo>();
        try {
            JSONObject objectdata = new JSONObject(json);
//            JSONObject itemdata = objectdata.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            JSONObject object = new JSONObject(itemdata.toString());
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_USERS);

//            JSONObject itemdata = objectdata.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            JSONObject object = new JSONObject(itemdata.toString());
            JSONArray item = objectdata.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                DanhBaInfo thongBaoInfo = new DanhBaInfo();
                thongBaoInfo.setApp_id(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_APP_ID));
                thongBaoInfo.setPassword(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PASSWORD));
                thongBaoInfo.setAvatar(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_AVATAR));
                thongBaoInfo.setId(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ID));
                thongBaoInfo.setPhone(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_PHONE));
                thongBaoInfo.setUsername(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_USERNAME));
                thongBaoInfo.setName(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_NAME));
                thongBaoInfo.setEmail(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_EMAIL));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_CREATEDAT));
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_UPDATEDAT));
                thongBaoInfo.setOnline(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ONLINE));
                thongBaoInfo.setRoom_id(getStringValue(items,
                        WebServiceConfig.KEY_DANHBA_ROOM_ID));
                thongBaoInfo.setCheck_chon("");
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }


    //------ parser list nhom danh ba --------------
    public static ArrayList<NhomDanhBaInfo> parselistNhomDanhBa(String json) {
        ArrayList<NhomDanhBaInfo> arrayList = new ArrayList<NhomDanhBaInfo>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                NhomDanhBaInfo thongBaoInfo = new NhomDanhBaInfo();
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_UPDATEDAT));
                thongBaoInfo.setId(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_ID));
                thongBaoInfo.setUser_id(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_USER_ID));
                thongBaoInfo.setStatus(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_STATUS));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_CREATEDAT));
                thongBaoInfo.setName(getStringValue(items,
                        WebServiceConfig.KEY_NHOM_DANHBA_NAME));
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }

    public static NhomDanhBaInfo parseCreateGroup(String json) {
        NhomDanhBaInfo thongBaoInfo = new NhomDanhBaInfo();

        try {
            JSONObject object = new JSONObject(json);
            JSONObject items = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
            thongBaoInfo.setUpdatedAt(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_UPDATEDAT));
            thongBaoInfo.setId(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_ID));
            thongBaoInfo.setUser_id(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_USER_ID));
            thongBaoInfo.setStatus(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_STATUS));
            thongBaoInfo.setCreatedAt(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_CREATEDAT));
            thongBaoInfo.setName(getStringValue(items,
                    WebServiceConfig.KEY_NHOM_DANHBA_NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return thongBaoInfo;
    }


    //------ parser list tin nhan --------------
    public static ArrayList<TinNhanInfo> parselistTinNhan(String json) {
        ArrayList<TinNhanInfo> arrayList = new ArrayList<TinNhanInfo>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                TinNhanInfo thongBaoInfo = new TinNhanInfo();
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_UPDATEDAT));
                thongBaoInfo.setId(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_ID));
                thongBaoInfo.setUser_id(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_USER_ID));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_CREATEDAT));
                thongBaoInfo.setName(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_NAME));
                thongBaoInfo.setLast_time_send_message(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_LAST_TIME_SEND_MESSAGE));
                thongBaoInfo.setSender_id(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_SENDER_ID));
                thongBaoInfo.setLast_message(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_LAST_MESSAGE));
                thongBaoInfo.setType(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_TYPE));
                thongBaoInfo.setUnread_message(getStringValue(items,
                        WebServiceConfig.KEY_TINNHAN_UNREAD_MESSAGE));
                SenderChatInfo senderChatInfo = new SenderChatInfo();
                if (items.toString().contains("username")) {
                    JSONObject objectItem = new JSONObject(items.toString());
                    JSONObject itemObject = objectItem.getJSONObject(WebServiceConfig.TAG_KEY_SENDER);
                    senderChatInfo.setUpdatedAt(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_UPDATEDAT));
                    senderChatInfo.setId(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_ID));
                    senderChatInfo.setPhone(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_PHONE));
                    senderChatInfo.setUsername(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_USERNAME));
                    senderChatInfo.setName(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_NAME));
                    senderChatInfo.setEmail(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_EMAIL));
                    senderChatInfo.setCreatedAt(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_CREATEDAT));
                    senderChatInfo.setApp_id(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_APP_ID));
                    senderChatInfo.setAvatar(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_AVATAR));
                    senderChatInfo.setPassword(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_PASSWORD));
                }
                thongBaoInfo.setSenderChatInfo(senderChatInfo);
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }


    //------ parser message --------------
    public static ArrayList<NoiDungInfo> parseNoiDungChat(String json) {
        ArrayList<NoiDungInfo> arrayList = new ArrayList<NoiDungInfo>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
            for (int i = 0; i < item.length(); i++) {
                JSONObject items = item.getJSONObject(i);
                NoiDungInfo thongBaoInfo = new NoiDungInfo();
                thongBaoInfo.set_id(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_ID));
                thongBaoInfo.setContent(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_CONTENT));
                thongBaoInfo.setUser_id(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_USER_ID));
                thongBaoInfo.setRoom_id(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_ROOM_ID));
                thongBaoInfo.setCreatedAt(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_CREATEDAT));
                thongBaoInfo.setUpdatedAt(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_UPDATEDAT));
                thongBaoInfo.setMsg_id(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_MSG_ID));
                thongBaoInfo.setDeletedAt(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_DELETEDAT));
                thongBaoInfo.setType(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_CHAT_TYPE));
                thongBaoInfo.setFile_name(getStringValue(items,
                        WebServiceConfig.KEY_MESSAGE_FILE_NAME));
                thongBaoInfo.setThoi_gian_client("");
                thongBaoInfo.setLoai_tin_nhan("1");
                thongBaoInfo.setTrang_thai("0");
                thongBaoInfo.setTrang_thai_gui("1");
                SenderChatInfo senderChatInfo = new SenderChatInfo();
                if (items.toString().contains("username")) {
                    JSONObject objectItem = new JSONObject(items.toString());
                    JSONObject itemObject = objectItem.getJSONObject(WebServiceConfig.TAG_KEY_SENDER);
                    senderChatInfo.setUpdatedAt(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_UPDATEDAT));
                    senderChatInfo.setId(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_ID));
                    senderChatInfo.setPhone(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_PHONE));
                    senderChatInfo.setUsername(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_USERNAME));
                    senderChatInfo.setName(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_NAME));
                    senderChatInfo.setEmail(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_EMAIL));
                    senderChatInfo.setCreatedAt(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_CREATEDAT));
                    senderChatInfo.setApp_id(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_APP_ID));
                    senderChatInfo.setAvatar(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_AVATAR));
                    senderChatInfo.setPassword(getStringValue(itemObject,
                            WebServiceConfig.KEY_SENDER_CHAT_PASSWORD));
                }
                thongBaoInfo.setSenderChatInfo(senderChatInfo);
                arrayList.add(thongBaoInfo);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return arrayList;
    }

    public static NoiDungInfo parseItemNoiDung(String json) {
        NoiDungInfo thongBaoInfo = new NoiDungInfo();

        try {
            JSONObject object = new JSONObject(json);
//            JSONObject items = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
            thongBaoInfo.set_id(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_ID));
            thongBaoInfo.setContent(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_CONTENT));
            thongBaoInfo.setUser_id(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_USER_ID));
            thongBaoInfo.setRoom_id(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_ROOM_ID));
            thongBaoInfo.setCreatedAt(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_CREATEDAT));
            thongBaoInfo.setUpdatedAt(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_UPDATEDAT));
            thongBaoInfo.setMsg_id(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_MSG_ID));
            thongBaoInfo.setDeletedAt(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_DELETEDAT));
            thongBaoInfo.setType(getStringValue(object,
                    WebServiceConfig.KEY_MESSAGE_CHAT_TYPE));

            thongBaoInfo.setThoi_gian_client("");
            thongBaoInfo.setLoai_tin_nhan("1");
            thongBaoInfo.setTrang_thai("0");
            thongBaoInfo.setTrang_thai_gui("1");
            SenderChatInfo senderChatInfo = new SenderChatInfo();
            if (json.contains("username")) {
//                JSONObject objectItem = new JSONObject(items.toString());
                JSONObject itemObject = object.getJSONObject(WebServiceConfig.TAG_KEY_SENDER);
                senderChatInfo.setUpdatedAt(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_UPDATEDAT));
                senderChatInfo.setId(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_ID));
                senderChatInfo.setPhone(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_PHONE));
                senderChatInfo.setUsername(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_USERNAME));
                senderChatInfo.setName(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_NAME));
                senderChatInfo.setEmail(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_EMAIL));
                senderChatInfo.setCreatedAt(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_CREATEDAT));
                senderChatInfo.setApp_id(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_APP_ID));
                senderChatInfo.setAvatar(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_AVATAR));
                senderChatInfo.setPassword(getStringValue(itemObject,
                        WebServiceConfig.KEY_SENDER_CHAT_PASSWORD));
            }
            thongBaoInfo.setSenderChatInfo(senderChatInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return thongBaoInfo;
    }

    //  ------------------- parser typing ----------------
    public static SenderChatInfo parserTyping(String json) {
        SenderChatInfo thongBaoInfo = new SenderChatInfo();
        try {
            JSONObject object = new JSONObject(json);
            JSONObject items = object.getJSONObject(WebServiceConfig.TAG_KEY_USER);
            thongBaoInfo.setRoom_id(object.getString(WebServiceConfig.TAG_KEY_ROOM_ID));
//                JSONObject itemObject = object.getJSONObject(WebServiceConfig.TAG_KEY_SENDER);
            thongBaoInfo.setUpdatedAt(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_UPDATEDAT));
            thongBaoInfo.setId(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_ID));
            thongBaoInfo.setPhone(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_PHONE));
            thongBaoInfo.setUsername(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_USERNAME));
            thongBaoInfo.setName(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_NAME));
            thongBaoInfo.setEmail(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_EMAIL));
            thongBaoInfo.setCreatedAt(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_CREATEDAT));
            thongBaoInfo.setApp_id(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_APP_ID));
            thongBaoInfo.setAvatar(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_AVATAR));
            thongBaoInfo.setPassword(getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_PASSWORD));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return thongBaoInfo;
    }

    public static String parserRoomGroupId(String json) {
        String id = "";
        try {
            JSONObject object = new JSONObject(json);
            JSONObject items = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
            id = getStringValue(items,
                    WebServiceConfig.KEY_SENDER_CHAT_ID);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }
//    public static ArrayList<TinNhanInfo> parselistTinNhan(String json) {
//        ArrayList<TinNhanInfo> arrayList = new ArrayList<TinNhanInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                TinNhanInfo tinNhanInfo = new TinNhanInfo();
//                tinNhanInfo.setNguoi_gui(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_SENDER));
//                tinNhanInfo.setNguoi_gui_ten(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_SENDER_NAME));
//                tinNhanInfo.setNguoi_gui_avatar(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_SENDER_AVATAR));
//                tinNhanInfo.setThoi_gian(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_TIME));
//                tinNhanInfo.setTrang_thai(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_TRANG_THAI).equals("1") ? "0" : "1");
//                tinNhanInfo.setId_noi_dung(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_SENDER));
//                tinNhanInfo.setIs_group(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_IS_GROUP));
//
//                String group_info = items.toString();
//                String thanhvien = "";
//                String Last_sender = "";
//                String Last_sender_name = "";
//                String Admin = "";
//                if (group_info.contains("group_user")) {
//                    JSONObject object_group_info = new JSONObject(group_info);
//                    JSONObject item_group_info = object_group_info.getJSONObject(WebServiceConfig.TAG_KEY_GROUP_INFO);
//                    thanhvien = getStringValue(item_group_info,
//                            WebServiceConfig.TAG_KEY_THANH_VIEN_NHOM);
//                    Last_sender = getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_LAST_SENDER);
//                    Last_sender_name = getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_LAST_SENDER_NAME);
//                    Admin = getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_ADMIN);
//                }
//
//                tinNhanInfo.setLast_sender(Last_sender);
//                tinNhanInfo.setLast_sender_name(Last_sender_name);
//                tinNhanInfo.setNoi_dung(getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_IS_GROUP).equals("1") ? (!Last_sender_name.trim().equals("") ? (Last_sender_name.trim().substring(Last_sender_name.trim().lastIndexOf(" ") + 1) + ": " + getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_CONTENT)) : getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_CONTENT)) : getStringValue(items,
//                        WebServiceConfig.KEY_TINNHAN_CONTENT));
//                tinNhanInfo.setAdmin(Admin);
//                tinNhanInfo.setThanh_vien(thanhvien);
//                arrayList.add(tinNhanInfo);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return arrayList;
//    }
//
//    public static ArrayList<TinNhanInfo> parseItemTinNhan(String key_index,
//                                                          String json) {
//        ArrayList<TinNhanInfo> arrayList = new ArrayList<TinNhanInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                if (getStringValue(items,
//                        WebServiceConfig.KEY_CHAT_SENDER).trim().equals(key_index.trim()) || getStringValue(items,
//                        WebServiceConfig.KEY_CHAT_RECEIVER).trim().equals(key_index.trim())) {
//                    TinNhanInfo tinNhanInfo = new TinNhanInfo();
//                    String nguoi_gui = "";
////                    if (getStringValue(items,
////                            WebServiceConfig.KEY_TINNHAN_IS_GROUP).equals("1"))
////                    {
////                        nguoi_gui = getStringValue(items,
////                                WebServiceConfig.KEY_TINNHAN_SENDER);
////                    }else
////                    {
//                    nguoi_gui = getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER).trim().equals(key_index.trim()) ? getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_RECEIVER) : getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER);
////                    }
//                    tinNhanInfo.setNguoi_gui(nguoi_gui);
//                    tinNhanInfo.setNguoi_gui_ten(getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER).trim().equals(key_index.trim()) ? getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_RECEIVER_NAME) : getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER_NAME));
//                    tinNhanInfo.setNguoi_gui_avatar(getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER).trim().equals(key_index.trim()) ? getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_RECEIVER_AVATAR) : getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_SENDER_AVATAR));
//                    tinNhanInfo.setNoi_dung(getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_IS_GROUP).equals("1") ? (!getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_LAST_SENDER_NAME).trim().equals("") ? (getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_LAST_SENDER_NAME).trim().substring(getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_LAST_SENDER_NAME).trim().lastIndexOf(" ") + 1) + ": " + getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_CONTENT)) : getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_CONTENT)) : getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_CONTENT));
//                    tinNhanInfo.setThoi_gian(getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_TIME));
//                    tinNhanInfo.setTrang_thai("1");
//                    tinNhanInfo.setId_noi_dung((getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER).trim().equals(key_index.trim()) ? getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_RECEIVER) : getStringValue(items,
//                            WebServiceConfig.KEY_CHAT_SENDER)) + "_" + key_index);
//                    tinNhanInfo.setIs_group(getStringValue(items,
//                            WebServiceConfig.KEY_TINNHAN_IS_GROUP));
//
//                    arrayList.add(tinNhanInfo);
//                }
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return arrayList;
//    }


//    //------ parser Thong Bao --------------
//    public static ArrayList<ThongBaoInfo> parselistThongBao(String json) {
//        ArrayList<ThongBaoInfo> arrayList = new ArrayList<ThongBaoInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                ThongBaoInfo thongBaoInfo = new ThongBaoInfo();
//                thongBaoInfo.setThoi_gian(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_THOI_GIAN));
//                thongBaoInfo.setId_thong_bao(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_ID));
//                thongBaoInfo.setTen(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_TEN));
//                thongBaoInfo.setNoi_dung(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_NOI_DUNG));
//                thongBaoInfo.setEschool_id(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_ESCHOOL_ID));
//                thongBaoInfo.setPhone(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_PHONE));
//                thongBaoInfo.setEschool_time(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_ESCHOOL_TIME));
//                thongBaoInfo.setTrang_thai(getStringValue(items,
//                        WebServiceConfig.KEY_THONGBAO_DA_XOA).equals("1") ? "3" : "1");
//                arrayList.add(thongBaoInfo);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return arrayList;
//    }
//
//    public static ThongBaoInfo parseItemThongBao(String json) {
//        ThongBaoInfo thongBaoInfo = new ThongBaoInfo();
//
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            thongBaoInfo.setThoi_gian(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_THOI_GIAN));
//            thongBaoInfo.setId_thong_bao(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_ID));
//            thongBaoInfo.setTen(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_TEN));
//            thongBaoInfo.setNoi_dung(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_NOI_DUNG));
//            thongBaoInfo.setEschool_id(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_ESCHOOL_ID));
//            thongBaoInfo.setPhone(getStringValue(item,
//                    WebServiceConfig.KEY_THONGBAO_PHONE));
//            thongBaoInfo.setTrang_thai("1");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return thongBaoInfo;
//    }
//
//    //------ parser dang nhap --------------
//    public static ProfileInfo parseLoginProfile(String json) {
//        ProfileInfo profile = new ProfileInfo();
//        ArrayList<ProfileChildrenInfo> list;
//        ArrayList<ClassInfo> listClass;
//        ArrayList<SchoolInfo> schoolInfos;
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            profile.setThanh_pho(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_THANH_PHO));
//            profile.setMa_so(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_MA_SO));
//            profile.setMa_phong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_MA_PHONG));
//            profile.setPhone(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_PHONE));
//            profile.setTen_phu_huynh(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_PHU_HUYNH));
//            profile.setNgay_sinh(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_NGAY_SINH));
//            profile.setGioitinh(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_GIOITINH));
//            profile.setAvatar(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_AVATAR));
//            profile.setMa(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_MA));
//            profile.setKey_index(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_KEY_INDEX));
//            profile.setLast_update(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_LAST_UPDATE));
//            profile.setQuan_huyen(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_QUAN_HUYEN));
//            profile.setEmail(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_EMAIL));
//            profile.setTen_hien_thi(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_HIEN_THI));
//            profile.setDia_chi(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_DIA_CHI));
//            profile.setTen_dang_nhap(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_DANG_NHAP));
//            profile.setPassword(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_PASSWORD));
//            profile.setTen_lop(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_LOP));
//            profile.setMa_lop(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_MA_LOP));
//            profile.setTen_truong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_TRUONG));
//            profile.setTinh_diem(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TINH_DIEM));
//            profile.setMa_truong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_MA_TRUONG));
//            profile.setId_truong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_ID_TRUONG));
//            profile.setLop_id(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_ID_LOP));
//            profile.setTen_mon_hoc(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_MON_HOC));
//            profile.setSetting_display(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_SETTING_DISPLAY));
//            profile.setSo_truong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_SO_TRUONG));
//            profile.setHieu_truong(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_HIEU_TRUONG));
//            profile.setNam_hoc(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_NAM_HOC));
//            profile.setTen_nam_hoc(getStringValue(item,
//                    WebServiceConfig.KEY_PROFILE_TEN_NAM_HOC));
//            profile.setSocket_id(getStringValue(item, WebServiceConfig.KEY_PROFILE_SOCKET_ID));
//            list = new ArrayList<ProfileChildrenInfo>();
//            String sub = item.toString();
//            if (sub.contains("children")) {
//                JSONObject objectsub = new JSONObject(sub);
//                JSONArray arrjson2 = item.getJSONArray(WebServiceConfig.TAG_KEY_CHILDREN);
//
//                if (arrjson2 != null) {
//                    for (int j = 0; j < arrjson2.length(); j++) {
//                        ProfileChildrenInfo bditem = new ProfileChildrenInfo();
//                        JSONObject item2 = arrjson2.getJSONObject(j);
//
//                        bditem.setMa_lop(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_MA_LOP));
//                        bditem.setTen_hoc_sinh(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_TEN_HOC_SINH));
//                        bditem.setMa_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_MA_TRUONG));
//                        bditem.setId_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_ID_TRUONG));
//                        bditem.setTen_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_TEN_TRUONG));
//                        bditem.setTinh_diem(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_TINH_DIEM));
//                        bditem.setTen_lop(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_TEN_LOP));
//                        bditem.setChild_avatar(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_CHILD_AVATAR));
//                        bditem.setChild_ma(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_MA));
//                        bditem.setChild_key_index(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_KEY_INDEX));
//                        bditem.setId_lop(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_ID_LOP));
//                        bditem.setMa_so(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_MA_SO));
//                        bditem.setMa_phong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CHILDREN_MA_PHONG));
//                        list.add(bditem);
//
//                    }
//                }
//            }
//
//            listClass = new ArrayList<ClassInfo>();
//            String subClass = item.toString();
//            if (subClass.contains("danh_sach_lop")) {
////                JSONObject objectsub = new JSONObject(sub);
//                JSONArray arrjson3 = item.getJSONArray(WebServiceConfig.TAG_KEY_DANH_SACH_LOP);
//
//                if (arrjson3 != null) {
//                    for (int j = 0; j < arrjson3.length(); j++) {
//                        ClassInfo bditem = new ClassInfo();
//                        JSONObject item2 = arrjson3.getJSONObject(j);
//
//                        bditem.setTen_lop(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_TEN_LOP));
//                        bditem.setKey_index(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_KEY_INDEX));
//                        bditem.setTen_mon_hoc(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_LOP_TEN_MON_HOC));
//                        bditem.setMa_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_MA_TRUONG));
//                        bditem.setId_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_ID_TRUONG));
//                        bditem.setMa_lop(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_MA_LOP));
//                        bditem.setSo_hoc_sinh(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_SO_HOC_SINH));
//                        bditem.setTen_truong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_TEN_TRUONG));
//                        bditem.setTinh_diem(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_TINH_DIEM));
//                        bditem.setLop_id(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_LOP_ID));
//                        bditem.setMa_so(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_MA_SO));
//                        bditem.setMa_phong(getStringValue(item2,
//                                WebServiceConfig.KEY_PROFILE_CLASS_MA_PHONG));
//                        listClass.add(bditem);
//
//                    }
//                }
//            }
//
//            schoolInfos = new ArrayList<SchoolInfo>();
//            String subHieutruong = item.toString();
//            if (subHieutruong.contains("danh_sach_truong_hieu_truong")) {
//                JSONArray arrjson4 = item.getJSONArray(WebServiceConfig.TAG_KEY_DANH_SACH_TRUONG_HIEU_TRUONG);
//
//                if (arrjson4 != null) {
//                    for (int j = 0; j < arrjson4.length(); j++) {
//                        SchoolInfo bditem = new SchoolInfo();
//                        JSONObject item3 = arrjson4.getJSONObject(j);
//
//                        bditem.setMa_truong(getStringValue(item3,
//                                WebServiceConfig.KEY_PROFILE_HIEUTRUONG_MA_TRUONG));
//                        bditem.setTen_truong(getStringValue(item3,
//                                WebServiceConfig.KEY_PROFILE_HIEUTRUONG_TEN_TRUONG));
//                        bditem.setId_truong(getStringValue(item3,
//                                WebServiceConfig.KEY_PROFILE_HIEUTRUONG_ID_TRUONG));
//
//                        schoolInfos.add(bditem);
//
//                    }
//                }
//            }
//            profile.setProfileChildrenInfoArrayList(list);
//            profile.setClassInfoArrayList(listClass);
//            profile.setSchoolInfoArrayList(schoolInfos);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return profile;
//    }
//
//
//    //------ parser danh ba --------------
//    public static ArrayList<DanhBaInfo> parselistDanhBaGV(String json, String id_lop) {
//        ArrayList<DanhBaInfo> listbangtininfo = new ArrayList<DanhBaInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                DanhBaInfo productitem = new DanhBaInfo();
//                productitem.setTen_dang_nhap(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_DANG_NHAP));
//                productitem.setTen_hien_thi(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_HIEN_THI));
//                productitem.setAvatar(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_AVATAR));
//                productitem.setId_lop_hoc((!id_lop.equals("")) ? id_lop : getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_ID_LOP_HOC));
//                productitem.setPhone(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_PHONE));
//                productitem.setEmail(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_EMAIL));
//                productitem.setSetting_display(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_SETTING_DISPLAY));
//                productitem.setTen_lop(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_LOP));
//                productitem.setTrang_thai(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TRANGTHAI));
//                productitem.setGioi_tinh(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_GIOI_TINH));
//                productitem.setBadges(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_BADGES));
//                productitem.setLast_update(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_LAST_UPDATE));
//                productitem.setKey_index(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_KEY_INDEX));
//                productitem.setMa(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA));
//                productitem.setMo_ta(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_MON_HOC));
//                productitem.setTen_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_NHA_TRUONG));
//                productitem.setMa_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA_TRUONG));
//                productitem.setLoai_tai_khoan("2");
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }
//
//    public static ArrayList<DanhBaInfo> parselistDanhBaPH(String json) {
//        ArrayList<DanhBaInfo> listbangtininfo = new ArrayList<DanhBaInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                DanhBaInfo productitem = new DanhBaInfo();
//                productitem.setTen_dang_nhap(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_DANG_NHAP));
//                productitem.setTen_hien_thi(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_HIEN_THI));
//                productitem.setAvatar(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_AVATAR));
//                productitem.setId_lop_hoc(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_ID_LOP_HOC));
//                productitem.setPhone(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_PHONE));
//                productitem.setEmail(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_EMAIL));
//                productitem.setSetting_display(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_SETTING_DISPLAY));
//                productitem.setTen_lop(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_LOP));
//                productitem.setTrang_thai(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TRANGTHAI));
//                productitem.setGioi_tinh(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_GIOI_TINH));
//                productitem.setBadges(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_BADGES));
//                productitem.setLast_update(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_LAST_UPDATE));
//                productitem.setKey_index(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_KEY_INDEX));
//                productitem.setMa(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA));
//                productitem.setMo_ta(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_HOC_SINH));
//                productitem.setTen_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_NHA_TRUONG));
//                productitem.setMa_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA_TRUONG));
//                productitem.setLoai_tai_khoan("3");
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }
//
//
//    public static ArrayList<DanhBaInfo> parselistDanhBaHS(String json) {
//        ArrayList<DanhBaInfo> listbangtininfo = new ArrayList<DanhBaInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                DanhBaInfo productitem = new DanhBaInfo();
//                productitem.setTen_dang_nhap(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_DANG_NHAP));
//                productitem.setTen_hien_thi(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_HIEN_THI));
//                productitem.setAvatar(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_AVATAR));
//                productitem.setId_lop_hoc(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_ID_LOP_HOC));
//                productitem.setPhone(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_PHONE));
//                productitem.setEmail(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_EMAIL));
//                productitem.setSetting_display(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_SETTING_DISPLAY));
//                productitem.setTen_lop(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_LOP));
//                productitem.setTrang_thai(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TRANGTHAI));
//                productitem.setGioi_tinh(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_GIOI_TINH));
//                productitem.setBadges(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_BADGES));
//                productitem.setLast_update(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_LAST_UPDATE));
//                productitem.setKey_index(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_KEY_INDEX));
//                productitem.setMa(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA));
//                productitem.setMo_ta(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_PHU_HUYNH));
//                productitem.setTen_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_TEN_NHA_TRUONG));
//                productitem.setMa_nha_truong(getStringValue(items,
//                        WebServiceConfig.KEY_DANHBA_MA_TRUONG));
//                productitem.setLoai_tai_khoan("5");
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }
//
//    public static ArrayList<TinNhanInfo> parselistDanhSachNhom(String json) {
//        ArrayList<TinNhanInfo> listbangtininfo = new ArrayList<TinNhanInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                TinNhanInfo productitem = new TinNhanInfo();
//                productitem.setNguoi_gui(getStringValue(items,
//                        WebServiceConfig.KEY_DANHSACHNHOM_ID));
//                productitem.setNguoi_gui_ten(getStringValue(items,
//                        WebServiceConfig.KEY_DANHSACHNHOM_NAME));
//                productitem.setNguoi_gui_avatar(getStringValue(items,
//                        WebServiceConfig.KEY_DANHSACHNHOM_AVATAR));
//                productitem.setAdmin(getStringValue(items,
//                        WebServiceConfig.KEY_DANHSACHNHOM_ADMIN));
//                productitem.setThanh_vien(getStringValue(items,
//                        WebServiceConfig.KEY_DANHSACHNHOM_USERS));
//
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }


//    public static GroupInfo parerGroupInfo(String json) {
//        GroupInfo groupInfo = new GroupInfo();
//
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            groupInfo.setName(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_NAME));
//            groupInfo.setId(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_ID));
//            groupInfo.setAvatar(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_AVATAR));
//            groupInfo.setAdmin(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_ADMIN));
//            groupInfo.setAdmin_name(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_ADMIN_NAME));
//            groupInfo.setAdmin_avatar(getStringValue(item,
//                    WebServiceConfig.KEY_GROUPINFO_ADMIN_AVATAR));
//            groupInfo.setThanh_vien(getStringValue(item,
//                    WebServiceConfig.TAG_KEY_THANH_VIEN_NHOM));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return groupInfo;
//    }
//
//
//    public static ArrayList<ThanhVienNhomInfo> parselistThanhVienNhom(String json) {
//        ArrayList<ThanhVienNhomInfo> listbangtininfo = new ArrayList<ThanhVienNhomInfo>();
//        try {
//
////            JSONObject object = new JSONObject(json);
////            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
////            String group_user = "{\"group_user\":" + getStringValue(item,
////                    WebServiceConfig.TAG_KEY_THANH_VIEN_NHOM) + "}";
//
//            JSONObject objects = new JSONObject(json);
//            JSONArray items = objects.getJSONArray(WebServiceConfig.TAG_KEY_THANH_VIEN_NHOM);
//            for (int i = 0; i < items.length(); i++) {
//                JSONObject item1 = items.getJSONObject(i);
//                ThanhVienNhomInfo productitem = new ThanhVienNhomInfo();
//                productitem.setName(getStringValue(item1,
//                        WebServiceConfig.KEY_THANHVIENNHOM_NAME));
//                productitem.setAvatar(getStringValue(item1,
//                        WebServiceConfig.KEY_THANHVIENNHOM_AVATAR));
//                productitem.setKey_index(getStringValue(item1,
//                        WebServiceConfig.KEY_THANHVIENNHOM_KEY_INDEX));
//
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }
//
//    public static SettingInfo parSetting(String json) {
//        SettingInfo settingInfo = new SettingInfo();
//
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONObject item = object.getJSONObject(WebServiceConfig.TAG_KEY_DATA);
//            settingInfo.setDia_chi(getStringValue(item,
//                    WebServiceConfig.KEY_SETTING_DIA_CHI));
//            settingInfo.setNgay_sinh(getStringValue(item,
//                    WebServiceConfig.KEY_SETTING_NGAY_SINH));
//            settingInfo.setPhone(getStringValue(item,
//                    WebServiceConfig.KEY_SETTING_PHONE));
//            settingInfo.setEmail(getStringValue(item,
//                    WebServiceConfig.KEY_SETTING_EMAIL));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return settingInfo;
//    }

    //------ parser hoat dong --------------
//    public static ArrayList<HoatDongInfo> parselistHoatDong(String json) {
//        ArrayList<HoatDongInfo> listbangtininfo = new ArrayList<HoatDongInfo>();
//        ArrayList<YeuThichInfo> listYeuThichInfos = new ArrayList<YeuThichInfo>();
//        try {
//            JSONObject object = new JSONObject(json);
//            JSONArray item = object.getJSONArray(WebServiceConfig.TAG_KEY_DATA);
//            for (int i = 0; i < item.length(); i++) {
//                JSONObject items = item.getJSONObject(i);
//                HoatDongInfo productitem = new HoatDongInfo();
//                productitem.setId(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_ID));
//                productitem.setHinh_anh_ten(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_HINH_ANH_TEN));
//                productitem.setYeu_thich_tong(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_YEU_THICH_TONG));
//                productitem.setHinh_anh_url(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_HINH_ANH_URL));
//                productitem.setGiao_vien_ten(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_GIAO_VIEN_TEN));
//                productitem.setGiao_vien_id(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_GIAO_VIEN_ID));
//                productitem.setLop_hoc_ten(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_LOP_HOC_TEN));
//                productitem.setLop_hoc_id(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_LOP_HOC_ID));
//                productitem.setYoutube(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_YOUTUBE));
//                productitem.setLoai_hoat_dong(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_LOAI_HOAT_DONG));
//                productitem.setThoi_gian(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_THOI_GIAN));
//                productitem.setNoi_dung(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_NOI_DUNG));
//                productitem.setAvatar(getStringValue(items,
//                        WebServiceConfig.KEY_HOATDONG_AVATAR));
//                listYeuThichInfos = new ArrayList<YeuThichInfo>();
//                String subClass = (getStringValue(items,
//                        WebServiceConfig.TAG_KEY_YEU_THICH));
//                if (subClass.contains("ten")) {
//                    JSONArray arrjson2 = items.getJSONArray(WebServiceConfig.TAG_KEY_YEU_THICH);
//                    if (arrjson2 != null) {
//                        for (int j = 0; j < arrjson2.length(); j++) {
//
//                            YeuThichInfo bditem = new YeuThichInfo();
//                            JSONObject item2 = arrjson2.getJSONObject(j);
//
//                            bditem.setThoi_gian(getStringValue(item2,
//                                    WebServiceConfig.KEY_YEUTHICH_THOI_GIAN));
//                            bditem.setId(getStringValue(item2,
//                                    WebServiceConfig.KEY_YEUTHICH_ID));
//                            bditem.setTen(getStringValue(item2,
//                                    WebServiceConfig.KEY_YEUTHICH_TEN));
//                            bditem.setThoi_gian_txt(getStringValue(item2,
//                                    WebServiceConfig.KEY_YEUTHICH_THOI_GIAN_TXT));
//                            listYeuThichInfos.add(bditem);
//
//                        }
//                    }
//                }
//                productitem.setYeuThichInfoArrayList(listYeuThichInfos);
//                listbangtininfo.add(productitem);
//            }
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//        return listbangtininfo;
//    }


    //
    // // ========================= CORE FUNCTIONS ===========================
    //
    // /**
    // * Extract user information
    // *
    // * @param obj
    // * @param key
    // * @return
    // */
    public static String getStringValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? "" : obj.getString(key);
        } catch (JSONException e) {
            return "";
        }
    }

    /**
     * Get long value
     *
     * @param obj
     * @param key
     * @return
     */
    public static long getLongValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? 0L : obj.getLong(key);
        } catch (JSONException e) {
            return 0L;
        }
    }

    /**
     * Get int value
     *
     * @param obj
     * @param key
     * @return
     */
    public static int getIntValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? 0 : obj.getInt(key);
        } catch (JSONException e) {
            return 0;
        }
    }

    /**
     * Get Double
     *
     * @param obj
     * @param key
     * @return
     */
    public static Double getDoubleValue(JSONObject obj, String key) {
        double d = 0.0;
        try {
            return obj.isNull(key) ? d : obj.getDouble(key);
        } catch (JSONException e) {
            return d;
        }
    }

    /**
     * Get boolean
     *
     * @param obj
     * @param key
     * @return
     */
    public static boolean getBooleanValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? false : obj.getBoolean(key);
        } catch (JSONException e) {
            return false;
        }
    }

    @SuppressWarnings("deprecation")
    public static String JsonDateToDate(String jsonDate) {
        // "/Date(1321867151710)/"
        int idx1 = jsonDate.indexOf("(");
        int idx2 = jsonDate.indexOf(")");
        String s = jsonDate.substring(idx1 + 1, idx2);
        long l = Long.valueOf(s);
        Date result = new Date(l);
        int Year = result.getYear() + 1900;
        return result.getDate() + "/" + result.getMonth() + "/" + Year;
    }

}
