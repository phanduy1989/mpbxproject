package com.viettel;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;

import com.deploygate.sdk.DeployGate;

import java.text.SimpleDateFormat;

public class GlobalInfo extends Application{

	public static final String ROLE_ADMIN = "MOBILE_PBX_ADMIN";


	public static interface ServerConfig {
		public static final String DOMAIN = "http://171.244.9.8:8080/mpbx/";

//		public static final String DOMAIN = "http://10.58.71.230:9002/mpbx/";

//		public static final String DOMAIN = "http://10.58.71.230:9002/mpbx/";
//		public static final String DOMAIN = "http://203.190.170.35:8888/mpbx/";
//		public static final String DOMAIN = "http://203.190.170.135:8888/mpbx/";

		public static final String DATE_FORMAT_INBOX = "yyyy-MM-dd'T'HH:mm:ss";
//		public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
//		public static final String DATE_FORMAT = "yyyyMMddHHmmss";
		public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
	}

	public static interface AppConfig {
//		public static boolean isTestAccount = true;
		public static boolean isTestAccount = false;
		public static boolean isTestSynchro = true;
		public static int avatarBorderSize = 0;
		public static int avatarCorner = 15;
	}

	public static final String BUNDLE_KEY_IS_UPDATE = "IS_UPDATE";

	public static final String BUNDLE_KEY_CONVERSATION = "CONVERSATION";
	public static final String BUNDLE_KEY_MODE_CREATE_CUSTOMER = "CREATE_CUSTOMER";
	public static final String BUNDLE_KEY_NAME = "BUNDLE_KEY_NAME";
	public static final String BUNDLE_KEY_CUSTOMER_ID = "CUSTOMER_ID";
	public static final String BUNDLE_KEY_PHONE_NUMBER = "PHONE_NUMBER";
	public static final String BUNDLE_KEY_COLLEAGUE_ID = "COLLEAGUE_ID";
	public static final String BUNDLE_KEY_COLLEAGUE_OBJ = "COLLEAGUE_OBJ";
	public static final String BUNDLE_KEY_GROUP_ID = "GROUP_ID";
	public static final String BUNDLE_KEY_LIST_GROUP = "GROUP_LIST";
	public static final String BUNDLE_KEY_GROUP_NAME = "GROUP_NAME";
	public static final String BUNDLE_KEY_COLLEAGUE_ACTION = "COLLEAGUE_ACTION";
	public static final String BUNDLE_KEY_LIST_TYPE = "LIST_TYPE";
	public static final String BUNDLE_KEY_SUCCESS = "SUCCESS";
	public static final String BUNDLE_KEY_CONTACT = "CONTACT";
	public static final String BUNDLE_KEY_LIST_CONTACT = "LIST_CONTACT";
	public static final String BUNDLE_KEY_LIST_MEMBER = "LIST_MEMBER";
	public static final String BUNDLE_KEY_LIST_NON_MEMBER = "LIST_NON_MEMBER";
	public static final String BUNDLE_KEY_MODE = "MODE";
	public static final String BUNDLE_KEY_MODE_MULTI_SELECT = "MODE_MULTI_SELECT";
	public static final String BUNDLE_KEY_MODE_SYNCHROS = "MODE_SYNCHROS";

	public static final int REQUEST_CODE_DETAIL = 9669;
	public static final int REQUEST_CODE_CREATE = 6996;
	public static final int REQUEST_CODE_ACCOUNT = 6699;

	public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
	public static final SimpleDateFormat simpleDateFormatDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String dateFormateSendServer = "dd-MM-yyyy HH:mm:ss";
	public static final SimpleDateFormat simpleDateSendServer = new SimpleDateFormat(dateFormateSendServer);

	private static GlobalInfo instance = null;
	Context appContext;// application context
	Context activityContext;// activity context
	
	public static LayoutInflater globalInflater = null;
	public static Activity globalActivity = null;
	
	public static void setActivity(Activity activity) {
		if(globalActivity == null) {
			globalActivity = activity;
		}
	}
	
	public static void setLayoutInflater(LayoutInflater layoutInflater) {
		if(globalInflater == null) {
			globalInflater = layoutInflater;
		}
	}

	public static GlobalInfo getInstance() {
		if (instance == null) {
			instance = new GlobalInfo();
		}
		return instance;
	}

	public void setAppContext(Context context) {
		this.appContext = context;

	}

	public Context getAppContext() {
		if (appContext == null) {
			appContext = new GlobalInfo();
		}
		return appContext;
	}

	public void setActivityContext(Context context) {
		this.activityContext = context;
	}

	public Context getActivityContext() {
		return activityContext;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		DeployGate.install(this);
	}


}
