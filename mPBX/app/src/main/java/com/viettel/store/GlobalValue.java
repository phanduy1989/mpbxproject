package com.viettel.store;

import android.content.Context;
import android.net.Uri;

import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.CustomerModel;
import com.viettel.model.SpinnerModel;
import com.viettel.model.UserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import com.viettel.mpbx.R;


/**
 * GlobalValue class contains global static values
 */
public final class GlobalValue {

    private static GlobalValue instance = null;

    public static final String ACCOUNT_FB = "FB";
    public static final String ACCOUNT_IDOCTOR = "IDOCTOR";

    public static final String APP_NAME = "GNN_EXPRESS";

    public static UserProfile userProfile;
    public static Uri ringToneUri;
    public static int timeDataReload = 5, alertRepeatTime = 20;
    public static int minConfig = 0;

    public static String currentDeviceUUID;

    private static HashMap<String, String> hashMap;

    public static ArrayList<SpinnerModel> listGender;
    public static ArrayList<SpinnerModel> listTimeLoadData;

    public static ArrayList<ColleagueGroup> listColleageGroup;
    public static ArrayList<ColleagueModel> listColleageMember;
    public static ColleagueGroup currentCollectGroup;

    public static ArrayList<CustomerModel> listCustomer;

    public static int agentId, bussinessId;
    public static String token, numberOfCompany;

    static {
        listGender = new ArrayList<>();
        listGender.add(new SpinnerModel(0, "Nam"));
        listGender.add(new SpinnerModel(1, "Nữ"));
        listTimeLoadData = new ArrayList<>();
        listTimeLoadData.add(new SpinnerModel(30, "30 giây"));
        listTimeLoadData.add(new SpinnerModel(60, "1 phút"));
        listTimeLoadData.add(new SpinnerModel(90, "1.5 phút"));
        listTimeLoadData.add(new SpinnerModel(120, "2 phút"));
        listTimeLoadData.add(new SpinnerModel(300, "5 phút"));
    }

    public static GlobalValue getInstance() {
        if (instance == null) {
            instance = new GlobalValue();
        }
        return instance;
    }

    public static void removeGroup(ColleagueGroup colleagueGroup) {
        int index = -1;
        for (int i = 0, size = listColleageGroup.size(); i < size; i++) {
            ColleagueGroup cg = listColleageGroup.get(i);
            if (cg.getGroupId() == colleagueGroup.getGroupId()) {
                index = i;
                break;
            }
        }

        if (index >= 0)
            listColleageGroup.remove(index);

        if(currentCollectGroup != null && currentCollectGroup.getGroupId() == colleagueGroup.getGroupId()) {
            currentCollectGroup = null;
        }

        currentCollectGroup = getSelectedGroup();
    }

    public static ColleagueGroup getSelectedGroup() {

        if (currentCollectGroup != null) return currentCollectGroup;

        if (listColleageGroup == null || listColleageGroup.isEmpty()) {
            return null;
        }

        for (ColleagueGroup colleagueGroup : listColleageGroup) {
            if (colleagueGroup.isSelected()) {
                currentCollectGroup = colleagueGroup;
                return colleagueGroup;
            }
        }

        currentCollectGroup = listColleageGroup.get(0);
        currentCollectGroup.setSelected(true);
        return currentCollectGroup;
    }



    public static void changeCurrentGroup(ColleagueGroup colleagueGroup) {
        if (listColleageGroup == null || listColleageGroup.isEmpty() || colleagueGroup == null) return;

        for (ColleagueGroup colleagueGroup1 : listColleageGroup) {
            if (colleagueGroup1.getGroupId() == colleagueGroup.getGroupId()) {
                colleagueGroup1.setSelected(true);
                currentCollectGroup = colleagueGroup1;
            } else {
                colleagueGroup1.setSelected(false);
            }
        }
    }

    public static void initListColleagueMember() {
        if (listColleageGroup == null || listColleageGroup.isEmpty()) return;

        if (listColleageMember != null) {
            listColleageMember.clear();
        }

        HashMap<Integer, Integer> hashMap = new HashMap<>();

        for (ColleagueGroup colleagueGroup : listColleageGroup) {
            if (colleagueGroup.getListMember() != null && !colleagueGroup.getListMember().isEmpty()) {
                if (listColleageMember == null) {
                    listColleageMember = new ArrayList<>();
                }

                for(ColleagueModel colleagueModel : colleagueGroup.getListMember()) {
                    if(!hashMap.containsKey(colleagueModel.getColleagueId())) {
                        listColleageMember.add(colleagueModel);
                        hashMap.put(colleagueModel.getColleagueId(), colleagueModel.getColleagueId());
                    }
                }

//                listColleageMember.addAll(colleagueGroup.getListMember());
            }
        }
    }

    public static int getGenderPosition(int gender) {
        for (int i = 0, size = listGender.size(); i < size; i++) {
            SpinnerModel genderModel = listGender.get(i);
            if (gender == genderModel.getValue()) {
                return i;
            }
        }

        return 0;
    }

    public static int getTimeDataReloadPosition(int time) {
        for (int i = 0, size = listTimeLoadData.size(); i < size; i++) {
            SpinnerModel genderModel = listTimeLoadData.get(i);
            if (time == genderModel.getValue()) {
                return i;
            }
        }

        return 0;
    }

    public static void clearData() {
    }

    public static String getDayDisplay(Context context, String schedule) {
        if (hashMap == null) {

            hashMap = new HashMap<>();

            String[] arrayDayToServer = context.getResources().getStringArray(R.array.dayOfWeeksToServer);
            String[] arrayDayDisplay = context.getResources().getStringArray(R.array.dayOfWeekDisplay);

            for (int i = 0, length = arrayDayToServer.length; i < length; i++) {
                hashMap.put(arrayDayToServer[i], arrayDayDisplay[i]);
            }
        }

        String[] arraySchedule = schedule.split(Pattern.quote("-"));
        if (arraySchedule == null || arraySchedule.length == 0) {
            return schedule;
        } else {
            String timeScheduleDisplay = "";
            for (int i = 0, length = arraySchedule.length; i < length; i++) {
                if (hashMap.get(arraySchedule[i]) != null) {
                    if (timeScheduleDisplay.length() == 0) {
                        timeScheduleDisplay += hashMap.get(arraySchedule[i]);
                    } else {
                        timeScheduleDisplay += ", " + hashMap.get(arraySchedule[i]);
                    }
                }
            }

            if (timeScheduleDisplay.length() == 0) {
                return schedule;
            } else {
                return timeScheduleDisplay;
            }
        }
    }
}
