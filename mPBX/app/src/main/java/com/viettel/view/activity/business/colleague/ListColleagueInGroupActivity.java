package com.viettel.view.activity.business.colleague;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.SubpartAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.Agent;
import com.viettel.model.ColleagueModel;
import com.viettel.model.DepartmentModel;
import com.viettel.model.request.colleague.GetColleagueGroupCB2Request;
import com.viettel.model.request.colleague.GetColleagueInGroupRequest;
import com.viettel.model.response.colleague.GetColleagueGroupResponse;
import com.viettel.model.response.colleague.GetColleagueGroupSystemtResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.utils.AnimationUtil;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 11/22/16.
 */
public class ListColleagueInGroupActivity extends BABaseActivity {

    private ListView listView;
    private SubpartAdapter subpartAdapter;

    RelativeLayout layoutCall;
    TextView txtTitleHeader;

    private SwipeRefreshLayout swipeContainer;

    private Agent pickContact;

    private int groupId;
    private String groupName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        groupId = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, -1);
        groupName = intent.getStringExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME);

        setContentView(R.layout.layout_colleague_in_group_activity);
        initView();

        initData();
    }

    public void initView() {

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtTitleHeader.setText("" + groupName);

        listView = (ListView) findViewById(R.id.listView);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        if (swipeContainer != null) {
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initData();
                }
            });
            // Configure the refreshing colors
            swipeContainer.setColorScheme(R.color.main_green, R.color.main_green
                    , R.color.main_green, R.color.main_green);
        }


        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });
        layoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCallLayout();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = subpartAdapter.getItem(position);

                if(object instanceof DepartmentModel) {

                    DepartmentModel departmentModel = (DepartmentModel) object;

                    Intent intent = new Intent(ListColleagueInGroupActivity.this.getBaseContext(), ListColleagueInGroupActivity.class);
                    intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, departmentModel.getDeptId());
                    intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME, departmentModel.getDeptName());
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
//

            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                subpartAdapter.filter(text);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void goToColleagueInfo(Agent agent) {
        Intent intent = new Intent(ListColleagueInGroupActivity.this.getBaseContext(), ColleagueInfoActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ID, agent.getAgentId());
        intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, groupId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void goToChoseAvaiGroup(Object obj) {

        if(!(obj instanceof Agent)) {
            return;
        }

        Agent agent = (Agent) obj;

        final Intent intent = new Intent(ListColleagueInGroupActivity.this.getBaseContext(), ChoseAvaiColleagueGroupActivity.class);

        ColleagueModel colleagueModel = new ColleagueModel();
        colleagueModel.setAgentId(agent.getAgentId());
        colleagueModel.setColleagueName(agent.getEmployeeName());
        colleagueModel.setPhoneNumber(agent.getPhoneNumber());

        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_OBJ, colleagueModel);


        showLoading();

        GetColleagueGroupCB2Request getColleagueGroupRequest = new GetColleagueGroupCB2Request();
        getColleagueGroupRequest.setData("" + colleagueModel.getAgentId());

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupResponse responseObject = gson.fromJson(response, GetColleagueGroupResponse.class);


                    if (responseObject.getIsSuccess() == 1 && responseObject.getIsLogin() == 1) {
                        if(responseObject.getListGroup() != null && !responseObject.getListGroup().isEmpty()) {
                            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_GROUP, responseObject.getListGroup());
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        } else {
                            Toast.makeText(ListColleagueInGroupActivity.this, "Không tồn tại nhóm để thêm", Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        showError(signInResponse, true);
                        String message = null;

                        if(responseObject == null ) {
                            message = getResources().getString(R.string.text_no_network);
                        } else {

                            if(responseObject.getIsLogin() == -1) {
                                Toast.makeText(ListColleagueInGroupActivity.this, "Phiên làm việc đã hết hạn hoặc không hợp lệ!", Toast.LENGTH_SHORT).show();
                                goToLogin();
                                return;
                            }

                            message = responseObject.getMessage() != null && !responseObject.getMessage().isEmpty()
                                    ? responseObject.getMessage() : getResources().getString(R.string.text_no_network);
                        }

                        Toast.makeText(ListColleagueInGroupActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    showError(getResources().getString(R.string.text_no_network), true);
                    Toast.makeText(ListColleagueInGroupActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
//                showError(getResources().getString(R.string.text_no_network), true);
                Toast.makeText(ListColleagueInGroupActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void showLoading() {
        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
    }

    public void closeLoading() {
        swipeContainer.setRefreshing(false);
    }


    public void showError(String message, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        super.showError(message, isError);
    }
    public void showError(ResponseObject responseObject, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        super.showError(responseObject, isError);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void onClickRefresh(View v) {
        initData();
    }


    public void initData() {

        showLoading();

        GetColleagueInGroupRequest getColleagueGroupRequest = new GetColleagueInGroupRequest();
        getColleagueGroupRequest.setData(groupId);

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                Log.e("Response", "" + response);
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupSystemtResponse signInResponse = gson.fromJson(response, GetColleagueGroupSystemtResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(signInResponse);
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setListData(GetColleagueGroupSystemtResponse signInResponse) {

//        if(signInResponse. == null || listGroup.isEmpty()) {
//            showError(getResources().getString(R.string.alertNoColleagueFound), false);
//            return;
//        }

        subpartAdapter = new SubpartAdapter(this);
        listView.setAdapter(subpartAdapter);

        subpartAdapter.addData(signInResponse.getListDepart());
        subpartAdapter.addData(signInResponse.getListStaff());

    }

    OnChildSelectedListener onChildSelectedListener = new OnChildSelectedListener() {
        @Override
        public void onChildSelect(ColleagueModel menu) {

        }
    };



    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void goToColleagueInfo(ColleagueModel colleagueModel) {
        Intent intent = new Intent(ListColleagueInGroupActivity.this.getBaseContext(), ColleagueInfoActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ID, colleagueModel.getColleagueId());
        intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, colleagueModel.getGroupId());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }



    public void showCallLayout(Object obj) {
        pickContact = (Agent) obj;
        layoutCall.setVisibility(View.VISIBLE);
    }

    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public void onCallPrivate(View view) {
        callPrivatePhone(pickContact.getPhoneNumber());
    }

    public void onCallHost(View view) {
        callHostPhone(pickContact.getPhoneNumber());
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }

}
