package com.viettel.view.activity.business.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.ConversationMemberAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationMember;
import com.viettel.model.ConversationModel;
import com.viettel.model.request.conference.GetConversationInfoRequest;
import com.viettel.model.response.conference.GetConferenceInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.DateUtil;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by duyuno on 7/13/17.
 */
public class ConversationInviteActivity extends BABaseActivity {

    ConversationModel conversationModel;

    TextView txtTitleHeader;
    TextView txtStartTime, txtEndTime;

    RelativeLayout buttonHeaderRight;

    LinearLayout layoutStartTime, layoutEndTime, layoutTime;

    ListView listView;
    ConversationMemberAdapter conversationMemberAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_info_activity);

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);
        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        layoutStartTime = (LinearLayout) findViewById(R.id.layoutStartTime);
        layoutEndTime = (LinearLayout) findViewById(R.id.layoutEndTime);
        layoutTime = (LinearLayout) findViewById(R.id.layoutTime);
        listView = (ListView) findViewById(R.id.listView);
        buttonHeaderRight = (RelativeLayout) findViewById(R.id.buttonHeaderRight);

        txtTitleHeader.setSelected(true);

        conversationModel = getIntent().getParcelableExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION);

        txtTitleHeader.setText(conversationModel.getConferenceName());
        txtStartTime.setText("Thời gian bắt đầu: " + DateUtil.convertFullDate(conversationModel.getStartDate()));
        switch (conversationModel.getConversationType()) {
            case ConversationGroup.FINISHED:
                txtEndTime.setText("Thời gian kết thúc: " + DateUtil.convertFullDate(conversationModel.getEndDate()));
                buttonHeaderRight.setVisibility(View.GONE);
                break;
            case ConversationGroup.UP_COMMING:
                layoutEndTime.setVisibility(View.GONE);
                break;
            case ConversationGroup.ON_GOING:
                layoutStartTime.setVisibility(View.GONE);
                layoutEndTime.setVisibility(View.GONE);
                layoutTime.setVisibility(View.VISIBLE);
                buttonHeaderRight.setVisibility(View.GONE);
                setTime(conversationModel.getStartDate());
                break;
        }

        conversationMemberAdapter = new ConversationMemberAdapter(this, conversationModel.getConversationType(), conversationModel.getConferenceId());
        listView.setAdapter(conversationMemberAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void onClickHeaderRight(View v) {
        goToCreateConference();
    }

    public void initData() {
        showLoading();

        GetConversationInfoRequest getConversationInfoRequest = new GetConversationInfoRequest();
        getConversationInfoRequest.setData(conversationModel.getConferenceId());

        ApiController.doPostRequest(this, getConversationInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {


                closeLoading();

                if(response != null) {
                    Log.e("Response", "" + response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceInfoResponse signInResponse = gson.fromJson(response, GetConferenceInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(signInResponse.getListMember());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setListData(ArrayList<ConversationMember> list) {
        conversationMemberAdapter.setData(list);
    }

    public void goToCreateConference() {
        Intent intent = new Intent(ConversationInviteActivity.this.getBaseContext(), CreateConversationActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE, CreateConversationActivity.MODE_UPDATE);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, conversationModel);
        ArrayList<ConversationMember> listMember = conversationMemberAdapter.getListData();
        if(listMember != null) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT, listMember);
        }

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void setTime(Date date) {
        TextView hour1 = (TextView) layoutTime.findViewById(R.id.hour1);
        TextView hour2 = (TextView) layoutTime.findViewById(R.id.hour2);
        TextView minute1 = (TextView) layoutTime.findViewById(R.id.minute1);
        TextView minute2 = (TextView) layoutTime.findViewById(R.id.minute2);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        hour1.setText("" + (hour / 10));
        hour2.setText("" + (hour % 10));
        minute1.setText("" + (minute / 10));
        minute2.setText("" + (minute % 10));
    }

    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void onClickBtnInvite(View view) {

    }
    public void onClickBtnRefresh(View view) {
        initData();
    }
}
