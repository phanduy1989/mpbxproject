package com.viettel.view.activity.business.colleague;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.DepartAdapter;
import com.viettel.adapter.ExpandableListAdapter;
import com.viettel.adapter.ListColleagueAdapter;
import com.viettel.adapter.holder.ColleagueItem;
import com.viettel.api.ApiController;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.OnDepartSelectedListener;
import com.viettel.interfaces.OnGroupSelectedListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.Agent;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.DepartmentModel;
import com.viettel.model.request.colleague.GetColleagueGroupCB2Request;
import com.viettel.model.request.colleague.GetColleagueGroupRequest;
import com.viettel.model.request.colleague.GetColleagueGroupSystemRequest;
import com.viettel.model.response.colleague.GetColleagueGroupResponse;
import com.viettel.model.response.colleague.GetColleagueGroupSystemtResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.AnimationUtil;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/22/16.
 */
public class ListColleagueActivity extends BABaseActivity {

    private static final int ACCOUNT_MODE = 0;
    private static final int SYSTEM_MODE = 1;

    private int mode;

    private ListView listViewSystem;
    private ExpandableListView listViewAccount;
    private ExpandableListAdapter expandableAdapter;
    private DepartAdapter expandableAdapterSystem;

    RelativeLayout layoutCall;

    Button btnAccount, btnSystem;
    ViewSwitcher viewSwitcher;

    private SwipeRefreshLayout swipeContainerAccount, swipeContainerSystem;

    Animation slide_in_right, slide_out_right, slide_in_left, slide_out_left;

    private ColleagueModel pickContact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_colleague_activity);
        initView();

        initData();
    }

    public void initView() {
        slide_in_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        slide_out_right = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        slide_in_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        slide_out_left = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);
        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        btnAccount = (Button) findViewById(R.id.btnAccount);
        btnSystem = (Button) findViewById(R.id.btnSystem);

        listViewAccount = (ExpandableListView) findViewById(R.id.listViewAccount);
        listViewAccount.setGroupIndicator(null);

        listViewSystem = (ListView) findViewById(R.id.listViewSystem);
        listViewSystem.setAdapter(expandableAdapterSystem = new DepartAdapter(this));
//        listViewSystem.setLayoutManager(new LinearLayoutManager(this));

        listViewSystem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DepartmentModel departmentModel = expandableAdapterSystem.getItem(position);


                Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), ListColleagueInGroupActivity.class);
                intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, departmentModel.getDeptId());
                intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME, departmentModel.getDeptName());
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        swipeContainerAccount = (SwipeRefreshLayout) findViewById(R.id.swipeContainerAccount);
        if (swipeContainerAccount != null) {
            swipeContainerAccount.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initAccountData();
                }
            });
            // Configure the refreshing colors
            swipeContainerAccount.setColorScheme(R.color.main_green, R.color.main_green
                    , R.color.main_green, R.color.main_green);
        }
        swipeContainerSystem = (SwipeRefreshLayout) findViewById(R.id.swipeContainerSystem);
        if (swipeContainerSystem != null) {
            swipeContainerSystem.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initSystemData();
                }
            });
            // Configure the refreshing colors
            swipeContainerSystem.setColorScheme(R.color.main_green, R.color.main_green
                    , R.color.main_green, R.color.main_green);
        }

        expandableAdapter = new ExpandableListAdapter(this, listViewAccount);
        listViewAccount.setAdapter(expandableAdapter);
        listViewAccount.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                ColleagueGroup colleagueGroup = expandableAdapter.getGroup(groupPosition);
                colleagueGroup.setExpand(true);
                expandableAdapter.notifyDataSetChanged();
            }
        });

        listViewAccount.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                ColleagueGroup colleagueGroup = expandableAdapter.getGroup(groupPosition);
                colleagueGroup.setExpand(false);
                expandableAdapter.notifyDataSetChanged();
            }
        });

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });
//        layoutCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideCallLayout();
//            }
//        });

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                if(mode == ACCOUNT_MODE) {
                    expandableAdapter.filter(text);
                } else {
                    expandableAdapterSystem.filter(text);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

//        listViewAccount.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView absListView, int i) {
//            }
//
//            @Override
//            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (firstVisibleItem == 0 && listViewAccount.getChildAt(0) != null && listViewAccount.getChildAt(0).getTop()==0) {
//                    swipeContainerAccount.setEnabled(true);
//                } else {
//                    swipeContainerAccount.setEnabled(false);
//                }
//
////                boolean allow = false;
////
////                if(visibleItemCount>0) {
////                    long packedPosition = elvMounters.getExpandableListPosition(firstVisibleItem);
////                    int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
////                    int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
////                    allow = groupPosition==0 && childPosition==-1 && listViewAccount.getChildAt(0).getTop()==0;
////                }
////
////                swipeContainer.setEnabled(allow);
//            }
//        });

    }

    public void onListItemExpandedAction(ListColleagueAdapter expandableAdapter, int position) {
        Log.e("onListItemExpanded", "" + position);
        int positionExpanding = -1;
        int menuExpandingChildCount = -1;

        for (int i = 0, count = expandableAdapter.getItemCount(); i < count; i++) {
            if (i == position) {
                continue;
            }
            Object listItem = expandableAdapter.getListItem(i);
            if (listItem instanceof ParentWrapper) {
                ParentWrapper wrapper = (ParentWrapper) listItem;
                ParentListItem item = wrapper.getParentListItem();

                if (wrapper.isExpanded()) {
                    ColleagueItem menu = (ColleagueItem) item;
                    positionExpanding = i;
                    menuExpandingChildCount = menu.getChildItemList().size();
                    expandableAdapter.collapseParent(item);
                }
            }
        }
        int updateNextExpand = positionExpanding < 0 || positionExpanding > position ? position : position - menuExpandingChildCount;
        Object listItemPosition = expandableAdapter.getListItem(updateNextExpand);
        if (listItemPosition instanceof ParentWrapper) {
            ParentWrapper wrapper = (ParentWrapper) listItemPosition;
            ParentListItem item = wrapper.getParentListItem();
            if (!wrapper.isExpanded()) {
                expandableAdapter.collapseParent(item);
            } else {
                expandableAdapter.expandParent(item);
            }
        }
    }

    public void showLoading() {
        if(mode == ACCOUNT_MODE) {
            swipeContainerAccount.setVisibility(View.VISIBLE);
            swipeContainerAccount.setRefreshing(true);
        } else {
            swipeContainerSystem.setVisibility(View.VISIBLE);
            swipeContainerSystem.setRefreshing(true);
        }

    }

    public void closeLoading() {
        swipeContainerAccount.setRefreshing(false);
        swipeContainerSystem.setRefreshing(false);
    }


    public void showError(String message, boolean isError) {
        closeLoading();
//        swipeContainer.setVisibility(View.GONE);
        super.showError(message, isError);
    }
    public void showError(ResponseObject responseObject, boolean isError) {
        closeLoading();
//        swipeContainer.setVisibility(View.GONE);
        super.showError(responseObject, isError);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void onClickRefresh(View v) {
        initData();
    }

    public void initData() {
        switch (mode) {
            case ACCOUNT_MODE:
                initAccountData();
                break;
            case SYSTEM_MODE:
                initSystemData();
                break;
        }
    }

    public void initAccountData() {

        showLoading();

        GetColleagueGroupRequest getColleagueGroupRequest = new GetColleagueGroupRequest();
        getColleagueGroupRequest.setData();

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                Log.e("Response", "" + response);
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupResponse signInResponse = gson.fromJson(response, GetColleagueGroupResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListAccountData(signInResponse.getListGroup());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void initSystemData() {

        showLoading();

        GetColleagueGroupSystemRequest getColleagueGroupRequest = new GetColleagueGroupSystemRequest();
        getColleagueGroupRequest.setData();

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                Log.e("Response", "" + response);
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupSystemtResponse signInResponse = gson.fromJson(response, GetColleagueGroupSystemtResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListSystemData(signInResponse.getListDepart());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onCreateGroup(View v) {
        Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), CreateColleagueGroupActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CREATE);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != Activity.RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CODE_CREATE) {
            initData();
        }
    }

    public void setListAccountData(ArrayList<ColleagueGroup> listGroup) {

        if(listGroup == null || listGroup.isEmpty()) {
            GlobalValue.listColleageGroup = null;
            showError(getResources().getString(R.string.alertNoColleagueFound), false);
            return;
        }

        for(ColleagueGroup colleagueGroup : listGroup) {
            ArrayList<ColleagueModel> listModel = colleagueGroup.getListMember();
            if(listModel != null) {
                for(ColleagueModel colleagueModel : listModel) {
                    colleagueModel.setGroupId(colleagueGroup.getGroupId());
                }
            }
        }

        GlobalValue.listColleageGroup = listGroup;
        GlobalValue.initListColleagueMember();

        expandableAdapter.setData(listGroup);

    }

    public void setListSystemData(ArrayList<DepartmentModel> listGroup) {
        if(listGroup == null || listGroup.isEmpty()) {
            GlobalValue.listColleageGroup = null;
            showError(getResources().getString(R.string.alertNoColleagueFound), false);
            return;
        }

        expandableAdapterSystem.setData(listGroup);
    }

    OnChildSelectedListener onChildSelectedListener = new OnChildSelectedListener() {
        @Override
        public void onChildSelect(ColleagueModel menu) {

        }
    };

    OnGroupSelectedListener onGroupSelectedListener = new OnGroupSelectedListener() {
        @Override
        public void onGroupSelect(ColleagueGroup menu) {
            Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), ListColleagueInGroupActivity.class);
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, menu.getGroupId());
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME, menu.getGroupName());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        }
    };
    OnDepartSelectedListener onDepartSelectedListener = new OnDepartSelectedListener() {
        @Override
        public void onGroupSelect(DepartmentModel menu) {
            Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), ListColleagueInGroupActivity.class);
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, menu.getDeptId());
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME, menu.getDeptName());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        }
    };

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void goToColleagueInfo(ColleagueModel colleagueModel) {
        Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), ColleagueInfoActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ID, colleagueModel.getColleagueId());
        intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, colleagueModel.getGroupId());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void goToChoseAvaiGroup(Object obj) {

        final Intent intent = new Intent(ListColleagueActivity.this.getBaseContext(), ChoseAvaiColleagueGroupActivity.class);

        int agentId = -1;

        if(obj instanceof ColleagueModel) {
            ColleagueModel colleagueModel = (ColleagueModel) obj;
            colleagueModel.syncId();
            agentId = colleagueModel.getAgentId();
            intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_OBJ, colleagueModel);
        } else {

            Agent agent = (Agent) obj;

            ColleagueModel colleagueModel = new ColleagueModel();
            colleagueModel.setAgentId(agent.getAgentId());
            colleagueModel.setColleagueName(agent.getEmployeeName());
            colleagueModel.setPhoneNumber(agent.getPhoneNumber());

            agentId = agent.getAgentId();

            intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_OBJ, colleagueModel);
        }

        showLoading();

        GetColleagueGroupCB2Request getColleagueGroupRequest = new GetColleagueGroupCB2Request();
        getColleagueGroupRequest.setData("" + agentId);

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupResponse responseObject = gson.fromJson(response, GetColleagueGroupResponse.class);


                    if (responseObject.getIsSuccess() == 1 && responseObject.getIsLogin() == 1) {
                        if(responseObject.getListGroup() != null && !responseObject.getListGroup().isEmpty()) {
                            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_GROUP, responseObject.getListGroup());
                            startActivityForResult(intent, REQUEST_CODE_CREATE);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        } else {
                            Toast.makeText(ListColleagueActivity.this, "Đồng nghiệp này đã được thêm vào tất cả các nhóm", Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        showError(signInResponse, true);
                        String message = null;

                        if(responseObject == null ) {
                            message = getResources().getString(R.string.text_no_network);
                        } else {

                            if(responseObject.getIsLogin() == -1) {
                                Toast.makeText(ListColleagueActivity.this, "Phiên làm việc đã hết hạn hoặc không hợp lệ!", Toast.LENGTH_SHORT).show();
                                goToLogin();
                                return;
                            }

                            message = responseObject.getMessage() != null && !responseObject.getMessage().isEmpty()
                                    ? responseObject.getMessage() : getResources().getString(R.string.text_no_network);
                        }

                        Toast.makeText(ListColleagueActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    showError(getResources().getString(R.string.text_no_network), true);
                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
//                showError(getResources().getString(R.string.text_no_network), true);
                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void showCallLayout(Object obj) {
        pickContact = (ColleagueModel) obj;
        layoutCall.setVisibility(View.VISIBLE);

        ViewSwitcher viewSwitcherCall = (ViewSwitcher) layoutCall.findViewById(R.id.viewSwitcherCall);
//        viewSwitcherCall.showNext();
        viewSwitcherCall.setDisplayedChild(1);
    }

    public void onClickBtnAccount(View view) {
        mode = ACCOUNT_MODE;
        if(viewSwitcher.getDisplayedChild() == 1) {
            viewSwitcher.setInAnimation(slide_in_right);
            viewSwitcher.setOutAnimation(slide_out_right);
            viewSwitcher.showPrevious();

            btnAccount.setBackgroundResource(R.drawable.drawable_white_left_corner);
            btnAccount.setTextColor(getResources().getColor(R.color.main_green));

            btnSystem.setBackgroundResource(R.drawable.drawable_transparent);
            btnSystem.setTextColor(getResources().getColor(R.color.white));

            if(expandableAdapter == null) {
                initAccountData();
            } else {
                expandableAdapter.filter(editTextSearch.getEditableText().toString());
            }
        }
    }
    public void onClickBtnSystem(View view) {
        mode = SYSTEM_MODE;
        if(viewSwitcher.getDisplayedChild() == 0) {
            viewSwitcher.setInAnimation(slide_in_left);
            viewSwitcher.setOutAnimation(slide_out_left);
            viewSwitcher.showPrevious();

            btnAccount.setBackgroundResource(R.drawable.drawable_transparent);
            btnAccount.setTextColor(getResources().getColor(R.color.white));

            btnSystem.setBackgroundResource(R.drawable.drawable_white_right_corner);
            btnSystem.setTextColor(getResources().getColor(R.color.main_green));

            if(expandableAdapterSystem == null || expandableAdapterSystem.getCount() == 0) {
                initSystemData();
            } else {
                expandableAdapterSystem.filter(editTextSearch.getEditableText().toString());
            }
        }
    }

    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public void onCallPrivate(View view) {
        callPrivatePhone(pickContact.getPhoneNumber());
    }

    public void onCallHost(View view) {
        callHostPhone(pickContact.getPhoneNumber());
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }

    public void onKeyBack() {
        View layoutCall = findViewById(R.id.layoutCall);
        View layoutSearch = findViewById(R.id.layoutSearch);
        View layoutSearchFake = findViewById(R.id.layoutSearchFake);

        if (layoutCall != null && layoutCall.getVisibility() == View.VISIBLE) {

            ViewSwitcher viewSwitcher = (ViewSwitcher) layoutCall.findViewById(R.id.viewSwitcherCall);

            if(viewSwitcher == null) {
                layoutCall.setVisibility(View.GONE);
            } else {
                if(viewSwitcher.getDisplayedChild() == 1 && listPhoneNumbers.size() > 1) {
                    viewSwitcher.setInAnimation(slide_in_right);
                    viewSwitcher.setOutAnimation(slide_out_right);
                    viewSwitcher.showPrevious();
                } else {
                    layoutCall.setVisibility(View.GONE);
                }
            }
        } else if (layoutSearch != null && layoutSearchFake != null && layoutSearch.getVisibility() == View.VISIBLE) {
            layoutSearch.setVisibility(View.GONE);
            AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearchFake, true, R.anim.slide_in_right, new ShowHideListener() {
                @Override
                public void doAfter() {
                    hideAllKeyBoard();
                }
            });
        } else {
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }

}
