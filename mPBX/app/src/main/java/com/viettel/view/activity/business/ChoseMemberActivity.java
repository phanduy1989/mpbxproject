package com.viettel.view.activity.business;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.MPBXFilter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationMember;
import com.viettel.model.CustomerModel;
import com.viettel.model.PBXModel;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.model.request.colleague.GetListAddingColleagueRequest;
import com.viettel.model.request.conference.GetListConferenceColleagueRequest;
import com.viettel.model.request.conference.GetListConferenceCustomerRequest;
import com.viettel.model.response.conference.GetConferenceListAddCustomerResponse;
import com.viettel.model.response.colleague.GetListColleagueResponse;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.AnimationUtil;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.colleague.CreateColleagueGroupActivity;
import com.viettel.view.activity.business.customer.CustomerInfoActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by duyuno on 11/22/16.
 */
public class ChoseMemberActivity extends BABaseActivity {

    public static final int LIST_TYPE_COLLEAGUE = 0;
    public static final int LIST_TYPE_CUSTOMER = 1;

    public static final int MODE_CREATE = 0;
    public static final int MODE_EDIT = 1;
    public static final int MODE_ADD = 2;

    private int mode;
    private int groupId;
    private int listType;
    private String groupName;

    TextView txtTitleHeader;
    ImageView icon_right;

    private ListView listView;
    private MemberAdaper memberAdaper;

    private boolean isNext;
    ArrayList<ConversationMember> listMember;
    private int conferenceId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_chose_member_activity);

        initView();
    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        icon_right = (ImageView) findViewById(R.id.icon_right);
        listView = (ListView) findViewById(R.id.listView);


        icon_right.setImageResource(R.drawable.btn_next_icon);

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        memberAdaper = new MemberAdaper(this);
        listView.setAdapter(memberAdaper);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                memberAdaper.onTichItem(position);
                changeHeaderTitle();
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                memberAdaper.filter(text);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void changeHeaderTitle() {

        switch (mode) {
            case MODE_CREATE:
                txtTitleHeader.setText(String.format(getResources().getString(R.string.labelColleagueChoseMember), "" + memberAdaper.getCheckedCount(), "" + memberAdaper.getRealCount()));
                break;
            case MODE_EDIT:
                txtTitleHeader.setText(String.format(getResources().getString(R.string.labelColleagueChoseEditMember), "" + memberAdaper.getCheckedCount(), "" + memberAdaper.getRealCount()));
                break;
            case MODE_ADD:
                txtTitleHeader.setText(String.format(getResources().getString(R.string.labelPickedList), "" + memberAdaper.getCheckedCount(), "" + memberAdaper.getRealCount()));
                break;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void onClickRefresh(View v) {
        initData();
    }

    public void initData() {

        Intent intent = getIntent();
        mode = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ACTION, 0);
        groupId = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, 0);
        conferenceId = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, 0);
        listType = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_LIST_TYPE, 0);
        groupName = intent.getStringExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME);
        listMember = intent.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT);

        switch (listType) {
            case LIST_TYPE_COLLEAGUE:
                initColleagueList();
                break;
            case LIST_TYPE_CUSTOMER:
                initCustomerList();
                break;
        }
    }

    public void initColleagueList() {

        showLoading();

        BasePostRequestEntity basePostRequestEntity;

        if(mode == MODE_CREATE || mode == MODE_ADD) {
            GetListAddingColleagueRequest getListAddingColleagueRequest = new GetListAddingColleagueRequest();
            getListAddingColleagueRequest.setData();
            basePostRequestEntity = getListAddingColleagueRequest;
        } else if(mode == MODE_EDIT){
            GetListAddingColleagueRequest getListAddingColleagueRequest = new GetListAddingColleagueRequest();
            getListAddingColleagueRequest.setData("" + groupId);
            basePostRequestEntity = getListAddingColleagueRequest;
        } else {
            GetListConferenceColleagueRequest getListConferenceColleagueRequest = new GetListConferenceColleagueRequest();
            if(conferenceId > 0) {
                getListConferenceColleagueRequest.setData("" + conferenceId);
            } else {
                getListConferenceColleagueRequest.setData("");
            }
            basePostRequestEntity = getListConferenceColleagueRequest;
        }

        ApiController.doPostRequest(this, basePostRequestEntity, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();

                if (response != null) {

                    Log.e("Response", "" + response);

                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetListColleagueResponse signInResponse = gson.fromJson(response, GetListColleagueResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setColleagueListData(signInResponse.getListColleague());
                        changeHeaderTitle();
                    } else {
                        showError(signInResponse, true);
//                        onKeyBack();
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    onKeyBack();
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                onKeyBack();
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void initCustomerList() {

        showLoading();

        GetListConferenceCustomerRequest getListConferenceCustomerRequest = new GetListConferenceCustomerRequest();
//        getListConferenceCustomerRequest.setData(sharePreference.getAccessToken(), "" + sharePreference.getNumOfCompany(), "");

        if(conferenceId > 0) {
            getListConferenceCustomerRequest.setData( "" + conferenceId);
        } else {
            getListConferenceCustomerRequest.setData("-1");
        }

        ApiController.doPostRequest(this, getListConferenceCustomerRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();

                if (response != null) {

                    Log.e("Response", "" + response);

                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceListAddCustomerResponse signInResponse = gson.fromJson(response, GetConferenceListAddCustomerResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setCustomerListData(signInResponse.getListMember());
                        changeHeaderTitle();
                    } else {
                        showError(signInResponse, true);
//                        onKeyBack();
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    onKeyBack();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                onKeyBack();
            }
        });

    }

    public void onCreateGroup(View v) {
        startActivity(new Intent(ChoseMemberActivity.this.getBaseContext(), CreateColleagueGroupActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void setColleagueListData(ArrayList<ColleagueModel> listCollleague) {
        switch (mode) {
            case MODE_CREATE:
                memberAdaper.setData(listCollleague);
                break;
            case MODE_EDIT:
                memberAdaper.setEditColleagueData(listCollleague, GlobalValue.currentCollectGroup);
                break;
            case MODE_ADD:
                memberAdaper.setColleagueData(listCollleague, listMember);

                if(memberAdaper.getCount() == 0) {
                    showError("Không tìm thấy đồng nghiệp để thêm mới", false);
                }
                break;
        }

    }

    public void setCustomerListData(ArrayList<CustomerModel> listGroup) {
        GlobalValue.listCustomer = listGroup;

        switch (mode) {
            case MODE_CREATE:
            case MODE_EDIT:
                memberAdaper.setData(GlobalValue.listCustomer);
                break;
            case MODE_ADD:
                memberAdaper.setCustomerData(GlobalValue.listCustomer, listMember);

                if(memberAdaper.getCount() == 0) {
                    showError("Không tìm thấy khách hàng để thêm mới", false);
                }
                break;
        }
    }

    OnChildSelectedListener onChildSelectedListener = new OnChildSelectedListener() {
        @Override
        public void onChildSelect(ColleagueModel menu) {

        }
    };


    public void onClickHeaderRight(View v) {
        isNext = true;
        onKeyBack();
    }

    public void goToEditInfoAcitivity() {
//        startActivity(new Intent(ListColleagueActivity.this.getBaseContext(), EditCustomerInfoActivity.class));
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void goToDetail() {
        startActivity(new Intent(ChoseMemberActivity.this.getBaseContext(), CustomerInfoActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        Intent resultIntent = new Intent();
        if (isNext) {
            resultIntent.putExtra(GlobalInfo.BUNDLE_KEY_SUCCESS, isNext);
            ArrayList<PBXModel> list = memberAdaper.getListSelected();
            if (list != null) {
                resultIntent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER, list);
            }
            setResult(Activity.RESULT_OK, resultIntent);
        } else {
            setResult(Activity.RESULT_CANCELED, resultIntent);
        }
        finish();
    }

    class ViewHolder {
        public TextView textName, textPhone;
        public ImageView iconCheck, avatar;
    }

    class MemberAdaper extends BaseAdapter implements MPBXFilter{

        Activity activity;

        public ArrayList<PBXModel> listData;
        public ArrayList<PBXModel> listDataDisplay;

        public void setData(Object list) {

            if(list == null) return;

            ArrayList<PBXModel> listModel = (ArrayList<PBXModel>) list;
            for (PBXModel pbxModel : listModel) {
                pbxModel.setSelected(false);
            }

            PBXModel.formatPhoneNumber(listModel);

            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }

            listData.addAll(listModel);
            if (listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }

            listDataDisplay.addAll(listModel);
            notifyDataSetChanged();
        }
        public void setCustomerData(ArrayList<CustomerModel> list, ArrayList<ConversationMember> listMember) {

            if(list == null) return;

            HashMap<String, Integer> hash = new HashMap<>();
            if(listMember != null) {
                for (ConversationMember conversationMember : listMember) {
                    hash.put(PhoneUtil.makeCorrectPhoneFormat(conversationMember.getPhoneNumber()), 1);
                }
            }

            for (int size = list.size(), i = size - 1; i >=0; i--) {
                CustomerModel colleagueModel = list.get(i);
                if(hash.containsKey(PhoneUtil.makeCorrectPhoneFormat(colleagueModel.getPhoneNumber()))) {
                    list.remove(i);
                }
            }

            PBXModel.formatPhoneNumberCustomer(list);

            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }

            if (listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }

            listData.addAll(list);
            listDataDisplay.addAll(list);
            notifyDataSetChanged();
        }
        public void setColleagueData(ArrayList<ColleagueModel> list, ArrayList<ConversationMember> listMember) {

            if(list == null) return;

            HashMap<String, Integer> hash = new HashMap<>();
            if(listMember != null) {
                for (ConversationMember conversationMember : listMember) {
                    hash.put(PhoneUtil.makeCorrectPhoneFormat(conversationMember.getPhoneNumber()), 1);
                }
            }


//            for (ColleagueModel colleagueModel : list) {
//                if(!hash.containsKey(PhoneUtil.makeCorrectPhoneFormat(colleagueModel.getPhoneNumber()))) {
//                    colleagueModel.setSelected(false);
//                } else {
//                    colleagueModel.setSelected(true);
//                }
//            }
            for (int size = list.size(), i = size - 1; i >=0; i--) {
                ColleagueModel colleagueModel = list.get(i);
                if(hash.containsKey(PhoneUtil.makeCorrectPhoneFormat(colleagueModel.getPhoneNumber()))) {
                    list.remove(i);
                }
            }

            PBXModel.formatPhoneNumberColleague(list);

            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }

            listData.addAll(list);

            if (listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }
            listDataDisplay.addAll(list);
            notifyDataSetChanged();
        }



        public void setEditColleagueData(ArrayList<ColleagueModel> list, ColleagueGroup colleagueGroup) {

            if(list == null) return;

            if (colleagueGroup != null) {
                for (ColleagueModel colleagueModel : list) {
                    colleagueModel.setSelected(colleagueGroup.checkContainId(colleagueModel.getColleagueId()));
                }
            }

            PBXModel.formatPhoneNumberColleague(list);

            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }

            listData.addAll(list);
            if (listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }

            listDataDisplay.addAll(list);


            notifyDataSetChanged();
        }

        public ArrayList<PBXModel> getListSelected() {
            if (listDataDisplay == null || listDataDisplay.isEmpty()) return null;

            ArrayList<PBXModel> list = null;

            for (PBXModel pbxModel : listDataDisplay) {
                if (pbxModel.isSelected()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    }

                    list.add(pbxModel);
                }
            }


            return list;
        }

        public void onTichItem(int position) {
            PBXModel colleagueModel = listDataDisplay.get(position);
            colleagueModel.changeSelected();
            notifyDataSetChanged();
        }

        public int getCheckedCount() {
            int count = 0;
            if (listData == null) {
                return 0;
            }

            for (PBXModel colleagueModel : listData) {
                if (colleagueModel.isSelected()) {
                    count++;
                }
            }

            return count;
        }

        public MemberAdaper(Activity activity) {
            this.activity = activity;
        }

        public int getRealCount() {
            return listData != null ? listData.size() : 0;
        }

        @Override
        public int getCount() {
            return listDataDisplay != null ? listDataDisplay.size() : 0;
        }

        @Override
        public PBXModel getItem(int position) {
            return listDataDisplay != null ? listDataDisplay.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.item_chose_member, null);
                viewHolder = new ViewHolder();
                viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
                viewHolder.textPhone = (TextView) convertView.findViewById(R.id.textPhone);
                viewHolder.iconCheck = (ImageView) convertView.findViewById(R.id.iconCheck);
                viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            PBXModel colleagueModel = getItem(position);

            String name = null, phone = null;
            if (colleagueModel instanceof ColleagueModel) {
                name = ((ColleagueModel) colleagueModel).getColleagueName();
                phone = ((ColleagueModel) colleagueModel).getPhoneNumber();
            } else if (colleagueModel instanceof CustomerModel) {
                name = ((CustomerModel) colleagueModel).getCustomerName();
                phone = ((CustomerModel) colleagueModel).getPhoneNumber();
            }

            if (colleagueModel != null) {
                viewHolder.textName.setText(name);

                if(phone == null || phone.isEmpty()) {
                    viewHolder.textPhone.setVisibility(View.GONE);
                } else {
                    viewHolder.textPhone.setVisibility(View.VISIBLE);
                    viewHolder.textPhone.setText(phone);
                }


                if (colleagueModel.isSelected()) {
                    viewHolder.iconCheck.setImageResource(R.drawable.radio_check_icon);
                } else {
                    viewHolder.iconCheck.setImageResource(R.drawable.radio_icon);
                }

            }

            return convertView;
        }

        @Override
        public void filter(String key) {
            if(listData == null || listData.isEmpty()) return;

            ((BABaseActivity) activity).showContent();

            if(key == null || key.isEmpty()) {

                listDataDisplay = new ArrayList<>();
                listDataDisplay.addAll(listData);
                notifyDataSetChanged();
                return;
            }

            key = key.toLowerCase();

            listDataDisplay = new ArrayList<>();

            for(PBXModel obj : listData) {
                if(obj instanceof ColleagueModel) {

                    String name = StringUtility.convertVietnameseToAscii(((ColleagueModel) obj).getColleagueName());

                    if((name != null && name.contains(key)) || ((ColleagueModel) obj).getPhoneNumber().contains(key)) {
                        listDataDisplay.add(obj);
                    }
                } else {
                    String name = StringUtility.convertVietnameseToAscii(((CustomerModel) obj).getCustomerName());
                    if((name != null && name.contains(key)) || ((CustomerModel) obj).getPhoneNumber().contains(key)) {
                        listDataDisplay.add(obj);
                    }
                }
            }


            notifyDataSetChanged();
        }
    }

}
