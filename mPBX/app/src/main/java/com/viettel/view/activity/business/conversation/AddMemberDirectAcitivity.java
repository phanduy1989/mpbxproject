package com.viettel.view.activity.business.conversation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.viettel.GlobalInfo;
import com.viettel.model.ContactObject;
import com.viettel.mpbx.R;
import com.viettel.utils.DialogUtility;
import com.viettel.view.activity.business.ContactActivity;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/21/17.
 */
public class AddMemberDirectAcitivity extends BABaseActivity{
    TextView txtTitleHeader;
    EditText edtName, edtPhone;

    ContactObject contactModel;
    private boolean isSuccess;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contactModel = new ContactObject();

        setContentView(R.layout.layout_add_member_direct_activity);

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        edtName = (EditText) findViewById(R.id.edtName);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        txtTitleHeader.setText("Thêm thành viên trực tiếp");
    }

    public void onClickAddMember(View view) {
        String name = edtName.getEditableText().toString().trim();
        String phone = edtPhone.getEditableText().toString().trim();

        if (name.length() == 0) {
            DialogUtility.showDialogAlert(this, "", "Vui lòng nhập họ tên!",
                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtName);
            return;
        }
        if (phone.length() == 0) {
            DialogUtility.showDialogAlert(this, "", "Vui lòng nhập số điện thoại!",
                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtPhone);
            return;
        }

        contactModel.setContactName(name);
        contactModel.setPhoneNumber(phone);

        isSuccess = true;

        onKeyBack();
    }

    public void onClickAddFromContact(View view) {
        Intent intent = new Intent(getBaseContext(), ContactActivity.class);
//        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_MULTI_SELECT, false);
//        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_SYNCHROS, false);
        startActivityForResult(intent, REQUEST_CODE_CHOSE_MEMBER);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        Intent resultIntent = new Intent();
        if(isSuccess) {
            resultIntent.putExtra(GlobalInfo.BUNDLE_KEY_SUCCESS, isSuccess);

            resultIntent.putExtra(GlobalInfo.BUNDLE_KEY_CONTACT, contactModel);
            setResult(Activity.RESULT_OK, resultIntent);
        } else {
            setResult(Activity.RESULT_CANCELED, resultIntent);
        }
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == REQUEST_CODE_CHOSE_MEMBER && data != null) {
                ContactObject contactModel = data.getParcelableExtra(GlobalInfo.BUNDLE_KEY_CONTACT);
                if(contactModel != null) {
                    edtName.setText(contactModel.getContactName());
                    edtPhone.setText(contactModel.getPhoneNumber());
                }
            }
        }
    }
}
