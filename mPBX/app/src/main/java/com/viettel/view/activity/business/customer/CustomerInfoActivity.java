package com.viettel.view.activity.business.customer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.adapter.PhonesAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.CustomerModel;
import com.viettel.model.request.customer.GetCustomerInfoRequest;
import com.viettel.model.response.customer.GetCustomerInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.ImagePicker;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class CustomerInfoActivity extends BABaseActivity {

    TextView textName, textPhoneNumber, txtCompanyName, txtPosition, txtPhoneNumber2, txtPhoneNumber3;
    TextView txtTitleHeader;

    LinearLayout layoutPhoneNumber, layoutPhoneNumber2, layoutPhoneNumber3;

    RelativeLayout layoutCall;
    PhonesAdapter phonesAdapter;
    ImageView avatar;

    private int customerId;
    CustomerModel customerModel;
    private String phoneCallNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_customer_info);

        customerId = getIntent().getIntExtra(GlobalInfo.BUNDLE_KEY_CUSTOMER_ID, -1);

        if (customerId <= 0) {
            finish();
            return;
        }

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initView() {
        textName = (TextView) findViewById(R.id.textName);
        textPhoneNumber = (TextView) findViewById(R.id.textPhoneNumber);
        txtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtPhoneNumber2 = (TextView) findViewById(R.id.txtPhoneNumber2);
        txtPhoneNumber3 = (TextView) findViewById(R.id.txtPhoneNumber3);
        layoutPhoneNumber = (LinearLayout) findViewById(R.id.layoutPhoneNumber);
        layoutPhoneNumber2 = (LinearLayout) findViewById(R.id.layoutPhoneNumber2);
        layoutPhoneNumber3 = (LinearLayout) findViewById(R.id.layoutPhoneNumber3);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);
        avatar = (ImageView) findViewById(R.id.avatar);
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);

//        layoutPhoneNumber.setOnClickListener(onClickListener);
//        layoutPhoneNumber2.setOnClickListener(onClickListener);
//        layoutPhoneNumber3.setOnClickListener(onClickListener);


    }

    Bitmap bitmap = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CAMERA) {
            bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
            avatar.setImageBitmap(bitmap);
        }
    }

    public void initData() {
        showLoading();
        GetCustomerInfoRequest getCustomerInfoRequest = new GetCustomerInfoRequest();
        getCustomerInfoRequest.setData(customerId);

        ApiController.doPostRequest(this, getCustomerInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetCustomerInfoResponse signInResponse = gson.fromJson(response, GetCustomerInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchCustomerInfo(signInResponse.getCustomerInfo());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CustomerInfoActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.alertConnectFail), true);
            }
        });
    }

    public void fetchCustomerInfo(CustomerModel customerModel) {

        if (customerModel == null) {
            showError(getResources().getString(R.string.no_data), false);
            return;
        }

        this.customerModel = customerModel;

        txtTitleHeader.setText(customerModel.getCustomerName() != null ? customerModel.getCustomerName() : "");
        textName.setText(customerModel.getCustomerName() != null ? customerModel.getCustomerName() : "");
        textPhoneNumber.setText(customerModel.getPhoneNumber() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()) : "");
        txtCompanyName.setText(customerModel.getCompanyName() != null ? customerModel.getCompanyName() : "");
        txtPosition.setText(customerModel.getPosition() != null ? customerModel.getPosition() : "");
        txtPhoneNumber2.setText(customerModel.getPhoneNumber2() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber2()) : "");
        txtPhoneNumber3.setText(customerModel.getPhoneNumber3() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber3()) : "");

        if (customerModel.getFileName() != null)
            Picasso.with(this)
                    .load(customerModel.getFileName()).placeholder(getResources().getDrawable(R.drawable.avatar2))
                    .error(getResources().getDrawable(R.drawable.avatar2))
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(avatar);
    }

    public void onClickCall(View view) {

        if(listPhoneNumbers.isEmpty()) {
            if (customerModel.getPhoneNumber() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()));
            }

            if (customerModel.getPhoneNumber2() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber2()));
            }
            if (customerModel.getPhoneNumber3() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber3()));
            }
        }

        View layoutCall = findViewById(R.id.layoutCall);
        layoutCall.setVisibility(View.VISIBLE);

        final ViewSwitcher viewSwitcherCall = (ViewSwitcher) layoutCall.findViewById(R.id.viewSwitcherCall);

        int size = listPhoneNumbers.size();

        switch (size) {
            case 0:
                Toast.makeText(this, "Không có thông tin liên lạc!", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                phoneCallNumber = listPhoneNumbers.get(0);
                viewSwitcherCall.showNext();
                break;
            default:
                ListView listView = (ListView) layoutCall.findViewById(R.id.listPhoneNumber);
                listView.setAdapter(phonesAdapter = new PhonesAdapter(this, listPhoneNumbers));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        phoneCallNumber = phonesAdapter.getItem(position);
                        viewSwitcherCall.setInAnimation(slide_in_left);
                        viewSwitcherCall.setOutAnimation(slide_out_left);
                        viewSwitcherCall.showNext();
                    }
                });
                break;
        }

    }

    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public void onCallPrivate(View view) {
        callPrivatePhone(phoneCallNumber);
    }

    public void onCallHost(View view) {
        callHostPhone(phoneCallNumber);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void onClickHeaderRight(View v) {
        Intent intent = new Intent(CustomerInfoActivity.this.getBaseContext(), CreateCustomerActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, customerId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
