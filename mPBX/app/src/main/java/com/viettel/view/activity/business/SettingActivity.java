package com.viettel.view.activity.business;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.viettel.adapter.TimeReloadAdapter;
import com.viettel.mpbx.R;

import com.viettel.store.AppSharePreference;
import com.viettel.store.GlobalValue;
import com.viettel.view.base.BABaseActivity;

public class SettingActivity extends BABaseActivity {

	SeekBar seekBarTemple, seekBarAlertTime;
	TextView txtTemple, txtAlertTime;
	SwitchCompat switchViewTempleAlert, switchViewVibrate;

	Button btnChoseAlertTone;
	Spinner spinner;
	TimeReloadAdapter timeReloadAdapter;

	private int alertTempleRepeatTime;
	private float temple;

	AppSharePreference appSharePreference;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_setting_activity);
		appSharePreference = com.viettel.store.AppSharePreference.getInstance(this);

		initView();
	}


	public void initView() {

		seekBarTemple = (SeekBar) findViewById(R.id.seekBarTemple);
		seekBarAlertTime = (SeekBar) findViewById(R.id.seekBarAlertTime);
		txtTemple = (TextView) findViewById(R.id.txtTemple);
		txtAlertTime = (TextView) findViewById(R.id.txtAlertTime);
		btnChoseAlertTone = (Button) findViewById(R.id.btnChoseAlertTone);
		switchViewTempleAlert = (SwitchCompat) findViewById(R.id.switchViewTempleAlert);
		switchViewVibrate = (SwitchCompat) findViewById(R.id.switchViewVibrate);

		switchViewTempleAlert.setChecked(appSharePreference.getBooleanValue(AppSharePreference.IS_ALERT));
		switchViewVibrate.setChecked(appSharePreference.getBooleanValue(com.viettel.store.AppSharePreference.IS_VIBRATE));

		switchViewTempleAlert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				appSharePreference.putBooleanValue(com.viettel.store.AppSharePreference.IS_ALERT, isChecked);


			}
		});
		switchViewVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				appSharePreference.putBooleanValue(com.viettel.store.AppSharePreference.IS_VIBRATE, isChecked);
			}
		});

		seekBarTemple.setMax(500);
		seekBarAlertTime.setMax(115);

		seekBarTemple.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				int m = progress / 10;
				int n = progress % 10;
				temple = m * 1f + n * 0.1f;
				String templeStr = "" + m + (n != 0 ? "," + n : "") + "\u00B0C";
				txtTemple.setText(templeStr);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		seekBarAlertTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				int time = progress + 5;

				alertTempleRepeatTime = time;

				txtAlertTime.setText(time + " phút");
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});


		seekBarTemple.setProgress((int)(appSharePreference.getFloatValue(com.viettel.store.AppSharePreference.ALERT_TEMPLE, 37.5f) * 10));
		seekBarAlertTime.setProgress(GlobalValue.alertRepeatTime - 5);

		spinner = (Spinner) findViewById(R.id.spinner);
		timeReloadAdapter = new TimeReloadAdapter(this, R.layout.spinner_item_black);
		timeReloadAdapter.setDropDownViewResource(R.layout.spinner_item_black);
		timeReloadAdapter.setShowContent(true);
		spinner.setAdapter(timeReloadAdapter);

		spinner.setSelection(GlobalValue.getTimeDataReloadPosition(GlobalValue.timeDataReload));

		if(GlobalValue.ringToneUri != null) {
			chosenRingtone = GlobalValue.ringToneUri;
			Ringtone ringtone = RingtoneManager.getRingtone(this, chosenRingtone);
			btnChoseAlertTone.setText("" + ringtone.getTitle(this));
		}

	}

	public void onClickBack(View v) {
		onKeyBack();
	}

	Uri chosenRingtone;

	public void onClickRingTone(View v) {
		Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, chosenRingtone);
		this.startActivityForResult(intent, 5);

	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
	{
		if (resultCode == Activity.RESULT_OK && requestCode == 5)
		{
			chosenRingtone = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
			GlobalValue.ringToneUri = chosenRingtone;
//			Log.e("title", "" + getNotifications(chosenRingtone));
			Ringtone ringtone = RingtoneManager.getRingtone(this, chosenRingtone);
			btnChoseAlertTone.setText("" + ringtone.getTitle(this));
		}
	}

	public String getNotifications(Uri uri) {
		RingtoneManager manager = new RingtoneManager(this);
		manager.setType(RingtoneManager.TYPE_NOTIFICATION);
		Cursor cursor = manager.getCursor();

		while (cursor.moveToNext()) {
			String notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			String notificationUri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

			if(notificationUri.equals(uri.toString())) {
				return notificationTitle;
			}
		}

		return null;
	}

	@Override
	protected void onDestroy() {

		GlobalValue.alertRepeatTime = alertTempleRepeatTime;
		GlobalValue.timeDataReload = GlobalValue.listTimeLoadData.get(spinner.getSelectedItemPosition()).getValue();

		appSharePreference.putFloatValue(com.viettel.store.AppSharePreference.ALERT_TEMPLE, temple);
		appSharePreference.putIntValue(com.viettel.store.AppSharePreference.ALERT_REPEAT_TIME, alertTempleRepeatTime);
		appSharePreference.putIntValue(com.viettel.store.AppSharePreference.TIME_RELOAD, GlobalValue.listTimeLoadData.get(spinner.getSelectedItemPosition()).getValue());

		Intent intent = new Intent();
		// add data
		intent.setAction("UPDATE_CONFIG");
		intent.putExtra("UPDATE_IS_ALERT", switchViewTempleAlert.isChecked());
		intent.putExtra("UPDATE_IS_VIBRATE", switchViewVibrate.isChecked());
		intent.putExtra("ALERT_TEMPLE", temple);
		sendBroadcast(intent);

		super.onDestroy();



	}

	@Override
	public void onKeyBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
	}

}
