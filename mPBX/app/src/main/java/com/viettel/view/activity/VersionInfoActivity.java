package com.viettel.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.viettel.mpbx.R;
import com.viettel.store.AppSharePreference;
import com.viettel.utils.ConfigUtility;
import com.viettel.utils.FontTypeface;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 11/22/16.
 */
public class VersionInfoActivity extends BABaseActivity {

    AppSharePreference appSharePreference;

    TextView txtTitleHeader, txtVersion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appSharePreference = AppSharePreference.getInstance(this);
        setContentView(R.layout.layout_version_activity);
        initView();
        initData();
    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtVersion = (TextView) findViewById(R.id.txtVersion);
    }
    public void initData() {
        txtTitleHeader.setText(getString(R.string.labelVersionInfo));

        txtVersion.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));

        if(ConfigUtility.androidVersionName == null || ConfigUtility.androidVersionName.isEmpty()) {
            String versionInfo = appSharePreference.getStringValue(AppSharePreference.VERSION);
            if(versionInfo == null || versionInfo.isEmpty()) {
                txtVersion.setText("Không tìm được thông tin phiên bản");
            } else {
                txtVersion.setText(versionInfo);
            }

        } else {
            txtVersion.setText("Phiên bản hiện tại: " + ConfigUtility.androidVersionName);
        }

    }

    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        finish();
    }

}
