package com.viettel.view.activity.authen;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.ae.team.connectapi.utility.YeuThichListener;
import com.androidquery.AQuery;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.viettel.adapter.CropOptionAdapter;
import com.viettel.adapter.holder.ThanhVienNhomAdapter;
import com.viettel.model.CropOption;
import com.viettel.model.DanhBaInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.NetworkUtil;
import com.viettel.utils.ParserUtility;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;
import com.viettel.widgets.CustomTextView;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by VanHa on 7/21/2015.
 */
public class ThanhVienNhomActivity extends BABaseActivity {

    TextView txtTitleHeader;

    private boolean checknetwork = true, checkHide = true;
    public static boolean capnhat = false;
    private ImageView img_Avartar;
    public static ArrayList<DanhBaInfo> thanhVienNhomInfos;

    //    private GroupInfo groupInfo;
    private ThanhVienNhomAdapter thanhVienNhomAdapter;
    private ImageButton btnDoiTen;
    private CustomTextView txtDoiTenNhom;
    private TextView lblTenNhom;
    private RelativeLayout layoutName;
    private UltimateRecyclerView recyNhom;
    private LinearLayoutManager mLinearLayoutManager;
    private EditText txtTenNhom;
    private TextView lblMoiKhoiNhom, lblThemThanhVien, lblRoiNhom;
    private View viewMoiKhoiNhom;
    public static int vitri = 0;
    private StringBuilder s;
    // dang anh
    public static String folder = "/GalleryApp", group_rom_id = "", name = "", user_id = "", tag = "", thanh_vien_nhom_json = "";
    private Uri imageUri;
    static int id = 0;
    static File file;
    final int CAMERA_REQUEST = 1;
    final int PICK_FROM_GALLERY = 2;
    private static final int CROP_FROM_CAMERA = 3;
    private Uri mImageCaptureUri;
    private String sResponse;
    private int w = 84, h = 84;
    int scw, sch;
    private Bitmap photo;
    private AQuery aq;
    private ProgressDialog dialog;
    private ProgressBar progressBar;
    public static boolean checkChange = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actyvity_thanhviennhom);

        try {

            unitUi();
            if (!NetworkUtil.isConnectingToInternet(ThanhVienNhomActivity.this))
                checknetwork = false;
            binData();
            unitControll();
            ConnectSoket();
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
        }
    }

    private void ConnectSoket() {

        ConnectApiSocket.ConnectSocket(sharePreference.getChatToken());
        ConnectApiSocket.socketOnAddUser(new ResultListener() {
            @Override
            public void onSuccess(String json) {
            }
        });

        ConnectApiSocket.socketOnRemoveUser(new ResultListener() {
            @Override
            public void onSuccess(String json) {
                Log.e("", "sxxxxxxxxxx:   " + json);
            }
        });
    }


    public void binData() {
        try {
            String token = sharePreference.getChatToken();
            if (tag.equals("GROUP")) {
                ConnectApiSocket.getListUsersGroup(ThanhVienNhomActivity.this, token, group_rom_id, "0", new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.contains("Success")) {
                            thanh_vien_nhom_json = json;
                            thanhVienNhomInfos.clear();
                            thanhVienNhomInfos = ParserUtility.parselistUserGroup(json);
                            new LoadDataAsynTask1().execute();
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ModelManager.GetListUsersGroup(this, new ModelManagerListener() {
//                    @Override
//                    public void onError(VolleyError error) {
//                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onSuccess(String json) {
//                        if (json.contains("Success")) {
//                            thanh_vien_nhom_json = json;
//                            thanhVienNhomInfos = ParserUtility.parselistUserGroup(json);
//                            new LoadDataAsynTask1().execute();
//                        } else {
//                            Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                }, WebServiceConfig.URL_GET_LIST_USERS_GROUP + group_rom_id + "/users", group_rom_id, "0");
            } else {
                ConnectApiSocket.getListUsersRoom(ThanhVienNhomActivity.this, token, group_rom_id, "", new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.contains("Success")) {
                            thanh_vien_nhom_json = json;
                            thanhVienNhomInfos.clear();
                            thanhVienNhomInfos = ParserUtility.parselistUserRoom(json);
                            new LoadDataAsynTask1().execute();
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ModelManager.GetListUsersRoom(this, new ModelManagerListener() {
//                    @Override
//                    public void onError(VolleyError error) {
//                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onSuccess(String json) {
//
//                        if (json.contains("Success")) {
//                            thanh_vien_nhom_json = json;
//                            thanhVienNhomInfos = ParserUtility.parselistUserRoom(json);
//                            new LoadDataAsynTask1().execute();
//                        } else {
//                            Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                }, WebServiceConfig.URL_GET_LIST_USERS_ROOM + group_rom_id + "/users", group_rom_id, "0");
            }

        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
        }

    }


    private void unitUi() {
        try {
            capnhat = false;
            group_rom_id = getIntent().getStringExtra("group_rom_id");
            name = getIntent().getStringExtra("name");
            user_id = getIntent().getStringExtra("user_id");
            tag = getIntent().getStringExtra("tag");
            aq = new AQuery(this);
            dialog = new ProgressDialog(this);
            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            txtTitleHeader.setText(name);

//            lblTitleScreen.setText(name);

            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            img_Avartar = (ImageView) findViewById(R.id.imgAvatar);
            txtTenNhom = (EditText) findViewById(R.id.txtTenNhom);
            lblMoiKhoiNhom = (TextView) findViewById(R.id.lblMoiKhoiNhom);
            lblRoiNhom = (TextView) findViewById(R.id.lblRoiNhom);
            lblThemThanhVien = (TextView) findViewById(R.id.lblThemThanhVien);
            btnDoiTen = (ImageButton) findViewById(R.id.btnDoiTen);
            txtDoiTenNhom = (CustomTextView) findViewById(R.id.txtDoiTenNhom);
            layoutName = (RelativeLayout) findViewById(R.id.layoutName);
            lblTenNhom = (TextView) findViewById(R.id.lblTenNhom);
            viewMoiKhoiNhom = (View) findViewById(R.id.viewMoiKhoiNhom);
            layoutName.setVisibility(View.GONE);
            recyNhom = (UltimateRecyclerView) findViewById(R.id.recyNhom);
            mLinearLayoutManager = new LinearLayoutManager(this);
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyNhom.setLayoutManager(new GridLayoutManager(this, 2));
            recyNhom.setHasFixedSize(true);
            recyNhom.refreshDrawableState();
            thanhVienNhomInfos = new ArrayList<>();

            txtTenNhom.setText(name);
            lblTenNhom.setText(name);
//            btnControlLeft.setBackgroundResource(R.drawable.iconback);
//            btnControlRight.setVisibility(View.GONE);
//            btnControlRight.setBackgroundResource(R.drawable.btn_taochat);

//
//            lblLop.setVisibility(View.GONE);
            if (tag.equals("ROOM")) {
//                btnControlRight.setVisibility(View.GONE);
                lblRoiNhom.setText(this.getResources().getString(R.string.lblnhom_out));
                if (user_id.equals(sharePreference.getChatId())) {
                    lblMoiKhoiNhom.setVisibility(View.VISIBLE);
                    viewMoiKhoiNhom.setVisibility(View.VISIBLE);
                } else {
                    lblMoiKhoiNhom.setVisibility(View.GONE);
                    viewMoiKhoiNhom.setVisibility(View.GONE);
                }
            } else {
//                btnControlRight.setVisibility(View.VISIBLE);
                lblRoiNhom.setText(this.getResources().getString(R.string.lblnhom_delete));
            }
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
        }

    }

    private void unitControll() {
        try {
            final String[] optionBtn = new String[]{
                    getResources().getString(R.string.chuptumayanh),
                    getResources().getString(R.string.bosuutap)
            };
            ArrayAdapter<String> adapterBtn = new ArrayAdapter<String>(this,
                    android.R.layout.select_dialog_item, optionBtn);
            AlertDialog.Builder builderBtn = new AlertDialog.Builder(this);
            builderBtn.setTitle(getResources().getString(R.string.luachon));
            builderBtn.setAdapter(adapterBtn, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        callCamera();
                    }
                    if (which == 1) {
                        callGallery();
                    }
                }
            });
            final AlertDialog dialogBtn = builderBtn.create();

//dialog image oclick
            final String[] option = new String[]{
                    getResources().getString(R.string.chuptumayanh),
                    getResources().getString(R.string.bosuutap)
            };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.select_dialog_item, option);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.luachon));
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    if (which == 0) {
                        callCamera();
                    }
                    if (which == 1) {
                        callGallery();
                    }
                }
            });
            final AlertDialog dialog = builder.create();
            img_Avartar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.show();
                }
            });

//        txtTenNhom.addTextChangedListener(new TextWatcher() {
//                                              @Override
//                                              public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                                              }
//
//                                              @Override
//                                              public void onTextChanged(CharSequence s, int start, int before, int count) {
//                                                  try {
//                                                      if (txtTenNhom.length() > 250)
//                                                      {
//
//                                                      }
//                                                  } catch (Exception e) {
//                                                  }
//                                              }
//
//                                              @Override
//                                              public void afterTextChanged(Editable s) {
//                                              }
//                                          }
//
//        );

            lblThemThanhVien.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ThanhVienNhomActivity.this, TaoNhomDanhBaActivity.class);
                    intent.putExtra("tag", "ADD" + tag);
                    intent.putExtra("group_rom_id", group_rom_id);
                    intent.putExtra("thanh_vien_nhom_json", thanh_vien_nhom_json);
                    startActivity(intent);
                }
            });

            lblRoiNhom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(ThanhVienNhomActivity.this, AlertDialog.THEME_HOLO_LIGHT);

                    builder.setTitle(R.string.app_name);
                    builder.setMessage(R.string.pupthongbao)
                            .setCancelable(false)
                            .setPositiveButton(R.string.lblnhom_out,
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            RoiNhom();
                                            dialog.cancel();
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.cancel();
                                        }
                                    });

                    AlertDialog alert = builder.create();
                    alert.show();


                }
            });

//            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ThanhVienNhomActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//
//                    if (thanhVienNhomAdapter != null && thanhVienNhomAdapter.checkhide == false && ChatActivity.chat == false) {
//                        Intent intent = new Intent(ThanhVienNhomActivity.this, ChatActivity.class);
//                        intent.putExtra("group_rom_id", "");
//                        intent.putExtra("name", thanhVienNhomInfos.get(position).getName().equals("") ? thanhVienNhomInfos.get(position).getUsername() : thanhVienNhomInfos.get(position).getName());
//                        intent.putExtra("avatar", thanhVienNhomInfos.get(position).getAvatar());
//                        intent.putExtra("user_id", thanhVienNhomInfos.get(position).getId());
//                        intent.putExtra("id_tin_nhan", thanhVienNhomInfos.get(position).getId());
//                        intent.putExtra("is_group", "0");
//                        intent.putExtra("tag", "ROOM");
//                        startActivity(intent);
//                    }
//
//                }
//            }));
            lblMoiKhoiNhom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
//                        RoiNhom();
                        if (thanhVienNhomAdapter != null) {
                            if (thanhVienNhomAdapter.checkhide == true) {
                                thanhVienNhomAdapter.checkhide = false;
                                lblMoiKhoiNhom.setTextColor(ThanhVienNhomActivity.this.getResources().getColor(R.color.lblthich));
                            } else {
                                thanhVienNhomAdapter.checkhide = true;
                                lblMoiKhoiNhom.setTextColor(ThanhVienNhomActivity.this.getResources().getColor(R.color.lblname));
                            }

                            thanhVienNhomAdapter.notifyDataSetChanged();
                        }
                    } catch (NullPointerException e) {
                        Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
                    } catch (Exception ex) {
                        Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
                    } catch (Error er) {
                        Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
                    }


                }
            });

            txtDoiTenNhom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtTenNhom.setFocusableInTouchMode(true);
                    layoutName.setVisibility(View.VISIBLE);
                    lblTenNhom.setVisibility(View.GONE);
                    txtTenNhom.setSelection(txtTenNhom.getText().length());
                    Drawable img = ThanhVienNhomActivity.this.getResources().getDrawable(R.drawable.btn_edit_group);
                    txtDoiTenNhom.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                }
            });
            btnDoiTen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtTenNhom.setFocusableInTouchMode(false);
                    layoutName.setVisibility(View.GONE);
                    lblTenNhom.setVisibility(View.VISIBLE);
                    Drawable img = ThanhVienNhomActivity.this.getResources().getDrawable(R.drawable.btn_edit_group_hove);
                    txtDoiTenNhom.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    String token = sharePreference.getChatToken();

                    if (!txtTenNhom.getText().toString().trim().equals(name.trim())) {

                        if (tag.equals("GROUP")) {
                            ConnectApiSocket.RenameGroup(ThanhVienNhomActivity.this, token, group_rom_id, txtTenNhom.getText().toString(), new ResultListener() {
                                @Override
                                public void onSuccess(String json) {
                                    if (json.contains("Success")) {
//                                        for (int i = 0; i < NhomDanhBaFragment.danhBaInfoArrayList.size(); i++) {
//                                            if (NhomDanhBaFragment.danhBaInfoArrayList.get(i).getId().equals(group_rom_id)) {
//                                                NhomDanhBaFragment.danhBaInfoArrayList.get(i).setName(txtDoiTenNhom.getText().toString());
//                                            }
//                                        }
//                                        NhomDanhBaFragment.danhBaAdapter.notifyDataSetChanged();
                                        lblTenNhom.setText(txtTenNhom.getText().toString());
                                        txtTitleHeader.setText(txtTenNhom.getText().toString());
                                    } else {
                                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
//                            ModelManager.RenameGroup(ThanhVienNhomActivity.this, new ModelManagerListener() {
//                                @Override
//                                public void onError(VolleyError error) {
//                                    Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//
//                                }
//
//                                @Override
//                                public void onSuccess(String json) {
//                                    if (json.contains("Success")) {
//                                        for (int i = 0; i < NhomDanhBaFragment.danhBaInfoArrayList.size(); i++) {
//                                            if (NhomDanhBaFragment.danhBaInfoArrayList.get(i).getId().equals(group_rom_id)) {
//                                                NhomDanhBaFragment.danhBaInfoArrayList.get(i).setName(txtDoiTenNhom.getText().toString());
//                                            }
//                                        }
//                                        NhomDanhBaFragment.danhBaAdapter.notifyDataSetChanged();
//                                        lblTenNhom.setText(txtTenNhom.getText().toString());
//                                        lblTitleScreen.setText(txtTenNhom.getText().toString());
//
//                                    } else {
//                                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            }, WebServiceConfig.URL_RENAME_GROUP, group_rom_id, txtTenNhom.getText().toString());
                        } else {
                            ConnectApiSocket.RenameRoom(ThanhVienNhomActivity.this, token, group_rom_id, txtTenNhom.getText().toString(), new ResultListener() {
                                @Override
                                public void onSuccess(String json) {
                                    if (json.contains("Success")) {
//                                        for (int i = 0; i < NhomDanhBaFragment.danhBaInfoArrayList.size(); i++) {
//                                            if (NhomDanhBaFragment.danhBaInfoArrayList.get(i).getId().equals(group_rom_id)) {
//                                                NhomDanhBaFragment.danhBaInfoArrayList.get(i).setName(txtDoiTenNhom.getText().toString());
//                                            }
//                                        }
//                                        NhomDanhBaFragment.danhBaAdapter.notifyDataSetChanged();
                                        lblTenNhom.setText(txtTenNhom.getText().toString());
                                        txtTitleHeader.setText(txtTenNhom.getText().toString());
                                    } else {
                                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
//                            ModelManager.RenameRoom(ThanhVienNhomActivity.this, new ModelManagerListener() {
//                                @Override
//                                public void onError(VolleyError error) {
//                                    Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                                }
//
//                                @Override
//                                public void onSuccess(String json) {
//                                    if (json.contains("Success")) {
//                                        for (int i = 0; i < TinNhanActivity.danhBaInfoArrayList.size(); i++) {
//                                            if (TinNhanActivity.danhBaInfoArrayList.get(i).getId().equals(group_rom_id)) {
//                                                TinNhanActivity.danhBaInfoArrayList.get(i).setName(txtDoiTenNhom.getText().toString());
//                                            }
//                                        }
//                                        TinNhanActivity.danhBaAdapter.notifyDataSetChanged();
//                                        lblTenNhom.setText(txtTenNhom.getText().toString());
//                                        lblTitleScreen.setText(txtTenNhom.getText().toString());
//                                    } else {
//                                        Toast.makeText(ThanhVienNhomActivity.this, ThanhVienNhomActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            }, WebServiceConfig.URL_RENAME_ROOM, group_rom_id, txtTenNhom.getText().toString());
                        }

                    }

                }
            });


        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
        }
    }

    public void callGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2910);
            } else {
                // continue with your code
                intentGallery();
            }
        } else {
            // continue with your code
            intentGallery();
        }

    }

    public void callCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 2909);
            } else {
                // continue with your code
                intentCamera();
            }
        } else {
            // continue with your code
            intentCamera();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentCamera();
                } else {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 2910: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentGallery();
                } else {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == ThanhVienNhomActivity.this.RESULT_OK) {
                if (requestCode == PICK_FROM_GALLERY) {
                    Uri selectedImage = data.getData();
                    doCrop(selectedImage);
                }
                if (requestCode == CAMERA_REQUEST) {

                    Bundle extras2 = data.getExtras();
                    if (extras2 != null) {
                        Bitmap bm1 = extras2.getParcelable("data");
                        Uri selectedImage = data.getData();
                        doCrop(selectedImage);
                    }
                }
                if (requestCode == CROP_FROM_CAMERA) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Log.e("UpdateTaiKhoanHsActivity", "Start AyncTask to upload Image:   " + requestCode);
                        photo = extras.getParcelable("data");
                        if (photo != null && mImageCaptureUri.getPath() != null && !mImageCaptureUri.getPath().equals("")) {
                            photo = StringUtility.getRotatedBitmap(mImageCaptureUri.getPath(), photo);

                            img_Avartar.setImageBitmap(photo);
                            if (!NetworkUtil.isConnectingToInternet(ThanhVienNhomActivity.this)) {
                                Toast.makeText(ThanhVienNhomActivity.this, R.string.error_connect, Toast.LENGTH_LONG).show();
                            } else {
//                                if (!WebServiceConfig.mSocket.connected())
//                                    Toast.makeText(ThanhVienNhomActivity.this, R.string.error_connect, Toast.LENGTH_LONG).show();
//                                else {
//
//                                }
                                executeMultipartPost(photo);
                            }

                        } else {
                            Toast.makeText(
                                    ThanhVienNhomActivity.this,
                                    ThanhVienNhomActivity.this.getResources().getString(
                                            R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    private void doCrop(Uri nameImage) {
        mImageCaptureUri = nameImage;
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText(this, ThanhVienNhomActivity.this.getResources().getString(
                    R.string.lblerrcrop),
                    Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (mImageCaptureUri != null) {
                            getContentResolver().delete(mImageCaptureUri, null,
                                    null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }

    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return data.getByteCount();
        } else {
            return data.getAllocationByteCount();
        }
    }

    public void executeMultipartPost(Bitmap bm) {


        try {
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost postRequest = new HttpPost(WebServiceConfig.URL_GET_GROUPS_THAY_AVATAR);
//
//            MultipartEntity reqEntity = new MultipartEntity(
//                    HttpMultipartMode.BROWSER_COMPATIBLE);
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            bm.compress(Bitmap.CompressFormat.PNG, 100, bos);
//            byte[] data = bos.toByteArray();
//            ByteArrayBody bab = new ByteArrayBody(data, "image/png", "x.jpg");
//            reqEntity.addPart("avatar", bab);
//            reqEntity.addPart("token", new StringBody(GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_TOKEN)));
//            reqEntity.addPart("id", new StringBody(id_nhom));
//
//            postRequest.setEntity(reqEntity);
//            HttpResponse response = httpClient.execute(postRequest);
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    response.getEntity().getContent(), "UTF-8"));
//
//
//            s = new StringBuilder();
//
//            while ((sResponse = reader.readLine()) != null) {
//                s = s.append(sResponse);
//            }
//            if (!s.equals("") && !s.toString().contains("false") && !s.toString().contains("failed") && !s.toString().contains("DOCTYPE")) {
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(BangTinActivity.arrTinNhanInfo != null && BangTinActivity.arrTinNhanInfo.size() > 0 && bangTinAdapter != null)
//                        {
//                            for (int i = 0; i <  BangTinActivity.arrTinNhanInfo.size(); i++)
//                            {
//                                if (BangTinActivity.arrTinNhanInfo.get(i).getNguoi_gui_avatar().equals(s.toString()))
//                                {
//                                    BangTinActivity.arrTinNhanInfo.get(i).setNguoi_gui_ten(lblTenNhom.getText().toString());
//                                    bangTinAdapter.notifyDataSetChanged();
//
//                                }
//                            }
//
//                        }
//                        if(DanhSachNhom.danhSachNhomArrayListShow != null && DanhSachNhom.danhSachNhomArrayListShow.size() > 0 && DanhSachNhom.danhBaAdapter != null)
//                        {
//                            for (int i = 0; i < DanhSachNhom.danhSachNhomArrayListShow.size(); i++)
//                            {
//                                if (DanhSachNhom.danhSachNhomArrayListShow.get(i).getNguoi_gui_avatar().equals(s.toString()))
//                                {
//                                    DanhSachNhom.danhSachNhomArrayListShow.get(i).setNguoi_gui_ten(lblTenNhom.getText().toString());
//                                    DanhSachNhom.danhBaAdapter.notifyDataSetChanged();
//
//                                }
//                            }
//
//                        }
//                    }
//                });
//
//
//            } else {
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        Toast.makeText(
//                                ThanhVienNhomActivity.this,
//                                ThanhVienNhomActivity.this.getResources().getString(
//                                        R.string.lblerrdoianh), Toast.LENGTH_LONG).show();
//                    }
//                });
//
//            }

        } catch (Exception e) {

            Log.e("UpdateTaiKhoan", "catch : ");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrdoianh), Toast.LENGTH_LONG).show();
                }
            });

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (checkChange == true) {
                thanhVienNhomInfos.clear();
                binData();
                checkChange = false;
            }
            connectSocket();
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
        }
    }


    public void intentCamera() {
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, ThanhVienNhomActivity.this.getResources().getString(
                    R.string.lblerrcamera), Toast.LENGTH_LONG)
                    .show();
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
            try {
                ContentValues values = new ContentValues();
                intent.putExtra("return-data", true);
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                startActivityForResult(intent, CAMERA_REQUEST);
            } catch (NullPointerException e) {
                // Do nothing for now
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        dialog.cancel();
                        Toast.makeText(
                                ThanhVienNhomActivity.this,
                                ThanhVienNhomActivity.this.getResources().getString(
                                        R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                    }
                });
            } catch (ActivityNotFoundException e) {
                // Do nothing for now
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        dialog.cancel();
                        Toast.makeText(
                                ThanhVienNhomActivity.this,
                                ThanhVienNhomActivity.this.getResources().getString(
                                        R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }

    @SuppressLint("LongLogTag")
    public void intentGallery() {
        try {
            Intent photoPicker = new Intent(Intent.ACTION_GET_CONTENT);
            Log.e("FILE NAME FROM CALL GALLERY", Integer.toString(id) + "");

            file = new File(Environment.getExternalStorageDirectory() + "/"
                    + folder, id + ".jpg");
            Uri imgUri = Uri.fromFile(file);
            Log.d("URI from callgallery", imgUri.toString());
            photoPicker.setType("image/*");
            photoPicker.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            photoPicker.putExtra("return-data", true);
            photoPicker.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.name());
            startActivityForResult(photoPicker, PICK_FROM_GALLERY);
        } catch (NullPointerException e) {
            // Do nothing for now
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    dialog.cancel();
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
            });
        } catch (ActivityNotFoundException e) {
            // Do nothing for now
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    dialog.cancel();
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private String sizeimage(final ImageView imageView) {
        ViewTreeObserver vto = imageView.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                w = imageView.getMeasuredHeight();
                h = imageView.getMeasuredWidth();
                return true;
            }
        });
        return w + "_" + h + "/";
    }

    private void RoiNhom() {
        try {
            if (tag.equals("GROUP")) {

                ConnectApiSocket.RemoveGroup(ThanhVienNhomActivity.this, sharePreference.getChatToken(), group_rom_id, new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.toLowerCase().contains("success")) {
                            thanhVienNhomInfos.remove(vitri);
                            thanhVienNhomAdapter.notifyDataSetChanged();
                            finish();
                        } else {
                            Toast.makeText(
                                    ThanhVienNhomActivity.this,
                                    ThanhVienNhomActivity.this.getResources().getString(
                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } else {

                ConnectApiSocket.LeaveRoom(ThanhVienNhomActivity.this, sharePreference.getChatToken(), group_rom_id, new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.toLowerCase().contains("success")) {
                            Intent intent = new Intent(ThanhVienNhomActivity.this, TinNhanActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                        } else {
                            Toast.makeText(
                                    ThanhVienNhomActivity.this,
                                    ThanhVienNhomActivity.this.getResources().getString(
                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    private void XoaThanhVien(final String user_id_remove) {

        try {
            String token = sharePreference.getChatToken();
            if (tag.equals("GROUP")) {
                ConnectApiSocket.RemoveUserGroupContacts(ThanhVienNhomActivity.this, token, group_rom_id, user_id_remove, new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.toLowerCase().contains("success")) {
                            thanhVienNhomInfos.remove(vitri);
                            thanhVienNhomAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(
                                    ThanhVienNhomActivity.this,
                                    ThanhVienNhomActivity.this.getResources().getString(
                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ModelManager.RemoveUserGroupContacts(ThanhVienNhomActivity.this, new ModelManagerListener() {
//                    @Override
//                    public void onError(VolleyError error) {
//                        Toast.makeText(
//                                ThanhVienNhomActivity.this,
//                                ThanhVienNhomActivity.this.getResources().getString(
//                                        R.string.errchodiem), Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onSuccess(String json) {
//
//                        if (json.toLowerCase().contains("success")) {
//                            thanhVienNhomInfos.remove(vitri);
//                            thanhVienNhomAdapter.notifyDataSetChanged();
//                        } else {
//                            Toast.makeText(
//                                    ThanhVienNhomActivity.this,
//                                    ThanhVienNhomActivity.this.getResources().getString(
//                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }, WebServiceConfig.URL_REMOVE_USER_GROUP, group_rom_id, user_id_remove);
            } else {
                ConnectApiSocket.RemoveUserRoom(ThanhVienNhomActivity.this, token, group_rom_id, user_id_remove, new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.toLowerCase().contains("success")) {
                            thanhVienNhomInfos.remove(vitri);
                            thanhVienNhomAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(
                                    ThanhVienNhomActivity.this,
                                    ThanhVienNhomActivity.this.getResources().getString(
                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ModelManager.RemoveUserRoom(ThanhVienNhomActivity.this, new ModelManagerListener() {
//                    @Override
//                    public void onError(VolleyError error) {
//                        Toast.makeText(
//                                ThanhVienNhomActivity.this,
//                                ThanhVienNhomActivity.this.getResources().getString(
//                                        R.string.errchodiem), Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onSuccess(String json) {
//
//                        if (json.toLowerCase().contains("success")) {
//                            thanhVienNhomInfos.remove(vitri);
//                            thanhVienNhomAdapter.notifyDataSetChanged();
//                        } else {
//                            Toast.makeText(
//                                    ThanhVienNhomActivity.this,
//                                    ThanhVienNhomActivity.this.getResources().getString(
//                                            R.string.errchodiem), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }, WebServiceConfig.URL_REMOVE_USER_ROOM, group_rom_id, user_id_remove);
            }
        } catch (NullPointerException e) {
            Log.e("", "TAG QUAN LY NHOM:  " + e.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Log.e("", "TAG QUAN LY NHOM:  " + ex.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Error er) {
            Log.e("", "TAG QUAN LY NHOM:  " + er.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            ThanhVienNhomActivity.this,
                            ThanhVienNhomActivity.this.getResources().getString(
                                    R.string.errchodiem), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private class LoadDataAsynTask1 extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            try {

                if (thanhVienNhomInfos != null && thanhVienNhomInfos.size() > 0) {
                    thanhVienNhomAdapter = new ThanhVienNhomAdapter(ThanhVienNhomActivity.this, thanhVienNhomInfos, sharePreference.getChatId(), new YeuThichListener() {
                        @Override
                        public void onChangeYeuThich() {
                            XoaThanhVien(thanhVienNhomInfos.get(vitri).getId());
                        }
                    }, tag);

                }

            } catch (NullPointerException e) {
                Log.e("", "NHAT KY:  " + e.toString());
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                if (thanhVienNhomInfos.size() > 0) {
                    recyNhom.setAdapter(thanhVienNhomAdapter);
                    thanhVienNhomAdapter.notifyDataSetChanged();
                    recyNhom.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                } else {
                    recyNhom.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        WebServiceConfig.mSocket.off("user_change_status", user_change_status);
        ConnectApiSocket.socketOffUserChangeStatus();
    }

    private void connectSocket() {
        try {
            ConnectApiSocket.ConnectSocket(sharePreference.getChatToken());
            ConnectApiSocket.socketOnUserChangeStatus(new ResultListener() {
                @Override
                public void onSuccess(final String json) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DanhBaInfo danhBaInfo = ParserUtility.parseStatusOnline(json);
                            for (int i = 0; i < thanhVienNhomInfos.size(); i++) {
                                if (danhBaInfo.getId().equals(thanhVienNhomInfos.get(i).getId()))
                                    thanhVienNhomInfos.get(i).setOnline(danhBaInfo.getOnline());
                            }
                            if (thanhVienNhomAdapter != null)
                                thanhVienNhomAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });

//            if (WebServiceConfig.mSocket == null || !WebServiceConfig.mSocket.connected()) {
//                IO.Options opts = new IO.Options();
//                opts.forceNew = true;
//                opts.query = "token=" + GlobalValue.prefs.getStringValue(ChatSharedPreferences.PREF_SETTING_TOKEN);
//                WebServiceConfig.mSocket = IO.socket(WebServiceConfig.API_URL, opts);
//                WebServiceConfig.mSocket.connect();
//            }
//
//            WebServiceConfig.mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
//            WebServiceConfig.mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//            WebServiceConfig.mSocket.on("user_change_status", user_change_status);
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    public void onClickBack(View v) {
        onKeyBack();
    }


    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
