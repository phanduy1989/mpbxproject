package com.viettel.view.activity.authen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.UserProfile;
import com.viettel.model.response.CheckUserInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.store.AppSharePreference;
import com.viettel.store.GlobalValue;
import com.viettel.utils.FontTypeface;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;
import com.viettel.view.custom.TextViewWithFont;

import org.json.JSONObject;

public class AccountActivity extends BABaseActivity {

	EditText edtName, edtPhone, edtEmail, edtPart, edtNote;

	RelativeLayout btnRight;
	RelativeLayout layoutLoading;
	ImageView icon_header_right, icon_header_left;
	TextView txtTitleHeader;

	AppSharePreference appSharePreference;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		appSharePreference = AppSharePreference.getInstance(this);
		setContentView(R.layout.fragment_account);
		initView();

		initData();
	}
	public void initView() {
		txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
		txtTitleHeader.setText(getString(R.string.accountTitleInfo));

		edtName = (EditText) findViewById(R.id.edtName);
		edtPhone = (EditText) findViewById(R.id.edtPhone);
		edtEmail = (EditText) findViewById(R.id.edtEmail);
		edtPart = (EditText) findViewById(R.id.edtPart);
		edtNote = (EditText) findViewById(R.id.edtNote);

		edtName.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));
		edtEmail.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));
		edtPhone.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));
		edtPart.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));
		edtNote.setTypeface(FontTypeface.getTypecace(this, FontTypeface.FONT_ROBOTO_REGULAR));

		btnRight = (RelativeLayout) findViewById(R.id.btnRight);
		layoutLoading = (RelativeLayout) findViewById(R.id.layoutLoading);

		icon_header_left = (ImageView) findViewById(R.id.icon_header_left);
		icon_header_right = (ImageView) findViewById(R.id.icon_header_right);

	}

	public void initData() {

//		showLoading();
//			CheckUserInfoRequest checkUserInfoRequest = new CheckUserInfoRequest();
//			CheckUserInfoObject checkUserInfoObject = new CheckUserInfoObject();
////			checkUserInfoObject.setUserId(appSharePreference.getNumOfCompany());
//			checkUserInfoObject.setToken(appSharePreference.getAccessToken());
//			checkUserInfoRequest.setCheckUserInfoObject(checkUserInfoObject);
//
//			ApiController.doPostRequest(this, checkUserInfoRequest, new ResponseListener() {
//				@Override
//				public void processResponse(String response) {
//					hideLoading();
//					Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
//					CheckUserInfoResponse checkUserInfoResponse = gson.fromJson(response, CheckUserInfoResponse.class);
//					fetchData(checkUserInfoResponse);
//				}
//
//				@Override
//				public void processResponse(int error, String content) {
//					hideLoading();
//				}
//			});

	}

	public void fetchData(CheckUserInfoResponse checkUserInfoResponse) {
		if(!StringUtility.isEmpty(checkUserInfoResponse.getUser().getFullname())) {
			edtName.setText(checkUserInfoResponse.getUser().getFullname());
		}
		if(!StringUtility.isEmpty(checkUserInfoResponse.getUser().getEmail())) {
			edtEmail.setText(checkUserInfoResponse.getUser().getEmail());
		}
		if(!StringUtility.isEmpty(checkUserInfoResponse.getUser().getPhoneNumber())) {
			edtPhone.setText(checkUserInfoResponse.getUser().getPhoneNumber());
		}
	}

	public void onClickRight(View v) {
//		showLoading();
//		String fullName = edtName.getEditableText().toString().trim();
//		String phoneNumber = edtPhone.getEditableText().toString().trim();
//		String email = edtEmail.getEditableText().toString().trim();
//		String currentPassword = edtCurrentPassword.getEditableText().toString();
//		String newPassword = edtNewPassword.getEditableText().toString();
//		String confirmNewpassword = edtConfirmNewPassword.getEditableText().toString();
//
//		if(StringUtility.isEmpty(email)) {
//			new MaterialDialog.Builder(AccountActivity.this)
//					.title(getResources().getString(R.string.errorEmptyEmail))
//					.positiveText(getResources().getString(R.string.labelAccept))
//					.onPositive(new MaterialDialog.SingleButtonCallback() {
//						@Override
//						public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//						}
//					})
//					.show();
//			return;
//		}
//
//		if(isChangePassword) {
//			if(StringUtility.isEmpty(currentPassword)) {
//				new MaterialDialog.Builder(AccountActivity.this)
//						.title(getResources().getString(R.string.errorEmptyCurrentPassword))
//						.positiveText(getResources().getString(R.string.labelAccept))
//						.onPositive(new MaterialDialog.SingleButtonCallback() {
//							@Override
//							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//							}
//						})
//						.show();
//				return;
//			}
//			if(StringUtility.isEmpty(newPassword)) {
//				new MaterialDialog.Builder(AccountActivity.this)
//						.title(getResources().getString(R.string.errorEmptyNewPassword))
//						.positiveText(getResources().getString(R.string.labelAccept))
//						.onPositive(new MaterialDialog.SingleButtonCallback() {
//							@Override
//							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//							}
//						})
//						.show();
//				return;
//			}
//			if(StringUtility.isEmpty(confirmNewpassword)) {
//				new MaterialDialog.Builder(AccountActivity.this)
//						.title(getResources().getString(R.string.errorEmptyConfirmPassword))
//						.positiveText(getResources().getString(R.string.labelAccept))
//						.onPositive(new MaterialDialog.SingleButtonCallback() {
//							@Override
//							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//							}
//						})
//						.show();
//				return;
//			}
//			if(!newPassword.equals(confirmNewpassword)) {
//				new MaterialDialog.Builder(AccountActivity.this)
//						.title(getResources().getString(R.string.errorPasswordNotMatch))
//						.positiveText(getResources().getString(R.string.labelAccept))
//						.onPositive(new MaterialDialog.SingleButtonCallback() {
//							@Override
//							public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//							}
//						})
//						.show();
//				return;
//			}
//		}
//
//		UpdateAccountObject updateAccountObject = new UpdateAccountObject();
//		updateAccountObject.setToken(appSharePreference.getAccessToken());
//		updateAccountObject.setUserId(appSharePreference.getNumOfCompany());
//		updateAccountObject.setUsername(appSharePreference.getUserName());
//		updateAccountObject.setFullname(fullName);
//		updateAccountObject.setEmail(email);
//		updateAccountObject.setPhone(phoneNumber);
//
//		if(isChangePassword) {
//			updateAccountObject.setPassword(currentPassword);
//			updateAccountObject.setNewPassword(newPassword);
//			updateAccountObject.setReNewPassword(confirmNewpassword);
//		}
//
//		PostEntityResourceRequest postEntityResourceRequest = new PostEntityResourceRequest();
//		postEntityResourceRequest.setData(updateAccountObject, UpdateAccountObject.class);
//
//		ApiController.doPostRequest(this, postEntityResourceRequest, new ResponseListener() {
//			@Override
//			public void processResponse(String response) {
//				hideLoading();
//				Gson gson = new Gson();
//				ResponseObject responseObject = gson.fromJson(response, ResponseObject.class);
//
//				if(responseObject.getHttpStatus() == 200) {
//					new MaterialDialog.Builder(AccountActivity.this)
//							.title(getResources().getString(R.string.alertUpdateInfoSuccess))
//							.positiveText(getResources().getString(R.string.labelAccept))
//							.onPositive(new MaterialDialog.SingleButtonCallback() {
//								@Override
//								public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//								}
//							})
//							.show();
//
//				} else {
//					Toast.makeText(AccountActivity.this, responseObject.getMessage(), Toast.LENGTH_SHORT).show();
//				}
//
//			}
//
//			@Override
//			public void processResponse(int error, String content) {
//				Toast.makeText(AccountActivity.this, R.string.alertAcctionUnsucess, Toast.LENGTH_SHORT).show();
//				hideLoading();
//			}
//		});

	}

	public void onClickCreate(View view) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode != Activity.RESULT_OK) {
			return;
		}
	}

	public void onClickBack(View v) {
		onKeyBack();
	}
	@Override
	public void onKeyBack() {
		finish();
	}

}
