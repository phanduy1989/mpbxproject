package com.viettel.view.activity.authen;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rengwuxian.edittext.validation.METValidator;
import com.viettel.mpbx.R;
import com.viettel.view.base.BABaseActivity;
import com.viettel.view.custom.EdittextWithFont;

public class ChangePassActivity extends BABaseActivity {

	EditText edtNewPassword, edtOldPassword, edtConfirmNewPassword;
	ImageView iconShowNewPass, iconShowOldPass, iconShowConfirmNewPass;

	boolean isShowNewPass, isShowOldPass, isShowConfirmPass;
	TextView txtTitleHeader;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_activity_forgot_password);
		initView();
	}


	public void initView() {

		txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
		txtTitleHeader.setText(getString(R.string.labelChangePassword));

		edtNewPassword = (EditText) findViewById(R.id.edtNewPassword);
		edtOldPassword = (EditText) findViewById(R.id.edtOldPassword);
		edtConfirmNewPassword = (EditText) findViewById(R.id.edtConfirmNewPassword);

		iconShowNewPass = (ImageView) findViewById(R.id.iconShowNewPass);
		iconShowOldPass = (ImageView) findViewById(R.id.iconShowOldPass);
		iconShowConfirmNewPass = (ImageView) findViewById(R.id.iconShowConfirmNewPass);

		iconShowOldPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isShowOldPass) {
					isShowOldPass = false;
					iconShowOldPass.setImageResource(R.drawable.view_pass);
					edtOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					if(edtOldPassword.isFocused()) {
						edtOldPassword.setSelection(edtOldPassword.getText().length());
					}
				} else {
					isShowOldPass = true;
					iconShowOldPass.setImageResource(R.drawable.view_pass_active);
					edtOldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					if(edtOldPassword.isFocused()) {
						edtOldPassword.setSelection(edtOldPassword.getText().length());
					}

				}
			}
		});
		iconShowNewPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isShowNewPass) {
					isShowNewPass = false;
					iconShowNewPass.setImageResource(R.drawable.view_pass);
					edtNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					if(edtNewPassword.isFocused()) {
						edtNewPassword.setSelection(edtNewPassword.getText().length());
					}
				} else {
					isShowNewPass = true;
					iconShowNewPass.setImageResource(R.drawable.view_pass_active);
					edtNewPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					if(edtNewPassword.isFocused()) {
						edtNewPassword.setSelection(edtNewPassword.getText().length());
					}
				}
			}
		});
		iconShowConfirmNewPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isShowConfirmPass) {
					isShowConfirmPass = false;
					iconShowConfirmNewPass.setImageResource(R.drawable.view_pass);
					edtConfirmNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					if(edtConfirmNewPassword.isFocused()) {
						edtConfirmNewPassword.setSelection(edtConfirmNewPassword.getText().length());
					}
				} else {
					isShowConfirmPass = true;
					iconShowConfirmNewPass.setImageResource(R.drawable.view_pass_active);
					edtConfirmNewPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					if(edtConfirmNewPassword.isFocused()) {
						edtConfirmNewPassword.setSelection(edtConfirmNewPassword.getText().length());
					}
				}
			}
		});
	}

	public void onClickCreate(View view) {

	}

	public void onClickBack(View v) {
		onKeyBack();
	}

	public void onClickGetPass(View view) {
//		String email = edtEmail.getEditableText().toString().trim();
//
//		if (email.length() == 0) {
//			edtEmail.validate();
//			showKeyBoard(edtEmail);
//			return;
//		}
//
//		showLoading();
//
//		ForgotPasswordObject forgotPasswordObject = new ForgotPasswordObject();
//		forgotPasswordObject.setEmail(email);
//
//		PostEntityResourceRequest postEntityResourceRequest = new PostEntityResourceRequest();
//		postEntityResourceRequest.setData(forgotPasswordObject, ForgotPasswordObject.class);
//
//		ApiController.doPostRequest(this, postEntityResourceRequest, new ResponseListener() {
//			@Override
//			public void processResponse(String response) {
//				hideLoading();
//
//				Gson gson = new Gson();
//				ResponseObject responseObject = gson.fromJson(response, ResponseObject.class);
//
//				if(responseObject.getHttpStatus() == 200) {
//					new MaterialDialog.Builder(ChangePassActivity.this)
//							.title(getResources().getString(R.string.alertGetPasswordSuccess))
//							.positiveText(getResources().getString(R.string.labelClose))
//							.show();
//				} else {
//					Toast.makeText(ChangePassActivity.this, responseObject.getMessage(), Toast.LENGTH_SHORT).show();
//				}
//			}
//
//			@Override
//			public void processResponse(int error, String content) {
//				hideLoading();
//				Gson gson = new Gson();
//				try {
//					ErrorResponse errorResponse = gson.fromJson(content, ErrorResponse.class);
//
//					String message = "";
//
//					if (errorResponse != null && !StringUtility.isEmpty(errorResponse.getMessage())) {
//						message = errorResponse.getMessage();
//					} else {
//						message = getResources().getString(R.string.alertAcctionUnsucess);
//					}
//					Toast.makeText(ChangePassActivity.this, message, Toast.LENGTH_SHORT).show();
//				} catch (JsonSyntaxException e) {
//					Toast.makeText(ChangePassActivity.this, R.string.alertAcctionUnsucess, Toast.LENGTH_SHORT).show();
//				}
//
//			}
//		});

	}

	@Override
	public void onKeyBack() {
		finish();
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
	}

}
