package com.viettel.view.activity;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.store.AppSharePreference;
import com.viettel.view.base.BABaseActivity;

import me.relex.circleindicator.CircleIndicator;
import com.viettel.mpbx.R;

/**
 * Created by duyuno on 11/22/16.
 */
public class FirstIntroActivity extends BABaseActivity {

    AppSharePreference appSharePreference;

    private static String[] CONTENT = new String[] {
            "Theo dõi và cảnh báo nhiệt độ của bé mọi lúc mọi nơi màn hình",
            "Đơn giản và thân thiện màn hình",
            "Rất nhiều ưu đãi khi mua hàng nhân dịp năm mới"
    };
    private static int[] resId = new int[] {
//            R.drawable.first_intro_1,
//            R.drawable.first_intro_2,
//            R.drawable.first_intro_3,
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appSharePreference = com.viettel.store.AppSharePreference.getInstance(this);
        setContentView(R.layout.layout_first_introduce);
        initView();
        initData();
    }

    public void initView() {
        ViewPager viewpager = (ViewPager) findViewById(R.id.pager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewpager.setAdapter(new SamplePagerAdapter());
        indicator.setViewPager(viewpager);
    }
    public void initData() {

    }

    public void onClickSkip(View v) {
        appSharePreference.putBooleanValue(AppSharePreference.FIRST_RUNNED, true);
        finish();
    }

    @Override
    public void onKeyBack() {
//        finish();
    }

    public class SamplePagerAdapter extends PagerAdapter {

        public SamplePagerAdapter() {
        }


        @Override
        public int getCount() {
            return resId.length;
        }

        @Override public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override public void destroyItem(ViewGroup view, int position, Object object) {
            view.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {

            RelativeLayout relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_first_introduce, null);

            TextView textView = (TextView) relativeLayout.findViewById(R.id.description);
            ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.image);

            imageView.setImageResource(resId[position]);
            textView.setText(CONTENT[position]);

            view.addView(relativeLayout, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);


            return relativeLayout;
        }
    }
}
