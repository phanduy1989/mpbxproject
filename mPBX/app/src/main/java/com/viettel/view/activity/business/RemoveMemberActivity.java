package com.viettel.view.activity.business;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.MPBXFilter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.OnChildSelectedListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.ConversationMember;
import com.viettel.model.CustomerModel;
import com.viettel.model.PBXModel;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.model.request.colleague.GetListAddingColleagueRequest;
import com.viettel.model.request.conference.GetListConferenceColleagueRequest;
import com.viettel.model.request.conference.GetListConferenceCustomerRequest;
import com.viettel.model.response.colleague.GetListColleagueResponse;
import com.viettel.model.response.conference.GetConferenceListAddCustomerResponse;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.AnimationUtil;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.colleague.CreateColleagueGroupActivity;
import com.viettel.view.activity.business.customer.CustomerInfoActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by duyuno on 11/22/16.
 */
public class RemoveMemberActivity extends BABaseActivity {

    TextView txtTitleHeader;
    ImageView icon_right;

    private ListView listView;
    private MemberAdaper memberAdaper;

    private boolean isNext;
    ArrayList<ConversationMember> listMember;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_chose_member_activity);

        initView();
    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        icon_right = (ImageView) findViewById(R.id.icon_right);
        listView = (ListView) findViewById(R.id.listView);

        txtTitleHeader.setText("Chọn thành viên để xóa");


        icon_right.setImageResource(R.drawable.btn_next_icon);

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        memberAdaper = new MemberAdaper(this);
        listView.setAdapter(memberAdaper);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                memberAdaper.onTichItem(position);
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                memberAdaper.filter(text);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void onClickRefresh(View v) {
        initData();
    }

    public void initData() {

        Intent intent = getIntent();
        listMember = intent.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT);

        if(listMember == null || listMember.isEmpty()) {
            finish();
        } else {
            memberAdaper.setData(listMember);
        }
    }


    public void onClickHeaderRight(View v) {
        isNext = true;
        onKeyBack();
    }

    public void goToEditInfoAcitivity() {
//        startActivity(new Intent(ListColleagueActivity.this.getBaseContext(), EditCustomerInfoActivity.class));
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        Intent resultIntent = new Intent();
        if (isNext) {
            resultIntent.putExtra(GlobalInfo.BUNDLE_KEY_SUCCESS, isNext);
            ArrayList<ConversationMember> list = memberAdaper.getListRemain();
            if (list != null) {
                resultIntent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER, list);
            }
            setResult(Activity.RESULT_OK, resultIntent);
        } else {
            setResult(Activity.RESULT_CANCELED, resultIntent);
        }
        finish();
    }

    class ViewHolder {
        public TextView textName, textPhone;
        public ImageView iconCheck, avatar;
    }

    class MemberAdaper extends BaseAdapter implements MPBXFilter{

        Activity activity;

        public ArrayList<ConversationMember> listData;
        public ArrayList<ConversationMember> listDataDisplay;

        public void setData(Object list) {

            if(list == null) return;

            ArrayList<ConversationMember> listModel = (ArrayList<ConversationMember>) list;
            for (ConversationMember pbxModel : listModel) {
                pbxModel.setSelected(false);
            }

            PBXModel.formatMemberPhoneNumber(listModel);

            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }

            listData.addAll(listModel);
            if (listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }

            listDataDisplay.addAll(listModel);
            notifyDataSetChanged();
        }

        public ArrayList<ConversationMember> getListRemain() {
            if (listDataDisplay == null || listDataDisplay.isEmpty()) return null;

            ArrayList<ConversationMember> list = null;

            for (ConversationMember pbxModel : listData) {
                if (!pbxModel.isSelected()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    }

                    list.add(pbxModel);
                }
            }


            return list;
        }

        public void onTichItem(int position) {
            ConversationMember colleagueModel = listDataDisplay.get(position);
            colleagueModel.changeSelected();
            notifyDataSetChanged();
        }

        public MemberAdaper(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return listDataDisplay != null ? listDataDisplay.size() : 0;
        }

        @Override
        public ConversationMember getItem(int position) {
            return listDataDisplay != null ? listDataDisplay.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.item_chose_member, null);
                viewHolder = new ViewHolder();
                viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
                viewHolder.textPhone = (TextView) convertView.findViewById(R.id.textPhone);
                viewHolder.iconCheck = (ImageView) convertView.findViewById(R.id.iconCheck);
                viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            ConversationMember colleagueModel = getItem(position);

            if (colleagueModel != null) {
                viewHolder.textName.setText(colleagueModel.getMemberName());

                if(colleagueModel.getPhoneNumber() == null || colleagueModel.getPhoneNumber().isEmpty()) {
                    viewHolder.textPhone.setVisibility(View.GONE);
                } else {
                    viewHolder.textPhone.setVisibility(View.VISIBLE);
                    viewHolder.textPhone.setText(colleagueModel.getPhoneNumber());
                }


                if (colleagueModel.isSelected()) {
                    viewHolder.iconCheck.setImageResource(R.drawable.radio_check_icon);
                } else {
                    viewHolder.iconCheck.setImageResource(R.drawable.radio_icon);
                }

            }

            return convertView;
        }

        @Override
        public void filter(String key) {
            if(listData == null || listData.isEmpty()) return;

            ((BABaseActivity) activity).showContent();

            if(key == null || key.isEmpty()) {

                listDataDisplay = new ArrayList<>();
                listDataDisplay.addAll(listData);
                notifyDataSetChanged();
                return;
            }

            key = key.toLowerCase();

            listDataDisplay = new ArrayList<>();

            for(ConversationMember obj : listData) {

                String name = StringUtility.convertVietnameseToAscii(obj.getMemberName());

                if((name != null && name.contains(key)) || obj.getPhoneNumber().contains(key)) {
                    listDataDisplay.add(obj);
                }
            }


            notifyDataSetChanged();
        }
    }

}
