package com.viettel.view.activity.authen;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.ae.team.connectapi.utility.YeuThichListener;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.viettel.adapter.holder.TaoNhomDanhBaAdapter;
import com.viettel.model.DanhBaInfo;
import com.viettel.model.NhomDanhBaInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.ParserUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

import static com.viettel.view.activity.authen.DanhBaActivity.danhBaInfoArrayList;


public class TaoNhomDanhBaActivity extends BABaseActivity {

    TextView txtTitleHeader;
    ImageView icon_right;

    public static boolean checkChamdiem = false, ChekDelete = false;
    private UltimateRecyclerView recyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<DanhBaInfo> danhBaInfoArrayListShow;
    private TaoNhomDanhBaAdapter danhBaAdapter;
    private String tag = "", chon_thanh_vien = "", group_rom_id = "", thanh_vien_nhom_json = "";
    private ProgressBar bar;
    private EditText txtTenNhom;
    private RelativeLayout layoutTenNhom;
    private String token = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taonhomdanhba);
        unitUi();
        binData();
        unitControll();
    }


    public void binData() {
        if (danhBaInfoArrayList != null && danhBaInfoArrayList.size() > 0) {
            new LoadDataAsynTask1().execute();
        } else {

            ConnectApiSocket.getListContact(this, token, "", new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    if (json.contains("Success")) {
                        danhBaInfoArrayList = ParserUtility.parselistDanhBa(json);
                        new LoadDataAsynTask1().execute();
                    }
                }
            });
//            ModelManager.getListContact(this, new ModelManagerListener() {
//                @Override
//                public void onError(VolleyError error) {
//                }
//
//                @Override
//                public void onSuccess(String json) {
//                    danhBaInfoArrayList = ParserUtility.parselistDanhBa(json);
//                    new LoadDataAsynTask1().execute();
//
//                }
//            }, WebServiceConfig.URL_LIST_CONTACT, "");
        }

    }

    private void unitControll() {
        try {
            icon_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < danhBaInfoArrayList.size(); i++) {
                        if (!danhBaInfoArrayList.get(i).getCheck_chon().equals(""))
                            chon_thanh_vien = chon_thanh_vien + "," + danhBaInfoArrayList.get(i).getId();
                    }
                    final String token = sharePreference.getChatToken();

                    if (tag.equals("ADDGROUP") || tag.equals("ADDROOM")) {
                        if (chon_thanh_vien.equals("")) {
                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.empthanhviennhom), Toast.LENGTH_LONG).show();
                        } else {
                            chon_thanh_vien = chon_thanh_vien.substring(1, chon_thanh_vien.length());
                            if (tag.equals("ADDGROUP")) {
                                ConnectApiSocket.AddUserGroup(TaoNhomDanhBaActivity.this, token, group_rom_id, chon_thanh_vien, new ResultListener() {
                                    @Override
                                    public void onSuccess(String json) {
                                        if (json.contains("Success")) {
                                            ThanhVienNhomActivity.checkChange = true;
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.succthemthanhvien), Toast.LENGTH_LONG).show();
                                            finish();
                                        } else {
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            } else {
                                ConnectApiSocket.AddUserRoom(TaoNhomDanhBaActivity.this, token, group_rom_id, chon_thanh_vien, new ResultListener() {
                                    @Override
                                    public void onSuccess(String json) {
                                        if (json.contains("Success")) {
                                            ThanhVienNhomActivity.checkChange = true;
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.succthemthanhvien), Toast.LENGTH_LONG).show();
                                            finish();
                                        } else {
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }
                        }


                    } else {
                        if (tag.equals("NHOMDANHBA")) {
                            if (txtTenNhom.getText().toString().equals("")) {
                                Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.emptennhom), Toast.LENGTH_LONG).show();
                            } else if (chon_thanh_vien.equals("")) {
                                Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.empthanhviennhom), Toast.LENGTH_LONG).show();
                            } else {
                                chon_thanh_vien = chon_thanh_vien.substring(1, chon_thanh_vien.length());

                                ConnectApiSocket.CreateGroup(TaoNhomDanhBaActivity.this, token, txtTenNhom.getText().toString(), new ResultListener() {
                                    @Override
                                    public void onSuccess(String json) {
                                        if (json.contains("Success")) {
                                            final NhomDanhBaInfo nhomDanhBaInfo = ParserUtility.parseCreateGroup(json);
                                            new LoadDataAsynTask1().execute();
                                            ConnectApiSocket.AddUserGroup(TaoNhomDanhBaActivity.this, token, nhomDanhBaInfo.getId(), chon_thanh_vien, new ResultListener() {
                                                @Override
                                                public void onSuccess(String json) {
                                                    if (json.contains("Success")) {
                                                        finish();
                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }

                        } else if (tag.equals("DANHBA")) {

                            if (txtTenNhom.getText().toString().equals("")) {
                                Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.emptennhomchat), Toast.LENGTH_LONG).show();
                            } else if (chon_thanh_vien.equals("")) {
                                Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.empthanhviennhomchat), Toast.LENGTH_LONG).show();
                            } else {
                                chon_thanh_vien = chon_thanh_vien.substring(1, chon_thanh_vien.length());

                                ConnectApiSocket.CreateRoomChat(TaoNhomDanhBaActivity.this, token, txtTenNhom.getText().toString(), new ResultListener() {
                                    @Override
                                    public void onSuccess(String json) {
                                        if (json.contains("Success")) {
                                            final NhomDanhBaInfo nhomDanhBaInfo = ParserUtility.parseCreateGroup(json);
                                            ConnectApiSocket.AddUserRoom(TaoNhomDanhBaActivity.this, token, nhomDanhBaInfo.getId(), chon_thanh_vien, new ResultListener() {
                                                @Override
                                                public void onSuccess(String json) {
                                                    finish();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
//                    ModelManager.CreateRoomChat(this, new ModelManagerListener() {
//                        @Override
//                        public void onError(VolleyError error) {
//                            Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                        }
//
//
//                        @Override
//                        public void onSuccess(String json) {
//
//                            if (json.contains("Success")) {
//                                final NhomDanhBaInfo nhomDanhBaInfo = ParserUtility.parseCreateGroup(json);
//
//                                ModelManager.AddUserRoom(TaoNhomDanhBaActivity.this, new ModelManagerListener() {
//                                    @Override
//                                    public void onError(VolleyError error) {
//                                        Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                                    }
//
//                                    @Override
//                                    public void onSuccess(String json) {
//                                        finish();
//
//                                    }
//                                }, WebServiceConfig.URL_ADD_USER_ROOM, nhomDanhBaInfo.getId(), chon_thanh_vien);
//                            } else {
//                                Toast.makeText(TaoNhomDanhBaActivity.this, TaoNhomDanhBaActivity.this.getResources().getString(R.string.errorhethong), Toast.LENGTH_LONG).show();
//                            }
//
//
//                        }
//                    }, WebServiceConfig.URL_CREATE_ROOM_CHAT, txtTenNhom.getText().toString());
                            }
                        }
                    }
                }
            });
        } catch (NullPointerException e) {
            Log.e("", "TAG DANH BA:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG DANH BA:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG DANH BA:  " + er.toString());
        }

    }


    private class LoadDataAsynTask1 extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            try {

                if (danhBaInfoArrayList != null && danhBaInfoArrayList.size() > 0) {
                    if (ThanhVienNhomActivity.thanhVienNhomInfos != null && ThanhVienNhomActivity.thanhVienNhomInfos.size() > 0 && !thanh_vien_nhom_json.equals("")) {
                        for (int i = 0; i < danhBaInfoArrayList.size(); i++) {
                            if (!checkExist(ThanhVienNhomActivity.thanhVienNhomInfos, danhBaInfoArrayList.get(i)))
                                danhBaInfoArrayListShow.add(danhBaInfoArrayList.get(i));
                        }
                    } else {
                        danhBaInfoArrayListShow = danhBaInfoArrayList;
                    }

                    danhBaAdapter = new TaoNhomDanhBaAdapter(TaoNhomDanhBaActivity.this, danhBaInfoArrayListShow, new YeuThichListener() {
                        @Override
                        public void onChangeYeuThich() {
                            danhBaAdapter.notifyDataSetChanged();
                        }
                    });

//                recyclerView.setAdapter(danhBaAdapter);
//                danhBaAdapter.notifyDataSetChanged();
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                if (danhBaInfoArrayListShow.size() > 0) {
                    recyclerView.setAdapter(danhBaAdapter);
                    danhBaAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    bar.setVisibility(View.GONE);
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
        }
    }


    private void unitUi() {
        try {
            tag = getIntent().getStringExtra("tag");
            group_rom_id = getIntent().getStringExtra("group_rom_id");
            thanh_vien_nhom_json = getIntent().getStringExtra("thanh_vien_nhom_json");
            danhBaInfoArrayList = new ArrayList<>();
            mLinearLayoutManager = new LinearLayoutManager(this);
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView = (UltimateRecyclerView) findViewById(R.id.recyDanhBa);
            recyclerView.setLayoutManager(mLinearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.refreshDrawableState();
            bar = (ProgressBar) findViewById(R.id.progressBar);
            txtTenNhom = (EditText) findViewById(R.id.txtTenNhom);
            layoutTenNhom = (RelativeLayout) findViewById(R.id.layoutTenNhom);
            danhBaInfoArrayListShow = new ArrayList<>();
            token = sharePreference.getChatToken();

            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            icon_right = (ImageView) findViewById(R.id.icon_right);
            icon_right.setBackgroundResource(R.drawable.btndang);

            if (tag.equals("NHOMDANHBA")) {
                txtTitleHeader.setText(this.getResources().getString(R.string.txttitletaonhomdanhba));
            } else if (tag.equals("DANHBA")) {
                txtTitleHeader.setText(this.getResources().getString(R.string.txttitletaonhomchat));

            } else {
                layoutTenNhom.setVisibility(View.GONE);
                txtTitleHeader.setText(this.getResources().getString(R.string.txttitlethemthanhvien));
            }
        } catch (NullPointerException e) {
            Log.e("", "TAG DANH BA:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG DANH BA:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG DANH BA:  " + er.toString());
        }

    }

    private boolean checkExist(ArrayList<DanhBaInfo> listRss,
                               DanhBaInfo item) {
        boolean result = false;
        for (DanhBaInfo newsInfo : listRss) {

            if (newsInfo.getId().trim().equals(item.getId().trim())) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public void onResume() {
        super.onResume();
        connectSocket();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ConnectApiSocket.socketOffUserChangeStatus();
    }

    private void connectSocket() {
        try {
            ConnectApiSocket.ConnectSocket(sharePreference.getChatToken());
            ConnectApiSocket.socketOnUserChangeStatus(new ResultListener() {
                @Override
                public void onSuccess(final String json) {
                    if (!json.equals("")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DanhBaInfo danhBaInfo = ParserUtility.parseStatusOnline(json);
                                for (int i = 0; i < danhBaInfoArrayList.size(); i++) {
                                    if (danhBaInfo.getId().equals(danhBaInfoArrayList.get(i).getId()))
                                        danhBaInfoArrayList.get(i).setOnline(danhBaInfo.getOnline());
                                }
                                if (danhBaAdapter != null)
                                    danhBaAdapter.notifyDataSetChanged();
                            }
                        });


                    }
                }
            });
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }


}
