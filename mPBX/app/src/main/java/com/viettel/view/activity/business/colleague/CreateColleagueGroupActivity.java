package com.viettel.view.activity.business.colleague;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.MemberAdapter;
import com.viettel.adapter.holder.PaddingForVodLand;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.request.colleague.CreateColleagueGroupRequest;
import com.viettel.model.request.colleague.EditColleagueGroupRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.DialogUtility;
import com.viettel.view.activity.business.ChoseMemberActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/13/17.
 */
public class CreateColleagueGroupActivity extends BABaseActivity {

    public static final int MODE_CREATE = 0;
    public static final int MODE_EDIT = 1;

    TextView txtTitleHeader;
    TextView txtGroupName;
    TextView txtMemberTitle;
    RecyclerView listMember;

    MemberAdapter memberAdapter;

    ViewSwitcher viewSwitcher;
    EditText edtNewGroupName;
    LinearLayout layoutCreate, layoutEdit;

    Button btnCreate, btnEdit;

    public int mode;
    public String groupName;

    private ColleagueGroup currentSelectGroup;

    boolean isRefresh;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_create_colleague_group_activity);

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        layoutCreate = (LinearLayout) findViewById(R.id.layoutCreate);
        layoutEdit = (LinearLayout) findViewById(R.id.layoutEdit);
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtGroupName = (TextView) findViewById(R.id.txtGroupName);
        txtMemberTitle = (TextView) findViewById(R.id.txtMemberTitle);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        edtNewGroupName = (EditText) findViewById(R.id.edtNewGroupName);

        txtTitleHeader.setText(getResources().getString(R.string.labelCreateColleagueGroup));

        listMember = (RecyclerView) findViewById(R.id.listMember);
        LinearLayoutManager manager = new LinearLayoutManager(getLayoutInflater().getContext());
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);

        memberAdapter = new MemberAdapter(this);
        listMember.setAdapter(memberAdapter);
        listMember.setLayoutManager(manager);
        listMember.addItemDecoration(new PaddingForVodLand(getLayoutInflater().getContext()));



    }

    @Override
    protected void onResume() {
        super.onResume();

        currentSelectGroup = GlobalValue.getSelectedGroup();
        if(currentSelectGroup != null) {
            txtGroupName.setText(currentSelectGroup.getGroupName());
            listMember.setVisibility(View.VISIBLE);
            memberAdapter.setData(currentSelectGroup.getListMember());
            txtMemberTitle.setText("Thành viên (" + memberAdapter.getItemCount() + ")");
        } else {
            txtGroupName.setText("");
            listMember.setVisibility(View.GONE);
            txtMemberTitle.setVisibility(View.GONE);
        }
    }

    public void onClickBtnCreateGroup(View view) {
        mode = MODE_CREATE;
        if(viewSwitcher.getDisplayedChild() == 1) {
            viewSwitcher.setInAnimation(slide_in_right);
            viewSwitcher.setOutAnimation(slide_out_right);
            viewSwitcher.showPrevious();

            btnCreate.setBackgroundResource(R.drawable.drawable_orange_left_corner);
            btnCreate.setTextColor(getResources().getColor(R.color.white));

            btnEdit.setBackgroundResource(R.drawable.drawable_white_right_corner);
            btnEdit.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    public void onClickBtnEditGroup(View view) {
        mode = MODE_EDIT;
        if(viewSwitcher.getDisplayedChild() == 0) {
            viewSwitcher.setInAnimation(slide_in_left);
            viewSwitcher.setOutAnimation(slide_out_left);
            viewSwitcher.showNext();

            btnCreate.setBackgroundResource(R.drawable.drawable_white_left_corner);
            btnCreate.setTextColor(getResources().getColor(R.color.gray));

            btnEdit.setBackgroundResource(R.drawable.drawable_orange_right_corner);
            btnEdit.setTextColor(getResources().getColor(R.color.white));
        }
    }

    public void onClickChoseGroup(View view) {
        Intent intent = new Intent(CreateColleagueGroupActivity.this.getBaseContext(), ChoseColleagueGroupActivity.class);
        if(currentSelectGroup != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, currentSelectGroup.getGroupId());
        }
        startActivityForResult(intent, REQUEST_CODE_EDIT_GROUP);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
    public void onClickCreateGroup(View view) {
        String groupName = edtNewGroupName.getEditableText().toString().trim();

        if(groupName == null || groupName.isEmpty()) {
            DialogUtility.showDialogAlert(this, "", "Bạn chưa nhập tên nhóm!",
                    CreateColleagueGroupActivity.this.getResources().getString(R.string.close_button), null);
            showKeyBoard(edtNewGroupName);
            return;
        }

        this.groupName = groupName;

        goToChoseMember(groupName);
    }
    public void onClickEditGroup(View view) {
        goToChoseMember(null);
    }

    public void goToChoseMember(String groupName) {
        Intent intent = new Intent(CreateColleagueGroupActivity.this.getBaseContext(), ChoseMemberActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ACTION, mode);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_LIST_TYPE, ChoseMemberActivity.LIST_TYPE_COLLEAGUE);

        if(currentSelectGroup != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, currentSelectGroup.getGroupId());
        }

        if(groupName != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_GROUP_NAME, groupName);
        }
        startActivityForResult(intent, REQUEST_CODE_CHOSE_MEMBER);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == REQUEST_CODE_CHOSE_MEMBER && data != null) {
                boolean isSuccess = data.getBooleanExtra(GlobalInfo.BUNDLE_KEY_SUCCESS, false);
                if(isSuccess) {
                    ArrayList<ColleagueModel> listMember = data.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER);
                    if (mode == MODE_CREATE) {
                        doCreateGroup(listMember);
                    } else {
                        doEditGroup(listMember);
                    }
                }
            } else if (requestCode == REQUEST_CODE_EDIT_GROUP) {
                currentSelectGroup = GlobalValue.getSelectedGroup();
                if(currentSelectGroup != null) {
                    txtGroupName.setText(currentSelectGroup.getGroupName());
                    listMember.setVisibility(View.VISIBLE);
                    memberAdapter.setData(currentSelectGroup.getListMember());
                    txtMemberTitle.setText("Thành viên (" + memberAdapter.getItemCount() + ")");
                } else {
                    txtGroupName.setText("");
                    listMember.setVisibility(View.GONE);
                    txtMemberTitle.setVisibility(View.GONE);
                }
            }
        }
    }

    public void doCreateGroup(ArrayList<ColleagueModel> list) {
        showLoading();
        CreateColleagueGroupRequest createColleagueGroupRequest = new CreateColleagueGroupRequest();
        createColleagueGroupRequest.setData(list, groupName);

        ApiController.doPostRequest(this, createColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(CreateColleagueGroupActivity.this, getResources().getString(R.string.alertCreateColleageGroupSuccess), Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateColleagueGroupActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });

    }
    public void doEditGroup(final ArrayList<ColleagueModel> list) {
        showLoading();
        EditColleagueGroupRequest createColleagueGroupRequest = new EditColleagueGroupRequest();
        createColleagueGroupRequest.setData(list, currentSelectGroup.getGroupId(),"" + currentSelectGroup.getGroupName());

        ApiController.doPostRequest(this, createColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(CreateColleagueGroupActivity.this, getResources().getString(R.string.alertEditColleagueGroupSuccess), Toast.LENGTH_SHORT).show();
                        currentSelectGroup.setListMember(list);
                        txtGroupName.setText(currentSelectGroup.getGroupName());
                        listMember.setVisibility(View.VISIBLE);
                        memberAdapter.setData(currentSelectGroup.getListMember());
                        txtMemberTitle.setText("Thành viên (" + memberAdapter.getItemCount() + ")");

                        isRefresh = true;
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateColleagueGroupActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    @Override
    public void onKeyBack() {

        setResult(isRefresh ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }
    public void onClickBtnBottom(View view) {
    }
}
