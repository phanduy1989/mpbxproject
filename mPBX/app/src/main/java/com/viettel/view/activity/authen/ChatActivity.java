package com.viettel.view.activity.authen;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ae.team.connectapi.config.WebServiceConfig;
import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.ae.team.connectapi.utility.YeuThichListener;
import com.viettel.adapter.ChatAdapter;
import com.viettel.model.NoiDungInfo;
import com.viettel.model.SenderChatInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.NetworkUtil;
import com.viettel.utils.ParserUtility;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;
import com.viettel.widgets.XListView;
import com.wq.photo.MediaChoseActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

public class ChatActivity extends BABaseActivity implements XListView.IXListViewListener
{

    TextView txtTitleHeader;
    ImageView icon_right;

    private static XListView lsvChat;
    private static ArrayList<NoiDungInfo> arrChatInfosShow;
    private static ArrayList<NoiDungInfo> arrChatInfosNew;
    private static ArrayList<NoiDungInfo> arrChatInfosBin;
    private static ArrayList<NoiDungInfo> arrChatInfosNewLoadMore;
    //    private static ThongTinCaNhan thongTinCaNhan;
    private static ChatAdapter chatAdapter;
    private static EditText txtChatText;
    private static TextView  lblThongBao, lblNameTyping;
    private static LinearLayout llAddComment;
    private static Boolean check, typing;
    public static Boolean CheckNotify = false;
    public static Boolean CheckShowPush = false;
    private static LinearLayout layoutTyping;
    private ImageButton btnSend, btnImg, btnFile;
    public static String group_rom_id = "", tag = "", device_id, Trangthai, urlDownLoad = "", filename = "";
    public static String avatar = "", name = "", id_tin_nhan = "", is_group = "", admin = "", thanh_vien_nhom = "", user_id = "";
    private ImageView imgNewTinNhan;
    private CountDownTimer countDownTimer;
    public static boolean checkScroll = false, checkOl = true, checkstt, checknetwork = true, checkconnect = false, checkHide = true, checkami = true, loadmore = false;
    public static int star = 0, end = 20, vitri = 0;
    public static ChatActivity seft;
    public static boolean chat = false;
    private String token = "";

    Bitmap photo;
    public static final int REQUEST_IMAGE = 1000;
    int scw, sch;
    private static final int REQUEST_PICK_FILE = 1;
    private File selectedFile;

    private StringBuilder s;
    private String sResponse;

    private ProgressDialog pDialog;
    public static final int progress_bar_type = 4;

    int width = 100;
    Display display;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
        }
        display = this.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        unitUi();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver, filter);
        binData();
        hideKeyboard();
        unitControll();
    }





    public void onClickBack(View v) {
        onKeyBack();
    }


    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
    
    private void binData() {
        if (group_rom_id.equals("")) {
            ConnectApiSocket.getRoomGroupId(ChatActivity.this, token, name, user_id, new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    if (json.contains("id")) {
                        group_rom_id = ParserUtility.parserRoomGroupId(json);
                        ConnectApiSocket.getMessagesRoomChat(ChatActivity.this, token, group_rom_id, "", new ResultListener() {
                            @Override
                            public void onSuccess(String json) {
                                if (json.contains("sender")) {
                                    arrChatInfosBin = ParserUtility.parseNoiDungChat(json);
                                    new LoadDataAsynTask().execute(arrChatInfosBin);
                                }
                            }
                        });
//
                    }
                }
            });

        } else {
            ConnectApiSocket.getMessagesRoomChat(ChatActivity.this, token, group_rom_id, "", new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    if (json.contains("sender")) {
                        arrChatInfosBin = ParserUtility.parseNoiDungChat(json);
                        new LoadDataAsynTask().execute(arrChatInfosBin);
                    }
                }
            });
        }

    }

    private void binDataConnect() {
        try {
            if (!NetworkUtil.getConnectivityStatusString(this).equals("NO")) {
                connectSocket();
                checkconnect = false;
            } else {
                checkconnect = true;
                arrChatInfosBin.clear();
                new LoadDataAsynTask().execute(arrChatInfosBin);
            }

        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    private void connectSocket() {
        try {
            ConnectApiSocket.ConnectSocket(token);
            ConnectApiSocket.socketOnConnectError(new ResultListener() {
                @Override
                public void onSuccess(String json) {
                }
            });
            ConnectApiSocket.socketOnConnectTimeout(new ResultListener() {
                @Override
                public void onSuccess(String json) {
                }
            });
            ConnectApiSocket.socketEmitJoinRoom(group_rom_id);
            ConnectApiSocket.socketEmitReaded(group_rom_id);

            ConnectApiSocket.socketOnNewMessage(new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    final String conten = json;
                    if (!conten.equals("")) {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    arrChatInfosNew = new ArrayList<>();
                                    NoiDungInfo noiDungInfo = ParserUtility.parseItemNoiDung(conten);
                                    if (noiDungInfo.getRoom_id().equals(group_rom_id)) {
                                        ConnectApiSocket.socketEmitReaded(group_rom_id);
                                        arrChatInfosNew.add(noiDungInfo);

                                        if (checkScroll == true) {

                                            imgNewTinNhan.setVisibility(View.VISIBLE);
                                            imgNewTinNhan.startAnimation(AnimationUtils.loadAnimation(ChatActivity.this, R.anim.blink));


                                        } else {
                                            loadData(arrChatInfosNew);
                                            layoutTyping.setVisibility(View.GONE);

                                        }
                                    } else {
// xu ly cho man lich su chat
//                                        if (TinNhanActivity.danhBaInfoArrayList == null || TinNhanActivity.danhBaInfoArrayList.size() == 0) {
//                                            TinNhanInfo tinNhanInfo = new TinNhanInfo();
//                                            tinNhanInfo.setUpdatedAt(noiDungInfo.getUpdatedAt());
//                                            tinNhanInfo.setId(noiDungInfo.getRoom_id());
//                                            tinNhanInfo.setUser_id(GlobalValue.sharePreference.getChatToken());
//                                            tinNhanInfo.setCreatedAt(noiDungInfo.getCreatedAt());
//                                            tinNhanInfo.setName(noiDungInfo.getSenderChatInfo().getUsername());
//                                            tinNhanInfo.setLast_time_send_message(noiDungInfo.getCreatedAt());
//                                            tinNhanInfo.setSender_id(noiDungInfo.getSenderChatInfo().getId());
//                                            tinNhanInfo.setLast_message(noiDungInfo.getContent());
//                                            tinNhanInfo.setSenderChatInfo(noiDungInfo.getSenderChatInfo());
//                                            TinNhanActivity.danhBaInfoArrayList.add(tinNhanInfo);
//                                            TinNhanActivity.danhBaAdapter = new TinNhanAdapter(ChatActivity.this, TinNhanActivity.danhBaInfoArrayList);
//                                            TinNhanActivity.recyclerView.setAdapter(TinNhanActivity.danhBaAdapter);
//                                            TinNhanActivity.danhBaAdapter.notifyDataSetChanged();
//                                        } else {
//                                            for (int i = 0; i < TinNhanActivity.danhBaInfoArrayList.size(); i++) {
//                                                if (TinNhanActivity.danhBaInfoArrayList.get(i).getId().equals(noiDungInfo.getRoom_id())) {
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setUpdatedAt(noiDungInfo.getUpdatedAt());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setId(noiDungInfo.getRoom_id());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setUser_id(GlobalValue.sharePreference.getChatToken());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setCreatedAt(noiDungInfo.getCreatedAt());
////                                            TinNhanActivity.danhBaInfoArrayList.get(i).setName(noiDungInfo.getSenderChatInfo().getUsername());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setLast_time_send_message(noiDungInfo.getCreatedAt());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setSender_id(noiDungInfo.getSenderChatInfo().getId());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setLast_message(noiDungInfo.getContent());
//                                                    TinNhanActivity.danhBaInfoArrayList.get(i).setSenderChatInfo(noiDungInfo.getSenderChatInfo());
//                                                }
//
//                                            }
//                                            TinNhanActivity.danhBaAdapter.notifyDataSetChanged();
//                                        }
                                    }

                                }
                            });

//                    ArrayList<TinNhanInfo> arrTinNhanInfosNew = new ArrayList<>();
//                    if (is_group == null && is_group.contains("1"))
//                        arrTinNhanInfosNew = ParserUtility.parseItemTinNhan(GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX), args[1].toString());
//                    if (arrTinNhanInfosNew.size() > 0 && !tinNhanQuery.updateTinNhan(GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX), arrTinNhanInfosNew.get(0).getNguoi_gui(), arrTinNhanInfosNew.get(0))) {
//                        if (arrTinNhanInfosNew.get(0).getNguoi_gui().equals(guid))
//                            arrTinNhanInfosNew.get(0).setTrang_thai("0");
//                        tinNhanQuery.insertTinNhan(arrTinNhanInfosNew.get(0), GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX));
//                    }
                        } catch (NullPointerException nul) {
                            Log.e("", "TAG CHAT:  " + nul.toString());
                        } catch (Exception e) {
                            Log.e("", "TAG CHAT:  " + e.toString());
                        } catch (Error er) {
                            Log.e("", "TAG CHAT:  " + er.toString());
                        }

                    }
                }
            });

            ConnectApiSocket.socketOnUserTyping(new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    String conten = json;
                    if (!conten.equals("")) {
                        final SenderChatInfo senderChatInfo = ParserUtility.parserTyping(conten);
                        if (senderChatInfo.getRoom_id().equals(group_rom_id) && !senderChatInfo.getId().trim().equals(sharePreference.getChatToken())) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showTyping((senderChatInfo.getName() == null || senderChatInfo.getName().equals("") ? senderChatInfo.getUsername() : senderChatInfo.getName()));

                                }
                            });
                        }
                    }
                }
            });

            ConnectApiSocket.socketOnSendSuccess(new ResultListener() {
                @Override
                public void onSuccess(String json) {
                    try {
                        final String content = json;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                NoiDungInfo noiDungInfo = ParserUtility.parseItemNoiDung(content);
                                for (int i = 0; i < arrChatInfosShow.size(); i++) {
                                    if (group_rom_id.equals(noiDungInfo.getRoom_id()) && arrChatInfosShow.get(i).getMsg_id().equals(noiDungInfo.getMsg_id())) {
//                                Log.e("","")
                                        arrChatInfosShow.get(i).set_id(noiDungInfo.get_id());
                                        arrChatInfosShow.get(i).setCreatedAt(noiDungInfo.getCreatedAt());
                                        arrChatInfosShow.get(i).setUpdatedAt(noiDungInfo.getUpdatedAt());
                                        arrChatInfosShow.get(i).setTrang_thai(noiDungInfo.getTrang_thai());
                                        arrChatInfosShow.get(i).getSenderChatInfo().setUpdatedAt(noiDungInfo.getSenderChatInfo().getUpdatedAt());
                                        arrChatInfosShow.get(i).getSenderChatInfo().setCreatedAt(noiDungInfo.getSenderChatInfo().getCreatedAt());
                                        arrChatInfosShow.get(i).getSenderChatInfo().setPassword(noiDungInfo.getSenderChatInfo().getPassword());
//                                arrChatInfosShow.get(i).setThoi_gian_client(time);
//                                String trang_thai = "0";
//                                if (Trangthai.equals("10")) {
//                                    arrChatInfosShow.get(i).setTrang_thai("1");
//                                    trang_thai = "1";
//                                } else {
//                                    trang_thai = "0";
//                                    arrChatInfosShow.get(i).setTrang_thai("0");
//                                }
//                                arrChatInfosShow.get(i).setId_noi_dung(id);
//                                arrChatInfosShow.get(i).setThoi_gian(time);

//                                NoiDungInfo noiDungInfo = new NoiDungInfo(id, id_tin_nhan, GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX), txtChatText.getText().toString(), time, clientTime, realName, avatar, GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_AVATAR), "0", "0", trang_thai, "", "");
//                                if (!noiDungQuery.updateNoiDung(id_tin_nhan, id, noiDungInfo)) {
//                                    noiDungQuery.insertNoiDung(id_tin_nhan, noiDungInfo);
//                                }
//
//
//                                TinNhanInfo tinNhanInfo = new TinNhanInfo(guid, txtChatText.getText().toString(), time, realName, avatar, "0", guid, "", "", "", "", "", "");
//                                if (!tinNhanQuery.updateTinNhan(GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX), guid, tinNhanInfo)) {
//                                    tinNhanQuery.insertTinNhan(tinNhanInfo, GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_KEY_INDEX));
//                                }
                                    }
                                }
//
                                chatAdapter.notifyDataSetChanged();
                            }
                        });

                    } catch (NumberFormatException e) {
                        Log.e("", "TAG CHAT: " + e.toString());
                    } catch (NullPointerException nul) {
                        Log.e("", "TAG CHAT:  " + nul.toString());
                    } catch (Exception e) {
                        Log.e("", "TAG CHAT:  " + e.toString());
                    } catch (Error er) {
                        Log.e("", "TAG CHAT:  " + er.toString());
                    }
                }
            });
//            WebServiceConfig.mSocket.on("send_success", send_success);
//            WebServiceConfig.mSocket.on("user_typing", Showtyping);
//            WebServiceConfig.mSocket.on("readed", readed);

        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    private void unitUi() {
        try {
            seft = this;
            CheckShowPush = true;
            loadmore = false;
            group_rom_id = getIntent().getStringExtra("group_rom_id");
            name = getIntent().getStringExtra("name");
            user_id = getIntent().getStringExtra("user_id");
            avatar = getIntent().getStringExtra("avatar");
            Trangthai = getIntent().getStringExtra("Trangthai");
            id_tin_nhan = getIntent().getStringExtra("id_tin_nhan");
            is_group = getIntent().getStringExtra("is_group");
            tag = getIntent().getStringExtra("tag");


            is_group = (is_group == null || !is_group.equals("1") || is_group.equals("")) ? "0" : "1";
            admin = getIntent().getStringExtra("admin");
            id_tin_nhan = (!id_tin_nhan.equals("") ? id_tin_nhan : (sharePreference.getChatToken() + "_" + group_rom_id));

            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            txtTitleHeader.setText(name);
            icon_right = (ImageView) findViewById(R.id.icon_right);

//            btnControlRight.setBackgroundResource(R.drawable.iconsetting);
            chat = true;
//            if (is_group.equals("1")) {

                icon_right.setBackgroundResource(R.drawable.setting_icon);
                icon_right.setVisibility(View.VISIBLE);
//                lblLop.setVisibility(View.GONE);
//            } else {
//                icon_right.setVisibility(View.GONE);
////                lblLop.setVisibility(View.GONE);
//            }
            lsvChat = (XListView) findViewById(R.id.lsvChat);
            txtChatText = (EditText) findViewById(R.id.txtChatText);
//            lblNetWork = (TextView) findViewById(R.id.lblNetWork);
            lblThongBao = (TextView) findViewById(R.id.lblThongBao);
            txtChatText.setTextColor(this.getResources().getColor(R.color.lblname));
            llAddComment = (LinearLayout) findViewById(R.id.llAddComment);
            llAddComment.setBackgroundResource(R.drawable.layout_textchat);
            txtChatText.setBackgroundResource(R.color.bg_comments);
            btnSend = (ImageButton) findViewById(R.id.btnSend);
            btnImg = (ImageButton) findViewById(R.id.btnImg);
            btnFile = (ImageButton) findViewById(R.id.btnFile);
            layoutTyping = (LinearLayout) findViewById(R.id.layoutTyping);
            imgNewTinNhan = (ImageView) findViewById(R.id.imgNewTinNhan);
            lblNameTyping = (TextView) findViewById(R.id.lblNameTyping);
            lsvChat.setPullLoadEnable(false);
            lsvChat.setXListViewListener(this);
            lsvChat.setRefreshTime((String) DateFormat.format(
                    XListView.DATE_FORMAT_PARTTEN, System.currentTimeMillis()));
            arrChatInfosBin = new ArrayList<>();
            arrChatInfosShow = new ArrayList<>();
            arrChatInfosNewLoadMore = new ArrayList<>();
            lsvChat.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            token =  sharePreference.getChatToken();
//            mLinearLayoutManager = new LinearLayoutManager(this);
//            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//            recyclerView = (UltimateRecyclerView) findViewById(R.id.recyChat);
//            recyclerView.setLayoutManager(mLinearLayoutManager);
//            recyclerView.setHasFixedSize(true);
//            recyclerView.refreshDrawableState();
//            mLinearLayoutManager.setStackFromEnd(true);

//            device_id = GlobalValue.prefs.getStringValue(QiSharedPreferences.PREF_SETTING_ANDROID_ID);
            check = false;
            typing = false;
            layoutTyping.setVisibility(View.GONE);

            icon_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    try {
                        Intent intent = new Intent(ChatActivity.this, ThanhVienNhomActivity.class);
                        intent.putExtra("group_rom_id", group_rom_id);
                        intent.putExtra("name", name);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("tag", tag);
                        startActivity(intent);
//                    } catch (NullPointerException nul) {
//                        Log.e("", "TAG CHAT:  " + nul.toString());
//                    } catch (Exception e) {
//                        Log.e("", "TAG CHAT:  " + e.toString());
//                    } catch (Error er) {
//                        Log.e("", "TAG CHAT:  " + er.toString());
//                    }
                }
            });
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    protected void hideKeyboard() {
        try {
            this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            chat = false;
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    unregisterReceiver(networkStateReceiver);
                }
            });
            offSocket();
            chat = false;
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            offSocket();
            CheckShowPush = false;
            finish();
            chat = false;
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {


            chat = true;
//            if (WebServiceConfig.mSocket == null || !WebServiceConfig.mSocket.connected()) {
            binDataConnect();
//            }
            if (is_group != null || is_group.equals("1")) {
//                lblTitleScreen.setText(StringUtility.GetTen(this, name));
            }
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String status = NetworkUtil.getConnectivityStatusString(context);
                if (status.equals("NO")) {
//            khong co mang
//                    lblNetWork.setVisibility(View.VISIBLE);
//                    lblNetWork.startAnimation(AnimationUtils.loadAnimation(ChatActivity.this, R.anim.network_show));
                    checkconnect = true;

                } else {
                    // su lý khi có mạng lại
                    if (checkconnect == true) {
                        offSocket();
                        connectSocket();
                        checkconnect = false;
                    }
//                    lblNetWork.setVisibility(View.GONE);
//                    lblNetWork.startAnimation(AnimationUtils.loadAnimation(ChatActivity.this, R.anim.network_hide));


                }

            } catch (NullPointerException nul) {
                Log.e("", "TAG CHAT:  " + nul.toString());
            } catch (Exception e) {
                Log.e("", "TAG CHAT:  " + e.toString());
            } catch (Error er) {
                Log.e("", "TAG CHAT:  " + er.toString());
            }


        }
    };

    private void unitControll() {
        try {
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sendChatMessage();
                }
            });

            lsvChat.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard();
                    return false;
                }
            });
            lsvChat.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Boolean check = true;
                    if (!"remove_add_user".equals(arrChatInfosShow.get(position - 1).getType())) {
                        check = true;
                        ShowDiaLog(arrChatInfosShow.get(position - 1));
                    } else {
                        check = false;
                    }

                    return check;
                }
            });
            lsvChat.setOnScrollListener(new XListView.OnXScrollListener() {
                @Override
                public void onXScrolling(View view) {
                }

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    if ((firstVisibleItem + visibleItemCount) == totalItemCount || (totalItemCount - (firstVisibleItem + visibleItemCount) == 1)) {
                        checkScroll = false;
                    } else {

                        checkScroll = true;
                    }
                }
            });
            imgNewTinNhan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    checkScroll = false;
                    imgNewTinNhan.clearAnimation();
                    imgNewTinNhan.setVisibility(View.GONE);
                    loadData(arrChatInfosNew);
                    int vitri = arrChatInfosShow.size();
                    lsvChat.setSelection(vitri);
//                    recyChat.getLayoutManager().scrollToPosition(vitri);
                    layoutTyping.setVisibility(View.GONE);
                }
            });

            txtChatText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        sendChatMessage();
                        handled = true;
                    }
                    return handled;
                }
            });
            txtChatText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (txtChatText.length() > 0) {
                        btnSend.setBackgroundResource(R.drawable.icon_dang_hover);

                        ConnectApiSocket.socketEmitTyping(group_rom_id);
                        typing = true;
//

                    } else {
                        typing = false;
                        btnSend.setBackgroundResource(R.drawable.icon_dang);
                    }

                }
            });

            btnImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(ChatActivity.this)) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 2909);
                        } else {
                            // continue with your code
                            intentChoseImage();
                        }
                    } else {
                        // continue with your code
                        intentChoseImage();
                    }
                }
            });

            btnFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(intent, REQUEST_PICK_FILE);
//                    Intent intent = new Intent(ChatActivity.this, FilePicker.class);
//                    startActivityForResult(intent, REQUEST_PICK_FILE);
                }
            });
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    private void sendChatMessage() {
        try {

            if (validateComment()) {

                if (!NetworkUtil.isConnectingToInternet(ChatActivity.this))
                    checknetwork = false;
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                JSONObject finalobject = null;
                finalobject = new JSONObject();
                finalobject.put("room_id", group_rom_id);
                finalobject.put("content", txtChatText.getText().toString());
                finalobject.put("msg_id", uuid);
                finalobject.put("type", "0");
                ConnectApiSocket.socketEmitMessage(finalobject);
                ArrayList<NoiDungInfo> arrayList = new ArrayList<>();
//
                SenderChatInfo senderChatInfo = new SenderChatInfo("",
                        sharePreference.getChatId(),
                        sharePreference.getChatPhone(),
                        sharePreference.getChatUsername(),
                        sharePreference.getChatEmail(),
                        "",
                        sharePreference.getChatAppId(),
                        "",
                        sharePreference.getChatAvatar(),
                        sharePreference.getChatName(),
                        "");
                NoiDungInfo noiDungInfo = new NoiDungInfo(uuid, txtChatText.getText().toString(), sharePreference.getChatId(), group_rom_id, "", "", "", uuid, senderChatInfo, System.currentTimeMillis() / 1000 + "", "1", "3", "3", "", "");
                arrayList.add(noiDungInfo);
                if (chatAdapter != null) {
                    chatAdapter.setAnimationsLocked(false);
                    chatAdapter.setDelayEnterAnimation(false);
                }


                loadData(arrayList);
                txtChatText.setText("");

            }
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        }
    }

    private void loadData(ArrayList<NoiDungInfo> arrayList) {
        try {
            if (loadmore == true) {
                loadmore = false;
                Collections.sort(arrayList, new Comparator<NoiDungInfo>() {
                    @Override
                    public int compare(NoiDungInfo o1, NoiDungInfo o2) {
                        int ol1 = 0, ol2 = 0;
                        if (o1.getCreatedAt() != null && !o1.getCreatedAt().equals("0")) {
                            ol1 = Integer.parseInt(StringUtility.getDateTimeStamp(o1.getCreatedAt()));
                        } else {
                            ol1 = 0;
                        }
                        if (o2.getCreatedAt() != null && !o2.getCreatedAt().equals("0")) {
                            ol2 = Integer.parseInt(StringUtility.getDateTimeStamp(o2.getCreatedAt()));
                        } else {
                            ol2 = 0;
                        }
                        return ol2 - ol1;
                    }
                });
                for (NoiDungInfo item : arrayList) {
                    if (!checkExist(arrChatInfosShow, item))
                        arrChatInfosShow.add(0, item);

                }
                chatAdapter = new ChatAdapter(ChatActivity.this, arrChatInfosShow,sharePreference.getChatId(),  is_group, new YeuThichListener() {
                    @Override
                    public void onChangeYeuThich() {
                        new DownloadFileFromURL().execute(urlDownLoad);
                    }
                });

                chatAdapter.notifyDataSetChanged();
                lsvChat.setAdapter(chatAdapter);
                onLoad();
                if (check == false) {
                    lsvChat.setSelection(vitri);
                }

            } else {
                for (NoiDungInfo item : arrayList) {
                    if (!checkExistTimeClient(arrChatInfosShow, item)) {
                        arrChatInfosShow.add(item);
                    }
                }
                chatAdapter = new ChatAdapter(ChatActivity.this, arrChatInfosShow,sharePreference.getChatId(), is_group, new YeuThichListener() {
                    @Override
                    public void onChangeYeuThich() {
                        new DownloadFileFromURL().execute(urlDownLoad);
                    }
                });
                lsvChat.post(new Runnable() {
                    public void run() {
                        lsvChat.setSelection(lsvChat.getCount() - 1);
                    }
                });
                chatAdapter.notifyDataSetChanged();
                lsvChat.setAdapter(chatAdapter);
//                lsvChat.scrollTo(0, lsvChat.getHeight());
//                lsvChat.setSelector(chatAdapter.getCount() -1);
            }
        } catch (NumberFormatException e) {
            Log.e("", "TAG CHAT: " + e.toString());
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }


    }

    public static boolean checkExist(ArrayList<NoiDungInfo> listRss,
                                     NoiDungInfo item) {
        boolean result = false;
        for (NoiDungInfo newsInfo : listRss) {
            if (newsInfo.get_id().trim().equals(item.get_id().trim())) {
                result = true;
                break;
            }
        }
        return result;
    }

    private boolean checkExistTimeClient(ArrayList<NoiDungInfo> listRss,
                                         NoiDungInfo item) {
        boolean result = false;
        for (NoiDungInfo newsInfo : listRss) {
            if (newsInfo.get_id().trim().equals(item.get_id().trim())) {
                result = true;
                break;
            }
        }
        return result;
    }

    private void offSocket() {
        try {
            ConnectApiSocket.socketOffConnectError();
            ConnectApiSocket.socketOffConnectTimeout();
            ConnectApiSocket.socketOffNewMessage();
            ConnectApiSocket.socketOffSendSuccess();
            ConnectApiSocket.socketOffUserTyping();
            ConnectApiSocket.socketOffReaded();
            ConnectApiSocket.socketEmitLeaveRoom(group_rom_id);
            check = false;
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    private void showTyping(String name) {


        try {
            lblNameTyping.setText(name + " " + this.getResources().getString(R.string.lbldangsoantin));
            layoutTyping.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) ChatActivity.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            countDownTimer = new CountDownTimer(8000, 1000) {
                @Override
                public void onTick(long leftTimeInMilliseconds) {
                    long seconds = leftTimeInMilliseconds / 1000;
                }

                @Override
                public void onFinish() {
                    layoutTyping.setVisibility(View.GONE);
                }

            }.start();
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    @Override
    public void onCreateNavigateUpTaskStack(TaskStackBuilder builder) {
        super.onCreateNavigateUpTaskStack(builder);
    }

    private boolean validateComment() {

        if (txtChatText.getText().toString().trim().equals("") || TextUtils.isEmpty(txtChatText.getText().toString())) {
            btnSend.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            txtChatText.setText("");
            return false;
        }
        return true;
    }

    public void ShowDiaLog(final NoiDungInfo noiDungInfo) {
        try {
            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.popup_chitiet);
            TextView lblConten = (TextView) dialog
                    .findViewById(R.id.lblConten);
            TextView lblSaoChep = (TextView) dialog
                    .findViewById(R.id.lblSaoChep);
            TextView lblChiTiet = (TextView) dialog
                    .findViewById(R.id.lblChiTiet);
            TextView lblXoa = (TextView) dialog
                    .findViewById(R.id.lblXoa);
            lblConten.setText(noiDungInfo.getContent());

            lblSaoChep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    copyToClipboard(noiDungInfo.getContent());
                    dialog.dismiss();
                }
            });
            lblChiTiet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(ChatActivity.this, ChiTietChatActivity.class);
//                    intent.putExtra("Content", noiDungInfo.getNoi_dung());
//                    intent.putExtra("Avatar", avatar);
//                    intent.putExtra("Time", noiDungInfo.getThoi_gian());
//                    intent.putExtra("realName", realName);
//                    intent.putExtra("ten_nguoi_gui", noiDungInfo.getNguoi_gui_ten());
//                    intent.putExtra("Trangthai", Trangthai);
//                    intent.putExtra("Status", noiDungInfo.getTrang_thai());
//                    intent.putExtra("Sender", noiDungInfo.getNguoi_gui());
//                    dialog.dismiss();
//                    startActivity(intent);

                }
            });
            lblXoa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (!NetworkUtil.getConnectivityStatusString(ChatActivity.this).equals("NO")) {
                            JSONObject finalobject = null;
                            finalobject = new JSONObject();
                            finalobject.put("id", noiDungInfo.get_id());

//                            WebServiceConfig.mSocket.emit("xoa_tin_nhan", finalobject, new Ack() {
//                                @Override
//                                public void call(Object... args) {
//                                    final String content = args[0].toString();
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            if (content.contains("XOA_THANH_CONG")) {
//                                                for (int j = arrChatInfosShow.size(); j > 0; j--) {
//                                                    if (noiDungInfo.get_id().contains(arrChatInfosShow.get(j - 1).get_id())) {
////                                                        noiDungQuery.deleteItemNoiDung(noiDungInfo.get_id());
//                                                        arrChatInfosShow.remove(arrChatInfosShow.get(j - 1));
//                                                    }
//                                                }
//
//                                                if (chatAdapter != null)
//                                                    chatAdapter.notifyDataSetChanged();
//
//                                            } else {
//                                                Toast.makeText(getApplicationContext(), R.string.errxoa, Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//                                    });
//
//                                }
//                            });
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.errornetwork, Toast.LENGTH_SHORT).show();

                        }
                        dialog.dismiss();
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (Exception e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), R.string.errxoa, Toast.LENGTH_SHORT).show();
                            }
                        });
                        // TODO: handle exception
                    }

                }
            });
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }

    }

    public boolean copyToClipboard(String text) {
        try {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) ChatActivity.this
                        .getSystemService(ChatActivity.this.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) ChatActivity.this
                        .getSystemService(ChatActivity.this.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData
                        .newPlainText(
                                ChatActivity.this.getResources().getString(
                                        R.string.message), text);
                clipboard.setPrimaryClip(clip);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                offSocket();
//                if (CheckNotify == true) {
//                    CheckNotify = false;
//                    Intent intent = new Intent(this,
//                            HomeTabActivity.class);
//                    startActivity(intent);
//
//                }
//                finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    public void onRefresh() {
        try {
            if (end > arrChatInfosShow.size()) {
                lsvChat.stopRefresh();
                lsvChat.setPullRefreshEnable(false);

            } else {
                loadmore = true;
                vitri = star;
                star = arrChatInfosShow.size();
                end = end + 20;
                runOnUiThread(new Runnable() {
                    public void run() {
//                        WebServiceConfig.mSocket.emit("lay_du_lieu_hoi_thoai", sharePreference.getChatToken(), device_id, group_rom_id, star + "", end + "");
                    }
                });
            }
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }


    }

    @Override
    public void onLoadMore() {
        // load more

    }

    private void onLoad() {
        lsvChat.stopRefresh();
        lsvChat.stopLoadMore();
        lsvChat.setRefreshTime((String) DateFormat.format(
                XListView.DATE_FORMAT_PARTTEN, System.currentTimeMillis()));
    }


    public class LoadDataAsynTask extends AsyncTask<ArrayList<NoiDungInfo>, Void, Void> {
        ArrayList<NoiDungInfo> listTemp;

        @Override
        protected Void doInBackground(ArrayList<NoiDungInfo>... params) {
            try {
                listTemp = params[0];
                if (listTemp.size() > 0) {
                    for (int i = 0; i < listTemp.size(); i++) {
                        if (!checkExist(arrChatInfosShow, listTemp.get(i))) {
                            arrChatInfosShow.add(listTemp.get(i));
                        }
                    }
                    Collections.sort(arrChatInfosShow,
                            new Comparator<NoiDungInfo>() {
                                @Override
                                public int compare(NoiDungInfo lhs,
                                                   NoiDungInfo rhs) {
                                    int intlhs = Integer.parseInt((lhs.getCreatedAt() != null && !lhs.getCreatedAt().trim().equals("")) ? StringUtility.getDateTimeStamp(lhs.getCreatedAt()) : "1");
                                    int intrhs = Integer.parseInt((rhs.getCreatedAt() != null && !rhs.getCreatedAt().trim().equals("")) ? StringUtility.getDateTimeStamp(rhs.getCreatedAt()) : "1");
                                    return intlhs - intrhs;
                                }
                            });
                }
                chatAdapter = new ChatAdapter(seft, arrChatInfosShow, sharePreference.getChatId(), is_group, new YeuThichListener() {
                    @Override
                    public void onChangeYeuThich() {
                        new DownloadFileFromURL().execute(urlDownLoad);
                    }
                });
            } catch (NumberFormatException e) {
                Log.e("", "TAG CHAT: " + e.toString());
            } catch (NullPointerException nul) {
                Log.e("", "TAG CHAT:  " + nul.toString());
            } catch (Exception e) {
                Log.e("", "TAG CHAT:  " + e.toString());
            } catch (Error er) {
                Log.e("", "TAG CHAT:  " + er.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (arrChatInfosShow.size() > 0) {
                    lsvChat.setVisibility(View.VISIBLE);
                    lblThongBao.setVisibility(View.GONE);
                    lsvChat.setAdapter(chatAdapter);
                    chatAdapter.notifyDataSetChanged();
                    lsvChat.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    lsvChat.post(new Runnable() {
                        public void run() {
                            lsvChat.setSelection(lsvChat.getCount() - 1);
                        }
                    });
//                    lsvChat.scrollTo(0, lsvChat.getHeight());
                    if (arrChatInfosShow.size() < end) {
                        lsvChat.stopRefresh();
                        lsvChat.setPullRefreshEnable(false);
                    }
                } else {
                    if (checkconnect == true) {
                        lsvChat.setVisibility(View.GONE);
                        lblThongBao.setVisibility(View.VISIBLE);
                    }
                }
            } catch (NullPointerException nul) {
                Log.e("", "TAG CHAT:  " + nul.toString());
            } catch (Exception e) {
                Log.e("", "TAG CHAT:  " + e.toString());
            } catch (Error er) {
                Log.e("", "TAG CHAT:  " + er.toString());
            }
        }

        public class execute {
            public execute(ArrayList<NoiDungInfo> arrChatInfosBin) {
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentChoseImage();
                } else {
                    Toast.makeText(
                            ChatActivity.this,
                            ChatActivity.this.getResources().getString(
                                    R.string.lblerrquyen), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void intentChoseImage() {
        Intent intent = new Intent(ChatActivity.this, MediaChoseActivity.class);
        intent.putExtra("chose_mode", 1);
        intent.putExtra("max_chose_count", 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        dialog = new ProgressDialog(DangHoatDongActivity.this);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE: {
//                    Uri selectedImage = data.getData();
//                    Log.e("","xxxxxxxxxxxxxx: ima  " + selectedImage.getPath());
                    ArrayList<String> paths = data.getStringArrayListExtra("data");
                    for (String path : paths) {
//                layoutImage.setVisibility(View.VISIBLE);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(path, options);
                        int w = options.outWidth;
                        int h = options.outHeight;
                        int parw = 0;
                        int parh = 0;
                        if (w > scw) {
                            parw = scw;
                            parh = sch * w / h;
                        } else {
                            parh = h;
                            parw = w;
                        }
                        options.inJustDecodeBounds = false;
                        options.outWidth = parw;
                        options.outHeight = parh;
                        photo = BitmapFactory.decodeFile(path, options);
                        if (photo != null) {
                            File file = new File(path);
                            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                            UpLoadImage(token, uuid, group_rom_id, file, new ResultListener() {
                                @Override
                                public void onSuccess(String json) {
                                }
                            });
//                            ConnectApiSocket.UpLoadImage(token, uuid, group_rom_id, file, new ResultListener() {
//                                @Override
//                                public void onSuccess(String json) {
//                                }
//                            });
//                            executeMultipartPost(file);
                        }
                    }


                }
                break;
                case REQUEST_PICK_FILE: {
                    String filePath = data.getData().getPath();
//                    ArrayList<String> paths = data.getStringArrayListExtra("data");
//                    for (String path : paths) {
//                        File file = new File(path);
//
//                        executeMultipartPost(file);
//                    }
                    Uri selectedImage = data.getData();
//                    File file = new File(data.getStringExtra(FilePicker.EXTRA_FILE_PATH));
//                    executeMultipartPost(file);
//                    String selectedImage = data.getStringExtra("data");
                    Log.e("", "xxxxxxxxxxxxxx: file  " + selectedImage.getPath());
                    File file = new File(selectedImage.getPath().toString());
                    UpLoadImage(token, selectedImage.getPath(), group_rom_id, file, new ResultListener() {
                        @Override
                        public void onSuccess(String json) {
                        }
                    });
                }
//                    if(data.hasExtra(FilePicker.EXTRA_FILE_PATH)) {
//
//                        selectedFile = new File
//                                (data.getStringExtra(FilePicker.EXTRA_FILE_PATH));
////                        filePath.setText(selectedFile.getPath());
//                    }
                break;

            }
        }
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void UpLoadImage(String token, String msg_id, String group_rom_id, final File file, final ResultListener resultListener) {
        StringBuilder s;
        String sResponse;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(WebServiceConfig.URL_UPLOAD_FILE);
            String auth = "Bearer " + token;
            postRequest.setHeader("Authorization", auth);
            postRequest.setHeader("charset", "utf-8");
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("msg_id", new StringBody(msg_id));
            reqEntity.addPart("room_id", new StringBody(group_rom_id));
            reqEntity.addPart("file", new FileBody(file));

            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));

            s = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }

            Log.e("UpdateTaiKhoan", "Server response : " + s);
            resultListener.onSuccess(s.toString());

        } catch (Exception e) {
            Log.e("UpdateTaiKhoan", "catch : " + e.toString());
            resultListener.onSuccess(e.toString());

        }


    }
/*
    public void executeMultipartPost(final File bm) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String msg_id = UUID.randomUUID().toString().replaceAll("-", "");
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost postRequest = new HttpPost(WebServiceConfig.URL_UPLOAD_FILE);
                    String auth = "Bearer " + sharePreference.getChatToken();
                    postRequest.setHeader("Authorization", auth);
                    postRequest.setHeader("charset", "utf-8");
                    MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                    reqEntity.addPart("msg_id", new StringBody(msg_id));
                    reqEntity.addPart("room_id", new StringBody(group_rom_id));
                    reqEntity.addPart("file", new FileBody(bm));

                    postRequest.setEntity(reqEntity);
                    HttpResponse response = httpClient.execute(postRequest);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            response.getEntity().getContent(), "UTF-8"));

                    s = new StringBuilder();

                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }

//            if (!s.equals("") && !s.toString().contains("false") && !s.toString().contains("failed") && !s.toString().contains("DOCTYPE")) {
                    Log.e("UpdateTaiKhoan", "Server response : " + s);

//            } else {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
////                        dialog.cancel();
//                        Toast.makeText(
//                                ChatActivity.this,
//                                ChatActivity.this.getResources().getString(
//                                        R.string.lblerrdoianh), Toast.LENGTH_LONG).show();
//                    }
//                });
//            }

                } catch (Exception e) {
                    Log.e("UpdateTaiKhoan", "catch : " + e.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                    dialog.cancel();
                            Toast.makeText(
                                    ChatActivity.this,
                                    ChatActivity.this.getResources().getString(
                                            R.string.lblthongbao_chat), Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });


    }
*/
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString()
                        + "/chat/" + filename);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            Toast.makeText(
                    ChatActivity.this,
                    ChatActivity.this.getResources().getString(
                            R.string.succfile), Toast.LENGTH_LONG).show();
        }

    }

}
