package com.viettel.view.activity.business.conversation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.ListConferenceAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationModel;
import com.viettel.model.request.conference.GetConversationListRequest;
import com.viettel.model.response.conference.GetConferenceListResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.DataManager;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/22/16.
 */
public class ListConversationActivity extends BABaseActivity {


    TextView txtTitleHeader;

    private RecyclerView subMenuListView;
    private ListConferenceAdapter listConferenceAdapter;

    ImageView icon_right;
    private SwipeRefreshLayout swipeContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_activity);
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        icon_right = (ImageView) findViewById(R.id.icon_right);
        subMenuListView = (RecyclerView) findViewById(R.id.listView);
        txtTitleHeader.setText(getResources().getString(R.string.labelConferencList));
        icon_right.setImageResource(R.drawable.new_hoinghi_icon);

        listConferenceAdapter = new ListConferenceAdapter(this, getSupportFragmentManager(), subMenuListView, mCallback);
        subMenuListView.setAdapter(listConferenceAdapter);
        subMenuListView.setLayoutManager(new LinearLayoutManager(this));

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        if (swipeContainer != null) {
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initData();
                }
            });
            // Configure the refreshing colors
            swipeContainer.setColorScheme(R.color.main_green, R.color.main_green
                    , R.color.main_green, R.color.main_green);
        }
    }

    public void initData() {
        showLoading();

        GetConversationListRequest getConversationListRequest = new GetConversationListRequest();
        getConversationListRequest.setData();

        ApiController.doPostRequest(this, getConversationListRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {



                closeLoading();

                if(response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceListResponse signInResponse = gson.fromJson(response, GetConferenceListResponse.class);
                    GlobalValue.minConfig = signInResponse.getMinConfig();

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(DataManager.genListConverGroup(signInResponse));
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void setListData(ArrayList<ConversationGroup> listData) {
        if(listData == null || listData.isEmpty()) {
            if(listConferenceAdapter == null || listConferenceAdapter.getItemCount() == 0) {
                showError(getResources().getString(R.string.alertNoConferenceFound), false);
            }
            return;
        }

        for(ConversationGroup conversationGroup : listData) {
            if(conversationGroup.getListChild() != null) {
                for(ConversationModel conversationModel : conversationGroup.getListChild()) {
                    conversationModel.setConversationType(conversationGroup.getType());
                }
            }
        }

        listConferenceAdapter.setMenuList(listData);
    }

    private View.OnClickListener mCallback = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public void showLoading() {
        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
    }

    public void closeLoading() {
        swipeContainer.setRefreshing(false);
    }


    public void showError(String message, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        super.showError(message, isError);
    }

    public void showError(ResponseObject responseObject, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        if(listConferenceAdapter == null || listConferenceAdapter.getItemCount() == 0) {
            super.showError(responseObject, isError);
        }
    }

    public void onCallPrivate(View view) {

    }

    public void onClickHeaderRight(View v) {
        goToCreateConference();
    }

    public void goToCreateConference() {
        Intent intent = new Intent(ListConversationActivity.this.getBaseContext(), CreateConversationActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE, CreateConversationActivity.MODE_CREATE);
        startActivityForResult(intent, REQUEST_CODE_CREATE);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
    public void goToDetail(ConversationModel conversationModel) {

        Intent intent = new Intent(ListConversationActivity.this.getBaseContext(), ConversationInfoActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, conversationModel);

        startActivityForResult(intent, REQUEST_CODE_CREATE);

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == REQUEST_CODE_CREATE) {
                initData();
            }
        }
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        finish();
    }

}
