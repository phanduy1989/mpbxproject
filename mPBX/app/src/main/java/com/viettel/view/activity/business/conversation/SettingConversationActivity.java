package com.viettel.view.activity.business.conversation;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.viettel.adapter.MemberAdapter;
import com.viettel.adapter.holder.PaddingForVodLand;
import com.viettel.mpbx.R;
import com.viettel.utils.DataManager;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class SettingConversationActivity extends BABaseActivity {

    TextView txtTitleHeader;
    RecyclerView listMember;

    MemberAdapter memberAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_setting_activity);

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);

        listMember = (RecyclerView) findViewById(R.id.listMember);
        LinearLayoutManager manager = new LinearLayoutManager(getLayoutInflater().getContext());
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);

        memberAdapter = new MemberAdapter(this);
        listMember.setAdapter(memberAdapter);
        memberAdapter.setData(DataManager.genListColleagueModel());
        listMember.setLayoutManager(manager);
        listMember.addItemDecoration(new PaddingForVodLand(getLayoutInflater().getContext()));
    }


    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }
    public void onClickBtnBottom(View view) {
    }
}
