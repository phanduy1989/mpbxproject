package com.viettel.view.activity.business;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.SingleChoiceCallback;
import com.viettel.model.request.CheckLoginRequest;
import com.viettel.model.request.GetUserInfoRequest;
import com.viettel.model.request.colleague.GetColleagueInfoRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.model.response.SignInResponse;
import com.viettel.model.response.colleague.GetColleagueInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.store.AppSharePreference;
import com.viettel.store.GlobalValue;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.Utils;
import com.viettel.view.activity.business.colleague.ColleagueInfoActivity;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 11/22/16.
 */
public class KeyboardActivity extends BABaseActivity {

    AppSharePreference appSharePreference;

    TextView txtTitleHeader;
    ImageButton btnClear;
    EditText edtKeyword;
    StringBuilder sb = new StringBuilder("");
    int maxLength;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_keyboard_activity);
        maxLength = getResources().getInteger(R.integer.keyboard_maxlength);
        initView();
    }

    public void initView() {
        showLoading();
        CheckLoginRequest checkLoginRequest = new CheckLoginRequest();
        checkLoginRequest.setData(GlobalValue.token);

        ApiController.doPostRequest(this, checkLoginRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        SetInfo();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(KeyboardActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.alertConnectFail), true);
            }
        });
    }

    public  void SetInfo()
    {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        edtKeyword = (EditText) findViewById(R.id.edtKeyword);
        btnClear = (ImageButton) findViewById(R.id.btnClear);
        btnClear.setVisibility(View.INVISIBLE);
        txtTitleHeader.setText(getResources().getString(R.string.labelKeyboard));

//        sb.append("1599");
        edtKeyword.setText(sb.toString());
        edtKeyword.setSelection(edtKeyword.getText().length());
        edtKeyword.setMaxWidth(maxLength);

        edtKeyword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        btnClear.setOnClickListener(onClickListener);

        GridLayout grid = (GridLayout) findViewById(R.id.grid);
        for (int i = 0; i < grid.getChildCount(); i++) {
            Button button = (Button) grid.getChildAt(i);
            button.setOnClickListener(onClickListener);
        }
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v instanceof Button) {
                Button button = (Button) v;
                String label = button.getText().toString().trim();
                if(sb.length() == maxLength) return;
                if(label.length() != 0) {
                    sb.append(label);
                    edtKeyword.setText(sb.toString());
                    edtKeyword.setSelection(edtKeyword.getText().length());

                    if(sb.length() > 9) {
                        edtKeyword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                    } else {
                        edtKeyword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                    }

                    if(edtKeyword.length() > 0) {
                        btnClear.setVisibility(View.VISIBLE);
                    } else {
                        btnClear.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if(sb.length() != 0) {

//                        String[] s = new String[]{
//                                "Gọi cá nhân",
//                                "Gọi qua tổng đài"
//                        };
//
//                        DialogUtility.showDialogPick(KeyboardActivity.this, "Chọn phương thức gọi",s,new SingleChoiceCallback() {
//                                    @Override
//                                    public void onChose(int which, String text) {
//                                        switch (which) {
//                                            case 0:
//                                                callPrivatePhone(sb.toString());
//                                                break;
//                                            case 1:
//                                                callHostPhone(sb.toString());
//                                                break;
//                                        }
//                                    }
//                                });

//                        Utils.callPhone(KeyboardActivity.this, sb.toString());
                        callHostPhone(sb.toString());
                    }
                }



            } else if (v instanceof ImageButton) {
                if(sb.length() == 0) return;
                sb.deleteCharAt(sb.length() - 1);
                edtKeyword.setText(sb.toString());
                edtKeyword.setSelection(edtKeyword.getText().length());

                if(sb.length() > 9) {
                    edtKeyword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                } else {
                    edtKeyword.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                }
            }
        }
    };

    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        finish();
    }
}
