package com.viettel.view.activity.authen;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.marshalchen.ultimaterecyclerview.RecyclerItemClickListener;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.viettel.adapter.TinNhanAdapter;
import com.viettel.model.NoiDungInfo;
import com.viettel.model.TinNhanInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.ParserUtility;
import com.viettel.utils.StringUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TinNhanActivity extends BABaseActivity {

    TextView txtTitleHeader;
    public static UltimateRecyclerView recyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    public static ArrayList<TinNhanInfo> danhBaInfoArrayList;
    public static TinNhanAdapter danhBaAdapter;
    private ProgressBar bar;
    private ImageButton btnChatMoi;
    private String token = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tinnhan);
        initView();
        binData();
        ConnectSocket();
        control();
    }


    public void initView() {

        try {
            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            txtTitleHeader.setText(getString(R.string.labelTinNhanInfo));


            mLinearLayoutManager = new LinearLayoutManager(this.getParent());
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView = (UltimateRecyclerView) findViewById(R.id.recyTinNhan);
            recyclerView.setLayoutManager(mLinearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.refreshDrawableState();
            bar = (ProgressBar) findViewById(R.id.progressBar);
            btnChatMoi = (ImageButton) findViewById(R.id.btnChatMoi);
            danhBaInfoArrayList = new ArrayList<>();


            token = sharePreference.getChatToken();
            ConnectApiSocket.ConnectSocket(token);
        } catch (NullPointerException e) {
            Log.e("", "TAG DANH BA:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG DANH BA:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG DANH BA:  " + er.toString());
        }
    }

    public void binData() {
        ConnectApiSocket.getListRoomsChat(this, token, "0", new ResultListener() {
            @Override
            public void onSuccess(String json) {
                if (json.contains("Success")) {
                    danhBaInfoArrayList = new ArrayList<TinNhanInfo>();
                    danhBaInfoArrayList = ParserUtility.parselistTinNhan(json);
                    new LoadDataAsynTask1().execute();
                }
            }
        });
    }

    public void control() {
        try {
            btnChatMoi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TinNhanActivity.this, TaoNhomDanhBaActivity.class);
                    intent.putExtra("tag", "DANHBA");
                    intent.putExtra("group_rom_id", "");
                    intent.putExtra("thanh_vien_nhom_json", "");
                    startActivity(intent);
                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(TinNhanActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
//                    Log.e("","xxxxxxxxxxxxxxx:    " + danhBaInfoArrayList.size());
                    Intent intent = new Intent(TinNhanActivity.this, ChatActivity.class);
                    intent.putExtra("group_rom_id", danhBaInfoArrayList.get(position).getId());
                    intent.putExtra("name", danhBaInfoArrayList.get(position).getName());
                    intent.putExtra("avatar", danhBaInfoArrayList.get(position).getSenderChatInfo().getAvatar());
                    intent.putExtra("user_id", danhBaInfoArrayList.get(position).getUser_id());
                    intent.putExtra("id_tin_nhan", danhBaInfoArrayList.get(position).getId());
                    intent.putExtra("is_group", danhBaInfoArrayList.get(position).getType());
                    intent.putExtra("tag", "ROOM");
                    startActivity(intent);
                }
            }));

        } catch (NullPointerException e) {
            Log.e("", "NHAT KY:  " + e.toString());
        } catch (IndexOutOfBoundsException e) {
            Log.e("", "NHAT KY:  " + e.toString());
        } catch (Exception e) {
        }
    }


    public void onClickBack(View v) {
        onKeyBack();
    }


    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private class LoadDataAsynTask1 extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            try {

                if (danhBaInfoArrayList != null && danhBaInfoArrayList.size() > 0) {
                    danhBaAdapter = new TinNhanAdapter(TinNhanActivity.this, danhBaInfoArrayList, sharePreference.getChatId());

                }

            } catch (NullPointerException e) {
                Log.e("", "NHAT KY:  " + e.toString());
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                if (danhBaInfoArrayList.size() > 0) {
                    recyclerView.setAdapter(danhBaAdapter);
                    danhBaAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    btnChatMoi.setVisibility(View.GONE);
                    bar.setVisibility(View.GONE);
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
        }
    }


    private void ConnectSocket() {

        ConnectApiSocket.socketOnNewMessage(new ResultListener() {
            @Override
            public void onSuccess(String json) {

                final String conten = json;
                if (!conten.equals("")) {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                NoiDungInfo noiDungInfo = ParserUtility.parseItemNoiDung(conten);


                                if (danhBaInfoArrayList == null || danhBaInfoArrayList.size() == 0) {
                                    TinNhanInfo tinNhanInfo = new TinNhanInfo();
                                    tinNhanInfo.setUpdatedAt(noiDungInfo.getUpdatedAt());
                                    tinNhanInfo.setId(noiDungInfo.getRoom_id());
                                    tinNhanInfo.setUser_id(sharePreference.getChatId());
                                    tinNhanInfo.setCreatedAt(noiDungInfo.getCreatedAt());
                                    tinNhanInfo.setName(noiDungInfo.getSenderChatInfo().getUsername());
                                    tinNhanInfo.setLast_time_send_message(noiDungInfo.getCreatedAt());
                                    tinNhanInfo.setSender_id(noiDungInfo.getSenderChatInfo().getId());
                                    tinNhanInfo.setLast_message(noiDungInfo.getContent());
                                    tinNhanInfo.setSenderChatInfo(noiDungInfo.getSenderChatInfo());
                                    tinNhanInfo.setUnread_message("1");
                                    danhBaInfoArrayList.add(tinNhanInfo);
                                    danhBaAdapter = new TinNhanAdapter(TinNhanActivity.this, TinNhanActivity.danhBaInfoArrayList, sharePreference.getChatId());
                                    recyclerView.setAdapter(TinNhanActivity.danhBaAdapter);
                                    danhBaAdapter.notifyDataSetChanged();
                                } else {
                                    for (int i = 0; i < TinNhanActivity.danhBaInfoArrayList.size(); i++) {
                                        if (danhBaInfoArrayList.get(i).getId().equals(noiDungInfo.getRoom_id())) {
                                            danhBaInfoArrayList.get(i).setUpdatedAt(noiDungInfo.getUpdatedAt());
                                            danhBaInfoArrayList.get(i).setId(noiDungInfo.getRoom_id());
                                            danhBaInfoArrayList.get(i).setUser_id(sharePreference.getChatId());
                                            danhBaInfoArrayList.get(i).setCreatedAt(noiDungInfo.getCreatedAt());
//                                            TinNhanActivity.danhBaInfoArrayList.get(i).setName(noiDungInfo.getSenderChatInfo().getUsername());.
                                            danhBaInfoArrayList.get(i).setUnread_message("1");
                                            danhBaInfoArrayList.get(i).setLast_time_send_message(noiDungInfo.getCreatedAt());
                                            danhBaInfoArrayList.get(i).setSender_id(noiDungInfo.getSenderChatInfo().getId());
                                            danhBaInfoArrayList.get(i).setLast_message(noiDungInfo.getContent());
                                            danhBaInfoArrayList.get(i).setSenderChatInfo(noiDungInfo.getSenderChatInfo());
                                        }

                                    }

                                }
                                Collections.sort(danhBaInfoArrayList,
                                        new Comparator<TinNhanInfo>() {
                                            @Override
                                            public int compare(TinNhanInfo lhs,
                                                               TinNhanInfo rhs) {
                                                int intlhs = Integer.parseInt((lhs.getCreatedAt() != null && !lhs.getCreatedAt().trim().equals("")) ? StringUtility.getDateTimeStamp(lhs.getCreatedAt()) : "1");
                                                int intrhs = Integer.parseInt((rhs.getCreatedAt() != null && !rhs.getCreatedAt().trim().equals("")) ? StringUtility.getDateTimeStamp(rhs.getCreatedAt()) : "1");
                                                return intrhs - intlhs;
                                            }
                                        });
                                TinNhanActivity.danhBaAdapter.notifyDataSetChanged();
                            }
                        });

                    } catch (NullPointerException nul) {
                        Log.e("", "TAG CHAT:  " + nul.toString());
                    } catch (Exception e) {
                        Log.e("", "TAG CHAT:  " + e.toString());
                    } catch (Error er) {
                        Log.e("", "TAG CHAT:  " + er.toString());
                    }

                }

            }
        });
    }
}
