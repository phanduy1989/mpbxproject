package com.viettel.view.activity.authen;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.marshalchen.ultimaterecyclerview.RecyclerItemClickListener;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.viettel.adapter.DanhBaAdapter;
import com.viettel.model.DanhBaInfo;
import com.viettel.mpbx.R;
import com.viettel.utils.ParserUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

public class DanhBaActivity extends BABaseActivity {

    TextView txtTitleHeader;
    private UltimateRecyclerView recyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    public static ArrayList<DanhBaInfo> danhBaInfoArrayList;
    private DanhBaAdapter danhBaAdapter;
    private ProgressBar bar;
    private ImageButton btnChatMoi;
    private String token ="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_danhba);
        initView();
        binData();
        connectSocket();
        control();
    }


    public void initView() {

        try {
            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            txtTitleHeader.setText(getString(R.string.labelDanhBaInfo));
            danhBaInfoArrayList = new ArrayList<>();
            mLinearLayoutManager = new LinearLayoutManager(this);
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView = (UltimateRecyclerView) findViewById(R.id.recyDanhBa);
            recyclerView.setLayoutManager(mLinearLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.refreshDrawableState();
            bar = (ProgressBar) findViewById(R.id.progressBar);
            btnChatMoi = (ImageButton) findViewById(R.id.btnChatMoi);
            token = sharePreference.getChatToken();
        } catch (NullPointerException e) {
            Log.e("", "TAG DANH BA:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG DANH BA:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG DANH BA:  " + er.toString());
        }
    }
    public void binData()
    {
        ConnectApiSocket.getListContact(this, token, "", new ResultListener() {
            @Override
            public void onSuccess(String json) {
                if (json.toLowerCase().contains("success"))
                {
                    danhBaInfoArrayList = ParserUtility.parselistDanhBa(json);
                    new LoadDataAsynTask1().execute();
                }
            }
        });
    }
    public void control()
    {
        try {
            btnChatMoi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(DanhBaActivity.this, TaoNhomDanhBaActivity.class);
                    intent.putExtra("tag", "DANHBA");
                    intent.putExtra("group_rom_id", "");
                    intent.putExtra("thanh_vien_nhom_json", "");
                    startActivity(intent);
                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(DanhBaActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Intent intent = new Intent(DanhBaActivity.this, ChatActivity.class);
                    intent.putExtra("group_rom_id", danhBaInfoArrayList.get(position).getRoom_id() == null ? "" : danhBaInfoArrayList.get(position).getRoom_id());
                    intent.putExtra("name", (danhBaInfoArrayList.get(position).getName() == null || danhBaInfoArrayList.get(position).getName().equals("")) ? danhBaInfoArrayList.get(position).getUsername() : danhBaInfoArrayList.get(position).getName());
                    intent.putExtra("avatar", danhBaInfoArrayList.get(position).getAvatar());
                    intent.putExtra("user_id", danhBaInfoArrayList.get(position).getId());
                    intent.putExtra("id_tin_nhan", danhBaInfoArrayList.get(position).getId());
                    intent.putExtra("is_group", "0");
                    intent.putExtra("tag", "ROOM");
                    startActivity(intent);
                }
            }));
        } catch (NullPointerException e) {
            Log.e("", "TAG DANH BA:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG DANH BA:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG DANH BA:  " + er.toString());
        }
    }



    public void onClickBack(View v) {
        onKeyBack();
    }


    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
    private class LoadDataAsynTask1 extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            try {

                if (danhBaInfoArrayList != null && danhBaInfoArrayList.size() > 0) {
                    danhBaAdapter = new DanhBaAdapter(DanhBaActivity.this, danhBaInfoArrayList);

//                recyclerView.setAdapter(danhBaAdapter);
//                danhBaAdapter.notifyDataSetChanged();
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                if (danhBaInfoArrayList.size() > 0) {
                    recyclerView.setAdapter(danhBaAdapter);
                    danhBaAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    btnChatMoi.setVisibility(View.GONE);
                    bar.setVisibility(View.GONE);
                }

            } catch (NullPointerException e) {
                Log.e("", "TAG DANH BA:  " + e.toString());
            } catch (Exception e) {
            }
        }
    }

    private void connectSocket() {
        try {
            ConnectApiSocket.socketOnUserChangeStatus(new ResultListener() {
                @Override
                public void onSuccess(String json) {

                    final String content = json;
                    if (!content.equals("")) {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DanhBaInfo danhBaInfo = ParserUtility.parseStatusOnline(content);
                                    for (int i = 0; i < danhBaInfoArrayList.size(); i++) {
                                        if (danhBaInfo.getId().equals(danhBaInfoArrayList.get(i).getId()))
                                            danhBaInfoArrayList.get(i).setOnline(danhBaInfo.getOnline());
                                    }
                                    if (danhBaAdapter != null)
                                        danhBaAdapter.notifyDataSetChanged();
                                }
                            });


                        } catch (NullPointerException nul) {
                            Log.e("", "TAG CHAT:  " + nul.toString());
                        } catch (Exception e) {
                            Log.e("", "TAG CHAT:  " + e.toString());
                        } catch (Error er) {
                            Log.e("", "TAG CHAT:  " + er.toString());
                        }

                    }

                }
            });
//            WebServiceConfig.mSocket.on("user_offline", user_offline);
        }catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }
}
