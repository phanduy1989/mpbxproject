package com.viettel.view.fragment.business;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viettel.interfaces.ShowHideListener;
import com.viettel.utils.AnimationUtil;
import com.viettel.view.activity.business.MainHomeActivity;

import com.viettel.mpbx.R;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/2/17.
 */
public class ContactFragment extends BaseFragment {

    private ContactAdapter contactAdapter;

    MainHomeActivity mainHomeActivity;

    ListView mContactsList;
    LinearLayout layoutSearch, layoutSearchFake;
    EditText editTextSearch;

    View view;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainHomeActivity = (MainHomeActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact, container, false);

        initView();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void initView() {
        mContactsList = (ListView) view.findViewById(R.id.listContact);
        layoutSearchFake = (LinearLayout) view.findViewById(R.id.layoutSearchFake);
        layoutSearch = (LinearLayout) view.findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) view.findViewById(R.id.edtSearch);

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        contactAdapter = new ContactAdapter();
        mContactsList.setAdapter(contactAdapter);

        ArrayList<ContactModel> listContact = getContactNames();

        contactAdapter.setData(listContact);

    }

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(mainHomeActivity, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                mainHomeActivity.showKeyBoard(editTextSearch);
            }
        });
    }

    private ArrayList<ContactModel> getContactNames() {
        ArrayList<ContactModel> contacts = new ArrayList<>();

        Cursor phones = mainHomeActivity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            ContactModel contactModel = new ContactModel();
            contactModel.setName(name);
            contactModel.setPhone(phoneNumber);
            contacts.add(contactModel);
        }
        phones.close();


        return contacts;
    }

    @Override
    public boolean onKeyBack() {
        if(layoutSearch.getVisibility() == View.VISIBLE) {
            layoutSearch.setVisibility(View.GONE);
            AnimationUtil.setTranslateHorizontalAnimation(mainHomeActivity, layoutSearchFake, true, R.anim.slide_in_right, new ShowHideListener() {
                @Override
                public void doAfter() {
                    mainHomeActivity.hideAllKeyBoard();
                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onKeySearch() {

    }

    class ContactModel {
        String name;
        String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    class ContactAdapter extends BaseAdapter {

        public ArrayList<ContactModel> listData;

        public void setData(ArrayList<ContactModel> list) {
            listData = list;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return listData != null ? listData.size() : 0;
        }

        @Override
        public ContactModel getItem(int position) {
            return listData != null ? listData.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            public TextView name;
            public TextView phone;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if(convertView == null) {
                convertView = mainHomeActivity.getLayoutInflater().inflate(R.layout.item_contact, null);
                viewHolder = new ViewHolder();
                viewHolder.name = (TextView)convertView.findViewById(R.id.textName);
                viewHolder.phone = (TextView)convertView.findViewById(R.id.textPhone);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            ContactModel contactModel = getItem(position);

            if(contactModel != null) {
                viewHolder.name.setText(contactModel.getName());
                viewHolder.phone.setText(contactModel.getPhone());
            }

            return convertView;
        }
    }

}
