package com.viettel.view.activity.business;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.ContactAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.ContactObject;
import com.viettel.model.request.contact.GetListContactRequest;
import com.viettel.model.request.contact.SyncontactRequest;
import com.viettel.model.response.contact.GetListContactResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.AnimationUtil;
import com.viettel.utils.Utils;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 7/4/17.
 */
public class ContactActivity extends BABaseActivity {

    private ContactAdapter contactAdapter;

    ListView mContactsList;
    RelativeLayout layoutCall;
    LinearLayout layoutCallFunction;
    TextView txtTitleHeader;
    TextView txtSelected;
    ImageView iconPick;

    ContactObject pickContact;
    boolean isSelectAll;

    boolean isMultiSelect;
    boolean isSynchros;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contact_activity);

        isMultiSelect = getIntent().getBooleanExtra(GlobalInfo.BUNDLE_KEY_MODE_MULTI_SELECT, false);
        isSynchros = getIntent().getBooleanExtra(GlobalInfo.BUNDLE_KEY_MODE_SYNCHROS, false);

        initView();

        initData();
    }

    public void initView() {
        mContactsList = (ListView) findViewById(R.id.listContact);
        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtSelected = (TextView) findViewById(R.id.txtSelected);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);
        iconPick = (ImageView) findViewById(R.id.iconPick);

        txtTitleHeader.setText(getResources().getString(R.string.labelContact));

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        layoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCallLayout();
            }
        });

        contactAdapter = new ContactAdapter(this, isMultiSelect);
        mContactsList.setAdapter(contactAdapter);

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                contactAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

        mContactsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ContactModel contactModel = contactAdapter.getItem(position);
//                contactModel.changeChosed();
                contactAdapter.choseItem(position);
                setSelectedTitle();
            }
        });

        if(!isMultiSelect) {
            findViewById(R.id.layoutSelected).setVisibility(View.GONE);
        } else {
            setSelectedTitle();
        }

    }

    public void initData() {
        if(isSynchros) {
            ArrayList<ContactObject> listContact = getContactNames();
            contactAdapter.setData(listContact);
        } else {
            showLoading();

            GetListContactRequest getListCustomerRequest = new GetListContactRequest();
            getListCustomerRequest.setData();

            ApiController.doPostRequest(this, getListCustomerRequest, new ResponseListener() {
                @Override
                public void processResponse(String response) {

                    closeLoading();

                    if(response != null) {
                        Log.e("Response", response);
                        Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                        GetListContactResponse signInResponse = gson.fromJson(response, GetListContactResponse.class);


                        if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                            contactAdapter.setData(signInResponse.getListContact());
                        } else {
                            showError(signInResponse, true);
                        }
                    } else {
                        showError(getResources().getString(R.string.text_no_network), true);
                    }
                }

                @Override
                public void processResponse(int error, String content) {
                    closeLoading();
                    showError(getResources().getString(R.string.text_no_network), true);
                }
            });
        }

    }

    public void onClickSelectAll(View view) {
        isSelectAll = !isSelectAll;

        iconPick.setImageResource(isSelectAll ? R.drawable.check_invite : R.drawable.nocheck_invite);
        contactAdapter.setSelectAll(isSelectAll);

        setSelectedTitle();
    }

    public void setSelectedTitle() {

        if(!isMultiSelect) return;

        // txtSelected.setText("Đã chọn (" + contactAdapter.getSelectedCount() + "/" + contactAdapter.getCount() + ")");

        int selected = contactAdapter.getSelectedCount();
        int total = contactAdapter.getCount();

        txtSelected.setText("Đã chọn (" + selected + "/" + total + ")");

        if (total > 0)
        {
            if (selected == total)
            {
                iconPick.setImageResource(R.drawable.check_invite);
            }
            else
            {
                iconPick.setImageResource(R.drawable.nocheck_invite);
            }
        }
    }

    public void onClickConfirm(View view) {

        ArrayList<ContactObject> listContacs = contactAdapter.getListPicked();
        if(listContacs == null || listContacs.isEmpty()) {
            Toast.makeText(this, "Bạn chưa chọn danh bạ", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if(!isMultiSelect) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(GlobalInfo.BUNDLE_KEY_CONTACT, listContacs.get(0));
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            } else {
                showLoading();

                SyncontactRequest syncontactRequest = new SyncontactRequest();
                syncontactRequest.setData(listContacs);

                ApiController.doPostRequest(this, syncontactRequest, new ResponseListener() {
                    @Override
                    public void processResponse(String response) {
                        closeLoading();

                        if(response != null) {
                            Log.e("Response", response);
                            Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                            GetListContactResponse signInResponse = gson.fromJson(response, GetListContactResponse.class);


                            if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
//                                setListData(signInResponse.getListContact());

                                Intent resultIntent = new Intent();
                                resultIntent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_CONTACT, signInResponse.getListContact());
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            } else {
                                showError(signInResponse, true);
                            }
                        } else {
                            showError(getResources().getString(R.string.text_no_network), true);
                        }
                    }

                    @Override
                    public void processResponse(int error, String content) {
                        closeLoading();
                        showError(getResources().getString(R.string.text_no_network), true);
                    }
                });



            }
        }

    }

    public void showCallLayout() {
        layoutCall.setVisibility(View.VISIBLE);
    }

    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    private ArrayList<ContactObject> getContactNames() {
        ArrayList<ContactObject> contacts = new ArrayList<>();

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            ContactObject contactModel = new ContactObject();
            contactModel.setContactName(name);
            contactModel.setPhoneNumber(phoneNumber);
            contacts.add(contactModel);
        }
        phones.close();


        return contacts;
    }

    public void onCallPrivate(View view) {
        Utils.callPhone(this, pickContact.getPhoneNumber());
    }

    public void onCallHost(View view) {
        Utils.callPhone(this, pickContact.getPhoneNumber());
    }
}
