package com.viettel.view.activity.business.conversation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.MemberAdapter;
import com.viettel.adapter.holder.PaddingForVodLand;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ColleagueModel;
import com.viettel.model.ContactObject;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationMember;
import com.viettel.model.ConversationModel;
import com.viettel.model.CustomerModel;
import com.viettel.model.PBXModel;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.model.request.conference.CreateConversationRequest;
import com.viettel.model.request.conference.UpdateConversationRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.DateUtil;
import com.viettel.utils.DialogUtility;
import com.viettel.view.activity.business.ChoseMemberActivity;
import com.viettel.view.activity.business.RemoveMemberActivity;
import com.viettel.view.base.BABaseActivity;
import com.viettel.widgets.DatePicker;
import com.viettel.widgets.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by duyuno on 7/13/17.
 */
public class CreateConversationActivity extends BABaseActivity {

    public static final int MODE_CREATE = 0;
    public static final int MODE_UPDATE = 1;

    public int mode;

    TextView txtTitleHeader;
    TextView txtTopicTitle;
    TextView txtBtnBottom;
    TextView startTimeTitle, endTimeTitle;
    LinearLayout layoutStartTime, layoutEndtime;
    RelativeLayout btnCreateConference;

    EditText edtConferenceTopic;

    DatePicker startDate, endDate;
    TimePicker startTime, endTime;

    RecyclerView listMember;

    MemberAdapter memberAdapter;
    ConversationModel conversationModel;
    ArrayList<ConversationMember> listConferenceMember;
    ArrayList<ConversationMember> listConferenceNonMember;

    ArrayList<ColleagueModel> listColeague;

    boolean isCreated;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_create_activity);

        Intent intent = getIntent();
        mode = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_MODE, -1);
        if(mode < 0) {
            finish();
            return;
        }

        conversationModel = intent.getParcelableExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION);
        if(mode == MODE_UPDATE && conversationModel == null) {
            finish();
            return;
        }

        listConferenceMember = intent.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER);

        if(listConferenceMember == null) {
            listConferenceMember = new ArrayList<>();
        }

        listConferenceNonMember = intent.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_NON_MEMBER);

        initView();

    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtTopicTitle = (TextView) findViewById(R.id.txtTopicTitle);
        txtBtnBottom = (TextView) findViewById(R.id.txtBtnBottom);
        edtConferenceTopic = (EditText) findViewById(R.id.edtConferenceTopic);
        startDate = (DatePicker) findViewById(R.id.startDate);
        endDate = (DatePicker) findViewById(R.id.endDate);
        btnCreateConference = (RelativeLayout) findViewById(R.id.btnCreateConference);

        startTime = (TimePicker) findViewById(R.id.startTime);
        endTime = (TimePicker) findViewById(R.id.endTime);
        startTimeTitle = (TextView) findViewById(R.id.startTimeTitle);
        endTimeTitle = (TextView) findViewById(R.id.endTimeTitle);
        layoutStartTime = (LinearLayout) findViewById(R.id.layoutStartTime);
        layoutEndtime = (LinearLayout) findViewById(R.id.layoutEndtime);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, GlobalValue.minConfig);
        startDate.setDate(calendar.getTime());
        startTime.setTime(calendar.getTime());


        calendar.add(Calendar.HOUR_OF_DAY, 1);
        endDate.setDate(calendar.getTime());
        endTime.setTime(calendar.getTime());

        listMember = (RecyclerView) findViewById(R.id.listMember);
        LinearLayoutManager manager = new LinearLayoutManager(getLayoutInflater().getContext());
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);

        memberAdapter = new MemberAdapter(this);
        listMember.setAdapter(memberAdapter);
        memberAdapter.setData(listConferenceMember);
        listMember.setLayoutManager(manager);
        listMember.addItemDecoration(new PaddingForVodLand(getLayoutInflater().getContext()));

        if(mode == MODE_UPDATE) {
//            edtConferenceTopic.setVisibility(View.GONE);
//            txtTopicTitle.setVisibility(View.GONE);
//            listMember.setVisibility(View.GONE);
            txtTitleHeader.setText("Cài đặt hội nghị");

            txtBtnBottom.setText("Cập nhật");

            switch (conversationModel.getConversationType()) {
                case ConversationGroup.FINISHED:
                    startDate.setEnable(false);
                    edtConferenceTopic.setEnabled(false);
                    endDate.setEnable(false);
                    startTime.setEnable(false);
                    endTime.setEnable(false);
                    btnCreateConference.setVisibility(View.GONE);
                    break;
                case ConversationGroup.UP_COMMING:
                    break;
                case ConversationGroup.ON_GOING:
                    startTimeTitle.setVisibility(View.GONE);
                    edtConferenceTopic.setEnabled(false);
                    endTimeTitle.setVisibility(View.GONE);
                    layoutStartTime.setVisibility(View.GONE);
                    layoutEndtime.setVisibility(View.GONE);
//                    txtTopicTitle.setVisibility(View.GONE);
//                    edtConferenceTopic.setVisibility(View.GONE);
                    txtTitleHeader.setText("Thêm thành viên");
                    break;
            }

            return;
        } else {
            txtTitleHeader.setText("Tạo mới hội nghị");
            txtBtnBottom.setText("Tiếp tục");
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initData() {
        if(mode == MODE_CREATE) {
            return;
        }

//        showLoading();

        startDate.setDate(conversationModel.getStartDate());
        endDate.setDate(conversationModel.getEndDate());
        startTime.setTime(conversationModel.getStartDate());
        endTime.setTime(conversationModel.getEndDate());

        edtConferenceTopic.setText(conversationModel.getConferenceName());
    }

    public void onClickRemoveMember(View view) {
        Intent intent = new Intent(getBaseContext(), RemoveMemberActivity.class);
        if(memberAdapter != null && memberAdapter.getItemCount() > 0) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT, memberAdapter.getData());
            startActivityForResult(intent, REQUEST_CODE_REMOVE_MEMBER);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        } else {
            Toast.makeText(CreateConversationActivity.this, "Hiện chưa có thành viên nào!", Toast.LENGTH_SHORT).show();
        }

    }
    public void addColleague(View view) {
        Intent intent = new Intent(getBaseContext(), ChoseMemberActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_LIST_TYPE, ChoseMemberActivity.LIST_TYPE_COLLEAGUE);
        if(conversationModel != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, conversationModel.getConferenceId());
        }
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ACTION, ChoseMemberActivity.MODE_ADD);
        if(memberAdapter != null && memberAdapter.getItemCount() > 0) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT, memberAdapter.getData());
        }
        startActivityForResult(intent, REQUEST_CODE_CHOSE_MEMBER);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void addCustomer(View view) {
        Intent intent = new Intent(getBaseContext(), ChoseMemberActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_LIST_TYPE, ChoseMemberActivity.LIST_TYPE_CUSTOMER);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ACTION, ChoseMemberActivity.MODE_ADD);
        if(conversationModel != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, conversationModel.getConferenceId());
        }
        if(memberAdapter != null && memberAdapter.getItemCount() > 0) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_CONTACT, memberAdapter.getData());
        }
        startActivityForResult(intent, REQUEST_CODE_CHOSE_MEMBER);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
    public void addMemberDirectly(View view) {
        Intent intent = new Intent(getBaseContext(), AddMemberDirectAcitivity.class);
        startActivityForResult(intent, REQUEST_CODE_CHOSE_MEMBER);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(data == null) return;
            if(requestCode == REQUEST_CODE_CHOSE_MEMBER) {
                boolean isSuccess = data.getBooleanExtra(GlobalInfo.BUNDLE_KEY_SUCCESS, false);
                if(isSuccess) {
                    ArrayList<PBXModel> listMember = data.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER);
                    PBXModel pbxModel = data.getParcelableExtra(GlobalInfo.BUNDLE_KEY_CONTACT);

                    if(listMember != null) {
                        for(PBXModel pbxModel1 : listMember) {
                            pbxModel1.changePhoneFormat();
                        }
                    }

                    if (mode == MODE_CREATE) {
                        if(listMember != null) {

                            ArrayList<ConversationMember> listConversationMember = new ArrayList<>();
                            ArrayList<ConversationMember> listConversationNonMember = new ArrayList<>();

                            for(PBXModel pbxModel1 : listMember) {
                                if(pbxModel1 instanceof ColleagueModel) {

                                    ((ColleagueModel) pbxModel1).syncId();

                                    ConversationMember conversationMember = new ConversationMember();
                                    conversationMember.setConferenceMemberId(((ColleagueModel) pbxModel1).getAgentId());
                                    conversationMember.setMemberName(((ColleagueModel) pbxModel1).getColleagueName());
                                    conversationMember.setPhoneNumber(((ColleagueModel) pbxModel1).getPhoneNumber());

                                    listConversationMember.add(conversationMember);
                                } else if(pbxModel1 instanceof CustomerModel) {
                                    ConversationMember conversationMember = new ConversationMember();
//                                    conversationMember.setConferenceMemberId(-1);
                                    conversationMember.setMemberName(((CustomerModel) pbxModel1).getCustomerName());
                                    conversationMember.setPhoneNumber(((CustomerModel) pbxModel1).getPhoneNumber());

                                    listConversationMember.add(conversationMember);
                                }
                            }

//                            memberAdapter.setData(listConversationMember);

                            memberAdapter.addMembers(listConversationMember);
                        }

                        if(pbxModel != null) {

                            ConversationMember conversationMember = new ConversationMember();
//                            conversationMember.setConferenceMemberId(-1);
                            conversationMember.setMemberName(((ContactObject) pbxModel).getContactName());
                            conversationMember.setPhoneNumber(((ContactObject) pbxModel).getPhoneNumber());

                            memberAdapter.addMember(conversationMember);
                        }
                    } else {
                        if(listMember != null) {
                            addExistMembers(getListConversationMember(listMember));
                        } else if (pbxModel != null) {

                            ConversationMember conversationMember = new ConversationMember();
//                            conversationMember.setConferenceMemberId(-1);
                            conversationMember.setMemberName(((ContactObject) pbxModel).getContactName());
                            conversationMember.setPhoneNumber(((ContactObject) pbxModel).getPhoneNumber());

                            addDirectMembers(conversationMember);
                        }
                    }
                }
            } else if (requestCode == REQUEST_CODE_REMOVE_MEMBER) {
                ArrayList<ConversationMember> listMember = data.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER);
                if(listMember == null || listMember.isEmpty()) {
                    memberAdapter.clearData();
                } else {
                    memberAdapter.setData(listMember);
                }

            }
        }
    }

    public ArrayList<ConversationMember> getListConversationMember(ArrayList<PBXModel> listData) {
        if(listData == null || listData.isEmpty()) return null;

        ArrayList<ConversationMember> listConversationMember = new ArrayList<>();

        for(PBXModel pbxModel : listData) {
            if(pbxModel instanceof ContactObject) {

                ConversationMember conversationMember = new ConversationMember();
//                conversationMember.setConferenceMemberId(-1);
                conversationMember.setMemberName(((ContactObject) pbxModel).getContactName());
                conversationMember.setPhoneNumber(((ContactObject) pbxModel).getPhoneNumber());

                listConversationMember.add(conversationMember);
            } else if(pbxModel instanceof CustomerModel) {

                ConversationMember conversationMember = new ConversationMember();
//                conversationMember.setConferenceMemberId(-1);
                conversationMember.setMemberName(((CustomerModel) pbxModel).getCustomerName());
                conversationMember.setPhoneNumber(((CustomerModel) pbxModel).getPhoneNumber());

                listConversationMember.add(conversationMember);

//                CustomerModel customerModel = (CustomerModel) pbxModel;
//                ContactModel contactModel = new ContactModel();
//                contactModel.setName(customerModel.getCustomerName());
//                contactModel.setPhone(customerModel.getNumberOfExt() != null ? customerModel.getNumberOfExt() : customerModel.getPhoneNumber());
//                listContact.add(contactModel);
            } else {
                ((ColleagueModel) pbxModel).syncId();
                ConversationMember conversationMember = new ConversationMember();
                conversationMember.setConferenceMemberId(((ColleagueModel) pbxModel).getAgentId());
                conversationMember.setMemberName(((ColleagueModel) pbxModel).getColleagueName());
                conversationMember.setPhoneNumber(((ColleagueModel) pbxModel).getPhoneNumber());

                listConversationMember.add(conversationMember);

//                ContactModel contactModel = new ContactModel();
//                contactModel.setName(colleagueModel.getColleagueName());
//                contactModel.setAgentId(colleagueModel.getAgentId());
//                contactModel.setPhone(colleagueModel.getNumberOfExt() != null ? colleagueModel.getNumberOfExt() : colleagueModel.getPhoneNumber());
//                listContact.add(contactModel);
            }

        }
        return listConversationMember;
    }

    public void addExistMembers(ArrayList<ConversationMember> listMember) {

        if(listMember == null || listMember.isEmpty()) {
            return;
        }

        showLoading();

        listConferenceMember = new ArrayList<>();
        listConferenceNonMember = new ArrayList<>();
        listConferenceMember.addAll(memberAdapter.getListMember());
        listConferenceMember.addAll(memberAdapter.getListNonMember());
        for(ConversationMember conversationMember : listMember) {
            if(conversationMember.getConferenceMemberId() > 0) {
                listConferenceMember.add(conversationMember);
            } else {
                listConferenceNonMember.add(conversationMember);
            }
        }

        UpdateConversationRequest updateConversationRequest = new UpdateConversationRequest();
        updateConversationRequest.setData(listConferenceMember, listConferenceNonMember, conversationModel.getConferenceId(), conversationModel.getConferenceName()
                , DateUtil.convertFullDate(conversationModel.getStartDate()), DateUtil.convertFullDate(conversationModel.getEndDate()));

        ApiController.doPostRequest(this, updateConversationRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.alertAddMemberSuccess), Toast.LENGTH_SHORT).show();
                        isCreated = true;
                        onKeyBack();

                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });

//        AddExistMemberRequest addExistMemberRequest = new AddExistMemberRequest();
//        addExistMemberRequest.setData(listMember, sharePreference.getAccessToken()
//                ,"" + sharePreference.getNumOfCompany(), "" + conversationModel.getConferenceId());
//
//        ApiController.doPostRequest(this, addExistMemberRequest, new ResponseListener() {
//            @Override
//            public void processResponse(String response) {
//                closeLoading();
//                if (response != null) {
//                    Log.e("Response", response);
//                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
//                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);
//
//                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
//                        Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.alertAddMemberSuccess), Toast.LENGTH_SHORT).show();
//                        isCreated = true;
//                        onKeyBack();
//                    } else {
//                        showError(signInResponse, true);
//                    }
//                } else {
//                    Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void processResponse(int error, String content) {
//                showError(getResources().getString(R.string.text_no_network), true);
//            }
//        });
    }
    public void addDirectMembers(ConversationMember contactModel) {


        if(contactModel == null) {
            return;
        }

        showLoading();

        listConferenceMember = memberAdapter.getListMember();
        listConferenceNonMember = memberAdapter.getListNonMember();

        if(listConferenceNonMember == null) {
            listConferenceNonMember = new ArrayList<>();
        }

        listConferenceNonMember.add(contactModel);

        UpdateConversationRequest updateConversationRequest = new UpdateConversationRequest();
        updateConversationRequest.setData(listConferenceMember, listConferenceNonMember,  conversationModel.getConferenceId(), conversationModel.getConferenceName()
                , DateUtil.convertFullDate(conversationModel.getStartDate()), DateUtil.convertFullDate(conversationModel.getEndDate()));

        ApiController.doPostRequest(this, updateConversationRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.alertAddMemberSuccess), Toast.LENGTH_SHORT).show();
                        isCreated = true;
                        onKeyBack();

                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }


    @Override
    public void onKeyBack() {
        setResult(isCreated ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }
    public void onClickCreateConference(View view) {
        String conferenceTopic = edtConferenceTopic.getText().toString().trim();
        if (conferenceTopic.length() == 0) {
            DialogUtility.showDialogAlert(this, null, "Vui lòng nhập chủ đề hội nghị!",
                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtConferenceTopic);
            return;
        }

//        ArrayList<ContactModel> listContact = memberAdapter.getListContact();
        if(memberAdapter.getItemCount() == 0) {
            DialogUtility.showDialogAlert(this, null, "Bạn chưa thêm thành viên!",
                    getResources().getString(R.string.close_button), null);
            return;
        }

        if(startDate.compare(endDate) == 1
                || (startDate.compare(endDate) == 0 && startTime.compare(endTime) >= 0)) {
            DialogUtility.showDialogAlert(this, null, "Thời gian kết thúc không được lớn hơn hoặc bằng thời gian bắt đầu!",
                    getResources().getString(R.string.close_button), null);
            return;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, startDate.getDate().get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, startDate.getDate().get(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, startDate.getDate().get(Calendar.DAY_OF_MONTH));

        calendar.set(Calendar.HOUR_OF_DAY, startTime.getHour());
        calendar.set(Calendar.MINUTE, startTime.getMinute());

        long offset = (calendar.getTimeInMillis() - System.currentTimeMillis()) / 1000;
        if (offset < GlobalValue.minConfig * 60) {
            DialogUtility.showDialogAlert(this, null, "Thời gian bắt đầu phải lớn hơn thời điểm hiện tại ít nhất <b>" + GlobalValue.minConfig + " phút</b>!", "Đóng", null);
            return;
        }

        String startDateStr = startDate.getTimeStr() + " "+ startTime.getTimeStr();
        String endDateStr = endDate.getTimeStr() + " " + endTime.getTimeStr();



        listConferenceMember = new ArrayList<>();
        listConferenceMember.addAll(memberAdapter.getListMember());

        listConferenceNonMember = new ArrayList<>();
        listConferenceNonMember.addAll(memberAdapter.getListNonMember());

        if(mode == MODE_CREATE) {
            CreateConversationRequest conversationRequest = new CreateConversationRequest();
            conversationRequest.setData(listConferenceMember, listConferenceNonMember, conferenceTopic, startDateStr, endDateStr);
            doProcess(conversationRequest);
        } else if (mode == MODE_UPDATE) {
            UpdateConversationRequest conversationRequest = new UpdateConversationRequest();
            conversationRequest.setData(listConferenceMember, listConferenceNonMember,conversationModel.getConferenceId(), conferenceTopic, startDateStr, endDateStr);
            doProcess(conversationRequest);
        }

    }

    public void doProcess(BasePostRequestEntity basePostRequestEntity) {
        showLoading();
        ApiController.doPostRequest(this, basePostRequestEntity, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {

                        if(mode == MODE_CREATE) {
                            Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.alertCreateConferenceSuccess), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.alertEditConferenceSuccess), Toast.LENGTH_SHORT).show();
                        }
                        isCreated = true;
                        onKeyBack();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateConversationActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }
}
