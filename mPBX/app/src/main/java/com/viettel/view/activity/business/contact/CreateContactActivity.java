package com.viettel.view.activity.business.contact;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ContactObject;
import com.viettel.model.TextPhoneRole;
import com.viettel.model.TextRole;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.model.request.contact.CreateContactRequest;
import com.viettel.model.request.contact.EditContactRequest;
import com.viettel.model.request.contact.GetContactInfoRequest;
import com.viettel.model.request.UploadAvatarRequest;
import com.viettel.model.response.contact.GetContactInfoResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.model.response.UploadFileResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.ImagePicker;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.Utils;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class CreateContactActivity extends BABaseActivity {

    public static final int MODE_CREATE = 0;
    public static final int MODE_EDIT = 1;

    private int mode;

    TextView txtTitleHeader;
    TextView textReplaceAvatar;

    int contactId = -1;
    String contactName;

    EditText edtCustomerName, edtPhone, edtCompanyName, edtEmail, edtAddress, edtNote;
    TextView lblCustomerNameError, lblCustomerPhoneNumberError, lblCompanyError, lblEmailError;
    ImageView avatar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contact_create_activity);

        Intent intent = getIntent();

        contactId = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, -1);
        contactName = intent.getStringExtra(GlobalInfo.BUNDLE_KEY_NAME);
//        if (bundle != null) {
//            customerModel = bundle.getParcelable(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER);
//        }



        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        textReplaceAvatar = (TextView) findViewById(R.id.textReplaceAvatar);

        if (contactId > 0) {
            mode = MODE_EDIT;
        } else {
            mode = MODE_CREATE;
        }

        if (mode == MODE_CREATE) {
            txtTitleHeader.setText(getResources().getString(R.string.labelCreateContact));
        } else {
            txtTitleHeader.setText(contactName);
        }

        edtCustomerName = (EditText) findViewById(R.id.edtCustomerName);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtCompanyName = (EditText) findViewById(R.id.edtCompanyName);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtNote = (EditText) findViewById(R.id.edtNote);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        avatar = (ImageView) findViewById(R.id.avatar);

        lblCustomerNameError = (TextView) findViewById(R.id.lblCustomerNameError);
        lblCustomerPhoneNumberError = (TextView) findViewById(R.id.lblCustomerPhoneNumberError);
        lblCompanyError = (TextView) findViewById(R.id.lblCompanyError);
        lblEmailError = (TextView) findViewById(R.id.lblEmailError);

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(CreateContactActivity.this);
                startActivityForResult(chooseImageIntent, REQUEST_CAMERA);

            }
        });

        addTextWatcher();

        if (contactId > 0) {
            getCustomerInfo();
        }
    }

    Bitmap bitmap = null;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != Activity.RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CAMERA) {
            bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
            bitmap = Utils.cropToSquare(bitmap);
            avatar.setImageBitmap(bitmap);
        }
    }

    public void addTextWatcher() {

        addTextWatcher(edtCustomerName, lblCustomerNameError, new TextRole());
        addTextWatcher(edtPhone, lblCustomerPhoneNumberError, new TextPhoneRole());
//        addTextWatcher(edtCompanyName, lblCompanyError, new TextRole());
//        addTextWatcher(edtEmail, lblEmailError, new TextRole());

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getCustomerInfo() {
        showLoading();
        GetContactInfoRequest getCustomerInfoRequest = new GetContactInfoRequest();
        getCustomerInfoRequest.setData(contactId);

        ApiController.doPostRequest(this, getCustomerInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetContactInfoResponse signInResponse = gson.fromJson(response, GetContactInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchCustomerInfo(signInResponse.getContactObject());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateContactActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void fetchCustomerInfo(ContactObject customerModel) {

        if (customerModel == null) return;

        txtTitleHeader.setText(customerModel.getContactName() != null ? customerModel.getContactName() : "");
        edtCustomerName.setText(customerModel.getContactName() != null ? customerModel.getContactName() : "");
        edtPhone.setText(customerModel.getPhoneNumber() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()) : "");
        edtCompanyName.setText(customerModel.getCompanyName() != null ? customerModel.getCompanyName() : "");
        edtAddress.setText(customerModel.getAddress() != null ? customerModel.getAddress() : "");
        edtEmail.setText(customerModel.getEmail() != null ? customerModel.getEmail() : "");
        edtNote.setText(customerModel.getNote() != null ? customerModel.getNote() : "");

        if (customerModel.getFileName() != null)
            Picasso.with(this)
                    .load(customerModel.getFileName()).placeholder(getResources().getDrawable(R.drawable.avatar2))
                    .error(getResources().getDrawable(R.drawable.avatar2))
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(avatar);
    }

    public void onClickCreate(View view) {

        TextRole textRole = new TextRole();
        TextPhoneRole textPhoneRole = new TextPhoneRole();
        final String customerName = edtCustomerName.getEditableText().toString().trim();
        final String extPhone = edtPhone.getEditableText().toString().trim();
        final String companyName = edtCompanyName.getEditableText().toString().trim();
        final String address = edtAddress.getEditableText().toString().trim();
        final String note = edtNote.getEditableText().toString().trim();
        final String email = edtEmail.getEditableText().toString().trim();

        if (textRole.checkInputError(customerName) != null) {
            showErrorStatus(edtCustomerName, lblCustomerNameError, textRole.checkInputError(customerName));
            showKeyBoard(edtCustomerName);
            return;
        }
        if (textPhoneRole.checkInputError(extPhone) != null) {
            showErrorStatus(edtPhone, lblCustomerPhoneNumberError, textPhoneRole.checkInputError(extPhone));
            showKeyBoard(edtPhone);
            return;
        }
//        if (textRole.checkInputError(companyName) != null) {
//            showErrorStatus(edtCompanyName, lblCompanyError, textRole.checkInputError(companyName));
//            showKeyBoard(edtCompanyName);
//            return;
//        }
//        if (textRole.checkInputError(email) != null) {
//            showErrorStatus(edtEmail, lblEmailError, textRole.checkInputError(email));
//            showKeyBoard(edtEmail);
//            return;
//        }

        if(bitmap != null) {

            showLoading();
            UploadAvatarRequest uploadAvatarRequest = new UploadAvatarRequest();
            uploadAvatarRequest.setBitmap(bitmap);

            ApiController.doPostMultipart(this, uploadAvatarRequest, new ResponseListener() {
                @Override
                public void processResponse(String response) {
                    Log.e("Response: ", response);
                    closeLoading();
                    if (response != null) {
                        Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                        UploadFileResponse signInResponse = gson.fromJson(response, UploadFileResponse.class);

                        if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                            if (mode == MODE_CREATE) {
                                CreateContactRequest createCustomerRequest = new CreateContactRequest();

                                ContactObject contactObject = new ContactObject(customerName, PhoneUtil.makeCorrectPhoneFormat(extPhone),
                                        companyName, email, address, note, signInResponse.getFilename());
                                createCustomerRequest.setData(contactObject);

//                                createCustomerRequest.setData(sharePreference.getAccessToken(), "" + sharePreference.getNumOfCompany(), customerName, extPhone
//                                        , companyName, email, address, note, signInResponse.getFilename());
                                doProcess(createCustomerRequest);
                            } else {
                                EditContactRequest editCustomerRequest = new EditContactRequest();
                                ContactObject contactObject = new ContactObject(customerName, PhoneUtil.makeCorrectPhoneFormat(extPhone),
                                        companyName, email, address, note, signInResponse.getFilename(), contactId);
                                editCustomerRequest.setData(contactObject);
//                                editCustomerRequest.setData(sharePreference.getAccessToken(), "" + sharePreference.getNumOfCompany(), customerName, extPhone
//                                        , companyName, email, address, note, signInResponse.getFilename(), "" + contactId);
                                doProcess(editCustomerRequest);
                            }
                        } else {
                            showError(signInResponse, true);
                        }
                    } else {
                        Toast.makeText(CreateContactActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void processResponse(int error, String content) {
                    showError(getResources().getString(R.string.text_no_network), true);
                }
            });

            return;
        } else {
            if (mode == MODE_CREATE) {
                CreateContactRequest createCustomerRequest = new CreateContactRequest();

                ContactObject contactObject = new ContactObject(customerName, PhoneUtil.makeCorrectPhoneFormat(extPhone),
                        companyName, email, address, note, null);
                createCustomerRequest.setData(contactObject);

//                createCustomerRequest.setData(sharePreference.getAccessToken(), "" + sharePreference.getNumOfCompany(), customerName, extPhone
//                        , companyName, email, address, note, null);
                doProcess(createCustomerRequest);
            } else {
                EditContactRequest editCustomerRequest = new EditContactRequest();

                ContactObject contactObject = new ContactObject(customerName, PhoneUtil.makeCorrectPhoneFormat(extPhone),
                        companyName, email, address, note, null, contactId);
                editCustomerRequest.setData(contactObject);

//                editCustomerRequest.setData(sharePreference.getAccessToken(), "" + sharePreference.getNumOfCompany(), customerName, extPhone
//                        , companyName, email, address, note, null, "" + contactId);
                doProcess(editCustomerRequest);
            }
        }



    }

    public void doProcess(BasePostRequestEntity basePostRequestEntity) {
        showLoading();

        ApiController.doPostRequest(this, basePostRequestEntity, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    String message = signInResponse.getMessage() != null && !signInResponse.getMessage().isEmpty()
                            ? signInResponse.getMessage() : null;
                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        if (mode == MODE_CREATE) {
                            Toast.makeText(CreateContactActivity.this, getResources().getString(R.string.alertCreateContactSuccess), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateContactActivity.this, getResources().getString(R.string.alertEditContactSuccess), Toast.LENGTH_SHORT).show();
                        }
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateContactActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    @Override
    public void onKeyBack() {
        setResult(Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }
}
