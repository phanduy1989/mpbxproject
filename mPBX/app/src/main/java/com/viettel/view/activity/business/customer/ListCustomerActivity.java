package com.viettel.view.activity.business.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.ListCustomerAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.CustomerModel;
import com.viettel.model.request.customer.GetListCustomerRequest;
import com.viettel.model.response.customer.GetListCustomerResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.AnimationUtil;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/22/16.
 */
public class ListCustomerActivity extends BABaseActivity {


    TextView txtTitleHeader;

    private ListView mListView;
    private ListCustomerAdapter mAdapter;

    RelativeLayout layoutCall;
    ImageView icon_right;

    CustomerModel pickContact;
    private SwipeRefreshLayout swipeContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_customer_activity);

        initView();

        initData();
    }

    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        icon_right = (ImageView) findViewById(R.id.icon_right);
        mListView = (ListView) findViewById(R.id.listView);
        txtTitleHeader.setText(getResources().getString(R.string.labelCustomer));
        icon_right.setImageResource(R.drawable.new_people_icon);

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch =  findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        layoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCallLayout();
            }
        });

        mAdapter = new ListCustomerAdapter(this);
        mListView.setAdapter(mAdapter);
        mAdapter.setMode(Attributes.Mode.Single);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ((SwipeLayout)(mListView.getChildAt(position - mListView.getFirstVisiblePosition()))).open(true);
//                Toast.makeText(ListCustomerActivity.this, "OnItemClickListener", Toast.LENGTH_SHORT).show();
            }
        });
        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                Log.e("ListView", "OnTouch");
                return false;
            }
        });
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(ListCustomerActivity.this, "OnItemLongClickListener", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                Log.e("ListView", "onScrollStateChanged");
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

//        mListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.e("ListView", "onItemSelected:" + position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Log.e("ListView", "onNothingSelected:");
//            }
//        });

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        if (swipeContainer != null) {
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    initData();
                }
            });
            // Configure the refreshing colors
            swipeContainer.setColorScheme(R.color.main_green, R.color.main_green
                    , R.color.main_green, R.color.main_green);
        }

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();
                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }
                mAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void initData() {
        showLoading();
        GetListCustomerRequest getListCustomerRequest = new GetListCustomerRequest();
        getListCustomerRequest.setData();

        ApiController.doPostRequest(this, getListCustomerRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                closeLoading();

                if(response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetListCustomerResponse signInResponse = gson.fromJson(response, GetListCustomerResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(signInResponse.getListCustomer());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }
    public void initData(final int initPosition) {
        showLoading();
        GetListCustomerRequest getListCustomerRequest = new GetListCustomerRequest();
        getListCustomerRequest.setData();

        ApiController.doPostRequest(this, getListCustomerRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                closeLoading();

                if(response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetListCustomerResponse signInResponse = gson.fromJson(response, GetListCustomerResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(signInResponse.getListCustomer(), initPosition);
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void showLoading() {
        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
    }

    public void closeLoading() {
        swipeContainer.setRefreshing(false);
    }


    public void showError(String message, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        super.showError(message, isError);
    }

    public void showError(ResponseObject responseObject, boolean isError) {
        closeLoading();
        swipeContainer.setVisibility(View.GONE);
        if(mAdapter == null || mAdapter.getCount() == 0) {
            super.showError(responseObject, isError);
        }
    }

    public void setListData(ArrayList<CustomerModel> listData) {
        if(listData == null || listData.isEmpty()) {
            if(mAdapter == null || mAdapter.getCount() == 0) {
                showError(getResources().getString(R.string.alertNoCustomerFound), false);
            }

            return;
        }

        GlobalValue.listCustomer = listData;

        mAdapter = new ListCustomerAdapter(this);
        mListView.setAdapter(mAdapter);
        mAdapter.setListData(listData);

        showContent();
    }
    public void setListData(ArrayList<CustomerModel> listData, int initPositon) {
        if(listData == null || listData.isEmpty()) {
            if(mAdapter == null || mAdapter.getCount() == 0) {
                showError(getResources().getString(R.string.alertNoCustomerFound), false);
            }

            return;
        }

        GlobalValue.listCustomer = listData;

        mAdapter = new ListCustomerAdapter(this);
        mListView.setAdapter(mAdapter);
        mAdapter.setListData(listData);

        mListView.setSelection(initPositon - 3);

        showContent();
    }



    @Override
    protected void onResume() {
        super.onResume();
//        initData();
    }

    public void onClickRefresh(View v) {
        initData();
    }

    public void onClickHeaderRight(View v) {
        goToCreateCustomerAcitivity(null);
    }

    public void goToCreateCustomerAcitivity(CustomerModel customerModel) {
        Intent intent = new Intent(ListCustomerActivity.this.getBaseContext(), CreateCustomerActivity.class);
        if(customerModel != null) {
            intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, customerModel.getCustomerId());
        }
        startActivityForResult(intent, REQUEST_CODE_DETAIAIL);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void goToDetail(int customerId) {
        Intent intent = new Intent(ListCustomerActivity.this.getBaseContext(), CustomerInfoActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_CUSTOMER_ID, customerId);
        startActivityForResult(intent, REQUEST_CODE_DETAIAIL);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private static final int REQUEST_CODE_DETAIAIL = 6996;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_DETAIAIL) {
            initData();
        }
    }

    public void showCallLayout(CustomerModel customerModel) {
        pickContact = customerModel;
        layoutCall.setVisibility(View.VISIBLE);
    }

    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }
    public void onCallPrivate(View view) {
        callPrivatePhone(pickContact.getPhoneNumber());
    }

    public void onCallHost(View view) {
        callHostPhone(pickContact.getPhoneNumber());
    }

    public void showContent() {
        super.showContent();
        swipeContainer.setVisibility(View.VISIBLE);
    }
}
