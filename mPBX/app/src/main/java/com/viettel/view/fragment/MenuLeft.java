package com.viettel.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.request.SetCallbackRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.AppSharePreference;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.MainHomeActivity;

public class MenuLeft extends Fragment {


	private MainHomeActivity mainHomeActivity;
	TextView accountName;
	LinearLayout layoutToggle;

	private boolean isOnCallback;
	AppSharePreference appSharePreference;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_behind, null);
		v.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});

		mainHomeActivity = (MainHomeActivity) getActivity();

		accountName = (TextView) v.findViewById(R.id.accountName);
		layoutToggle = (LinearLayout) v.findViewById(R.id.layoutToggle);

		appSharePreference = com.viettel.store.AppSharePreference.getInstance(mainHomeActivity);
		String fullName = appSharePreference.getFullName();

		String account = "" + (StringUtility.isEmpty(fullName) ? appSharePreference.getUserName() : fullName);
		accountName.setText(account);

		String callback = appSharePreference.getCallback();

		isOnCallback = callback != null && callback.equals("1");

		layoutToggle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String message = isOnCallback ? "Bạn muốn tắt callback ?" : "Bạn muốn bật callback?";

				DialogUtility.showDialogConfirm(mainHomeActivity, message, new ConfirmListener() {
					@Override
					public void doAccept() {
						isOnCallback = !isOnCallback;
						doChangeCallbackRequest(isOnCallback);
					}

					@Override
					public void doCancel() {

					}
				});


			}
		});

		onChangeCallBack(isOnCallback);

		return v;
	}

	public void doChangeCallbackRequest(final boolean callback) {

		mainHomeActivity.showLoading();

		int callbackParam = isOnCallback ? 1 : 0;

		SetCallbackRequest setCallbackRequest = new SetCallbackRequest();
		setCallbackRequest.setData(callbackParam);

		ApiController.doPostRequest(mainHomeActivity, setCallbackRequest, new ResponseListener() {
			@Override
			public void processResponse(String response) {
				mainHomeActivity.closeLoading();

				if(response != null) {
					Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
					ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

					String message = signInResponse.getMessage() != null && !signInResponse.getMessage().isEmpty()
							? signInResponse.getMessage() : null;
					if (signInResponse.getIsSuccess() == 1) {
						onChangeCallBack(callback);
					} else {
						isOnCallback = !callback;
						Toast.makeText(mainHomeActivity,message != null ? message : getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
					}
				} else {
					isOnCallback = !callback;
					Toast.makeText(mainHomeActivity, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void processResponse(int error, String content) {
				mainHomeActivity.closeLoading();
				isOnCallback = !callback;
				Toast.makeText(mainHomeActivity, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
				mainHomeActivity.closeProgressDialog();
			}
		});
	}

	public void onChangeCallBack(boolean isOnCallback) {

		TextView textViewOn = (TextView) layoutToggle.findViewById(R.id.txtCallbackOn);
		TextView textViewOff = (TextView) layoutToggle.findViewById(R.id.txtCallbackOff);

		if(isOnCallback) {
			textViewOn.setTextColor(mainHomeActivity.getResources().getColor(R.color.white));
			textViewOn.setBackgroundResource(R.drawable.drawable_border_green);

			textViewOff.setTextColor(mainHomeActivity.getResources().getColor(R.color.gray));
			textViewOff.setBackgroundResource(R.drawable.drawable_border_white);
		} else {
			textViewOn.setTextColor(mainHomeActivity.getResources().getColor(R.color.gray));
			textViewOn.setBackgroundResource(R.drawable.drawable_border_white);

			textViewOff.setTextColor(mainHomeActivity.getResources().getColor(R.color.white));
			textViewOff.setBackgroundResource(R.drawable.drawable_border_green);
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
}
