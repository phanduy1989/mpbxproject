package com.viettel.view.activity.business.contact;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ContactObject;
import com.viettel.model.request.contact.GetContactInfoRequest;
import com.viettel.model.response.contact.GetContactInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.ImagePicker;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class ContactInfoActivity extends BABaseActivity {

    TextView textName, textPhoneNumber, txtCompanyName, txtEmail, txtAddress, txtNote;
    TextView txtTitleHeader;

    LinearLayout layoutPhoneNumber, layoutPhoneNumber2, layoutPhoneNumber3;

    RelativeLayout layoutCall;
    ImageView avatar;

    private int contactId;
    private String phoneNumber;
    ContactObject customerModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contact_info);

        contactId = getIntent().getIntExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, -1);

        if (contactId <= 0) {
            finish();
            return;
        }

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initView() {
        textName = (TextView) findViewById(R.id.textName);
        textPhoneNumber = (TextView) findViewById(R.id.textPhoneNumber);
        txtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtNote = (TextView) findViewById(R.id.txtNote);
        layoutPhoneNumber = (LinearLayout) findViewById(R.id.layoutPhoneNumber);
        layoutPhoneNumber2 = (LinearLayout) findViewById(R.id.layoutPhoneNumber2);
        layoutPhoneNumber3 = (LinearLayout) findViewById(R.id.layoutPhoneNumber3);
        layoutCall = (RelativeLayout) findViewById(R.id.layoutCall);
        avatar = (ImageView) findViewById(R.id.avatar);
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);

        layoutPhoneNumber.setOnClickListener(onClickListener);
        layoutPhoneNumber2.setOnClickListener(onClickListener);
        layoutPhoneNumber3.setOnClickListener(onClickListener);


    }

    Bitmap bitmap = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CAMERA) {
            bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
            avatar.setImageBitmap(bitmap);
        }
    }

    public void initData() {
        showLoading();
        GetContactInfoRequest getCustomerInfoRequest = new GetContactInfoRequest();
        getCustomerInfoRequest.setData(contactId);

        ApiController.doPostRequest(this, getCustomerInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetContactInfoResponse signInResponse = gson.fromJson(response, GetContactInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchCustomerInfo(signInResponse.getContactObject());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(ContactInfoActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void fetchCustomerInfo(ContactObject customerModel) {

        if (customerModel == null) {
            showError(getResources().getString(R.string.no_data), false);
            return;
        }

        this.customerModel = customerModel;

        txtTitleHeader.setText(customerModel.getContactName() != null ? customerModel.getContactName() : "");
        textName.setText(customerModel.getContactName() != null ? customerModel.getContactName() : "");
        textPhoneNumber.setText(customerModel.getPhoneNumber() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()) : "");
        txtCompanyName.setText(customerModel.getCompanyName() != null ? customerModel.getCompanyName() : "");
        txtEmail.setText(customerModel.getEmail() != null ? customerModel.getEmail() : "");
        txtAddress.setText(customerModel.getAddress() != null ? customerModel.getAddress() : "");
        txtNote.setText(customerModel.getNote() != null ? customerModel.getNote() : "");

        if (customerModel.getFileName() != null)
            Picasso.with(this)
                    .load(customerModel.getFileName()).placeholder(getResources().getDrawable(R.drawable.avatar2))
                    .error(getResources().getDrawable(R.drawable.avatar2))
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(avatar);
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.layoutPhoneNumber:
                    showCallLayout(customerModel.getPhoneNumber());
                    break;
//                case R.id.layoutPhoneNumber2:
//                    showCallLayout(customerModel.getPhoneNumber2());
//                    break;
//                case R.id.layoutPhoneNumber3:
//                    showCallLayout(customerModel.getPhoneNumber3());
//                    break;
            }
        }
    };

    public void onClickCall(View view) {

        if(listPhoneNumbers.isEmpty()) {
            if (customerModel.getPhoneNumber() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()));
            }
        }

        View layoutCall = findViewById(R.id.layoutCall);
        layoutCall.setVisibility(View.VISIBLE);

        final ViewSwitcher viewSwitcherCall = (ViewSwitcher) layoutCall.findViewById(R.id.viewSwitcherCall);

        int size = listPhoneNumbers.size();

        switch (size) {
            case 0:
                Toast.makeText(this, "Không có thông tin liên lạc!", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                phoneNumber = listPhoneNumbers.get(0);
                if(viewSwitcherCall.getDisplayedChild() == 0) {
                    viewSwitcherCall.showNext();
                }
                break;
        }

    }


    public void hideCallLayout() {
        layoutCall.setVisibility(View.GONE);
    }

    public void onCallPrivate(View view) {
        callPrivatePhone(phoneNumber);
    }

    public void onCallHost(View view) {
        callHostPhone(phoneNumber);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void onClickHeaderRight(View v) {
        Intent intent = new Intent(ContactInfoActivity.this.getBaseContext(), CreateContactActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, contactId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
