package com.viettel.view.activity;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.mpbx.R;

import com.viettel.store.AppSharePreference;
import com.viettel.view.base.BABaseActivity;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by duyuno on 11/22/16.
 */
public class IntroduceActivity extends BABaseActivity {

    AppSharePreference appSharePreference;

    private static String[] CONTENT = new String[] {
            "Tải và cài đặt phần mềm iBaby trên Apple Store hoặc Google Play",
            "Tạo tài khoản dịch vụ",
            "Tạo thông tin bé trên ứng dụng",
            "Bật nhiệt kế và đeo nhiệt kế vào tay cho bé",
            "Bật bluetooth kết nối nhiệt kế",
            "Kết nối thành công, bắt đầu sử dụng để theo dõi nhiệt độ"
    };
    private static int[] resId = new int[] {
//            R.drawable.intro_1,
//            R.drawable.intro_2,
//            R.drawable.intro_3,
//            R.drawable.intro_4,
//            R.drawable.intro_5,
//            R.drawable.intro_6,
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appSharePreference = com.viettel.store.AppSharePreference.getInstance(this);
        setContentView(R.layout.layout_introduce);
        initView();
        initData();
    }

    public void initView() {
        ViewPager viewpager = (ViewPager) findViewById(R.id.pager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewpager.setAdapter(new SamplePagerAdapter());
        indicator.setViewPager(viewpager);
    }
    public void initData() {

    }

    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        finish();
    }

    public class SamplePagerAdapter extends PagerAdapter {

        public SamplePagerAdapter() {
        }


        @Override
        public int getCount() {
            return resId.length;
        }

        @Override public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override public void destroyItem(ViewGroup view, int position, Object object) {
            view.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {

            RelativeLayout relativeLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_introduce, null);

            TextView textView = (TextView) relativeLayout.findViewById(R.id.description);
            ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.image);

            imageView.setImageResource(resId[position]);
            textView.setText(CONTENT[position]);

            view.addView(relativeLayout, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);


            return relativeLayout;
        }
    }
}
