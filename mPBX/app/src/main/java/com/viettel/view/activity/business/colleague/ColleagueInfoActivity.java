package com.viettel.view.activity.business.colleague;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.PhonesAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ColleagueModel;
import com.viettel.model.request.colleague.GetColleagueInfoRequest;
import com.viettel.model.response.colleague.GetColleagueInfoResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.PhoneUtil;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class ColleagueInfoActivity extends BABaseActivity {

    TextView textName, textPhoneExt, txtCompanyName, txtPosition
            , txtMainPhone, txtSubPhone;

    private int colleagueId, groupId;
    TextView txtTitleHeader;
    PhonesAdapter phonesAdapter;

    private String phoneCallNumber;

    private ColleagueModel colleagueModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_colleague_info);

        colleagueId = getIntent().getIntExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_ID, -1);
        groupId = getIntent().getIntExtra(GlobalInfo.BUNDLE_KEY_GROUP_ID, -1);

        if(colleagueId <= 0 || groupId <= 0) {
            finish();
            return;
        }

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initView() {
        textName = (TextView) findViewById(R.id.textName);
        textPhoneExt = (TextView) findViewById(R.id.textPhoneExt);
        txtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        txtMainPhone = (TextView) findViewById(R.id.txtMainPhone);
        txtSubPhone = (TextView) findViewById(R.id.txtSubPhone);

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
    }

    public void initData() {
        showLoading();
        GetColleagueInfoRequest getCustomerInfoRequest = new GetColleagueInfoRequest();
        getCustomerInfoRequest.setData(groupId, colleagueId);

        ApiController.doPostRequest(this, getCustomerInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueInfoResponse signInResponse = gson.fromJson(response, GetColleagueInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchColleagueInfo(signInResponse.getMemberInfo());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(ColleagueInfoActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.alertConnectFail), true);
            }
        });
    }

    public void fetchColleagueInfo(ColleagueModel customerModel) {

        if (customerModel == null) {
            showError(getResources().getString(R.string.no_data), false);
            return;
        }

        colleagueModel = customerModel;

        txtTitleHeader.setText(customerModel.getColleagueName() != null ? customerModel.getColleagueName() : "N/A");
        textName.setText(customerModel.getColleagueName() != null ? customerModel.getColleagueName() : "N/A");
        textPhoneExt.setText(customerModel.getNumberOfExt() != null ? customerModel.getNumberOfExt() : "N/A");
        txtCompanyName.setText(customerModel.getDepartmentName() != null ? customerModel.getDepartmentName() : "N/A");
        txtPosition.setText(customerModel.getPosition() != null ? customerModel.getPosition() : "N/A");
        txtMainPhone.setText("SĐT: " + (customerModel.getPhoneNumber() != null ? customerModel.getPhoneNumber() : "N/A"));
        txtSubPhone.setText(customerModel.getNumberOfCompany() != null ? customerModel.getNumberOfCompany() : "N/A");
    }

    public void onClickCall(View view) {

        if(listPhoneNumbers.isEmpty()) {
            if (colleagueModel.getPhoneNumber() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(colleagueModel.getPhoneNumber()));
            }

            if (colleagueModel.getNumberOfCompany() != null) {
                listPhoneNumbers.add(PhoneUtil.makeCorrectPhoneFormat(colleagueModel.getNumberOfCompany()));
            }
        }

        View layoutCall = findViewById(R.id.layoutCall);
        layoutCall.setVisibility(View.VISIBLE);

        final ViewSwitcher viewSwitcherCall = (ViewSwitcher) layoutCall.findViewById(R.id.viewSwitcherCall);

        int size = listPhoneNumbers.size();

        switch (size) {
            case 0:
                Toast.makeText(this, "Không có thông tin liên lạc!", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                phoneCallNumber = listPhoneNumbers.get(0);
                viewSwitcherCall.showNext();
                break;
            default:
                ListView listView = (ListView) layoutCall.findViewById(R.id.listPhoneNumber);
                listView.setAdapter(phonesAdapter = new PhonesAdapter(this, listPhoneNumbers));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        phoneCallNumber = phonesAdapter.getItem(position);
                        viewSwitcherCall.setInAnimation(slide_in_left);
                        viewSwitcherCall.setOutAnimation(slide_out_left);
                        viewSwitcherCall.showNext();
                    }
                });
                break;
        }

    }



    public void onCallPrivate(View view) {
        callPrivatePhone(phoneCallNumber);
    }

    public void onCallHost(View view) {
        callHostPhone(phoneCallNumber);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void onClickHeaderRight(View v) {
//        Intent intent = new Intent(ColleagueInfoActivity.this.getBaseContext(), CreateColleagueGroupActivity.class);
//        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, colleagueId);
//        startActivity(intent);
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
