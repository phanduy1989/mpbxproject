package com.viettel.view.activity.business.conversation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.ConversationInviteMemberAdapter;
import com.viettel.adapter.ConversationMemberAdapter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ConversationGroup;
import com.viettel.model.ConversationMember;
import com.viettel.model.ConversationModel;
import com.viettel.model.request.conference.GetConversationInfoRequest;
import com.viettel.model.request.conference.GetConversationMemberLeftRequest;
import com.viettel.model.request.conference.UpdateConversationRequest;
import com.viettel.model.response.conference.GetConferenceInfoResponse;
import com.viettel.model.response.conference.GetConferenceMemberLeftResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.DateUtil;
import com.viettel.utils.DialogUtility;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by duyuno on 7/13/17.
 */
public class ConversationInfoActivity extends BABaseActivity {

    ConversationModel conversationModel;

    TextView txtTitleHeader;
    TextView txtStartTime, txtEndTime;

    RelativeLayout buttonHeaderRight;
    ImageView icon_right;
    ViewSwitcher viewSwitcher;
    LinearLayout layoutBtn;

    LinearLayout layoutStartTime, layoutEndTime, layoutTime;

    ListView listView;
    ListView listViewInviteMembers;

    ArrayList<ConversationMember> listMember;

    ConversationMemberAdapter conversationMemberAdapter;
    ConversationInviteMemberAdapter conversationInviteMemberAdapter;

    Animation slide_in_right, slide_out_right, slide_in_left, slide_out_left;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_info_activity);

        slide_in_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        slide_out_right = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        slide_in_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        slide_out_left = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);
        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        layoutStartTime = (LinearLayout) findViewById(R.id.layoutStartTime);
        layoutEndTime = (LinearLayout) findViewById(R.id.layoutEndTime);
        layoutTime = (LinearLayout) findViewById(R.id.layoutTime);
        listView = (ListView) findViewById(R.id.listView);
        listViewInviteMembers = (ListView) findViewById(R.id.listViewInviteMembers);

        icon_right = (ImageView) findViewById(R.id.icon_right);
        buttonHeaderRight = (RelativeLayout) findViewById(R.id.buttonHeaderRight);
        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        layoutBtn = (LinearLayout) findViewById(R.id.layoutBtn);

        txtTitleHeader.setSelected(true);

        conversationModel = getIntent().getParcelableExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION);

        txtTitleHeader.setText(conversationModel.getConferenceName());
        txtStartTime.setText("Thời gian bắt đầu: " + DateUtil.convertFullDate(conversationModel.getStartDate()));
        switch (conversationModel.getConversationType()) {
            case ConversationGroup.FINISHED:
                txtEndTime.setText("Thời gian kết thúc: " + DateUtil.convertFullDate(conversationModel.getEndDate()));
                buttonHeaderRight.setVisibility(View.GONE);
                layoutBtn.setVisibility(View.GONE);
                break;
            case ConversationGroup.UP_COMMING:
                layoutBtn.setVisibility(View.GONE);
                layoutEndTime.setVisibility(View.GONE);
                break;
            case ConversationGroup.ON_GOING:
                layoutStartTime.setVisibility(View.GONE);
                icon_right.setImageResource(R.drawable.confer_add_member);
//                layoutEndTime.setVisibility(View.GONE);
//                layoutTime.setVisibility(View.VISIBLE);
//                buttonHeaderRight.setVisibility(View.GONE);
//                setTime(conversationModel.getStartDate());
                txtEndTime.setText("Thời gian kết thúc: " + DateUtil.convertFullDate(conversationModel.getEndDate()));
                break;
        }

        long offset = (conversationModel.getStartDate().getTime() - System.currentTimeMillis()) / 1000;

        conversationMemberAdapter = new ConversationMemberAdapter(this, conversationModel.getConversationType(), conversationModel.getConferenceId());
        conversationMemberAdapter.setUpcomingTime(offset < GlobalValue.minConfig * 60);

        conversationInviteMemberAdapter = new ConversationInviteMemberAdapter(this, conversationModel.getConversationType(), conversationModel.getConferenceId());
        listView.setAdapter(conversationMemberAdapter);
        listViewInviteMembers.setAdapter(conversationInviteMemberAdapter);

        listViewInviteMembers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ConversationMember conversationMember = conversationInviteMemberAdapter.getItem(position);
                conversationMember.changeChosed();
                conversationInviteMemberAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void remarkEditted() {
        isEditted = true;
    }

    public void onClickHeaderRight(View v) {
        if(conversationModel.getConversationType() == ConversationGroup.UP_COMMING) {
            long offset = (conversationModel.getStartDate().getTime() - System.currentTimeMillis()) / 1000;
            if (offset < GlobalValue.minConfig * 60) {
                buttonHeaderRight.setEnabled(false);
                DialogUtility.showDialogAlert(this, null, "Hội nghị chỉ còn <b>" + offset / 60 + " phút</b> là diễn ra. Không thể sửa thông tin hội nghị!", "Đóng", null);
                return;
            }
        }

        goToCreateConference();
    }

    public void initData() {
        showLoading();

        GetConversationInfoRequest getConversationInfoRequest = new GetConversationInfoRequest();
        getConversationInfoRequest.setData(conversationModel.getConferenceId());

        ApiController.doPostRequest(this, getConversationInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {


                closeLoading();

                if(response != null) {
                    Log.e("Response", "" + response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceInfoResponse signInResponse = gson.fromJson(response, GetConferenceInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchData(signInResponse);
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fetchData(GetConferenceInfoResponse signInResponse) {

        conversationModel.setConferenceName(signInResponse.getConferenceName());
        conversationModel.setStartDate(signInResponse.getStart());
        conversationModel.setEndDate(signInResponse.getEnd());

        txtTitleHeader.setText(conversationModel.getConferenceName());
        txtStartTime.setText("Thời gian bắt đầu: " + DateUtil.convertFullDate(conversationModel.getStartDate()));
        switch (conversationModel.getConversationType()) {
            case ConversationGroup.FINISHED:
                txtEndTime.setText("Thời gian kết thúc: " + DateUtil.convertFullDate(conversationModel.getEndDate()));
                buttonHeaderRight.setVisibility(View.GONE);
                layoutBtn.setVisibility(View.GONE);
                break;
            case ConversationGroup.UP_COMMING:
                layoutBtn.setVisibility(View.GONE);
                layoutEndTime.setVisibility(View.GONE);
                break;
            case ConversationGroup.ON_GOING:
                layoutStartTime.setVisibility(View.GONE);
                icon_right.setImageResource(R.drawable.confer_add_member);
//                layoutEndTime.setVisibility(View.GONE);
//                layoutTime.setVisibility(View.VISIBLE);
//                buttonHeaderRight.setVisibility(View.GONE);
//                setTime(conversationModel.getStartDate());
                txtEndTime.setText("Thời gian kết thúc: " + DateUtil.convertFullDate(conversationModel.getEndDate()));
                break;
        }

        setListData(signInResponse.getListMember());
        initListNonmember();
    }

    public void setListData(ArrayList<ConversationMember> list) {
        listMember = list;
        conversationMemberAdapter.setData(listMember);
    }

    public void goToCreateConference() {
        Intent intent = new Intent(ConversationInfoActivity.this.getBaseContext(), CreateConversationActivity.class);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_MODE, CreateConversationActivity.MODE_UPDATE);
        intent.putExtra(GlobalInfo.BUNDLE_KEY_CONVERSATION, conversationModel);
        ArrayList<ConversationMember> listMember = conversationMemberAdapter.getListData();
        if(listMember != null) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_MEMBER, listMember);
        }

        ArrayList<ConversationMember> listNonMember = conversationInviteMemberAdapter.getListData();
        if(listNonMember != null) {
            intent.putParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_NON_MEMBER, listNonMember);
        }

        startActivityForResult(intent,REQUEST_CODE_CREATE);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void setTime(Date date) {
        TextView hour1 = (TextView) layoutTime.findViewById(R.id.hour1);
        TextView hour2 = (TextView) layoutTime.findViewById(R.id.hour2);
        TextView minute1 = (TextView) layoutTime.findViewById(R.id.minute1);
        TextView minute2 = (TextView) layoutTime.findViewById(R.id.minute2);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        hour1.setText("" + (hour / 10));
        hour2.setText("" + (hour % 10));
        minute1.setText("" + (minute / 10));
        minute2.setText("" + (minute % 10));
    }

    boolean isEditted;

    @Override
    public void onKeyBack() {
        setResult(isEditted ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }

    public void onClickBtnInvite(View view) {

        showLoading();
        GetConversationMemberLeftRequest getConversationMemberLeftRequest = new GetConversationMemberLeftRequest();
        getConversationMemberLeftRequest.setData(conversationModel.getConferenceId());


        ApiController.doPostRequest(this, getConversationMemberLeftRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                closeLoading();

                if(response != null) {
                    Log.e("Response", "" + response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceMemberLeftResponse signInResponse = gson.fromJson(response, GetConferenceMemberLeftResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        if(signInResponse.getListMemberLeft() != null && signInResponse.getListMemberLeft().size() > 0) {
                            for(ConversationMember conversationMember : signInResponse.getListMemberLeft()) {
                                conversationMember.setStatus(ConversationMember.STATUS_LEFT);
                            }
                            conversationInviteMemberAdapter.setData(signInResponse.getListMemberLeft());
                            viewSwitcher.setInAnimation(slide_in_left);
                            viewSwitcher.setOutAnimation(slide_out_left);
                            viewSwitcher.showNext();
                        } else {
                            Toast.makeText(ConversationInfoActivity.this, "Không có thành viên để mời!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });

//        conversationInviteMemberAdapter.clearData();



//        initListNonmember();
    }

    public void initListNonmember() {
        showLoading();
        GetConversationMemberLeftRequest getConversationMemberLeftRequest = new GetConversationMemberLeftRequest();
        getConversationMemberLeftRequest.setData(conversationModel.getConferenceId());


        ApiController.doPostRequest(this, getConversationMemberLeftRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {


                closeLoading();

                if(response != null) {
                    Log.e("Response", "" + response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetConferenceMemberLeftResponse signInResponse = gson.fromJson(response, GetConferenceMemberLeftResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        if(signInResponse.getListMemberLeft() != null) {
                            for(ConversationMember conversationMember : signInResponse.getListMemberLeft()) {
                                conversationMember.setStatus(ConversationMember.STATUS_LEFT);
                            }
                        }
                        conversationInviteMemberAdapter.setData(signInResponse.getListMemberLeft());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClickBtnConfirmInvite(View view) {

        ArrayList<ConversationMember> listNonMember = conversationInviteMemberAdapter.getListPicked();
        if(listNonMember == null) {
            DialogUtility.showDialogAlert(this, null, "Bạn chưa chọn thành viên nào!", "Đóng", null);
            return;
        }

        showLoading();

        UpdateConversationRequest updateConversationRequest = new UpdateConversationRequest();
        updateConversationRequest.setData(listMember, listNonMember, conversationModel.getConferenceId(), conversationModel.getConferenceName()
                , DateUtil.convertFullDate(conversationModel.getStartDate()), DateUtil.convertFullDate(conversationModel.getEndDate()));

        ApiController.doPostRequest(this, updateConversationRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(ConversationInfoActivity.this, getResources().getString(R.string.alertAddMemberSuccess), Toast.LENGTH_SHORT).show();
                        viewSwitcher.setInAnimation(slide_in_right);
                        viewSwitcher.setOutAnimation(slide_out_right);
                        viewSwitcher.showPrevious();
                        isEditted = true;
                        initData();

                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(ConversationInfoActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == REQUEST_CODE_CREATE) {
                isEditted = true;
                initData();
            }
        }
    }

    public void onClickBtnRefresh(View view) {
        initData();
    }
}
