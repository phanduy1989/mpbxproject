package com.viettel.view.activity.business.colleague;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.adapter.MPBXFilter;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.interfaces.ShowHideListener;
import com.viettel.model.ColleagueGroup;
import com.viettel.model.ColleagueModel;
import com.viettel.model.request.colleague.AddColleagueToGroupRequest;
import com.viettel.model.request.colleague.GetColleagueGroupCB2Request;
import com.viettel.model.response.colleague.GetColleagueGroupResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.mpbx.R;
import com.viettel.store.GlobalValue;
import com.viettel.utils.AnimationUtil;
import com.viettel.utils.StringUtility;
import com.viettel.view.activity.business.customer.CustomerInfoActivity;
import com.viettel.view.base.BABaseActivity;

import java.util.ArrayList;

/**
 * Created by duyuno on 11/22/16.
 */
public class ChoseAvaiColleagueGroupActivity extends BABaseActivity {


    TextView txtTitleHeader;

    private ListView listView;
    private GroupAdaper groupAdaper;

//    int agentId;

    ColleagueModel colleagueModel;

    ArrayList<ColleagueGroup> listGroup;

    boolean isChanged;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        colleagueModel = intent.getParcelableExtra(GlobalInfo.BUNDLE_KEY_COLLEAGUE_OBJ);
        listGroup = intent.getParcelableArrayListExtra(GlobalInfo.BUNDLE_KEY_LIST_GROUP);

        setContentView(R.layout.layout_avail_colleague_group_activity);

        initView();
    }


    public void initView() {
        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        listView = (ListView) findViewById(R.id.listView);
        txtTitleHeader.setText(getResources().getString(R.string.labelColleagueChoseGroup));

        layoutSearchFake = findViewById(R.id.layoutSearchFake);
        layoutSearch = findViewById(R.id.layoutSearch);
        editTextSearch = (EditText) findViewById(R.id.edtSearch);

        layoutSearchFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchBox();
            }
        });

        GlobalValue.changeCurrentGroup(GlobalValue.currentCollectGroup);

        groupAdaper = new GroupAdaper(this);
        listView.setAdapter(groupAdaper);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                groupAdaper.setSelected(position);

            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editTextSearch.getText().toString();

                if(text.length() > 0) {
                    findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.btnClear).setVisibility(View.GONE);
                }

                groupAdaper.filter(text);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void onClickSave(View view) {
        ColleagueGroup colleagueGroup = groupAdaper.getSelectedGroup();
        if(colleagueGroup == null) {
            finish();
            return;
        }

        showLoading();

        AddColleagueToGroupRequest addColleagueToGroupRequest = new AddColleagueToGroupRequest();
        addColleagueToGroupRequest.setData(colleagueModel, "" + colleagueGroup.getGroupId());

        ApiController.doPostRequest(this, addColleagueToGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();
                if (response != null) {
                    Log.e("Response", response);
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        Toast.makeText(ChoseAvaiColleagueGroupActivity.this, getResources().getString(R.string.alertAddColleageToGroupSuccess), Toast.LENGTH_SHORT).show();
                        isChanged = true;
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(ChoseAvaiColleagueGroupActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void onClickRefresh(View v) {
        initData();
    }

    public void initData() {

        if(listGroup != null && !listGroup.isEmpty()) {
            setListData(listGroup);
            return;
        }

        showLoading();

        GetColleagueGroupCB2Request getColleagueGroupRequest = new GetColleagueGroupCB2Request();
        getColleagueGroupRequest.setData("" + colleagueModel.getAgentId());

        ApiController.doPostRequest(this, getColleagueGroupRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                closeLoading();

                if(response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetColleagueGroupResponse signInResponse = gson.fromJson(response, GetColleagueGroupResponse.class);


                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        setListData(signInResponse.getListGroup());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    showError(getResources().getString(R.string.text_no_network), true);
//                    Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                closeLoading();
                showError(getResources().getString(R.string.text_no_network), true);
//                Toast.makeText(ListColleagueActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onCreateGroup(View v) {
        startActivity(new Intent(ChoseAvaiColleagueGroupActivity.this.getBaseContext(), CreateColleagueGroupActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void setListData(ArrayList<ColleagueGroup> listGroup) {
        groupAdaper.setData(listGroup);
    }

    public void showSearchBox() {
        layoutSearchFake.setVisibility(View.GONE);
        AnimationUtil.setTranslateHorizontalAnimation(this, layoutSearch, true, R.anim.slide_in_left, new ShowHideListener() {
            @Override
            public void doAfter() {
                showKeyBoard(editTextSearch);
            }
        });
    }

    public void goToDetail() {
        startActivity(new Intent(ChoseAvaiColleagueGroupActivity.this.getBaseContext(), CustomerInfoActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    public void onClickBack(View v) {
        onKeyBack();
    }

    @Override
    public void onKeyBack() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    class ViewHolder {
        public TextView txtGroupName;
        public ImageView iconCheck;
        public ImageButton btnEdit, btnDelete;
    }

    class GroupAdaper extends BaseAdapter implements MPBXFilter{

        ChoseAvaiColleagueGroupActivity activity;

        private ArrayList<ColleagueGroup> listData;
        private ArrayList<ColleagueGroup> listDataDisplay;

        public GroupAdaper(ChoseAvaiColleagueGroupActivity activity) {
            this.activity = activity;
        }

        public void setData(ArrayList<ColleagueGroup> list) {
            if(listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }
            listData.addAll(list);

            if(listDataDisplay == null) {
                listDataDisplay = new ArrayList<>();
            } else {
                listDataDisplay.clear();
            }
            listDataDisplay.addAll(list);
            notifyDataSetChanged();
        }

        public void setSelected(int position) {
            for(int i = 0, size = listDataDisplay.size(); i < size; i++) {
                ColleagueGroup colleagueGroup = listDataDisplay.get(i);
                if(i == position) {
                    colleagueGroup.setSelected(true);
                } else {
                    colleagueGroup.setSelected(false);
                }
            }

            notifyDataSetChanged();
        }

        public ColleagueGroup getSelectedGroup() {
            for(ColleagueGroup colleagueGroup : listDataDisplay) {
                if(colleagueGroup.isSelected()) {
                    return colleagueGroup;
                }
            }

            return null;
        }


        @Override
        public int getCount() {
            return listDataDisplay != null ? listDataDisplay.size() : 0;
        }

        @Override
        public ColleagueGroup getItem(int position) {
            return listDataDisplay != null ? listDataDisplay.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.item_chose_group, null);
                viewHolder = new ViewHolder();
                viewHolder.txtGroupName = (TextView) convertView.findViewById(R.id.txtGroupName);
                viewHolder.iconCheck = (ImageView) convertView.findViewById(R.id.iconCheck);
                viewHolder.btnEdit = (ImageButton) convertView.findViewById(R.id.btnEdit);
                viewHolder.btnDelete = (ImageButton) convertView.findViewById(R.id.btnDelete);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final ColleagueGroup colleagueGroup = getItem(position);

            if (colleagueGroup != null) {
                viewHolder.txtGroupName.setText(colleagueGroup.getGroupName());
                viewHolder.iconCheck.setVisibility(View.VISIBLE);
                viewHolder.btnEdit.setVisibility(View.GONE);
                viewHolder.btnDelete.setVisibility(View.GONE);

                if(colleagueGroup.isSelected()) {
                    viewHolder.iconCheck.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.iconCheck.setVisibility(View.GONE);
                }

            }

            return convertView;
        }

        @Override
        public void filter(String key) {
            if(listData == null || listData.isEmpty()) return;

            ((BABaseActivity) activity).showContent();

            if(key == null || key.isEmpty()) {

                listDataDisplay = new ArrayList<>();
                listDataDisplay.addAll(listData);
                notifyDataSetChanged();
                return;
            }
            key = key.trim().toLowerCase();


            listDataDisplay = new ArrayList<>();

            for(ColleagueGroup colleagueGroup : listData) {
                String groupName = StringUtility.convertVietnameseToAscii(colleagueGroup.getGroupName());
                if(groupName != null && groupName.contains(key)) {
                    listDataDisplay.add(colleagueGroup);
                }
            }


            notifyDataSetChanged();
        }
    }



}
