package com.viettel.view.activity.authen;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.viettel.mpbx.R;
import com.viettel.view.base.BABaseActivity;

import java.io.File;



/**
 * Created by VanHa on 7/21/2015.
 */
public class ViewImageActivity extends BABaseActivity {

    TextView txtTitleHeader;
    private String img = "", tengieovien = "", tenlophoc = "", loaihoatdong = "";
    private ImageView imgNoiDung;
    Display display;
    Point size;
    int width;
    int height;
    private AQuery aq;
    Thread splashTread;

    private String downloadLocalPath;
    private File file;


    private Matrix mCurrentDisplayMatrix = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewimage);

        try {
            unitUi();

        } catch (NullPointerException e) {
            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
        }
    }

    private void unitUi() {
        try {

            aq = new AQuery(this);
            display = this.getWindowManager().getDefaultDisplay();
            size = new Point();
            display.getSize(size);
            width = size.x;
            height = size.y;
            imgNoiDung = (ImageView) findViewById(R.id.imgNoiDung);
            img = getIntent().getStringExtra("img");
            txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
            txtTitleHeader.setText(sharePreference.getChatName());

//            tengieovien = getIntent().getStringExtra("tengieovien");
//            tenlophoc = getIntent().getStringExtra("tenlophoc");
//            loaihoatdong = getIntent().getStringExtra("loaihoatdong");

//            lblTitleScreen.setText(GlobalValue.prefs.getStringValue(ChatSharedPreferences.PREF_SETTING_NAME));

//            btnControlRight.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.icondownload));
//            btnControlLeft.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.iconback));
                imgNoiDung.setVisibility(View.VISIBLE);
                binData();

        } catch (NumberFormatException e) {
            Log.e("", "TAG VIEW IMAGE: " + e.toString());
        }catch (NullPointerException e) {
            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
        }


    }

    private void binData() {
        aq.id(imgNoiDung).image( img,
                true, true, width, 0, null, 0, 0);
    }

//    @Override
//    protected void onButtonControlLeftClick() {
//        super.onButtonControlLeftClick();
//        finish();
//    }
//
//    @Override
//    protected void onButtonControlRightClick() {
//        super.onButtonControlRightClick();
//        try {
//            if (NetworkUtil.getConnectivityStatusString(ViewImageActivity.this).equals("NO")) {
//                Toast.makeText(ViewImageActivity.this, R.string.error_connect, Toast.LENGTH_LONG).show();
//            } else {
////                String urlDownLoad = "";
////                if ((!loaihoatdong.equals("4") || !loaihoatdong.equals("2")) &&  ChiTietHoatDong_notyActivity.arrImageInfos.size() <= 0 ) {
////                    urlDownLoad = img;
////
////                } else {
////                    for (int i = 0; i < ChiTietHoatDong_notyActivity.arrImageInfos.size(); i++) {
////                        if (i == pagerNewsImage.getCurrentScreen()) {
////                            urlDownLoad = ChiTietHoatDong_notyActivity.arrImageInfos.get(i).getPath();
////                        }
////                    }
////                }
//                downloadLocalPath = Environment
//                        .getExternalStorageDirectory()
//                        + File.separator + "enetviet/";
//
//                File f = new File(downloadLocalPath);
//                if (!f.exists()) {
//                    f.mkdirs();
//                }
//                String imageUrl =  img;
//                String image = imageUrl.substring(imageUrl
//                        .lastIndexOf("/") + 1);
//                file = new File(downloadLocalPath + image);
//                if (file.exists()) {
////                    progress.cancel();
////                    progress.dismiss();
//                    Toast.makeText(this,
//                            this.getResources().getString(R.string.txtnotept),
//                            Toast.LENGTH_LONG).show();
//                } else {
//                    file_download(imageUrl, image);
//                }
//
//            }
//        } catch (NullPointerException e) {
//            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
//        } catch (Exception ex) {
//            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
//        } catch (Error er) {
//            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
//        }
//
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
        } catch (NullPointerException e) {
            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
        } catch (NullPointerException e) {
            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {


        } catch (NullPointerException e) {
            Log.e("", "TAG VIEW IMAGE:  " + e.toString());
        } catch (Exception ex) {
            Log.e("", "TAG VIEW IMAGE:  " + ex.toString());
        } catch (Error er) {
            Log.e("", "TAG VIEW IMAGE:  " + er.toString());
        }
    }



    public void file_download(String uRl, String imageName) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/enetviet");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Chat")
                .setDescription(getResources().getString(R.string.error_connect))
                .setDestinationInExternalPublicDir("/chat", imageName);

        mgr.enqueue(request);
        Toast.makeText(this, this.getResources().getString(R.string.txtsucctaianh),
                Toast.LENGTH_LONG).show();
    }


}

