package com.viettel.view.fragment.business;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
	public abstract boolean onKeyBack();
	public abstract void onKeySearch();
	
	public static final int NEXT = 0;
	public static final int BACK = 1;
}
