package com.viettel.view.activity.business.customer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.CustomerModel;
import com.viettel.model.TextPhoneRole;
import com.viettel.model.TextPhoneRoleNullAble;
import com.viettel.model.TextRole;
import com.viettel.model.request.BasePostRequestEntity;
import com.viettel.model.request.customer.CreateCustomerRequest;
import com.viettel.model.request.customer.EditCustomerRequest;
import com.viettel.model.request.customer.GetCustomerInfoRequest;
import com.viettel.model.request.UploadAvatarRequest;
import com.viettel.model.response.customer.GetCustomerInfoResponse;
import com.viettel.model.response.ResponseObject;
import com.viettel.model.response.UploadFileResponse;
import com.viettel.mpbx.R;
import com.viettel.utils.ImagePicker;
import com.viettel.utils.PhoneUtil;
import com.viettel.utils.RoundedTransformation;
import com.viettel.utils.Utils;
import com.viettel.view.base.BABaseActivity;

/**
 * Created by duyuno on 7/13/17.
 */
public class CreateCustomerActivity extends BABaseActivity {

    public static final int MODE_CREATE = 0;
    public static final int MODE_EDIT = 1;

    private int mode;

    TextView txtTitleHeader;
    ImageView avatar;

    int customerId = -1;

    EditText edtCustomerName, edtCompanyName, edtPosition, edtPhonenumber, edtPhonenumber2, edtPhonenumber3;
    TextView lblCustomerNameError, lblCustomerPhoneNumberError, lblCustomerCompanyError
            , lblCustomerPositionError, lblCustomerPhonenumber2Error, lblCustomerPhonenumber3Error;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_customer_create_activity);

        Intent intent = getIntent();

        customerId = intent.getIntExtra(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER, -1);
//        if (bundle != null) {
//            customerModel = bundle.getParcelable(GlobalInfo.BUNDLE_KEY_MODE_CREATE_CUSTOMER);
//        }

        if (customerId > 0) {
            mode = MODE_EDIT;
        } else {
            mode = MODE_CREATE;
        }

        txtTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);
        if (mode == MODE_CREATE) {
            txtTitleHeader.setText(getResources().getString(R.string.labelCreateCustomer));
        } else {
            txtTitleHeader.setText(getResources().getString(R.string.labelEditCustomer));
        }

        edtCustomerName = (EditText) findViewById(R.id.edtCustomerName);
        edtCompanyName = (EditText) findViewById(R.id.edtCompanyName);
        edtPosition = (EditText) findViewById(R.id.edtPosition);
        edtPhonenumber = (EditText) findViewById(R.id.edtPhonenumber);
        edtPhonenumber2 = (EditText) findViewById(R.id.edtPhonenumber2);
        edtPhonenumber3 = (EditText) findViewById(R.id.edtPhonenumber3);

        lblCustomerNameError = (TextView) findViewById(R.id.lblCustomerNameError);
        lblCustomerPhoneNumberError = (TextView) findViewById(R.id.lblCustomerPhoneNumberError);
        lblCustomerCompanyError = (TextView) findViewById(R.id.lblCustomerCompanyError);
        lblCustomerPositionError = (TextView) findViewById(R.id.lblCustomerPositionError);
        lblCustomerPhonenumber2Error = (TextView) findViewById(R.id.lblCustomerPhonenumber2Error);
        lblCustomerPhonenumber3Error = (TextView) findViewById(R.id.lblCustomerPhonenumber3Error);

        avatar = (ImageView) findViewById(R.id.avatar);

        addTextWatcher();

        if (customerId > 0) {
            getCustomerInfo();
        }

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(CreateCustomerActivity.this);
                startActivityForResult(chooseImageIntent, REQUEST_CAMERA);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    Bitmap bitmap = null;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != Activity.RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CAMERA) {
            bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
            bitmap = Utils.cropToSquare(bitmap);
            avatar.setImageBitmap(bitmap);
        }
    }

    public void getCustomerInfo() {
        showLoading();
        GetCustomerInfoRequest getCustomerInfoRequest = new GetCustomerInfoRequest();
        getCustomerInfoRequest.setData(customerId);

        ApiController.doPostRequest(this, getCustomerInfoRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    GetCustomerInfoResponse signInResponse = gson.fromJson(response, GetCustomerInfoResponse.class);

                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        fetchCustomerInfo(signInResponse.getCustomerInfo());
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateCustomerActivity.this, getResources().getString(R.string.alertConnectFail), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    public void fetchCustomerInfo(CustomerModel customerModel) {

        if (customerModel == null) return;

        edtCustomerName.setText(customerModel.getCustomerName() != null ? customerModel.getCustomerName() : "");
//        edtExtPhone.setText(customerModel.getNumberOfExt() != null ? customerModel.getNumberOfExt() : "");
        edtCompanyName.setText(customerModel.getCompanyName() != null ? customerModel.getCompanyName() : "");
        edtPosition.setText(customerModel.getPosition() != null ? customerModel.getPosition() : "");
        edtPhonenumber.setText(customerModel.getPhoneNumber() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber()) : "");
        edtPhonenumber2.setText(customerModel.getPhoneNumber2() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber2()) : "");
        edtPhonenumber3.setText(customerModel.getPhoneNumber3() != null ? PhoneUtil.makeCorrectPhoneFormat(customerModel.getPhoneNumber3()) : "");


        if (customerModel.getFileName() != null) {
            Picasso.with(this)
                    .load(customerModel.getFileName()).placeholder(getResources().getDrawable(R.drawable.avatar2))
                    .error(getResources().getDrawable(R.drawable.avatar2))
                    .transform(new RoundedTransformation(GlobalInfo.AppConfig.avatarBorderSize, GlobalInfo.AppConfig.avatarCorner))
                    .into(avatar);
        }
    }

    public void addTextWatcher() {

        edtCustomerName.addTextChangedListener(new CustomWatcher(edtCustomerName, lblCustomerNameError, new TextRole()));
        edtPhonenumber.addTextChangedListener(new CustomWatcher(edtPhonenumber, lblCustomerPhoneNumberError, new TextPhoneRole()));
        edtPhonenumber2.addTextChangedListener(new CustomWatcher(edtPhonenumber2, lblCustomerPhonenumber2Error, new TextPhoneRoleNullAble()));
        edtPhonenumber3.addTextChangedListener(new CustomWatcher(edtPhonenumber3, lblCustomerPhonenumber3Error, new TextPhoneRoleNullAble()));

//        addTextWatcher(edtCustomerName, lblCustomerNameError, new TextRole());
//        addTextWatcher(edtPhonenumber, lblCustomerPhoneNumberError, new TextPhoneRole());
////        addTextWatcher(edtCompanyName, lblCustomerCompanyError, new TextRole());
////        addTextWatcher(edtPosition, lblCustomerPositionError, new TextRole());
//        addTextWatcher(edtPhonenumber2, lblCustomerPhonenumber2Error, new TextPhoneRoleNullAble());
//        addTextWatcher(edtPhonenumber3, lblCustomerPhonenumber3Error, new TextPhoneRoleNullAble());

    }

    class CustomWatcher implements TextWatcher {

        public EditText editText;
        public TextView txtError;
        public TextRole textRole;

        public CustomWatcher(EditText editText, TextView textView, TextRole textRole) {
            this.editText = editText;
            txtError = textView;
            this.textRole = textRole;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String checkString = textRole.checkInputError(s.toString());
            if(checkString != null) {
                showErrorStatus(editText, txtError, checkString);
            } else {
                hideErrorStatus(editText, txtError);
            }
        }
    }

    public void onClickCreate(View view) {

        TextRole textRole = new TextRole();
        TextPhoneRole textPhoneRole = new TextPhoneRole();
        TextPhoneRoleNullAble textPhoneRoleNullAble = new TextPhoneRoleNullAble();
        final String customerName = edtCustomerName.getEditableText().toString().trim();
//        String extPhone = edtExtPhone.getEditableText().toString().trim();
        final String companyName = edtCompanyName.getEditableText().toString().trim();
        final String position = edtPosition.getEditableText().toString().trim();
        final String phonenumber = edtPhonenumber.getEditableText().toString().trim();
        final String phonenumber2 = edtPhonenumber2.getEditableText().toString().trim();
        final String phonenumber3 = edtPhonenumber3.getEditableText().toString().trim();

        if (textRole.checkInputError(customerName) != null) {
//            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyCustomerName),
//                    getResources().getString(R.string.close_button), null);
            showErrorStatus(edtCustomerName, lblCustomerNameError, textRole.checkInputError(customerName));
            showKeyBoard(edtCustomerName);
            return;
        }
        if (textPhoneRole.checkInputError(phonenumber) != null) {
//            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyPhoneNumber),
//                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtPhonenumber);
            showErrorStatus(edtPhonenumber, lblCustomerPhoneNumberError, textPhoneRole.checkInputError(phonenumber));
            return;
        }
//        if (textRole.checkInputError(companyName) != null) {
////            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyCustomerCompanyName),
////                    getResources().getString(R.string.close_button), null);
//            showKeyBoard(edtCompanyName);
//
//            showErrorStatus(edtCompanyName, lblCustomerCompanyError, textRole.checkInputError(companyName));
//            return;
//        }
//        if (textRole.checkInputError(position) != null) {
////            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyPosition),
////                    getResources().getString(R.string.close_button), null);
//            showKeyBoard(edtPosition);
//            showErrorStatus(edtPosition, lblCustomerPositionError, textRole.checkInputError(position));
//            return;
//        }

        if (textPhoneRoleNullAble.checkInputError(phonenumber2) != null) {
//            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyPhoneNumber2),
//                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtPhonenumber2);

            showErrorStatus(edtPhonenumber2, lblCustomerPhonenumber2Error, textPhoneRoleNullAble.checkInputError(phonenumber2));
            return;
        }
        if (textPhoneRoleNullAble.checkInputError(phonenumber3) != null) {
//            DialogUtility.showDialogAlert(this, "", getResources().getString(R.string.alertEmptyPhoneNumber3),
//                    getResources().getString(R.string.close_button), null);
            showKeyBoard(edtPhonenumber3);
            showErrorStatus(edtPhonenumber3, lblCustomerPhonenumber3Error, textPhoneRoleNullAble.checkInputError(phonenumber3));
            return;
        }

        if(bitmap != null) {

            showLoading();
            UploadAvatarRequest uploadAvatarRequest = new UploadAvatarRequest();
            uploadAvatarRequest.setBitmap(bitmap);

            ApiController.doPostMultipart(this, uploadAvatarRequest, new ResponseListener() {
                @Override
                public void processResponse(String response) {
                    Log.e("Response: ", response);
                    closeLoading();
                    if (response != null) {
                        Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                        UploadFileResponse signInResponse = gson.fromJson(response, UploadFileResponse.class);

                        if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                            if (mode == MODE_CREATE) {
                                CreateCustomerRequest createCustomerRequest = new CreateCustomerRequest();
                                createCustomerRequest.setData(customerName
                                        , companyName, position, phonenumber, phonenumber2, phonenumber3, signInResponse.getFilename());
                                doProcess(createCustomerRequest);
                            } else {
                                EditCustomerRequest editCustomerRequest = new EditCustomerRequest();
                                editCustomerRequest.setData(customerName
                                        , companyName, position, phonenumber, phonenumber2, phonenumber3, customerId, signInResponse.getFilename());
                                doProcess(editCustomerRequest);
                            }
                        } else {
                            showError(signInResponse, true);
                        }
                    } else {
                        Toast.makeText(CreateCustomerActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void processResponse(int error, String content) {
                    showError(getResources().getString(R.string.text_no_network), true);
                }
            });

            return;
        } else {
            if (mode == MODE_CREATE) {
                CreateCustomerRequest createCustomerRequest = new CreateCustomerRequest();
                createCustomerRequest.setData(customerName
                        , companyName, position, phonenumber, phonenumber2, phonenumber3, null);
                doProcess(createCustomerRequest);
            } else {
                EditCustomerRequest editCustomerRequest = new EditCustomerRequest();
                editCustomerRequest.setData(customerName
                        , companyName, position, phonenumber, phonenumber2, phonenumber3, customerId, null);
                doProcess(editCustomerRequest);
            }
        }




    }

    public void doProcess(BasePostRequestEntity basePostRequestEntity) {
        showLoading();

        ApiController.doPostRequest(this, basePostRequestEntity, new ResponseListener() {
            @Override
            public void processResponse(String response) {
                Log.e("Response: ", response);
                closeLoading();
                if (response != null) {
                    Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                    ResponseObject signInResponse = gson.fromJson(response, ResponseObject.class);

                    String message = signInResponse.getMessage() != null && !signInResponse.getMessage().isEmpty()
                            ? signInResponse.getMessage() : null;
                    if (signInResponse.getIsSuccess() == 1 && signInResponse.getIsLogin() == 1) {
                        if (mode == MODE_CREATE) {
                            Toast.makeText(CreateCustomerActivity.this, getResources().getString(R.string.alertCreateCustomerSuccess), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateCustomerActivity.this, getResources().getString(R.string.alertEditCustomerSuccess), Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        showError(signInResponse, true);
                    }
                } else {
                    Toast.makeText(CreateCustomerActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                showError(getResources().getString(R.string.text_no_network), true);
            }
        });
    }

    @Override
    public void onKeyBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void onClickBack(View view) {
        onKeyBack();
    }
}
