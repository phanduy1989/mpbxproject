package com.viettel.view.activity.business;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ae.team.connectapi.connect.ConnectApiSocket;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.deploygate.sdk.DeployGate;
import com.deploygate.sdk.DeployGateCallback;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.GlobalInfo;
import com.viettel.api.ApiController;
import com.viettel.interfaces.ConfirmListener;
import com.viettel.interfaces.OnItemMenuClicked;
import com.viettel.interfaces.ResponseListener;
import com.viettel.model.ProfileInfo;
import com.viettel.model.ToolEntity;
import com.viettel.model.request.SignOutRequest;
import com.viettel.model.response.ResponseObject;
import com.viettel.store.AppSharePreference;
import com.viettel.utils.DialogUtility;
import com.viettel.utils.ParserUtility;
import com.viettel.view.activity.IntroduceActivity;
import com.viettel.view.activity.LinkActivity;
import com.viettel.view.activity.VersionInfoActivity;
import com.viettel.view.activity.authen.AccountActivity;
import com.viettel.view.activity.authen.ChangePassActivity;
import com.viettel.view.activity.authen.DanhBaActivity;
import com.viettel.view.activity.authen.LoginActivity;
import com.viettel.view.activity.authen.TinNhanActivity;
import com.viettel.view.activity.business.colleague.ListColleagueActivity;
import com.viettel.view.activity.business.contact.ListContactActivity;
import com.viettel.view.activity.business.conversation.ListConversationActivity;
import com.viettel.view.activity.business.customer.ListCustomerActivity;
import com.viettel.view.base.BABaseActivity;
import com.viettel.view.fragment.MenuLeft;
import com.viettel.view.fragment.business.BaseFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.viettel.mpbx.R;

@SuppressLint("ValidFragment")
public class MainHomeActivity extends BABaseActivity implements OnItemMenuClicked, DeployGateCallback {

    RelativeLayout btnSideBar;
    ImageView toggleIcon;
    TextView textTitleHeader;

    RelativeLayout layoutHeader;
    RelativeLayout listColleagueGroup, listCustomerGroup, contactGroup, keyboardGroup, conferenceCallGroup;
    LinearLayout layoutContact, layoutTroChuyen;

    public BaseFragment preFragment, curFragment;
    FragmentManager fragmentManager;

    public static HashMap<Integer, Integer> hashMap = new HashMap<>();

    static {
        hashMap.put(ToolEntity.MENU_COLLEAGUE, R.id.listColleagueGroup);
        hashMap.put(ToolEntity.MENU_CUSTOMER, R.id.listCustomerGroup);
        hashMap.put(ToolEntity.MENU_CONTACT, R.id.contactGroup);
        hashMap.put(ToolEntity.MENU_KEYBOARD, R.id.keyboardGroup);
        hashMap.put(ToolEntity.MENU_CONFERENCE, R.id.conferenceCallGroup);
    }

    private MenuLeft mFrag;
    private ToolEntity currentMenu;

    private final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 9669;

    private DrawerLayout mDrawerLayout;


    AppSharePreference appSharePreference;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {

            }

        }
    };

    //    private GoogleApiClient client;
    public MainHomeActivity() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (savedInstanceState == null) {
            Log.e("savedInstanceState", "null");
        } else {

            Intent refresh = new Intent(this, MainHomeActivity.class);
            refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(refresh);// Start the same Activity
            return;
        }

        appSharePreference = com.viettel.store.AppSharePreference.getInstance(this);

        GlobalInfo.getInstance().setActivityContext(this);
        GlobalInfo.getInstance().setAppContext(this);


        // set the Above View
        setContentView(R.layout.layout_main_home);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        layoutHeader = (RelativeLayout) findViewById(R.id.layoutHeader);
        toggleIcon = (ImageView) findViewById(R.id.toggleIcon);
        textTitleHeader = (TextView) findViewById(R.id.txtTitleHeader);


        btnSideBar = (RelativeLayout) findViewById(R.id.btnSideBar);

        listColleagueGroup = (RelativeLayout) findViewById(R.id.listColleagueGroup);
        listCustomerGroup = (RelativeLayout) findViewById(R.id.listCustomerGroup);
        contactGroup = (RelativeLayout) findViewById(R.id.contactGroup);
        keyboardGroup = (RelativeLayout) findViewById(R.id.keyboardGroup);
        conferenceCallGroup = (RelativeLayout) findViewById(R.id.conferenceCallGroup);

        layoutContact = (LinearLayout) findViewById(R.id.layoutContact);
        layoutTroChuyen = (LinearLayout) findViewById(R.id.layoutTroChuyen);


        showTitleHeader(getResources().getString(R.string.labelMainHome));

        if (!appSharePreference.getRole().equals(GlobalInfo.ROLE_ADMIN) && appSharePreference.getIntValue(AppSharePreference.AGENT_ID) > 0) {
            findViewById(R.id.layoutConference).setVisibility(View.INVISIBLE);
        }

        initSideBar(savedInstanceState);

        fragmentManager = getSupportFragmentManager();
        init();
        control();
    }

    public void init() {
//        setupMainFragment(DataManager.getMenu(ToolEntity.MENU_CONTACT));
    }
    public void control() {
//        setupMainFragment(DataManager.getMenu(ToolEntity.MENU_CONTACT));
        layoutContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectApiSocket.Login(MainHomeActivity.this, "admin", "0988094398", "123456", new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.contains("Success"))
                        {
                           ProfileInfo profileInfo = ParserUtility.parseLogin(json);
                            sharePreference.putChatToken(profileInfo.getToken());
                            sharePreference.putChatUpdatedat(profileInfo.getUpdatedAt());
                            sharePreference.putChatId(profileInfo.getId());
                            sharePreference.putChatPhone(profileInfo.getPhone());
                            sharePreference.putChatUsername(profileInfo.getUsername());
                            sharePreference.putChatEmail(profileInfo.getEmail());
                            sharePreference.putChatCreatedat(profileInfo.getCreatedAt());
                            sharePreference.putChatAppId(profileInfo.getApp_id());
                            sharePreference.putChatAppId(profileInfo.getAvatar());
                            sharePreference.putChatName(profileInfo.getName());
                            sharePreference.putChatPass("123456");
                            ConnectApiSocket.ConnectSocket(profileInfo.getToken());
                            startActivity(new Intent(MainHomeActivity.this.getBaseContext(), DanhBaActivity.class));
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        }else
                        {
//                            progressBar.setVisibility(View.GONE);
//                            Toast.makeText(LoginActivity.this, R.string.errLogin, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        layoutTroChuyen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectApiSocket.Login(MainHomeActivity.this, "admin", "0988094398", "123456", new ResultListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (json.contains("Success"))
                        {
                            ProfileInfo profileInfo = ParserUtility.parseLogin(json);
                            sharePreference.putChatToken(profileInfo.getToken());
                            sharePreference.putChatUpdatedat(profileInfo.getUpdatedAt());
                            sharePreference.putChatId(profileInfo.getId());
                            sharePreference.putChatPhone(profileInfo.getPhone());
                            sharePreference.putChatUsername(profileInfo.getUsername());
                            sharePreference.putChatEmail(profileInfo.getEmail());
                            sharePreference.putChatCreatedat(profileInfo.getCreatedAt());
                            sharePreference.putChatAppId(profileInfo.getApp_id());
                            sharePreference.putChatAppId(profileInfo.getAvatar());
                            sharePreference.putChatName(profileInfo.getName());
                            sharePreference.putChatPass("123456");
                            ConnectApiSocket.ConnectSocket(profileInfo.getToken());
                            startActivity(new Intent(MainHomeActivity.this.getBaseContext(), TinNhanActivity.class));
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        }else
                        {
//                            progressBar.setVisibility(View.GONE);
//                            Toast.makeText(LoginActivity.this, R.string.errLogin, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    public void onClickListColleague(View view) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ListColleagueActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickListCustomer(View view) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ListCustomerActivity.class));
//        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ListViewExample.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickContact(View view) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ListContactActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickKeyboard(View view) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), KeyboardActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickConference(View view) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ListConversationActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void changeColorPrimary(int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(color));
            getWindow().setStatusBarColor(getResources().getColor(color));
        }
    }

    public void showTitleHeader(String title) {
        textTitleHeader.setVisibility(View.VISIBLE);
        textTitleHeader.setText(title);
    }

    public void onClickBtnProfile(View v) {
        startActivity(new Intent(this, AccountActivity.class));
    }

    public void onClickSideBar(View v) {
        if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.openDrawer(GravityCompat.START, true);
        }
    }

    public void onClickHeaderRight(View v) {

    }

    public void onClickServiceInfo(View v) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), LinkActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickVersionInfo(View v) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), VersionInfoActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickAccountInfo(View v) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), AccountActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void onClickChangePassword(View v) {
        startActivity(new Intent(MainHomeActivity.this.getBaseContext(), ChangePassActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void initSideBar(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
            mFrag = new MenuLeft();
            t.replace(R.id.left_drawer, mFrag);
            t.commit();
        } else {
            mFrag = (MenuLeft) this.getSupportFragmentManager().findFragmentById(R.id.left_drawer);
        }

    }

    public void setupMainFragment(ToolEntity toolEntity) {

//        currentMenu = toolEntity;
//
//        if(toolEntity.getToolId() == ToolEntity.MENU_CONFERENCE) {
//            showRightIcon(R.drawable.icon_thietlap);
//        } else {
//            hideRightIcon();
//        }
//
//        showTitleHeader(toolEntity.getToolName());
//
//        if (curFragment != null && curFragment.getClass().getName().equals(toolEntity.getFragmentName())) {
//            mDrawerLayout.closeDrawers();
//            return;
//        } else {
//            preFragment = curFragment;
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            Fragment fragment = fragmentManager.findFragmentByTag(toolEntity.getFragmentName());
//
//            if (fragment == null) {
//                fragment = Fragment.instantiate(this, toolEntity.getFragmentName());
//                fragmentTransaction.add(R.id.content_frame, fragment);
//                fragmentTransaction.addToBackStack(null);
//            } else {
//                if (!fragment.isAdded()) {
//                    fragmentTransaction.addToBackStack(null);
//                }
//                fragmentTransaction.show(fragment);
//            }
//            curFragment = (BaseFragment) fragment;
////
//            if (preFragment != null) {
//                fragmentTransaction.hide(preFragment);
//            } else {
//                preFragment = curFragment;
//            }
//            fragmentTransaction.commit();
//
//        }
//
//        setFocus();
    }

    public void setFocus() {
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            RelativeLayout group = (RelativeLayout) findViewById(entry.getValue());
            ImageView imageView = (ImageView) group.getChildAt(0);
            if (entry.getKey() == currentMenu.getToolId()) {
                imageView.setColorFilter(ContextCompat.getColor(this, R.color.main_green));
            } else {
                imageView.setColorFilter(ContextCompat.getColor(this, R.color.gray));
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        final ToolEntity toolEntity = (ToolEntity) (l.getAdapter().getItem(position));
        if (toolEntity != null) {
            int catId = toolEntity.getToolId();
            switch (catId) {
                case ToolEntity.MENU_COLLEAGUE:
                    startActivity(new Intent(this, SettingActivity.class));
                    break;
                case ToolEntity.MENU_CUSTOMER:
                    Intent browserIntentNews = new Intent(this, LinkActivity.class);
                    browserIntentNews.putExtra("TITLE", toolEntity.getToolName());
                    startActivity(browserIntentNews);
                    break;
                case ToolEntity.MENU_CONTACT:
                    startActivity(new Intent(this, IntroduceActivity.class));
                    break;
                case ToolEntity.MENU_CONFERENCE:
                    Intent browserIntentPolicy = new Intent(this, LinkActivity.class);
                    browserIntentPolicy.putExtra("TITLE", toolEntity.getToolName());
                    startActivity(browserIntentPolicy);
                    break;
                case ToolEntity.MENU_KEYBOARD:
//                    Intent browserIntentQA = new Intent(Intent.ACTION_VIEW, Uri.parse(GlobalInfo.ServerConfig.QA_URL));
                    Intent browserIntentQA = new Intent(this, LinkActivity.class);
                    browserIntentQA.putExtra("TITLE", toolEntity.getToolName());
                    startActivity(browserIntentQA);
                    break;
            }

        }
    }

    public void onClickBtnLogout(View v) {
        DialogUtility.showDialogConfirm(this, getResources().getString(R.string.msgLogoutApp), new ConfirmListener() {

            @Override
            public void doCancel() {

            }

            @Override
            public void doAccept() {
                doLogoutRequest();
            }
        });
    }

    public void doLogoutRequest() {
        showProgressDialog(getResources().getString(R.string.txtPleaseWait));
        SignOutRequest signOutRequest = new SignOutRequest();
        signOutRequest.setData();

        ApiController.doPostRequest(this, signOutRequest, new ResponseListener() {
            @Override
            public void processResponse(String response) {

                closeProgressDialog();
                Gson gson = new GsonBuilder().setDateFormat(GlobalInfo.ServerConfig.DATE_FORMAT).create();
                ResponseObject responseObject = gson.fromJson(response, ResponseObject.class);

                if (responseObject.getIsSuccess() == 1 || responseObject.getIsLogin() == -1) {
                    doLogout();
                }
            }

            @Override
            public void processResponse(int error, String content) {
                Toast.makeText(MainHomeActivity.this, getResources().getString(R.string.text_no_network), Toast.LENGTH_SHORT).show();
                closeProgressDialog();

            }
        });
    }

    public void doLogout() {
        clearCache();
        startActivity(new Intent(MainHomeActivity.this, LoginActivity.class));
        finish();
    }

    private long preTimeStamp;

    public void onKeyBack() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {
            if (curFragment != null) {
                if (!curFragment.onKeyBack()) {
                    onQuit();
                }
            } else {
                onQuit();
            }

        }

    }

    private void onQuit() {
        long current = System.currentTimeMillis();
        if (current - preTimeStamp > 2000) {
            Toast.makeText(this, getResources().getString(R.string.msgExitApp), Toast.LENGTH_SHORT).show();
            preTimeStamp = current;
        } else {
            // TCPControl.getInstance().disconnect();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                onKeyBack();
                break;
        }
        return true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case GlobalInfo.REQUEST_CODE_DETAIL:
                case GlobalInfo.REQUEST_CODE_CREATE:

                    boolean isUpdateListBilling = data.getBooleanExtra(GlobalInfo.BUNDLE_KEY_IS_UPDATE, false);
                    if (!isUpdateListBilling) {
                        return;
                    }
                    List<Fragment> allFragments = getSupportFragmentManager().getFragments();
                    if (allFragments == null || allFragments.isEmpty()) {
                        return;
                    }

                    for (Fragment fragment : allFragments) {
                        if (fragment instanceof BaseFragment) {
                            ((BaseFragment) fragment).onKeySearch();
                        }
                    }

                    break;
                case GlobalInfo.REQUEST_CODE_ACCOUNT:
                    boolean isUpdate = data.getBooleanExtra(GlobalInfo.BUNDLE_KEY_IS_UPDATE, false);
                    if (isUpdate) {
                        doLogout();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    boolean isRegisterd;

    @Override
    protected void onPause() {
        DeployGate.unregisterCallback(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        DeployGate.registerCallback(this, true);
        super.onResume();
    }

    @Override
    public void onInitialized(boolean arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(boolean arg0, boolean arg1, String arg2, boolean arg3) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpdateAvailable(int arg0, String arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStart() {
        super.onStart();
    }
}