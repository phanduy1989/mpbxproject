package com.rengwuxian.edittext;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import java.util.Calendar;

public class MaterialDatePicker extends MaterialEditText implements DatePickerDialog.OnDateSetListener {

	protected int year;
	protected int month;
	protected int day;
	protected OnTimeSetListener onTimeSetListener = null;
	protected java.text.DateFormat dateFormat;


	public OnTimeSetListener getOnDateSetListener() {
		return onTimeSetListener;
	}

	public MaterialDatePicker(Context context, AttributeSet attrs) {
		super(context, attrs);

		dateFormat = DateFormat.getMediumDateFormat(getContext());
		
		setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
		setFocusable(false);
		
		setCurrentTime();
	}
	
	public void setTime(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
		updateText();
	}
	
	public String getTime() {
		
		String dayTime = day < 10 ? "0" + day : "" + day;
		String monthTime = (month + 1) < 10 ? "0" + (month + 1) : "" + (month + 1);
		
		return "" + dayTime + "/" + monthTime + "/" + year;
	}
	
	public void setCurrentTime() {
		Calendar c = Calendar.getInstance();
		setTime(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN)
			showDatePicker();
		
		return super.onTouchEvent(event);
	}

	public void showDatePicker() {
		DatePickerDialog datePickerDialog;
		datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
		datePickerDialog.show();
	}
	
	public java.text.DateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(java.text.DateFormat dateFormat) {
		this.dateFormat = dateFormat;
		updateText();
	}

	
	public void updateText() {
		setText(getTime());
	}


	@Override
	public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		setTime(year, monthOfYear, dayOfMonth);
		clearFocus();
	}
}
