package com.rengwuxian.edittext.validation;

/**
 * Created by mariotaku on 15/4/12.
 */
public abstract class METLengthChecker {

    public abstract int getLength(CharSequence text);

}
