package com.ae.team.connectapi.connect;

import android.content.Context;
import android.util.Log;

import com.ae.team.connectapi.config.WebServiceConfig;
import com.ae.team.connectapi.modelmanager.ModelManager;
import com.ae.team.connectapi.modelmanager.ModelManagerListener;
import com.ae.team.connectapi.modelmanager.ResultListener;
import com.android.volley.VolleyError;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

/**
 * Created by VanHa on 8/4/2017.
 */

public class ConnectApiSocket {

    String token = "";

    public static Socket mSocket;

    public static void ConnectSocket(String token) {
        try {
            if (mSocket == null || !mSocket.connected()) {
                IO.Options opts = new IO.Options();
                opts.forceNew = true;
                opts.query = "token=" + token;
                mSocket = IO.socket(WebServiceConfig.API_URL, opts);
                mSocket.connect();
            }

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (NullPointerException nul) {
            Log.e("", "TAG CHAT:  " + nul.toString());
        } catch (Exception e) {
            Log.e("", "TAG CHAT:  " + e.toString());
        } catch (Error er) {
            Log.e("", "TAG CHAT:  " + er.toString());
        }
    }

    public static void DisConnectSocket() {
        if (mSocket!= null)
        mSocket.disconnect();
    }

    // ----------------- emit socket ----------------------
    public static void socketEmitJoinRoom(String room_id) {
        mSocket.emit("join_room", room_id);
    }

    public static void socketEmitLeaveRoom(String room_id) {
        mSocket.emit("leave_room", room_id);
    }

    public static void socketEmitMessage(JSONObject finalobject) {
        mSocket.emit("message", finalobject);
    }

    public static void socketEmitTyping(String roomId) {
        mSocket.emit("typing", roomId);
    }

    public static void socketEmitReaded(String roomId) {
        mSocket.emit("readed", roomId);
    }

    // ----------------- on socket ----------------------
    public static void socketOnSendSuccess(final ResultListener resultListener) {
        mSocket.on("send_success", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnNewMessage(final ResultListener resultListener) {
        mSocket.on("new_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnReaded(final ResultListener resultListener) {
        mSocket.on("readed", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnUserTyping(final ResultListener resultListener) {
        mSocket.on("user_typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnConnectError(final ResultListener resultListener) {
        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnConnectTimeout(final ResultListener resultListener) {
        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnUserChangeStatus(final ResultListener resultListener) {
        mSocket.on("user_change_status", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnAddUser(final ResultListener resultListener) {
        mSocket.on("add_user", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnRemoveUser(final ResultListener resultListener) {
        mSocket.on("remove_user", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    public static void socketOnLeaveRoom(final ResultListener resultListener) {
        mSocket.on("leave_room", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                resultListener.onSuccess(args[0].toString());
            }
        });
    }

    // ----- off connect ---------------------
    public static void socketOffSendSuccess() {
        mSocket.off("send_success", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffNewMessage() {
        mSocket.off("new_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffReaded() {
        mSocket.off("readed", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffUserTyping() {
        mSocket.off("user_typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffUserChangeStatus() {
        mSocket.off("user_change_status", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffConnectError() {
        mSocket.off(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffConnectTimeout() {
        mSocket.off(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffAddUser() {
        mSocket.off("add_user", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffRemoveUser() {
        mSocket.off("remove_user", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    public static void socketOffLeaveRoom() {
        mSocket.off("leave_room", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
            }
        });
    }

    //  ----------- connect api --------
    public static void Login(Context context, String app_username, String username, String password, final ResultListener resultListener) {
        ModelManager.Login(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, app_username, username, password);
    }

    public static void getListContact(Context context, String token, String page, final ResultListener resultListener) {
        ModelManager.getListContact(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, page);
    }

    public static void getListGroup(Context context, String token, String page, final ResultListener resultListener) {
        ModelManager.getListGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, page);
    }

    public static void CreateGroup(Context context, String token, String name, final ResultListener resultListener) {
        ModelManager.CreateGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, name);
    }

    public static void getListUsersGroup(Context context, String token, String group_id, String page, final ResultListener resultListener) {
        ModelManager.getListUsersGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id, page);
    }

    public static void AddUserGroup(Context context, String token, String group_id, String user_id, final ResultListener resultListener) {
        ModelManager.AddUserGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id, user_id);
    }

    public static void RemoveUserGroupContacts(Context context, String token, String group_id, String user_id, final ResultListener resultListener) {
        ModelManager.RemoveUserGroupContacts(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id, user_id);
    }

    public static void CreateRoomChat(Context context, String token, String name, final ResultListener resultListener) {
        ModelManager.CreateRoomChat(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, name);
    }

    public static void AddUserRoom(Context context, String token, String room_id, String user_id, final ResultListener resultListener) {
        ModelManager.AddUserRoom(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, room_id, user_id);
    }

    public static void getListRoomsChat(Context context, String token, String page, final ResultListener resultListener) {
        ModelManager.getListRoomsChat(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, page);
    }

    public static void getListUsersRoom(Context context, String token, String room_id, String page, final ResultListener resultListener) {
        ModelManager.getListUsersRoom(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, room_id, page);
    }

    public static void RemoveUserRoom(Context context, String token, String room_id, String user_id, final ResultListener resultListener) {
        ModelManager.RemoveUserRoom(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, room_id, user_id);
    }

    public static void RenameRoom(Context context, String token, String group_id, String room_name, final ResultListener resultListener) {
        ModelManager.RenameRoom(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id, room_name);
    }

    public static void RenameGroup(Context context, String token, String group_id, String group_name, final ResultListener resultListener) {
        ModelManager.RenameGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id, group_name);
    }

    public static void LeaveRoom(Context context, String token, String room_id, final ResultListener resultListener) {
        ModelManager.LeaveRoom(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, room_id);
    }

    public static void getMessagesRoomChat(Context context, String token, String room_id, String createdAt, final ResultListener resultListener) {
        ModelManager.getMessagesRoomChat(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, room_id, createdAt);
    }

    public static void getRoomGroupId(Context context, String token, String name, String user_id, final ResultListener resultListener) {
        ModelManager.getRoomGroupId(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, name, user_id);
    }

    public static void RemoveGroup(Context context, String token, String group_id, final ResultListener resultListener) {
        ModelManager.RemoveGroup(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, group_id);
    }

    public static void ChangeInfoUser(Context context, String token, String name, String phone, String email, String password, final ResultListener resultListener) {
        ModelManager.ChangeInfoUser(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, name, phone, email, password);
    }

    public static void ChangePass(Context context, String token, String password_old, String password_new, String password_confirm, final ResultListener resultListener) {
        ModelManager.ChangePass(context, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                resultListener.onSuccess(error.toString());
            }

            @Override
            public void onSuccess(String json) {
                resultListener.onSuccess(json);
            }
        }, token, password_old, password_new, password_confirm);
    }

//    public static void UpLoadImage(String token, String msg_id, String group_rom_id, final File file, final ResultListener resultListener) {
//        StringBuilder s;
//        String sResponse;
//        try {
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost postRequest = new HttpPost(WebServiceConfig.URL_UPLOAD_FILE);
//            String auth = "Bearer " + token;
//            postRequest.setHeader("Authorization", auth);
//            postRequest.setHeader("charset", "utf-8");
//            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//            reqEntity.addPart("msg_id", new StringBody(msg_id));
//            reqEntity.addPart("room_id", new StringBody(group_rom_id));
//            reqEntity.addPart("file", new FileBody(file));
//
//            postRequest.setEntity(reqEntity);
//            HttpResponse response = httpClient.execute(postRequest);
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    response.getEntity().getContent(), "UTF-8"));
//
//            s = new StringBuilder();
//
//            while ((sResponse = reader.readLine()) != null) {
//                s = s.append(sResponse);
//            }
//
//            Log.e("UpdateTaiKhoan", "Server response : " + s);
//            resultListener.onSuccess(s.toString());
//
//        } catch (Exception e) {
//            Log.e("UpdateTaiKhoan", "catch : " + e.toString());
//            resultListener.onSuccess(e.toString());
//
//        }
//
//
//    }

}
