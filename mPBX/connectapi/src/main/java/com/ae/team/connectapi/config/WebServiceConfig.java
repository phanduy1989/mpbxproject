package com.ae.team.connectapi.config;

public final class WebServiceConfig {
    // Network time out: 60s
    public static int NETWORK_TIME_OUT = 30000;
    public static int RESULT_OK = 1;
    public static String STRING_INVALID_TOKEN = "AccessToken invalid";
    public static String STRING_EXPIRE_TOKEN = "AccessToken expired";
    // ===================== DOMAIN ===================
    public static String URL_THUMB_YOUTUBE_1 = "http://img.youtube.com/vi/";
    public static String URL_THUMB_YOUTUBE_2 = "/default.jpg";
    public static String PROTOCOL_HTTP = "http://";
    public static String PROTOCOL_HTTPS = "https://";
    public static String API_URL = PROTOCOL_HTTP + "125.212.225.58:3003";
    public static String URL_LOGIN = API_URL + "/users/login";
    public static String URL_LIST_CONTACT = API_URL + "/users/contacts";
    public static String URL_LIST_GROUP = API_URL + "/users/group-contacts";
    public static String URL_CREATE_GROUP = API_URL + "/users/group-contacts/create";
    public static String URL_GET_LIST_USERS_GROUP = API_URL + "/users/group-contacts/";
    public static String URL_REMOVE_USER_GROUP = API_URL + "/users/group-contacts/remove-user";
    public static String URL_ADD_USER_GROUP = API_URL + "/users/group-contacts/add";
    public static String URL_CREATE_ROOM_CHAT = API_URL + "/users/rooms/create";
    public static String URL_GET_LIST_ROOMS_CHAT = API_URL + "/users/rooms";
    public static String URL_ADD_USER_ROOM = API_URL + "/users/rooms/add";
    public static String URL_GET_LIST_USERS_ROOM = API_URL + "/users/rooms/";
    public static String URL_REMOVE_USER_ROOM = API_URL + "/users/rooms/removeUser";
    public static String URL_LEAVE_ROOM = API_URL + "/users/rooms/leave";
    public static String URL_RENAME_GROUP = API_URL + "/users/group-contacts/rename";
    public static String URL_RENAME_ROOM = API_URL + "/users/rooms/rename";
    public static String URL_MESSAGES_ROOM_CHAT = API_URL + "/users/rooms/";
    public static String URL_REMOVE_GROUP = API_URL + "/users/group-contacts/remove";
    public static String URL_GET_ROOM_GROUP_ID = API_URL + "/users/rooms/create-for-user";
    public static String URL_CHANGE_INFO_USER = API_URL + "/users/change-info";
    public static String URL_UPLOAD_FILE = API_URL + "/users/message-upload";


//    public static void connect(String token) {
//        Socket mSocket;
//
//        {
//            try {
//                IO.Options opts = new IO.Options();
//                opts.forceNew = true;
//                opts.query = "token=" + token;
//                mSocket = IO.socket(WebServiceConfig.API_URL, opts);
//            } catch (URISyntaxException e) {
//                throw new RuntimeException(e);
//            }
//        }
//    }


    // ============== KEY JSON =================
    //=================== TAG KEY =============
    public static String TAG_KEY_DATA = "data";
    //    public static String TAG_KEY_CHILDREN = "children";
    public static String TAG_KEY_USERINFO = "userInfo";
    public static String TAG_KEY_TOKEN = "token";
    public static String TAG_KEY_USERS = "users";
    public static String TAG_KEY_USER = "user";
    public static String TAG_KEY_SENDER = "sender";
    public static String TAG_KEY_ROOM_ID = "room_id";

    //===================KEY PROFILE=================
    public static String KEY_PROFILE_UPDATEDAT = "updatedAt";
    public static String KEY_PROFILE_ID = "id";
    public static String KEY_PROFILE_PHONE = "phone";
    public static String KEY_PROFILE_USERNAME = "username";
    public static String KEY_PROFILE_EMAIL = "email";
    public static String KEY_PROFILE_CREATEDAT = "createdAt";
    public static String KEY_PROFILE_APP_ID = "app_id";
    public static String KEY_PROFILE_AVATAR = "avatar";


    //===================KEY DANH BA =================
    public static String KEY_DANHBA_APP_ID = "app_id";
    public static String KEY_DANHBA_PASSWORD = "password";
    public static String KEY_DANHBA_AVATAR = "avatar";
    public static String KEY_DANHBA_ID = "id";
    public static String KEY_DANHBA_PHONE = "phone";
    public static String KEY_DANHBA_USERNAME = "username";
    public static String KEY_DANHBA_EMAIL = "email";
    public static String KEY_DANHBA_CREATEDAT = "createdAt";
    public static String KEY_DANHBA_UPDATEDAT = "updatedAt";


    //===================KEY NHOM DANH BA =================
    public static String KEY_NHOM_DANHBA_UPDATEDAT = "updatedAt";
    public static String KEY_NHOM_DANHBA_ID = "id";
    public static String KEY_NHOM_DANHBA_USER_ID = "user_id";
    public static String KEY_NHOM_DANHBA_STATUS = "status";
    public static String KEY_NHOM_DANHBA_CREATEDAT = "createdAt";
    public static String KEY_NHOM_DANHBA_NAME = "name";

    //===================KEY TIN NHAN =================
    public static String KEY_TINNHAN_UPDATEDAT = "updatedAt";
    public static String KEY_TINNHAN_ID = "id";
    public static String KEY_TINNHAN_USER_ID = "user_id";
    public static String KEY_TINNHAN_CREATEDAT = "createdAt";
    public static String KEY_TINNHAN_NAME = "name";
    public static String KEY_TINNHAN_LAST_TIME_SEND_MESSAGE = "last_time_send_message";
    public static String KEY_TINNHAN_SENDER_ID = "sender_id";
    public static String KEY_TINNHAN_LAST_MESSAGE = "last_message";

    //===================KEY SENDER CHAT =================
    public static String KEY_SENDER_CHAT_ID = "id";
    public static String KEY_SENDER_CHAT_APP_ID = "app_id";
    public static String KEY_SENDER_CHAT_USERNAME = "username";
    public static String KEY_SENDER_CHAT_EMAIL = "email";
    public static String KEY_SENDER_CHAT_PASSWORD = "password";
    public static String KEY_SENDER_CHAT_AVATAR = "avatar";
    public static String KEY_SENDER_CHAT_PHONE = "phone";
    public static String KEY_SENDER_CHAT_CREATEDAT = "createdAt";
    public static String KEY_SENDER_CHAT_UPDATEDAT = "updatedAt";

    //===================KEY MESSAGE =================
    public static String KEY_MESSAGE_CHAT_ID = "_id";
    public static String KEY_MESSAGE_CHAT_CONTENT = "content";
    public static String KEY_MESSAGE_CHAT_USER_ID = "user_id";
    public static String KEY_MESSAGE_CHAT_ROOM_ID = "room_id";
    public static String KEY_MESSAGE_CHAT_CREATEDAT = "createdAt";
    public static String KEY_MESSAGE_CHAT_UPDATEDAT = "updatedAt";
    public static String KEY_MESSAGE_CHAT_DELETEDAT = "deletedAt";
    public static String KEY_MESSAGE_CHAT_MSG_ID = "msg_id";
    public static String KEY_MESSAGE_CHAT_TYPE = "type";


}
