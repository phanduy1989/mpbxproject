package com.ae.team.connectapi.network;

import java.util.HashMap;
import java.util.Map;

public final class ParameterFactory {

	public static Map<String, String> Login(String app_username, String username, String password) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("app_username", app_username);
		parameters.put("username", username);
		parameters.put("password", password);

		return parameters;

	}
	public static Map<String, String> ListContact(String page) {

		Map<String, String> parameters = new HashMap<>();

		parameters.put("page", page);

		return parameters;

	}

	// --------------  GROUP -----------------

	public static Map<String, String> ListGroup(String page) {

		Map<String, String> parameters = new HashMap<>();

		parameters.put("", page);

		return parameters;

	}

	public static Map<String, String> GetListUsersGroup(String group_id,String page) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("group_id", group_id);
		parameters.put("page", page);

		return parameters;

	}
	public static Map<String, String> AddUserGroup(String group_id,String user_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("group_id", group_id);
		parameters.put("user_ids", user_id);

		return parameters;

	}
	public static Map<String, String> RemoveUserGroupContacts(String group_id,String user_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("group_id", group_id);
		parameters.put("user_id", user_id);

		return parameters;

	}

	// -----------------  room chat ----------------------
	public static Map<String, String> CreateGroup(String name) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("name", name);

		return parameters;

	}


	public static Map<String, String> CreateRoomChat(String name) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("name", name);

		return parameters;

	}
	public static Map<String, String> AddUserRoom(String room_id,String user_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);
		parameters.put("user_ids", user_id);

		return parameters;

	}

	public static Map<String, String> GetListRoomsChat(String page) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("", page);

		return parameters;

	}

	public static Map<String, String> GetListUsersRoom(String room_id, String page) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);
		parameters.put("page", page);

		return parameters;

	}
	public static Map<String, String> RenameRoom(String room_id, String room_name) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);
		parameters.put("room_name", room_name);

		return parameters;

	}

	public static Map<String, String> RenameGroup(String group_id, String group_name) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("group_id", group_id);
		parameters.put("group_name", group_name);

		return parameters;

	}

	public static Map<String, String> RemoveUserRoom(String room_id, String user_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);
		parameters.put("user_id", user_id);

		return parameters;

	}

	public static Map<String, String> LeaveRoom(String room_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);

		return parameters;

	}
	public static Map<String, String> GetRoomGroupId(String name,String user_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("name", name);
		parameters.put("user_id", user_id);

		return parameters;

	}
	public static Map<String, String> RemoveGroup(String group_id) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("group_id", group_id);

		return parameters;

	}

	public static Map<String, String> GetMessagesRoomChat(String room_id) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("room_id", room_id);

		return parameters;

	}
	public static Map<String, String> ChangeInfoUser(String password, String name, String phone, String email) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("password", password);
		parameters.put("name", name);
		parameters.put("phone", phone);
		parameters.put("email", email);

		return parameters;

	}

	public static Map<String, String> ChangePass(String password_old,String password_new, String password_confirm) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("password_old", password_old);
		parameters.put("password_new", password_new);
		parameters.put("password_confirm", password_confirm);

		return parameters;

	}
	public static Map<String, String> UploadImage(String msg_id,String room_id, String file) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put("msg_id", msg_id);
		parameters.put("room_id", room_id);
		parameters.put("file", file);

		return parameters;

	}
}
