package com.ae.team.connectapi.modelmanager;

public interface ResultListener {
    public void onSuccess(String json);
}
