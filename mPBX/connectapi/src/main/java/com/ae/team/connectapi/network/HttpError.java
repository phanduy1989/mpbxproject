package com.ae.team.connectapi.network;

import com.android.volley.VolleyError;

/**
 * Created by van ha on 20/10/2015.
 */
public interface HttpError {
    void onHttpError(VolleyError volleyError);
}
