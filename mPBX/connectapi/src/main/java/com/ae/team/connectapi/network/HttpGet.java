package com.ae.team.connectapi.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by van ha on 27/10/2015.
 */
public class HttpGet extends HttpRequest {

//    MyProgressDialog dialog;
private String token;

    public HttpGet(Context context, String url, Map<String, String> params,String token, HttpListener httpListener, HttpError httpError) {
        super(context, HttpRequest.METHOD_GET, url, httpListener, httpError);
//        dialog = new MyProgressDialog(context);
        this.params = params;
        this.url = getUrl(url, this.params).replace(" ", "%20");
        this.token = token;
        sendRequest();
    }

    protected void sendRequest() {
        request = getJsonObjectRequest();
        super.sendRequest();
    }

    private Request getStringRequest() {
//        if (isShowDialog)
//            showDialog();
//        dialog.show();
        Response.Listener successResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (isShowDialog)
//                    closeDialog();
//                dialog.dismiss();
                httpListener.onHttpRespones(response);

            }
        };
        Response.ErrorListener errorResponse = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                if (isShowDialog)
//                    closeDialog();
//                    dialog.dismiss();
                httpError.onHttpError(error);

            }
        };
        StringRequest request = new StringRequest(requestMethod, url, successResponse, errorResponse) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String auth = "bearer " + token;
                params.put("Authorization", auth);
                return params;
            }
        };


        return request;
    }

    private Request getJsonObjectRequest() {

        Response.Listener successResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                if (isShowDialog)
//                    closeDialog();
                httpListener.onHttpRespones(response);

            }
        };
        Response.ErrorListener errorResponse = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                if (isShowDialog)
//                    closeDialog();
                httpError.onHttpError(error);

            }
        };
        JSONObject jsonParams = params != null ? new JSONObject(params) : null;
        JsonObjectRequest request = new JsonObjectRequest(requestMethod, url, jsonParams, successResponse, errorResponse) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String auth = "bearer " + token;
                headers.put("Authorization", auth);
                return headers;
            }
        };

        return request;
    }

    protected String getUrl(String url, Map<String, String> params) {
        if (params != null) {
            String fullUrl = url;
            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
            int i = 1;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (i == 1) {
                    fullUrl += "?" + entry.getKey() + "=" + entry.getValue();
                } else {
                    fullUrl += "&" + entry.getKey() + "=" + entry.getValue();
                }
                iterator.remove();
                i++;
            }
            return fullUrl;
        }
        return url;

    }

}
