package com.ae.team.connectapi.config;

public class GlobalValue {
    public static boolean DEBUG_MODE = true;
    public static boolean DEBUG_DB = true;
    public static String IMAGE_PATH = "images/";
    public static final String QI_NEWS_PREFERENCES = "QI_NEWS_PREFERENCES";
    public static final String QI_DROID_PREFERENCES = "QI_DROID_PREFERENCES";
    public static final String JMSDF_PREFERENCES = "JMSDF_PREFERENCES";
    public static final String VUONTUOITHO_PREFERENCES = "VUONTUOITHO_PREFERENCES";

    public static final String SETING_BANGTIN = "SETING_BANGTIN";
    public static final String SETING_NOTIFICATION  = "SETING_NOTIFICATION";
    public static final String SETING_DSNHOM = "SETING_DSNHOM";
    public static final String SETING_DANHBA = "SETING_DANHBA";
    // For update profile
    // public static FacebookConnector facebookConnector;
    public static double mLatitude = 0;
    public static double mLongitude = 0;
    public static int PERIOD_UPDATE_LOCATION = 60 * 1000;// 30 s :


    public static String token = "";


}
