/*
 * Name: $RCSfile: StringUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 1:54:00 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.ae.team.connectapi.utility;

import android.widget.EditText;

import java.util.UUID;


/**
 * StringControlsUtility class
 *
 * @author Lemon
 */
public final class StringControlsUtility {
    /**
     * Check Edit Text input string
     *
     * @param editText
     * @return
     */
    public static boolean isEmpty(EditText editText) {
        if (editText == null || editText.getEditableText() == null
                || editText.getEditableText().toString().trim().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }
    public  static String msg_id()
    {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


}
