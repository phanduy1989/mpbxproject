package com.ae.team.connectapi.modelmanager;


import android.content.Context;

import com.ae.team.connectapi.config.WebServiceConfig;
import com.ae.team.connectapi.network.HttpError;
import com.ae.team.connectapi.network.HttpGet;
import com.ae.team.connectapi.network.HttpListener;
import com.ae.team.connectapi.network.HttpPost;
import com.ae.team.connectapi.network.ParameterFactory;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;


public class ModelManager {

    public static void Login(Context context, final ModelManagerListener listener, String app_username, String username, String password) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .Login(app_username, username, password);
        new HttpPost(context, WebServiceConfig.URL_LOGIN, params, "", 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getListContact(Context context, final ModelManagerListener listener, String token, String page) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .ListContact(page);
        new HttpGet(context, WebServiceConfig.URL_LIST_CONTACT, params, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    // ---------- GROUP -------------------
    public static void getListGroup(Context context, final ModelManagerListener listener, String token, String page) {
        Map<String, String> params = (Map<String, String>) ParameterFactory.ListGroup(page);
        new HttpGet(context, WebServiceConfig.URL_LIST_GROUP, params, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void CreateGroup(Context context, final ModelManagerListener listener, String token, String name) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .CreateGroup(name);
        new HttpPost(context, WebServiceConfig.URL_CREATE_GROUP, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getListUsersGroup(Context context, final ModelManagerListener listener, String token, String group_id, String page) {
//        Map<String, String> params = (Map<String, String>) ParameterFactory
//                .GetListUsersGroup(group_id, page);
        Map<String, String> parameters = new HashMap<>();
        new HttpGet(context, WebServiceConfig.URL_GET_LIST_USERS_GROUP + group_id + "/users?page="+page, parameters, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void AddUserGroup(Context context, final ModelManagerListener listener, String token, String group_id, String user_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .AddUserGroup(group_id, user_id);
        new HttpPost(context, WebServiceConfig.URL_ADD_USER_GROUP, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void RemoveUserGroupContacts(Context context, final ModelManagerListener listener, String token, String group_id, String user_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .RemoveUserGroupContacts(group_id, user_id);
        new HttpPost(context, WebServiceConfig.URL_REMOVE_USER_GROUP, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    // ---------------- ROOM CHAT -----------------
    public static void CreateRoomChat(Context context, final ModelManagerListener listener, String token, String name) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .CreateRoomChat(name);
        new HttpPost(context, WebServiceConfig.URL_CREATE_ROOM_CHAT, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void AddUserRoom(Context context, final ModelManagerListener listener, String token, String room_id, String user_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .AddUserRoom(room_id, user_id);
        new HttpPost(context, WebServiceConfig.URL_ADD_USER_ROOM, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getListRoomsChat(Context context, final ModelManagerListener listener, String token, String page) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .GetListRoomsChat(page);
        new HttpGet(context, WebServiceConfig.URL_GET_LIST_ROOMS_CHAT, params, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getListUsersRoom(Context context, final ModelManagerListener listener, String token, String room_id, String page) {
//        Map<String, String> params = (Map<String, String>) ParameterFactory
//                .GetListUsersRoom(room_id, page);
        Map<String, String> parameters = new HashMap<>();
        new HttpGet(context, WebServiceConfig.URL_GET_LIST_USERS_ROOM + room_id +"/users?page=" +page, parameters, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void RemoveUserRoom(Context context, final ModelManagerListener listener, String token, String room_id, String user_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .RemoveUserRoom(room_id, user_id);
        new HttpPost(context, WebServiceConfig.URL_REMOVE_USER_ROOM, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void RenameRoom(Context context, final ModelManagerListener listener, String token, String group_id, String room_name) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .RenameRoom(group_id, room_name);
        new HttpPost(context, WebServiceConfig.URL_RENAME_ROOM, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void RenameGroup(Context context, final ModelManagerListener listener, String token, String group_id, String group_name) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .RenameGroup(group_id, group_name);
        new HttpPost(context, WebServiceConfig.URL_RENAME_GROUP, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void LeaveRoom(Context context, final ModelManagerListener listener, String token, String room_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .LeaveRoom(room_id);
        new HttpPost(context, WebServiceConfig.URL_LEAVE_ROOM, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getMessagesRoomChat(Context context, final ModelManagerListener listener, String token, String room_id, String createdAt) {
//        Map<String, String> params = (Map<String, String>) ParameterFactory
//                .GetMessagesRoomChat(room_id);
        Map<String, String> parameters = new HashMap<>();
        new HttpGet(context, WebServiceConfig.URL_MESSAGES_ROOM_CHAT + room_id + "/messages?createdAt=" + createdAt, parameters, token, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getRoomGroupId(Context context, final ModelManagerListener listener, String token, String name, String user_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .GetRoomGroupId(name, user_id);
        new HttpPost(context, WebServiceConfig.URL_GET_ROOM_GROUP_ID, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void RemoveGroup(Context context, final ModelManagerListener listener, String token, String group_id) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .RemoveGroup(group_id);
        new HttpPost(context, WebServiceConfig.URL_REMOVE_GROUP, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void ChangeInfoUser(Context context, final ModelManagerListener listener, String token, String name, String phone, String email, String password) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .ChangeInfoUser(password, name, phone, email);
        new HttpPost(context, WebServiceConfig.URL_CHANGE_INFO_USER, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void ChangePass(Context context, final ModelManagerListener listener, String token, String password_old, String password_new, String password_confirm) {
        Map<String, String> params = (Map<String, String>) ParameterFactory
                .ChangePass(password_old, password_new, password_confirm);
        new HttpPost(context, WebServiceConfig.URL_CHANGE_INFO_USER, params, token, 1, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }


}